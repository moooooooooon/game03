#include "Client/ClientApp.h"
#include "MainSystems/GameWorld/GameWorld.h"

#include "ECSComponents/Common/CommonComponents.h"
#include "ECSSystems/Common/CommonSystems.h"

#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "ECSSystems/MapObject/MapObjectSystems.h"

#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSSystems/Character/CharacterSystems.h"

#include "ECSListeners/Common/CommonListeners.h"

#include "MainSystems/GameWorld/GameWorld.h"

#include "CommonDefines.h"
#include "Client/ClientSubSystems/ClientSubSystems.h"
#include "MatchingScene.h"


void MatchingScene::Init()
{
	m_LastMatchingTimeLimit = -1;
	m_IsMatchingTimeOver = false;
	m_CountDownTime = 0;
	m_IsError = false;
	m_ErrorMsg.clear();
	m_IsStartCountDown = false;
	m_DisConnectedPopUpFlg = false;
	CLIENT.AddRecvCallBack("MatchingScene",
		[this](const NetWork::PacketHeader& header, void* data)
	{
		switch (header.MessageType)
		{
			case MessageTypes::NotifyLogin:
			{
				NotifyLoginData* loginData = (NotifyLoginData*)data;
				ZUUID uuid(loginData->UserUUID);
				m_MatchingPlayers[uuid] = loginData->LoginData;
				m_OrderdPlayers.push_back(m_MatchingPlayers.find(uuid));
			}
			break;

			case MessageTypes::LoginError:
			{
				m_IsError = true;
				LoginErrorData* loginErrorData = (LoginErrorData*)data;
				m_ErrorMsg = loginErrorData->ErrorMsg;
			}
			break;

			case MessageTypes::NotifyLastMatchingTimeLimit:
			{
				NotifyLastMatchingTimeLimitData* timeLimitData = (NotifyLastMatchingTimeLimitData*)data;
				m_LastMatchingTimeLimit = timeLimitData->LastMatchingTimeLimit;
			}
			break;

			case MessageTypes::NotifyMatchingTimeOver:
			{
				m_IsMatchingTimeOver = true;
				CLIENT.DisConnect();
			}
			break;

			case MessageTypes::NotifyLogout:
			case MessageTypes::NotifyDisConnect:
			{
				NotifyLogoutData* logoutData = (NotifyLogoutData*)data;
				ZUUID uuid(logoutData->UserUUID);
				if (m_MatchingPlayers.find(uuid) == m_MatchingPlayers.end())
					return;
				{
					auto it = std::find(m_OrderdPlayers.begin(), m_OrderdPlayers.end(), m_MatchingPlayers.find(uuid));
					m_OrderdPlayers.erase(it);
				}
				m_MatchingPlayers.erase(uuid);
			}
			break;

			case MessageTypes::NotifyStartMatch:
			{



			}
			break;
		}
	});

	CLIENT.AddDisConnectCallBack("MatchingScene",[this] { m_DisConnectedPopUpFlg = true; });
}

void MatchingScene::Update()
{
	if (GW.m_IsFading)return;

	CLIENT.Update();

	if (m_LastMatchingTimeLimit > 0)
	{
		m_LastMatchingTimeLimit -= APP.m_DeltaTime;
		// マッチングタイムオーバー
		if (m_LastMatchingTimeLimit <= -0.5f)
		{
			m_LastMatchingTimeLimit = 0;
			// マッチングタイムオーバー後サーバーと切断できていない -> パケットロス
			if (CLIENT.IsConnected())CLIENT.DisConnect();
		}
	}

	if(m_IsStartCountDown)
	{
		if (m_CountDownTime > 0)
		{
			CTMgr.AddTimedClosure("CountDown",
				[this](float elapsedTime, bool timeOver)
			{
				if (timeOver)
					m_CountDownTime--;
			}, 1);
		}
		else
			DW_SCROLL(0, "Start Match");
	}

	// マッチングシーンに移行
	if(INPUT.KeyEnter(VK_ESCAPE))
	{
		if (CLIENT.IsConnected())
			CLIENT.DisConnect();
		else
			CLIENT.ResetConnection();
		GW.ChangeScene("Connect",false);
	}
}

void MatchingScene::ImGuiUpdate()
{
	auto matchingPlayerWindow =
		[this]
	{
		if(m_IsError)
		{
			ImGui::OpenPopup("Error!");
			m_IsError = false;
		}
		if (ImGui::BeginPopupModal("Error!", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("Error Message: %s", m_ErrorMsg.c_str());
			float buttonXSize = 120;
			float popupWndXSize = ImGui::GetWindowWidth();
			float buttonPos = popupWndXSize / 2 - buttonXSize / 2;
			ImGui::SetCursorPosX(buttonPos);
			ImGui::Spacing();
			if (ImGui::Button("OK", ImVec2(buttonXSize, 0)))
			{
				ImGui::CloseCurrentPopup();
				CLIENT.DisConnect();
				GW.ChangeScene("Connect", false);
			}
			ImGui::EndPopup();
			return;
		}

		if (m_IsMatchingTimeOver)
		{
			ImGui::OpenPopup("Notify Time Limit");
			m_IsMatchingTimeOver = false;
		}
		if(ImGui::BeginPopupModal("Notify Time Limit",NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("Matching Time Over!");
			float buttonXSize = 120;
			float popupWndXSize = ImGui::GetWindowWidth();
			float buttonPos = popupWndXSize / 2 - buttonXSize / 2;
			ImGui::NewLine();
			ImGui::SetCursorPosX(buttonPos);
			if (ImGui::Button("OK", ImVec2(buttonXSize, 0)))
			{
				ImGui::CloseCurrentPopup();
				GW.ChangeScene("Connect", false);
			}
			ImGui::EndPopup();
			return;
		}

		if (CLIENT.IsConnected() == false)
		{
			if (m_DisConnectedPopUpFlg)
			{
				ImGui::OpenPopup("Disconnected!");
				m_DisConnectedPopUpFlg = false;
			}
			if (ImGui::BeginPopupModal("Disconnected!", NULL, ImGuiWindowFlags_AlwaysAutoResize))
			{
				ImGui::Text("Disconnected from Server!");
				float buttonXSize = 120;
				float popupWndXSize = ImGui::GetWindowWidth();
				float buttonPos = popupWndXSize / 2 - buttonXSize / 2;
				ImGui::SetCursorPosX(buttonPos);
				ImGui::Spacing();
				if (ImGui::Button("OK", ImVec2(buttonXSize, 0)))
				{
					ImGui::CloseCurrentPopup();
					GW.ChangeScene("Connect", false);
				}
				ImGui::EndPopup();
			}
			return;
		}

		const ImVec2 connectionInfoWindowSize( ImGui::GetStyle().ItemSpacing.x * 50,ImGui::GetFrameHeightWithSpacing() * 15);
		ImGui::SetNextWindowSize(connectionInfoWindowSize);
		if (ImGui::Begin("Connection Info"))
		{
			//ImGui::GetStyle().ItemSpacing.y + ImGui::GetFrameHeightWithSpacing()
			const float playersViewWindowHeight = ImGui::GetFrameHeightWithSpacing() * 10;
			ImGui::BeginChild("Connected Players", ImVec2(0, playersViewWindowHeight));
			for (auto& player : m_OrderdPlayers)
			{
				ImGui::PushID(player->first.GetUUIDStr().c_str());
				ImGui::Text("Name: %s,PlayStyle: %s",player->second.Name,LoginData::GetPlayStyleName(player->second.Style));
				ImGui::PopID();
			}
			ImGui::EndChild();

			ImGui::Separator();

			ImGui::BeginChild("Matching Info");
			if (m_LastMatchingTimeLimit >= 0)
				ImGui::Text("TimeLimit at %.0f seconds ...", m_LastMatchingTimeLimit);

			if (m_IsStartCountDown)
				ImGui::Text("%d Second To Start ...", m_CountDownTime);
			ImGui::EndChild();
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(matchingPlayerWindow);
}

void MatchingScene::Draw()
{
	auto& pSr = ShMgr.GetRenderer<SpriteRenderer>();
	pSr.Begin(false, true);
	{
		// 中央にロゴ表示
		auto texInfo = GW.m_TexBackImg->GetInfo();
		auto& window = APP.m_Window;
		float y = (window->GetHeight() - texInfo.Height) / 2.0f;

		pSr.Draw2D(*GW.m_TexBackImg, 0, y, (float)window->GetWidth(), (float)texInfo.Height, &ZVec4(0.2f, 0.2f, 0.2f, 1.f));
	}
	pSr.End();
}

void MatchingScene::Release()
{
	CLIENT.RemoveAllCallBackFunction("MatchingScene");
}
