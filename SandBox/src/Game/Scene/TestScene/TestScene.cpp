#include "Game/Application.h"

#include "ECSComponents/Components.h"
#include "ECSSystems/Systems.h"
#include "ECSListeners/Listeners.h"
#include "MainSystems/Fade/Fade.h"

#include "TestScene.h"


void TestScene::Release()
{
	m_Players.clear();
	ECSEntity::RemoveAllEntity(GW.m_Entities);
	
	GW.m_IsDrawHalfFPSMode = true;
	
	m_Listeners.clear();
	ECS.RemoveAllListener();
	EFFECT.StopAllEffect();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();
	ShowCursor(true);
	INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(),false);
}

void TestScene::Init()
{
	DW_SCROLL(0, "タイトルシーン初期化");
	ShowCursor(false);
	INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), true);
	// 平行光源作成
	m_DirLight = LiMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(1, -1, -0.5f),		// ライト方向
		ZVec4(0.2f, 0.2f, 0.2f, 1)	// 基本色
	);

	// 環境色
	LiMgr.SetAmbientLightColor(ZVec3(0.5f, 0.5f, 0.5f));

	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);


	// コリジョンワールド デフォルト値で初期化
	ColEng.Init();
	
	// リスナー作成
	{
		m_Listeners.push_back(Make_Shared(ModelBoneControllerListener, appnew));
		m_Listeners.push_back(Make_Shared(ModelColliderListener, appnew));
		m_Listeners.push_back(Make_Shared(StateMachineListener, appnew));
		for (auto& listener : m_Listeners)
			ECS.AddListener(listener.GetPtr());
	}

	// キャラクター・マップ読み込み
	{
		// サバイバー
		ECSEntity::MakeEntityFromJsonFile("data/Json/Chara/TestChara.json", &GW.m_Entities);
		ECSEntity::MakeEntityFromJsonFile("data/Json/Chara/survivor.json", &GW.m_Entities);
		// キラー
		ECSEntity::MakeEntityFromJsonFile("data/Json/Chara/killer.json", &GW.m_Entities);
		
		// マップ
		//ECSEntity::MakeEntityFromJsonFile("data/Json/testmap.json",&m_Entities);
		ECSEntity::MakeEntityFromJsonFile("data/Json/AccesTest.json", &GW.m_Entities);
		//ECSEntity::MakeEntityFromJsonFile("data/Json/Office.json", &GW.m_Entities);
	}

	for (auto& entity : GW.m_Entities)
	{
		auto playerComp = entity->GetComponent<PlayerComponent>();
		if(playerComp)
			m_Players.push_back(entity);
	}

	//
	m_SkyTex = APP.m_ResStg.LoadTexture("data/Texture/title_back.png");
	m_texBack = Make_Shared(ZTexture, appnew);
	m_texBack->CreateRT(ZDx.GetRezoW(), ZDx.GetRezoH());

	ShMgr.GetRenderer<BlurShader>().CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));

//	GW.m_IsDrawHalfFPSMode = true;

	/*-----------------------------------------------------------------*/
	GW.m_GameState.InitGame();
	/*-----------------------------------------------------------------*/

	

	// システム準備
	
	m_UpdateSystems.AddSystem(Make_Shared(SecurityCheckSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(HackerCheckSystem, appnew));



	m_PreUpdateSystems.AddSystem(Make_Shared(ColliderDefenseRegisterSystem,appnew));
	m_PreUpdateSystems.AddSystem(Make_Shared(HackerPreUpdateSystem, appnew));
	m_PreUpdateSystems.AddSystem(Make_Shared(StateMachineSystem, appnew));
	m_PreUpdateSystems.AddSystem(Make_Shared(GravitySystem, appnew));
	m_PreUpdateSystems.AddSystem(Make_Shared(HackerMoveSystem, appnew));

	m_UpdateSystems.AddSystem(Make_Shared(ResolveCollisionSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(PlayerUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(SecurityUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(HackerUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(SecurityTerminalUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(ServerUpdateSystem,appnew));

	m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem, appnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,appnew));
	
}

void TestScene::PreUpdate()
{
	ECS.UpdateSystems(m_PreUpdateSystems, APP.m_DeltaTime);
}

void TestScene::Update()
{
	DW_STATIC(1, "PerformanceTest_Scene");
	// EscでVRSceneへ
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
		GW.ChangeScene("Title",false);
		return;
	}

	/*--------------------------------------------------------------------*/
	if (GW.m_GameState.HackerWin) {
		if (MessageFunc == nullptr) {
			m_EndMessage = APP.m_ResStg.LoadTexture("data/UI/HackerWIN.png");
			SetMessageFunc();
		}
		return;
	}

	if (GW.m_GameState.SecurityWin) {
		if (MessageFunc == nullptr) {
			m_EndMessage = APP.m_ResStg.LoadTexture("data/UI/SecurityWIN.png");
			SetMessageFunc();
		}
		return;
	}
	/*--------------------------------------------------------------------*/



	//カメラ操作
	m_Cam.Update(GW.m_MoveInputValue[0]);
	ECS.UpdateSystems(m_UpdateSystems,APP.m_DeltaTime);

	
	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());
}

void TestScene::ImGuiUpdate()
{
	GW.m_PostEffects.ImGui();
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("SystemInfo") == false)
		{
			ImGui::End();
			return;
		}

		// 物理エンジン
		{
			bool isEnablePhysicsDebug = PHYSICS.IsEnableDebugDraw();
			ImGui::Checkbox("Physics Debug Draw", &isEnablePhysicsDebug);
			PHYSICS.SetDebugDrawMode(isEnablePhysicsDebug);
			ImGui::Separator();
		}

		// 各システム
		auto debugImGui = [](ZSP<ECSSystemBase> system)
		{
			ImGui::Text(system->GetSystemName().c_str());
			system->DebugImGuiRender();
			ImGui::Separator();
		};

		for (auto system : m_UpdateSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		for (auto system : m_DrawSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);


	RenderingPipeline.ImGui();
	ShMgr.GetRenderer<BlurShader>().ImGui();
	LiMgr.ImGui();

}

void TestScene::Draw()
{
	// 簡易的に画面分割表示
	int cnt = 0;

	ColEng.DebugDraw(1);
	//	3D描画開始
	RenderingPipeline.Begin3DRendering();
	uint numVP = 5;
	D3D11_VIEWPORT vps[5];
	ZDx.GetDevContext()->RSGetViewports(&numVP, vps);
	// 半分に
	for (auto& vp : vps)
		vp.Width = vp.Width / 2;

	for (auto& entity : m_Players)
	{
		if (entity->IsActive() == false)
			continue;
		auto camComp = entity->GetComponent<CameraComponent>();
		auto contComp = entity->GetComponent<PlayerControllerComponent>();
		if (camComp)
		{
			if (cnt < 2)
			{
				GW.m_NowCamera = camComp;
			
				{
					for (auto& vp : vps)
						vp.TopLeftX = vp.Width * cnt;
					
					// ビューポート切り替え
					ZDx.GetDevContext()->RSSetViewports(numVP, vps);

					// 半透明モード
					ShMgr.m_bsAlpha.SetState();

					//// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
					//ShMgr.m_bsAlpha_AtoC.SetState();
					// カメラやライトのデータをシェーダ側に転送する
					{
						if (GW.m_NowCamera)
						{
							INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), GW.m_NowCamera->Cam.m_IsFPSMode);

							GW.m_NowCamera->Cam.SetCamera();
						}
						else
							m_Cam.SetCamera();

						// ライト情報をシェーダ側に書き込む
						LiMgr.Update();
					}

					// [2D]背景描画
					{
						auto& Ss = ShMgr.GetRenderer<SpriteRenderer>();
						Ss.Begin(false, true);
						Ss.Draw2D(m_SkyTex->GetTex(), 0, 0, (float)ZDx.GetRezoW(), (float)ZDx.GetRezoH());
						Ss.End();
					}

					// [3D]モデル描画
					ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);

					// 物理エンジンのデバッグ描画
					//m_PhysicsWorld->DebugDraw();
					if (PHYSICS.IsEnableDebugDraw())
						ColEng.DebugDraw(1.0f);

					// シャドウマップ用
					auto camPos = GW.m_NowCamera->Cam.m_mCam.GetPos();
					LiMgr.SetShadowCamTargetPoint(camPos);	//	現在のカメラをセット
					//	シャドウマップ描画	
					RenderingPipeline.DrawShadow();

					//	3D描画
					RenderingPipeline.Draw();

					// ライン描画
					ShMgr.GetRenderer<LineRenderer>().Flash();

					EFFECT.Draw();
					// ~~~~~~
				}

				
				cnt++;
			}
		}
	}
	//	3D描画終了
	RenderingPipeline.End3DRendering();

	//	ポストエフェクト
	GW.m_PostEffects.RenderPostEffects(RenderingPipeline.m_RenderTargets->GetTexZSP(0));



	//	2D描画(UIなど)
	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, ZDx.GetBackBuffer()->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	auto& sr = ShMgr.GetRenderer<SpriteRenderer>();
	sr.Begin(false, true);


	//...



	if (MessageFunc) { MessageFunc(); }
	sr.End();

	

}

void TestScene::SetMessageFunc() {
	
	if (MessageFunc)return;

	MessageFunc = [this, cnt = 0, scale = 3.f, alpha = 0.f]()mutable
	{
		if (alpha <= 2.f) {
			alpha += 0.1f;
		}
		else if (scale >= 1.f) {
			scale -= 0.3f;
		}
		else {
			cnt++;
			if (cnt >= 50) { GW.ChangeScene("Title", true, 1); }
		}

		auto& sr = ShMgr.GetRenderer<SpriteRenderer>();


		sr.Draw2D(ZDx.GetBlackTex()->GetTex(), 0.f, 0.f, 1280.f, 720.f, &ZVec4(1.f, 1.f, 1.f, alpha*0.3f));

		ShMgr.m_bsAdd.SetState();
		ZMatrix mat;
		mat.CreateMove(1280.f* 0.5f, 720.f * 0.5f, 0.f);
		mat.Scale_Local(scale, scale, 1);
		sr.Draw2D(m_EndMessage->GetTex(), ZVec2(800, 300), mat, &ZVec4(1, 1, 1, alpha - 1.f));

		ShMgr.m_bsAlpha.SetState();
	};
}