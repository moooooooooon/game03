#ifndef __TEST_SCENE_H__
#define __TEST_SCENE_H__

#include "Camera/GameCamera.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSListeners/Listeners.h"

struct DirLight;
class TestScene : public ZSceneBase
{
public:
	virtual ~TestScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void PreUpdate()override;
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

	void SetMessageFunc();
public:
	// 平行光源
	ZSP<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;
	
	// テクスチャ
	ZSP<ZTexture> m_texBack;
	
	ZSP<ZPhysicsWorld> m_PhysicsWorld;

	ZAVector<ZSP<ECSEntity>> m_Players;

	//---------------------------------------------------------------------------
	ZSP<ZTexture> m_SkyTex;	//	テスト

	ZSP<ZTexture> m_EndMessage = nullptr;
	std::function<void()>	MessageFunc = nullptr;
};

#endif