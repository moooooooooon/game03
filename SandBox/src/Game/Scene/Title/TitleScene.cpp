#include "Game/Application.h"
#include "ECSComponents/Components.h"
#include "ECSSystems/Systems.h"


#include "TitleScene.h"

void TitleScene::Release()
{
	if (m_BGM)
	{
		m_BGM->Stop();
		m_BGM = nullptr;
	}

	// エフェクト停止
	EFFECT.StopEffect(m_EffectHadle);

	m_DirLight = nullptr;
	
	m_BackTexture = nullptr;
	m_SkyTexture = nullptr;
	
	// エンティティ削除
	ECS.RemoveAllListener();
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();
	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();
	m_2DDrawSystem.Release();

	RenderingPipeline.m_StaticModelRenderer->SetDistanceFog(false);
}

void TitleScene::Init()
{
	DW_SCROLL(0, "タイトルシーン初期化");

	// 平行サイト作成
	m_DirLight = LiMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(0.8f, -1, 0.8f),		// ライト方向
		ZVec4(0.3f, 0.3f, 0.3f, 1)	// 基本色
	);

	// 環境色
	LiMgr.m_AmbientLightColor.Set(0.1f, 0.1f, 0.1f);
	// フォグ
	RenderingPipeline.m_StaticModelRenderer->SetDistanceFog(true, &ZVec3(0.1f, 0.1f, 0.1f), 0.04f);
	
	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(-0.3f, 0.9f, -4.4f);


	ECSEntity::MakeEntityFromJsonFile("data/Json/titleinit.json", &m_Entities);

	{
		SpriteComponent* spritecomp = ECS.MakeComponent<SpriteComponent>();
		spritecomp->Size.Set(800, 180);
		spritecomp->Tex = APP.m_ResStg.LoadTexture("data/UI/title_br.png");
		spritecomp->BsAdd = true;
		TransformComponent* trans = ECS.MakeComponent<TransformComponent>();
		trans->Transform.SetPos(640, 300, 0);

		auto entity = ECS.MakeEntity(trans, spritecomp);
		m_Entities.push_back(entity);
	}
	{
		SpriteComponent* spritecomp = ECS.MakeComponent<SpriteComponent>();
		spritecomp->Size.Set(800, 180);
		spritecomp->Tex = APP.m_ResStg.LoadTexture("data/UI/title.png");

		TransformComponent* trans = ECS.MakeComponent<TransformComponent>();
		trans->Transform.SetPos(640, 300, 0);

		auto entity = ECS.MakeEntity(trans, spritecomp);
		m_Entities.push_back(entity);
	}


	m_BackTexture = APP.m_ResStg.LoadTexture("data/Texture/title.bmp");
	{
		ECS.RegisterClassReflection<GameModelComponent>("GameModel");
		ECS.RegisterClassReflection<TransformComponent>("Transform");
		//	背景ステージ読み込み
		ECSEntity::MakeEntityFromJsonFile("data/Json/test.json", &m_Entities);
	}
	//	ポストエフェクトを作成
	GW.m_PostEffects.Release();
	GW.m_PostEffects.LoadSettingFromJson("data/Scene/HkScene/PostEffect.json");
	// EffectPass追加
	{
		auto* setupPass = appnew(SetupPass);
		auto* hePass = appnew(HEPass);
		auto* ssaoPass = appnew(SSAOPass);
		auto* lightBloomPass = appnew(LightBloomPass);
		setupPass->Init();
		hePass->Init();
		ssaoPass->Init();
		lightBloomPass->Init();
		GW.m_PostEffects.AddEffectPass("Setup", setupPass);
		GW.m_PostEffects.AddEffectPass("HE", hePass);
		GW.m_PostEffects.AddEffectPass("SSAO", ssaoPass);
		GW.m_PostEffects.AddEffectPass("LightBloom", lightBloomPass);
	}

	m_SkyTexture = APP.m_ResStg.LoadTexture("data/Texture/Sky.png");

	auto& br = ShMgr.GetRenderer<BlurShader>();
	br.CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));


	auto sound	= APP.m_ResStg.LoadSound("data/Sound/bgm/title/Phantom.wav");
	m_BGM		= sound->CreateInstance(false);
	m_BGM->Play(true);


	//システム準備
	m_UpdateSystems.AddSystem(Make_Shared(TitleMenuUpdateSystem, sysnew));

	m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem, sysnew));
	
	m_2DDrawSystem.AddSystem(Make_Shared(SpriteDrawSystem, sysnew));
	m_2DDrawSystem.AddSystem(Make_Shared(RadioButtonDrawSystem, sysnew));


	m_TitleEffect = EFFECT.LoadEffect("data/Effect/Title/Title.efk");
	
	m_EffectHadle = EFFECT.SubmitPlayEffect(m_TitleEffect,ZVec3(0, -2,3));

}

void TitleScene::Update()
{
	DW_STATIC(1, "Title");

	// EscでVRSceneへ
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
		APP.ExitGameLoop();
		return;
	}

	m_Cam.Update();
	ECS.UpdateSystems(m_UpdateSystems, APP.m_DeltaTime);
}

void TitleScene::ImGuiUpdate()
{
}

void TitleScene::Draw()
{
	//	3D描画開始
	RenderingPipeline.Begin3DRendering();
	{
		// 半透明モード
		ShMgr.m_bsAlpha.SetState();

		// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
		ShMgr.m_bsAlpha_AtoC.SetState();

		// カメラやライトのデータをシェーダ側に転送する
		{
			// カメラ設定& シェーダに書き込み
			m_Cam.SetCamera();
			// ライト情報をシェーダ側に書き込む
			LiMgr.Update();
		}
		
		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);

		//	シャドウマップ描画
		LiMgr.SetShadowCamTargetPoint(m_Cam.m_mCam.GetPos()); //	現在のカメラをセット
		RenderingPipeline.DrawShadow();

		//	3D描画
		RenderingPipeline.Draw();
		
		// ライン描画
		ShMgr.GetRenderer<LineRenderer>().Flash();

		// エフェクト描画
		EFFECT.Draw();


	}	//	3D描画終了
	RenderingPipeline.End3DRendering();

	// バックバッファにポストエフェクト処理
	GW.m_PostEffects.RenderPostEffects(RenderingPipeline.m_RenderTargets->GetTexZSP(0));


	//	2D描画(UIなど)
	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, ZDx.GetBackBuffer()->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	auto& sr = ShMgr.GetRenderer<SpriteRenderer>();
	sr.Begin(false, true);

	ECS.UpdateSystems(m_2DDrawSystem, APP.m_DeltaTime, true);
	
	sr.End();

}

