#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__

#include "Camera/TitleCamera.h"

struct GameModelComponent;
class Fade;
class TitleScene : public ZSceneBase
{
public:
	virtual ~TitleScene() { Release(); }

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

public:
	// その他
	ECSSystemList				m_2DDrawSystem;

	// エフェクト
	Effekseer::Effect*			m_TitleEffect;
	ZEffectManager::EffectHandle m_EffectHadle;

	// エンティティハンドル
	ZAVector<ZSP<ECSEntity>>	m_Entities;

	// 平行光源
	ZSP<DirLight>				m_DirLight;

	// カメラ
	TitleCamera					m_Cam;
	ZSP<ZTexture>				m_BackTexture;
	ZSP<ZTexture>				m_SkyTexture;

	ZSP<ZSound>					m_BGM;


	ZSP<ZSound>					TestSE;
	ZVec3 testPos;
	float						testRad = 1;
};

#endif