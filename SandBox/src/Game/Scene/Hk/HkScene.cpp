#include "Game/Application.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSSystems/Common/CommonSystems.h"
#include "HkScene.h"

void HkScene::Release()
{
	m_DirLight = nullptr;
	m_PostEffects.Release();

	// エンティティ削除
	// ※ 削除方法について詳しくはECSEnity.h参照
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();
	m_PhysicsWorld->Release();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();

}

void HkScene::Init()
{

	DW_SCROLL(0, "タイトルシーン初期化");

	m_PhysicsWorld = Make_Shared(ZPhysicsWorld,sysnew);

	m_PhysicsWorld->Init();

	// 平行サイト作成
	m_DirLight = LiMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(0.8f, -1, 0.8f),		// ライト方向
		ZVec4(0.7f, 0.7f, 0.7f, 1)	// 基本色
	);

	// 環境色
	LiMgr.SetAmbientLightColor(ZVec3(0.3f, 0.3f, 0.3f));

	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);

	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		//auto model = APP.m_ResStg.LoadMesh("data/TestRoom/Test.xed");
		auto model = APP.m_ResStg.LoadMesh("data/Map/map3/map.xed");
		modelcomp->Model = model;
		modelcomp->RenderFlg.Character = false;
		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();

		transcomp->Transform.SetPos(5.f, -0.5f, 5.f);
		auto entity = ECS.MakeEntity(transcomp, modelcomp);
		m_Entities.push_back(entity);
	}
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		auto model = APP.m_ResStg.LoadMesh("data/Model/box/box.xed");
		modelcomp->Model = model;
		modelcomp->RenderFlg.Character = true;

		/*-------------------------------------------------------------------------------*/
		//	半透明にして消すタイミングで以下の二つのフラグを入れること
		modelcomp->RenderFlg.Delete = true;
		modelcomp->RenderFlg.SemiTransparent = true;
		/*-------------------------------------------------------------------------------*/

		/*-------------------------------------------------------------------------------*/
		modelcomp->RenderFlg.XRayCol.Set(1.f, 0.f, 1.f, 1.f);
		/*-------------------------------------------------------------------------------*/

		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
		auto entity = ECS.MakeEntity(transcomp, modelcomp);
		m_Entities.push_back(entity);
	}
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		auto model = APP.m_ResStg.LoadMesh("data/Model/box/box.xed");
		modelcomp->Model = model;
		modelcomp->RenderFlg.Character = false;

		/*-------------------------------------------------------------------------------*/
		//	半透明にして消すタイミングで以下の二つのフラグを入れること
		modelcomp->RenderFlg.Delete = true;
		modelcomp->RenderFlg.SemiTransparent = true;
		/*-------------------------------------------------------------------------------*/
		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();

		transcomp->Transform.SetPos(0.2f, 0.5f, 0.2f);
		auto entity = ECS.MakeEntity(transcomp, modelcomp);
		m_Entities.push_back(entity);
	}
	//	テストキャラ
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		auto model = APP.m_ResStg.LoadMesh("data/Model/Survivor/Survivor.xed");
		modelcomp->Model = model;
		modelcomp->RenderFlg.Character = true;

		/*-------------------------------------------------------------------------------*/
		modelcomp->RenderFlg.XRayCol.Set(1.f, 0.f, 0.f, 1.f);
		/*-------------------------------------------------------------------------------*/

		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
		transcomp->Transform.SetPos(-5.f, -0.5f, 0);
		ModelBoneControllerComponent* bonecontrollercomp = ECS.MakeComponent<ModelBoneControllerComponent>();
		AnimatorComponent* animatorcomp = ECS.MakeComponent<AnimatorComponent>();
		animatorcomp->Animator = Make_Shared(ZAnimator, sysnew);

		bonecontrollercomp->BoneController.SetModel(modelcomp->Model);
		bonecontrollercomp->BoneController.AddAllPhysicsObjToPhysicsWorld(*m_PhysicsWorld.GetPtr());

		bonecontrollercomp->BoneController.InitAnimator(*animatorcomp->Animator);
		animatorcomp->Animator->ChangeAnime("Movement", true);
		animatorcomp->Animator->EnableRootMotion(false);

		auto entity = ECS.MakeEntity(transcomp, modelcomp, bonecontrollercomp, animatorcomp);
		m_Entities.push_back(entity);
	}
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		auto model = APP.m_ResStg.LoadMesh("data/Model/Killer/killer.xed");
		modelcomp->Model = model;
		modelcomp->RenderFlg.Character = true;

		/*-------------------------------------------------------------------------------*/
		modelcomp->RenderFlg.XRayCol.Set(1.f, 1.f, 0.f, 1.f);
		/*-------------------------------------------------------------------------------*/

		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
		transcomp->Transform.SetPos(5.f, -0.5f, 0);
		ModelBoneControllerComponent* bonecontrollercomp = ECS.MakeComponent<ModelBoneControllerComponent>();
		AnimatorComponent* animatorcomp = ECS.MakeComponent<AnimatorComponent>();
		animatorcomp->Animator = Make_Shared(ZAnimator, sysnew);

		bonecontrollercomp->BoneController.SetModel(modelcomp->Model);
		bonecontrollercomp->BoneController.AddAllPhysicsObjToPhysicsWorld(*m_PhysicsWorld.GetPtr());

		bonecontrollercomp->BoneController.InitAnimator(*animatorcomp->Animator);
		animatorcomp->Animator->ChangeAnime("Movement", true);
		animatorcomp->Animator->EnableRootMotion(false);

		auto entity = ECS.MakeEntity(transcomp, modelcomp, bonecontrollercomp, animatorcomp);
		m_Entities.push_back(entity);
	}
	/*-------------------------------------------------------------------------------*/

	//	ポストエフェクトを作成
	m_PostEffects.Init();
	// EffectPass追加
	{
		auto* setupPass = appnew(SetupPass);
		auto* dofPass = appnew(DOFPass);
		auto* xRayPass = appnew(XRayPass);
		auto* lightBloomPass = appnew(LightBloomPass);
		setupPass->Init();
		dofPass->Init();
		xRayPass->Init();
		lightBloomPass->Init();
		m_PostEffects.AddEffectPass("Setup", setupPass);
		m_PostEffects.AddEffectPass("DoF", dofPass);
		m_PostEffects.AddEffectPass("XRay", xRayPass);
		m_PostEffects.AddEffectPass("LightBloom", lightBloomPass);
	}

	//
	m_SkyTex = APP.m_ResStg.LoadTexture("data/Texture/title_back.png");

	ShMgr.GetRenderer<BlurShader>().CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));


	/*-------------------------------------------------------------------------------*/



	 //システム準備
	/// 更新システム
	m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,sysnew));
	/// 描画システム
	m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem,sysnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,sysnew));

}

void HkScene::Update()
{
	DW_STATIC(1, "Hk_Scene");

	// Escで終了
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
		APP.ExitGameLoop();
		return;
	}

	//カメラ操作
	m_Cam.Update(GW.m_MoveInputValue[0]);

	ECS.UpdateSystems(m_UpdateSystems, APP.m_DeltaTime);

	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());

}

void HkScene::ImGuiUpdate()
{

	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("SystemInfo") == false)
		{
			ImGui::End();
			return;
		}

		// 物理エンジン
		{
			bool isEnablePhysicsDebug = m_PhysicsWorld->IsEnableDebugDraw();
			ImGui::Checkbox("Physics Debug Draw", &isEnablePhysicsDebug);
			m_PhysicsWorld->SetDebugDrawMode(isEnablePhysicsDebug);
			ImGui::Separator();
		}

		// 各システム
		auto debugImGui = [](ZSP<ECSSystemBase> system)
		{
			ImGui::Text(system->GetSystemName().c_str());
			system->DebugImGuiRender();
			ImGui::Separator();
		};

		for (auto system : m_UpdateSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		for (auto system : m_DrawSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);

	RenderingPipeline.ImGui();
	m_PostEffects.ImGui();
	ShMgr.GetRenderer<BlurShader>().ImGui();
	LiMgr.ImGui();

}

void HkScene::Draw()
{
	//	3D描画開始
	RenderingPipeline.Begin3DRendering();
	{
		// 半透明モード
		ShMgr.m_bsAlpha.SetState();

		// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
		ShMgr.m_bsAlpha_AtoC.SetState();

		// カメラやライトのデータをシェーダ側に転送する
		{
			// カメラ設定& シェーダに書き込み
			m_Cam.SetCamera();
			// ライト情報をシェーダ側に書き込む
			LiMgr.Update();
		}

		// [2D]背景描画
		{
			auto& Ss = ShMgr.GetRenderer<SpriteRenderer>();
			Ss.Begin(false, true);
			Ss.Draw2D(m_SkyTex->GetTex(), 0, 0, 1280, 720);
			Ss.End();
		}


		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);
	
		// 物理エンジンのデバッグ描画
		m_PhysicsWorld->DebugDraw();


		//	シャドウマップ描画	
		LiMgr.SetShadowCamTargetPoint(m_Cam.m_mCam.GetPos());	//	現在のカメラをセット
		RenderingPipeline.DrawShadow();

		//	3D描画
		ShMgr.GetRenderer<LineRenderer>().Flash();
		RenderingPipeline.Draw();

		//	3Dエフェクト描画

		// ~~~~~~
	}	//	3D描画終了
	RenderingPipeline.End3DRendering();
	
	// バックバッファにポストエフェクト処理
	m_PostEffects.RenderPostEffects(RenderingPipeline.m_RenderTargets->GetTexZSP(0));
}

