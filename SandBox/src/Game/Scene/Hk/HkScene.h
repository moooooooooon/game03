#ifndef __HK_SCENE_H__
#define __HK_SCENE_H__

#include "Camera/GameCamera.h"
#include "Shader/PostEffect/PostEffects.h"

struct GameModelComponent;

class HkScene : public ZSceneBase
{
public:
	virtual ~HkScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();


public:
	// その他

	// エンティティハンドル
	ZAVector<ZSP<ECSEntity>> m_Entities; 

	// 平行光源
	ZSP<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;

	// テクスチャ
	ZSP<ZTexture> m_texBack;

	ZSP<ZPhysicsWorld> m_PhysicsWorld;


	//	ポストエフェクト用(宣言はどこかシングルトンクラス内に作った方がいいけど、テスト的にここで)
	PostEffects			m_PostEffects;

	//---------------------------------------------------------------------------
	ZSP<ZTexture> m_SkyTex;	//	テスト

};

#endif