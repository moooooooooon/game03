#include "Application.h"

Application::Application(const char* wndTitle, const ZWindowProperties & properties)
	: ZMainFrame::ZMainFrame(wndTitle,properties)
{
}

bool Application::Init()
{
	if (ZMainFrame::Init() == false)
		return false;

	// 頂点シェーダ 入力レイアウト設定項目
	ZVertexShader::AddForceUseFormat("POSITION", DXGI_FORMAT_R32G32B32_FLOAT);
	ZVertexShader::AddForceUseFormat("BLENDWEIGHT", DXGI_FORMAT_R8G8B8A8_UNORM);
	ZVertexShader::AddForceUseFormat("BLENDINDICES", DXGI_FORMAT_R16G16B16A16_UINT);
	ZVertexShader::ForceUseInputLayoutParam param;
	param.DataStepRate = 1;
	param.InputClassification = D3D11_INPUT_PER_INSTANCE_DATA;
	param.SlotIndex = 1;
	ZVertexShader::AddForceUseInputLayoutParam("MATRIX", param);
	param.SlotIndex = 2;
	ZVertexShader::AddForceUseInputLayoutParam("XRAY", param);
	param.SlotIndex = 3;
	ZVertexShader::AddForceUseInputLayoutParam("MULCOLOR", param);

	GW.Init();

	return true;
}

void Application::FrameUpdate()
{
	// サウンド処理
	ZSndMgr.Update();
	GW.Update();
	GW.Draw();
}

void Application::Release()
{
	GW.Release();
	ZMainFrame::Release();
}
