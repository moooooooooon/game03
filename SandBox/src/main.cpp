#include "Game/Application.h"
#include "Game/Scene/Hk/HkScene.h"
#include "Game/Scene/TestScene/TestScene.h"
#include "Game/Scene/Title/TitleScene.h"


LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EzLib::MainFrame_WindowProc(hWnd, msg, wParam, lParam);
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpszArgs, int nWinMode)
{
#ifdef _DEBUG
	// メモリリーク検知
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#else
	ZAllocator::IsDumpingMemoryInfo = false; // 実行終了時にメモリ情報をダンプしないようにする
#endif

	ZWindowProperties properties{ hInstance,1280,720,false,false,&WindowProc};

	if (IDYES == MessageBox(NULL, "フルスクリーンにしますか?", "確認", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2))
		properties.UseFullScreen = true;
	
 	Application app("Sprint Burst", properties);
	app.SetFrameRate(60);
	GW.m_SceneMgr.SubmitSceneGenerater<TestScene>("Test");
	GW.m_SceneMgr.SubmitSceneGenerater<TitleScene>("Title");

	GW.m_SceneMgr.ChangeScene("Title");
	app.Start();

	return 0;
}

