#ifndef __MATCHING_STATE_H__
#define __MATCHING_STATE_H__

#include "MessageTypes.h"
#include "State/State.h"

class MatchingStateBase : public StateBase
{
public:
	virtual void Start() = 0;
	virtual void Update(float delta) = 0;
	virtual void OnChangeEvent() = 0;
	virtual void OnNetWorkEvent(MessageTypes type) = 0;
	virtual void OnLoginPlayer(const ZUUID& playerUUID) = 0;
	virtual void ImGui(){}
};

class MatchingState_NoClient : public MatchingStateBase
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
	void OnNetWorkEvent(MessageTypes type);
	void OnLoginPlayer(const ZUUID& playerUUID);
};

class MatchingState_MatchingCountDown : public MatchingStateBase
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
	void OnNetWorkEvent(MessageTypes type);
	void OnLoginPlayer(const ZUUID& playerUUID);
	void ImGui()override;
};

class MatchingState_MatchStartCountDown : public MatchingStateBase
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
	void OnNetWorkEvent(MessageTypes type);
	void OnLoginPlayer(const ZUUID& playerUUID);
	void ImGui()override;

private:
	float m_LastMatchingCount;	// このステート移行時のマッチングカウントダウン保持用
};

class MatchingStateMachine : public StateMachine
{
public:
	MatchingStateMachine();

	void ImGui();
	
	void OnLoginPlayer(const ZUUID& playerUUID);

	// プレイスタイル別のプレイヤー数設定
	void SetPlayerCount(LoginData::PlayStyle style, uint count);
	// プレイスタイル別のプレイヤー数取得
	uint GetPlayerCount(LoginData::PlayStyle style);
	
	void OnNetWorkEvent(MessageTypes type);

public:
	float m_CountDownTimer;

private:
	ZMap<LoginData::PlayStyle, uint> m_PlayerCnt;
};

#endif