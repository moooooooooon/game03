#ifndef __SERVER_APP_H__
#define __SERVER_APP_H__

#include "MainFrame/ZMainFrame.h"

#include "Camera/LightCamera.h"
#include "Shader/Light/LightManager.h"
#include "Shader/Renderer/Renderers.h"
#include "Shader/Renderer/ModelRenderer/ModelRenderingPipeline.h"
#include "Shader/ShaderManager.h"

#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"

#include "Shader/PostEffect/PostEffects.h"
#include "Shader/PostEffect/EffectPass/SetupPass.h"
#include "Shader/PostEffect/EffectPass/DOFPass.h"
#include "Shader/PostEffect/EffectPass/XRayPass.h"
#include "Shader/PostEffect/EffectPass/LightBloomPass.h"
#include "Shader/PostEffect/EffectPass/HEPass.h"
#include "Shader/PostEffect/EffectPass/SSAOPass.h"

#include "Common/CommonSubSystems.h"
#include "System/ServerSubSystems.h"

class ServerApp : public ZMainFrame
{
public:
	ServerApp(const char* wndTitle, const ZWindowProperties& properties);

protected:
	virtual bool Init()override;
	virtual void FrameUpdate()override;
	virtual void TickUpdate()override;
	virtual void Release()override;

};


#endif