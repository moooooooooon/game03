#include "Server/ServerApp.h"
#include "Server/Scene/MatchingScene.h"

LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EzLib::MainFrame_WindowProc(hWnd, msg, wParam, lParam);
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpszArgs, int nWinMode)
{
#ifdef _DEBUG
	// メモリリーク検知
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#else
	ZAllocator::IsDumpingMemoryInfo = false; // 実行終了時にメモリ情報をダンプしないようにする
#endif

	ZWindowProperties properties{ hInstance,640,360,false,false,&WindowProc };

	ServerApp app("ServerApp", properties);
	app.SetFrameRate(60);
	GW.m_SceneMgr.SubmitSceneGenerater<MatchingScene>("Matching");
	GW.m_SceneMgr.ChangeScene("Matching");
	app.Start();

	return 0;
}