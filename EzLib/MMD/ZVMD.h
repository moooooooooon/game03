#ifndef __ZVMD_H__
#define __ZVMD_H__

#include <string>
#include <cstdint>
#include <vector>
#include <array>

#include "SystemDefines.h"

#include "ZMMDFileStr.h"

#include "Math/ZMath.h"

namespace EzLib
{
	template<size_t size>
	using ZVMDStr = ZMMDFileStr<size>;

	struct ZVMDHeader
	{
		ZMMDFileStr<30> Header;
		ZMMDFileStr<20> m_ModelName;
	};

	struct ZVMDMotion
	{
		ZVMDStr<15> BoneName;
		uint32 Frame;
		ZVec3 Translate;
		ZQuat Quaternion;
		std::array<uint8, 64> Interpolation;
	};

	struct ZVMDMorph
	{
		ZVMDStr<15> BlendShapeName;
		uint32 Frame;
		float Weight;
	};

	struct ZVMDCamera
	{
		uint32 Frame;
		float Distance;
		ZVec3 Interest;
		ZVec3 Rotate;
		std::array<uint8, 24> Interpolation;
		uint32 ViewAngle;
		uint8 IsPerspective;
	};

	struct ZVMDLight
	{
		uint32 Frame;
		ZVec3 Color;
		ZVec3 Position;
	};

	struct ZVMDShadow
	{
		uint32 Frame;
		uint8 ShadowType;
		float Distance;
	};

	struct ZVMDIKInfo
	{
		ZVMDStr<20> Name;
		uint8 Enable;
	};

	struct ZVMDIK
	{
		uint32 Frame;
		uint8 Show;
		ZAVector<ZVMDIKInfo> IKInfos;
	};

	struct ZVMD
	{
		ZVMDHeader Header;
		ZAVector<ZVMDMotion> Motions;
		ZAVector<ZVMDMorph> Morphs;
		ZAVector<ZVMDCamera> Cameras;
		ZAVector<ZVMDLight> Lights;
		ZAVector<ZVMDShadow> Shadows;
		ZAVector<ZVMDIK> IKs;
	};

	bool ReadVMDFile(ZVMD* vmd, const char* fileName);
}

#endif