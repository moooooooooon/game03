#include "PCH/pch.h"

ZSceneManager::ZSceneManager()
{
	m_NowScene = nullptr;
	m_NextChangeSceneName.clear();
	m_SceneGenerateMap.clear();
}

void ZSceneManager::Init()
{
	m_NowScene = nullptr;
	// 初期シーン作成
	if (m_NextChangeSceneName.empty() == false)
		m_NowScene = m_SceneGenerateMap[m_NextChangeSceneName]();
	m_NextChangeSceneName.clear();

	// 初期シーン初期化
	if(m_NowScene)
		m_NowScene->Init();
}

bool ZSceneManager::Start()
{
	if (m_NowScene == nullptr)return false;
	if (m_NowScene->m_IsStarted)return false;
	m_NowScene->Start();
	m_NowScene->m_IsStarted = true;
	return true;
}

void ZSceneManager::PreUpdate()
{
	if (m_NowScene)m_NowScene->PreUpdate();
}

void ZSceneManager::Update()
{
	if (m_NextChangeSceneName.empty() == false)
	{
		m_NowScene = nullptr;

		// 存在するシーン名か
		if (m_SceneGenerateMap.end() == m_SceneGenerateMap.find(m_NextChangeSceneName))
			DW_SCROLL(0, "存在シーンです : %s\n", m_NextChangeSceneName.c_str());
		else
		{
			// 存在するならシーン作成
			m_NowScene = m_SceneGenerateMap[m_NextChangeSceneName]();
			// 初期化
			m_NowScene->Init();
		}

		// シーン切り替え用文字列クリア
		m_NextChangeSceneName = "";
	}

	if (m_NowScene)
		m_NowScene->Update();
}

void ZSceneManager::TickUpdate()
{
	if (m_NowScene)
		m_NowScene->TickUpdate();
}

void ZSceneManager::ImGuiUpdate()
{
	if (m_NowScene)
		m_NowScene->ImGuiUpdate();
}

void ZSceneManager::Draw()
{
	if (m_NowScene)
		m_NowScene->Draw();
}

void ZSceneManager::Release()
{
	m_NowScene = nullptr;
	m_NextChangeSceneName.clear();
	m_SceneGenerateMap.clear();
}

void ZSceneManager::ChangeScene(const ZString& sceneName)
{
	m_NextChangeSceneName = sceneName;
}

void ZSceneManager::NowChangeScene(const ZString & sceneName)
{
	// 存在するシーン名か
	if (m_SceneGenerateMap.end() == m_SceneGenerateMap.find(sceneName))
		DW_SCROLL(0, "存在シーンです : %s\n", sceneName.c_str());
	else
	{
		// 存在するならシーン作成
		m_NowScene = m_SceneGenerateMap[sceneName]();
		// 初期化
		m_NowScene->Init();
	}
}

ZSP<ZSceneBase> ZSceneManager::GetNowScene()
{
	return m_NowScene;
}
