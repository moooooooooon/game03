#ifndef __NETWORK_COMMON_H__
#define __NETWORK_COMMON_H__

namespace EzLib
{
namespace NetWork
{
	//=============定数====================
	static constexpr uint PORT_NO = 50000;	//ポート番号
	static constexpr uint BUFF_SIZE = 20;	//データバッファ数
	static constexpr uint CHANNEL_SIZE = 2;	//通信チャンネル数

	// ヘッダ
	struct PacketHeader
	{
	public:
		PacketHeader()
			: MessageType(0),
			SessionID(0),
			LastRecvSessionID(0)
		{
		}

		inline void ToString(ZString& str)const
		{
			using namespace std::chrono;
			std::string tmp = "MassageType: " + std::to_string((int)MessageType) + "," +
							  "SessionID: " + std::to_string(SessionID) + "," +
							  "LastRecvSessionID: " + std::to_string(LastRecvSessionID);
			str = tmp.c_str();
		}

	public:
		char MessageType; // MessageType::Login etc...
		// パケットロス検出用
		size_t SessionID;
		size_t LastRecvSessionID;
	};


	// (PacketHeader + 任意のデータ) <- このパケット生成
	// 引数
	//  _msgType ... 何を意味するメッセージか(例 loginメッセージなら 1,logoutメッセージなら 2 etc...)
	//  _sessionID ... パケットロスト検出用
	//  _data ... 送信するデータ
	//  _size ... 送信するデータのサイズ
	//  _packetFlag ... ENetのパケットフラグ(詳細はENetのリファレンス参照)
	ENetPacket* CreatePacket(char _msgType, size_t _sessionID,size_t _lastRecvSessionID, const void* _data, int _size,uint _packetFlag = ENET_PACKET_FLAG_RELIABLE);
}
}

#endif