#include "PCH/pch.h"
#include "SubSystems.h"

namespace EzLib
{
namespace NetWork
{
	const uint ZClient::TimeOut = 3000;

	ZClient::ZClient()
	{
		m_Host = nullptr;
		m_Peer = nullptr;
		m_IsConnected = false;
		m_SendCnt = 0;
	}

	ZClient::~ZClient()
	{
		Release();
	}

	bool ZClient::Init()
	{
		std::string errMsg; // エラーメッセージ用

		if (enet_initialize() != 0)
		{
			errMsg = "An error occurred while initializing ENet.";
			goto INIT_ERROR;
		}
		atexit(enet_deinitialize);

		m_Host = enet_host_create
		(
			NULL		,	/*クライアント作成*/
			1			,	/*最大発信接続数*/
			CHANNEL_SIZE,	/*チャンネル数(0 〜 n-1)*/
			0			,	/*着信帯域幅*/
			0				/*着信帯域幅*/
		);

		if (m_Host == NULL)
		{
			//エラー処理
			errMsg = "An error occurred while trying to create an ENet client host.";
			goto INIT_ERROR;
		}

		//チャンネルの分だけ受信データバッファを確保
		m_Channel = std::vector<std::deque<enet_uint8 *>>(CHANNEL_SIZE, std::deque<enet_uint8*>(BUFF_SIZE, 0));

		return true;

	INIT_ERROR:
		MessageBox(NULL, errMsg.c_str(), "ENet Error", MB_OK);
		return false;
	}

	void ZClient::Update()
	{
		if (m_IsConnected == false)return;

		while (enet_host_service(m_Host, &m_Event, 0) > 0)
		{
			switch (m_Event.type)
			{
				case ENET_EVENT_TYPE_CONNECT:
				{
					DW_SCROLL(1,"new client %d:%u.data:%d",
								  m_Event.peer->address.host,
								  m_Event.peer->address.port,
								  m_Event.data);

					// ~~接続時のコールバック~~
					if (m_OnConnectCallBackFuncs.empty() == false)
					{
						//UUIDを取得
						for (auto& func : m_OnConnectCallBackFuncs)
							func.second();
					}
				}
				break;
				
				case ENET_EVENT_TYPE_RECEIVE:
				{
					//[dataLength]をもとにメモリ確保
					enet_uint8* data;
					data = (enet_uint8 *)calloc(m_Event.packet->dataLength, sizeof(enet_uint8));

					//受信データをコピー
					memcpy(data, m_Event.packet->data, m_Event.packet->dataLength);

					//格納データがバッファサイズを超えた場合、先頭（最も古い）から削除
					if (m_Channel[m_Event.channelID].size() > BUFF_SIZE)
					{
						auto d = m_Channel[m_Event.channelID].front();
						m_Channel[m_Event.channelID].pop_front();
						free(d);
					}

					//最後尾に最新データを格納
					m_Channel[m_Event.channelID].push_back(data);

					/*パケットをクリーンアップ*/
					enet_packet_destroy(m_Event.packet);
					
					PacketHeader header = *((PacketHeader*)data);
					m_LastRecvID = header.LastRecvSessionID;

					// ~~データ受信時のコールバック~~
					if (m_OnRecvCallBackFuncs.empty() == false)
					{
						for (auto& func : m_OnRecvCallBackFuncs)
							func.second(header,(void*)(data + sizeof(PacketHeader)));
					}

					
				}
				break;

				case ENET_EVENT_TYPE_DISCONNECT:
				{
					DW_SCROLL(1,"%s disconnected.", m_Event.peer->data);
					/* クライアント情報をリセット */
					m_Event.peer->data = nullptr;

					ResetConnection();

					// ~~切断時のコールバック~~
					if (m_OnDisConnectCallBackFuncs.empty() == false)
					{
						for (auto& func : m_OnDisConnectCallBackFuncs)
							func.second();
					}
					return;
				}
				break;
			
			} // switch (m_Event.type)

		} // while (enet_host_service(m_Host, &m_Event, 0) > 0)

	}

	void ZClient::Release()
	{
		if(m_Channel.empty() == false)
		{
			for (uint i = 0; i < CHANNEL_SIZE; i++)
			{
				for (auto data : m_Channel[i])
					free(data);
				m_Channel[i].clear();
				m_Channel[i].shrink_to_fit();
			}
			m_Channel.clear();
		}

		// ENet終了
		if (m_Peer != nullptr) DisConnect();
		if (m_Host != nullptr)
		{
			enet_host_destroy(m_Host);
			m_Host = nullptr;
		}

		RemoveAllCallBackFunction();
	}

	void ZClient::ResetConnection()
	{
		if (m_IsConnected == false)return;

		if(m_Peer) enet_peer_reset(m_Peer);
		m_Peer = nullptr;
		if (m_Host != nullptr)
		{
			enet_host_destroy(m_Host);
			// 新しくホスト作成
			m_Host = enet_host_create
			(
				NULL,			/*クライアント作成*/
				1,				/*最大発信接続数*/
				CHANNEL_SIZE,	/*チャンネル数(0 〜 n-1)*/
				0,				/*着信帯域幅*/
				0				/*着信帯域幅*/
			);
		}
		m_IsConnected = false;
	}

	void ZClient::Connect(const char* _address)
	{
		if (m_IsConnected) return;
		enet_address_set_host(&m_Address, _address);
		m_Address.port = PORT_NO;

		// 接続開始
		m_Peer = enet_host_connect(m_Host, &m_Address, 2, 0);
		if (m_Peer == nullptr)
		{
			assert(false);
			MessageBox(NULL, "No available peers for initiating an ENet connection.", "ENet Error", MB_OK);
			return;
		}
		
		// 接続待機(3秒)
		if (enet_host_service(m_Host, &m_Event, TimeOut) <= 0 || m_Event.type != ENET_EVENT_TYPE_CONNECT)
		{
			// 接続失敗
			enet_peer_reset(m_Peer);
			DW_SCROLL(0,"Connection failed.");
			m_Peer = nullptr;
			return;
		}
		
		// 接続成功
		m_IsConnected = true;
		DW_SCROLL(0,"Connection succeeded.");
	}

	void ZClient::Connect(const char * _address, ConnectResultCallBackFunc _resultFunc)
	{
		if (m_IsConnected) return;
		enet_address_set_host(&m_Address, _address);
		m_Address.port = PORT_NO;

		// 接続開始
		m_Peer = enet_host_connect(m_Host, &m_Address, 2, 0);
		if (m_Peer == nullptr)
		{
			assert(false);
			MessageBox(NULL, "No available peers for initiating an ENet connection.", "ENet Error", MB_OK);
			return;
		}

		THPOOL.AddTask([this,_resultFunc]()
		{
			// 接続待機(3秒)
			if (enet_host_service(m_Host, &m_Event, TimeOut) <= 0 || m_Event.type != ENET_EVENT_TYPE_CONNECT)
			{
				// 接続失敗
				enet_peer_reset(m_Peer);
				DW_SCROLL(0, "Connection failed.");
				if (_resultFunc)_resultFunc(false);
				m_Peer = nullptr;
				return;
			}

			// 接続成功
			m_IsConnected = true;
			if (_resultFunc)_resultFunc(true);
			DW_SCROLL(0, "Connection succeeded.");
		});
	}

	void ZClient::DisConnect()
	{
		// 接続が確立されていなければ
		if (m_IsConnected == false) return;
		
		enet_peer_disconnect(m_Peer, 0);
		while (enet_host_service(m_Host, &m_Event, TimeOut) > 0)
		{
			switch (m_Event.type)
			{
				case ENET_EVENT_TYPE_RECEIVE:
					enet_packet_destroy(m_Event.packet);
				break;
				
				case ENET_EVENT_TYPE_DISCONNECT:
					DW_SCROLL(0,"Disconnection succeeded.");
					m_Peer = nullptr;
					m_IsConnected = false;
				return;
			}
		}

		// 切断失敗
		// 強制切断
		ResetConnection();
	}

	void ZClient::SendData(char _msgType, void* _data, int _size, int _channel)
	{
		// 接続が確立されていなければ
		if (m_IsConnected == false)return;
		if (_size <= 0) return;

		// 送信パケット生成
		ENetPacket* packet = CreatePacket(_msgType, m_SendCnt, m_LastRecvID,_data, _size);
		// 送信(送信バッファに格納されるだけ)
		enet_peer_send(m_Peer, _channel, packet);
		enet_host_flush(m_Host); // 送信バッファ内のパケット送信

		m_SendCnt++;
	}

	bool ZClient::IsConnected()
	{
		return m_IsConnected;
	}

	void ZClient::AddConnectCallBack(const std::string& name, ConnectionCallBackFunc func)
	{
		auto it = m_OnConnectCallBackFuncs.find(name);
		if (it != m_OnConnectCallBackFuncs.end())
			return;
		m_OnConnectCallBackFuncs[name] = func;
	}

	void ZClient::AddRecvCallBack(const std::string & name, RecvCallBackFunc func)
	{
		auto it = m_OnRecvCallBackFuncs.find(name);
		if (it != m_OnRecvCallBackFuncs.end())
			return;
		m_OnRecvCallBackFuncs[name] = func;
	}

	void ZClient::AddDisConnectCallBack(const std::string& name, ConnectionCallBackFunc func)
	{
		auto it = m_OnDisConnectCallBackFuncs.find(name);
		if (it != m_OnDisConnectCallBackFuncs.end())
			return;
		m_OnDisConnectCallBackFuncs[name] = func;
	}

	void ZClient::RemoveAllCallBackFunction()
	{
		m_OnConnectCallBackFuncs.clear();
		m_OnRecvCallBackFuncs.clear();
		m_OnDisConnectCallBackFuncs.clear();
	}

	void ZClient::RemoveAllCallBackFunction(const std::string& funcName)
	{
		RemoveConnectCallBack(funcName);
		RemoveRecvCallBack(funcName);
		RemoveDisConnectCallBack(funcName);
	}

	void ZClient::RemoveConnectCallBack(const std::string & funcName)
	{
		auto it = m_OnConnectCallBackFuncs.find(funcName);
		if (it != m_OnConnectCallBackFuncs.end())m_OnConnectCallBackFuncs.erase(it);
	}

	void ZClient::RemoveRecvCallBack(const std::string & funcName)
	{
		auto it = m_OnRecvCallBackFuncs.find(funcName);
		if (it != m_OnRecvCallBackFuncs.end())m_OnRecvCallBackFuncs.erase(it);
	}

	void ZClient::RemoveDisConnectCallBack(const std::string & funcName)
	{
		auto it = m_OnDisConnectCallBackFuncs.find(funcName);
		if (it != m_OnDisConnectCallBackFuncs.end())m_OnDisConnectCallBackFuncs.erase(it);
	}


}
}
