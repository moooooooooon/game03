//===============================================================
//   ゲーム3Dモデルクラス
// 
//===============================================================
#ifndef ZGameModel_h
#define ZGameModel_h

#include "Animation/ZKeyframeAnimation.h"
#include "Physics/ZBullet_ObjectBlueprint.h"

namespace EzLib
{

	//=======================================================================
	//   剛体データ ZBP_RigidBody継承  参照:ZBullet_ObjectBlueptint.h
	//=======================================================================
	struct ZGM_RigidBodyData : public ZBP_RigidBody
	{
	public:
		enum class CalcType : uint8
		{
			Static,
			Dynamic,
			DynamicAndBoneMerges
		};

	public:
		ZString RigidBodyName;								// 剛体名
		int32 BoneIndex = 0;								// 関連ボーンIndex - 関連なしの場合は-1
		CalcType PhysicsCalcType = CalcType::Static;		// 剛体の物理演算 - 0:ボーン追従(static) 1:物理演算(dynamic) 2:物理演算 + Bone位置合わせ

		// Rotate, Translateから行列を返す
		ZMatrix GetMatrix() const;
		

	};

	//=======================================================================
	//   ジョイントデータ ZBP_Joint継承  参照:ZBullet_ObjectBlueptint.h
	//=======================================================================
	struct ZGM_JointData : public ZBP_Joint
	{
		ZString JointName;								// ジョイント名	
		int RigidBodyAIndex = -1;							// 関連剛体AのIndex - 関連なしの場合は-1
		int RigidBodyBIndex = -1;							// 関連剛体BのIndex - 関連なしの場合は-1
		
		ZVec3 SpringMoveDamping = ZVec3(1, 1, 1);			// バネ減衰-移動(x,y,z)
		ZVec3 SpringRotationDamping = ZVec3(1, 1, 1);		// バネ減衰-回転(x,y,z)

		float CFM = 0.0f;									// 拘束の強さ
		float ERP = 0.2f;									// 計算誤差を減少させる

		// Rotate, Translateから行列を返す
		ZMatrix GetMatrix() const;
		

	};

	//=================================================================================
	//   GameModel用の物理演算設定をまとめたもの
	//=================================================================================
	class ZPhysicsRigidBody;
	class ZPhysicsJoint_Base;
	class ZPhysicsWorld;
	
	struct ZGM_PhysicsDataSet
	{
		ZAVector<ZGM_RigidBodyData>	RigidBodyDataTbl;	// 剛体情報の配列
		ZAVector<ZGM_JointData>		JointDataTbl;		// ジョイント情報の配列
	};


	//=================================================================================
	//   ゲームに適した多機能モデルクラス
	//  
	//  複数のモデルデータ・ボーン階層構造・アニメーションなどを保持するクラス	\n	
	//  スキンメッシュ・スタティックメッシュ両方扱えます						\n
	// 	[主な機能]																\n
	//    ・複数のメッシュデータ(複数のZSingleModel)							\n
	//    ・ボーンデータ														\n
	//    　※スタティックメッシュでも最低１つはボーンができる(Rootボーン)		\n
	//        ただしこのボーンは動かしても全く頂点には影響しない				\n
	//    ・アニメーションデータ												\n
	// 																			\n
	//   ボーン操作やアニメーションを行いたい場合は、ZBoneControllerクラスにセットして使用する。		\n
	// 
	//  @ingroup Graphics_Model_Important
	//=================================================================================
	class ZGameModel : public ZIResource
	{
	public:
		// ZGameModel用ボーンノードクラス
		class BoneNode;

	public:
		//=================================================================================
		// 情報取得
		//=================================================================================

		//   ボーン参照用配列取得
		ZAVector<ZSP<BoneNode>>&			GetBoneTree() { return m_BoneTree; }

		//   ルートボーン取得
		ZSP<BoneNode>						GetRoot() { return (m_BoneTree.size() == 0 ? nullptr : m_BoneTree[0]); }

		//   メッシュ配列取得(全て)
		ZAVector<ZSP<ZSingleModel>>&		GetModelTbl() { return m_ModelTbl; }

		//   メッシュ配列取得(スキンメッシュのみ)
		ZAVector<ZSP<ZSingleModel>>&		GetModelTbl_Skin() { return m_RefModelTbl_SkinOnly; }

		//   メッシュ配列取得(スタティックメッシュのみ)
		ZAVector<ZSP<ZSingleModel>>&		GetModelTbl_Static() { return m_RefModelTbl_StaticOnly; }

		//   アニメーションリストを取得
		ZAVector<ZSP<ZAnimationSet>>&		GetAnimeList() { return m_AnimeList; }

		//   物理演算データセットのリストを取得
		ZAVector<ZGM_PhysicsDataSet>&		GetPhysicsDataSetList() { return m_PhysicsDataSetList; }

		//   ファイルまでのパス取得
		const ZString&						GetFilePath() const { return m_FilePath; }

		//   ファイル名取得
		const ZString&						GetFileName() const { return m_FileName; }

		//   パスを含めたファイル名取得
		ZString								GetFullFileName() const { return m_FilePath + m_FileName; }

		//======================================================
		// 境界データ取得系
		//======================================================

		//   全メッシュのAABBの中心座標を取得
		const ZVec3&	GetAABB_Center() const { return m_AABB_Center; }

		//   全メッシュのAABBのハーフサイズを取得
		const ZVec3&	GetAABB_HalfSize() const { return m_AABB_HalfSize; }

		//   全メッシュの境界球のセンター座標(ローカル)を取得
		const ZVec3&	GetBSCenter() const { return m_vBSCenter; }

		//   全メッシュの境界球の半径を取得
		float			GetBSRadius() const { return m_fBSRadius; }

		//   全メッシュを覆う、AABBとバウンディングスフィアを計算
		void			CalcBoundingData();

		//------------------------------------------------------
		//   メッシュ読み込み(スタティックメッシュ、スキンメッシュ両対応)
		// 	対応形式：Xed
		//  	fileName	… ファイル名
		//    bakeCurve	… 曲線補間系のアニメキーは、全て線形補間として変換・追加する(XED用 処理が軽くなります)
		//------------------------------------------------------
		bool LoadMesh(const ZString& fileName, bool bakeCurve = true);

	private:
		//------------------------------------------------------
		//   XED(独自形式)読み込み
		// 
		//  XEDファイルはKAMADA SkinMesh Editorから出力できるモデル形式です		\n
		//  "//"から始まるアニメ名のアニメは無視されます	\n
		// 
		//    bakeCurve	… 曲線補間系のアニメキーは、全て線形補間として変換・追加する(処理が軽くなります)
		//------------------------------------------------------
		bool LoadXEDFile(const ZString& fileName, bool bakeCurve = true);

		bool LoadPMXFile(const ZString& fileName);

	public:
		//------------------------------------------------------
		//   解放
		//------------------------------------------------------
		void Release();

		//=================================================================================
		// その他
		//=================================================================================

		//------------------------------------------------------
		//   ボーン名検索 見つからない場合はnullptr
		//  	Name	… ボーン名
		//  @return ボーン発見:ボーンデータのアドレス  ボーン無し:nullptr
		//------------------------------------------------------
		ZSP<BoneNode> SearchBone(const ZString& Name)
		{
			for (UINT i = 0; i < m_BoneTree.size(); i++)
			{
				// 文字列判定
				if (m_BoneTree[i]->BoneName == Name)
				{
					return m_BoneTree[i];
				}
			}
			return nullptr;
		}

		//------------------------------------------------------
		//   ボーン名からインデックス番号検索 見つからない場合は-1
		//  	Name	… ボーン名
		//  @return ボーン発見:ボーン番号  ボーン無し:-1
		//------------------------------------------------------
		int SearchBoneIndex(const ZString& Name) const
		{
			for (int i = 0; i < (int)m_BoneTree.size(); i++)
			{
				// 文字列判定
				if (m_BoneTree[i]->BoneName == Name)
				{
					return i;
				}
			}
			return -1;
		}

		//------------------------------------------------------
		//   アニメーションが存在するか？
		//------------------------------------------------------
		bool IsAnimationexist()
		{
			return !m_AnimeList.empty();
		}

		//------------------------------------------------------
		//   アニメーション名からIndexを取得。ない場合は-1
		//  	AnimeName	… アニメ名
		//  @return 発見：アニメ番号　アニメ無し：-1
		//------------------------------------------------------
		int SearchAnimation(const ZString& AnimeName)
		{
			for (UINT i = 0; i < m_AnimeList.size(); i++)
			{
				if (m_AnimeList[i]->m_AnimeName == AnimeName)
				{
					return i;
				}
			}
			return -1;
		}

		//------------------------------------------------------
		//   アニメーションを全削除
		//------------------------------------------------------
		void DeleteAnimation()
		{
			m_AnimeList.clear();
			m_AnimeList.shrink_to_fit();
		}

		//------------------------------------------------------
		//   リソース情報
		//------------------------------------------------------
		virtual ZString GetTypeName() const override
		{
			ZString str = "ZGameModel : ";
			str += GetFullFileName();
			return str;
		}

		// 
		ZGameModel()
		{
		}
		// 
		~ZGameModel();


	private:
		ZString						m_FilePath;			// ファイルまでのパス
		ZString						m_FileName;			// ファイル名
		
		ZVec3							m_AABB_Center;		// 全てのメッシュのAABBのローカル中心座標
		ZVec3							m_AABB_HalfSize;	// 全てのメッシュのAABBのハーフサイズ
		ZVec3							m_vBSCenter;		// このメッシュの境界球ローカル座標
		float							m_fBSRadius = 0;	// このメッシュの境界球半径

		//---------------------------------------------------------------------------------
		// メッシュテーブル
		//---------------------------------------------------------------------------------
		ZAVector<ZSP<ZSingleModel>>	m_ModelTbl;						// 全てのメッシュへの参照が入っている配列

		ZAVector<ZSP<ZSingleModel>>	m_RefModelTbl_SkinOnly;			// m_ModelTblの中で、スキンメッシュのみのメッシュへの参照が入っている配列　CreateRefMeshTbl関数で作成される
		ZAVector<ZSP<ZSingleModel>>	m_RefModelTbl_StaticOnly;		// m_ModelTblの中で、スタティックメッシュのみのメッシュへの参照が入っている配列　CreateRefMeshTbl関数で作成される
		void CreateRefMeshTbl();		// m_ModelTblの内容から、スキンメッシュとスタティックメッシュの参照を分けたテーブル作成

		//---------------------------------------------------------------------------------
		//   ボーン参照用配列(ボーン配列)
		//  [0]がRoot ツリー構造であるが、1次元配列としてもアクセス可能。
		//---------------------------------------------------------------------------------
		ZAVector<ZSP<BoneNode>>			m_BoneTree;

		//---------------------------------------------------------------------------------
		// アニメーションリスト
		//---------------------------------------------------------------------------------
		ZAVector<ZSP<ZAnimationSet>>	m_AnimeList;			// アニメーションリスト

		//---------------------------------------------------------------------------------
		// 物理演算オブジェクトリスト
		//---------------------------------------------------------------------------------
		ZAVector<ZGM_PhysicsDataSet>	m_PhysicsDataSetList;	// KAMADA SkinMesh Editorでは0〜9の10個登録できるので、m_PhysicsTbl[0〜9]となる

	private:
		// コピー禁止用
		ZGameModel(const ZGameModel& src) {}
		void operator=(const ZGameModel& src) {}

	public:
		//=================================================================================
		//   ZGameModel用ボーンノード
		//  @ingroup Graphics_Model
		//=================================================================================
		class BoneNode
		{
		public:
			//=========================
			// ボーンデータ
			//=========================
			ZString					BoneName;		// ボーン名
			int							Level = -1;		// 階層 Rootが0
			ZMatrix					OffsetMat;		// オフセット行列　ローカル座標系の頂点を、ボーン座標系に変換する行列 つまりDefLocalMatの逆行列です
			DWORD						OffsetID = 0;	// 行列テーブルのインデックス番号

			bool					DeformAfterPhysics; // 物理ワールド更新後に処理するか

			ZMatrix					DefTransMat;	// デフォルト姿勢での変換行列　親ボーンからの相対行列
			ZMatrix					DefLocalMat;	// デフォルト姿勢でのローカル行列　モデル座標系での行列

			ZWP<BoneNode>				pMother;		// 親ボーンのアドレス
			ZAVector<ZSP<BoneNode>>	Child;			// 子ボーン配列

			//=========================
			// XED拡張データ
			//=========================
			#pragma region XED拡張データ
					//   ボーン表示レイヤー(Xed用)
			int						BoneLayers = 0;

			//   ボーンフラグ(Xed用)
			int						BoneFlag = 0;

			//   ボーンフラグ用定数(Xed用)
			enum
			{
				BF_ROTATE = 0x00000002,					// 回転可
				BF_MOVE = 0x00000004,					// 移動可
				BF_DISPBone = 0x00000008,				// 表示
				BF_OPERATE = 0x00000010,				// 操作可
				BF_IK = 0x00000020,						// IK有効

				BF_PROVIDELOCAL = 0x00000080,			// ローカル付与 付与対象 0: ユーザー変形地/IKリンク/多重付与 1: 親のローカル変化量
				BF_PROVIDEROTA = 0x00000100,			// 回転付与
				BF_PROVIDEMOVE = 0x00000200,			// 移動付与

				BF_DEFORM_AFTER_PHYSICS = 0x00001000,	// 物理後変形
				BF_DEFORM_OUTER_PARENT = 0x00002000,	// 外部親変形

				BF_DISPBoneLINE = 0x00010000,			// 親からのボーンライン表示ON
			};

			//   ボーンフラグ：回転有効？(Xed用)
			bool BF_IsRotate() const { return (BoneFlag & BF_ROTATE ? true : false); }
			//   ボーンフラグ：移動有効？(Xed用)
			bool BF_IsMove() const { return (BoneFlag & BF_MOVE ? true : false); }
			//   ボーンフラグ：ボーン表示有効？(Xed用)
			bool BF_IsDispBone() const { return (BoneFlag & BF_DISPBone ? true : false); }
			//   ボーンフラグ：IK有効？(Xed用)
			bool BF_IsIK() const { return (BoneFlag & BF_IK ? true : false); }
			//   ボーンフラグ：操作可能？(Xed用)
			bool BF_IsOperate() const { return (BoneFlag & BF_OPERATE ? true : false); }
			//   ボーンフラグ：親からのボーンライン表示有効？(Xed用)
			bool BF_IsDispBoneLine() const { return (BoneFlag & BF_DISPBoneLINE ? true : false); }
			//   ボーンフラグ：回転付与有効？(Xed用)
			bool BF_IsProvideRotate() const { return (BoneFlag & BF_PROVIDEROTA ? true : false); }
			//   ボーンフラグ：移動付与有効？(Xed用)
			bool BF_IsProvideMove() const { return (BoneFlag & BF_PROVIDEMOVE ? true : false); }
			//   ボーンフラグ：物理後変形？
			bool BF_IsDeformAfterPhysics()const { return (BoneFlag & BF_DEFORM_AFTER_PHYSICS ? true : false); }
			

			// IKデータ(Xed用)
			struct IKData
			{
				int		boneIdx = -1;			// ターゲットボーンIndex
				int		LoopCnt = 0;			// 計算ループカウント
				float	ikLimitedAng = 0;		// 単位角
				BYTE	flags = 0;				// 未使用

				struct LinkData
				{
					int			boneIdx = -1;		// リンクボーンIndex
					bool		bLimitAng = false;	// 角度制限有効？
					ZVec3		minLimitAng;		// 角度制限：最低角度
					ZVec3		maxLimitAng;		// 角度制限：最大角度
				};

				ZAVector<LinkData>	LinkList;
			};
			IKData					IK;	// IKデータ(Xed用)

			#pragma endregion

		};
	};


}

#include "Tools/XEDLoader/ZXEDLoader.h"


#endif
