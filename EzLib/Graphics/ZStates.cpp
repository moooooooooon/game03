#include "PCH/pch.h"

using namespace EzLib;


//===================================================
// ブレンドステート
//===================================================
void ZBlendState::SetAll_GetState()
{
	Safe_Release(m_pBlendState);

	float oldFactor[4];
	UINT oldMask;
	ZDx.GetDevContext()->OMGetBlendState(&m_pBlendState, oldFactor, &oldMask);

	m_pBlendState->GetDesc(&m_Desc);
}

void EzLib::ZBlendState::Set_Disable(int RT_No)
{
	Safe_Release(m_pBlendState);

	if (RT_No < 0)
	{
		for (int i = 0; i < 8; i++)
		{
			Set_Disable(i);
		}
	}
	else
	{
		m_Desc.RenderTarget[RT_No].BlendEnable = FALSE;
	}
}

void ZBlendState::Set_NoAlpha(int RT_No, bool bOverrideAlpha)
{
	Safe_Release(m_pBlendState);

	if (RT_No < 0)
	{
		for (int i = 0; i < 8; i++)
		{
			Set_NoAlpha(i, bOverrideAlpha);
		}
	}
	else
	{
		m_Desc.RenderTarget[RT_No].BlendEnable = TRUE;

		m_Desc.RenderTarget[RT_No].BlendOp = D3D11_BLEND_OP_ADD;
		m_Desc.RenderTarget[RT_No].SrcBlend = D3D11_BLEND_ONE;
		m_Desc.RenderTarget[RT_No].DestBlend = D3D11_BLEND_ZERO;

		m_Desc.RenderTarget[RT_No].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		m_Desc.RenderTarget[RT_No].SrcBlendAlpha = D3D11_BLEND_ONE;
		if (bOverrideAlpha)
		{	// 書き込み側のアルファを上書きする
			m_Desc.RenderTarget[RT_No].DestBlendAlpha = D3D11_BLEND_ZERO;
		}
		else
		{				// 書き込み側のアルファと合成する
			m_Desc.RenderTarget[RT_No].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
		}

		//		m_Desc.RenderTarget[RT_No].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	}
}

void ZBlendState::Set_Alpha(int RT_No)
{
	Safe_Release(m_pBlendState);

	if (RT_No < 0)
	{
		for (int i = 0; i < 8; i++)
		{
			Set_Alpha(i);
		}
	}
	else
	{
		m_Desc.RenderTarget[RT_No].BlendEnable = TRUE;

		m_Desc.RenderTarget[RT_No].BlendOp = D3D11_BLEND_OP_ADD;
		m_Desc.RenderTarget[RT_No].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		m_Desc.RenderTarget[RT_No].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

		m_Desc.RenderTarget[RT_No].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		m_Desc.RenderTarget[RT_No].SrcBlendAlpha = D3D11_BLEND_ONE;
		m_Desc.RenderTarget[RT_No].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;

		//		m_Desc.RenderTarget[RT_No].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	}
}
void ZBlendState::Set_Add(int RT_No)
{
	Safe_Release(m_pBlendState);

	if (RT_No < 0)
	{
		for (int i = 0; i < 8; i++)
		{
			Set_Add(i);
		}
	}
	else
	{
		m_Desc.RenderTarget[RT_No].BlendEnable = TRUE;

		m_Desc.RenderTarget[RT_No].BlendOp = D3D11_BLEND_OP_ADD;
		m_Desc.RenderTarget[RT_No].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		m_Desc.RenderTarget[RT_No].DestBlend = D3D11_BLEND_ONE;

		m_Desc.RenderTarget[RT_No].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		m_Desc.RenderTarget[RT_No].SrcBlendAlpha = D3D11_BLEND_ONE;
		m_Desc.RenderTarget[RT_No].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;

		//		m_Desc.RenderTarget[RT_No].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	}
}

void ZBlendState::Set_WriteMask(int RT_No, bool r, bool g, bool b, bool a)
{
	Safe_Release(m_pBlendState);

	if (RT_No < 0)
	{
		for (int i = 0; i < 8; i++)
		{
			Set_WriteMask(i, r, g, b, a);
		}
	}
	else
	{
		m_Desc.RenderTarget[RT_No].RenderTargetWriteMask = 0;
		if (r)m_Desc.RenderTarget[RT_No].RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_RED;
		if (g)m_Desc.RenderTarget[RT_No].RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_GREEN;
		if (b)m_Desc.RenderTarget[RT_No].RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_BLUE;
		if (a)m_Desc.RenderTarget[RT_No].RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_ALPHA;
	}
}

bool ZBlendState::Create()
{
	Safe_Release(m_pBlendState);

	HRESULT hr = ZDx.GetDev()->CreateBlendState(&m_Desc, &m_pBlendState);
	if (FAILED(hr))return false;
	return true;
}

void ZBlendState::SetState()
{

	// オブジェクトが未作成の場合は、作成する
	if (m_pBlendState == nullptr)
	{
		if (Create() == false)return;
	}

	ZDx.GetDevContext()->OMSetBlendState(m_pBlendState, m_BlendFactor, m_SampleMask);
}

EzLib::ZBlendState::ZBlendState() : m_pBlendState(0)
{
	ZeroMemory(&m_Desc, sizeof(D3D11_BLEND_DESC));
	Set_Alpha(0);
	ZeroMemory(m_BlendFactor, sizeof(m_BlendFactor));
	m_SampleMask = 0xFFFFFFFF;
	Set_WriteMask(-1);
}

//================================================
// 深度ステンシルステートクラス
//================================================
void ZDepthStencilState::SetAll_GetState()
{
	Safe_Release(m_pDepthStencilState);

	UINT StencilRef;
	ZDx.GetDevContext()->OMGetDepthStencilState(&m_pDepthStencilState, &StencilRef);

	m_pDepthStencilState->GetDesc(&m_Desc);
}

void ZDepthStencilState::SetAll_Standard()
{
	Safe_Release(m_pDepthStencilState);
	ZeroMemory(&m_Desc, sizeof(m_Desc));

	m_Desc.DepthEnable = TRUE;							// 深度テストを使用する
	m_Desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	m_Desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
}

// 現在のDescの内容を元に作成
bool ZDepthStencilState::Create()
{
	Safe_Release(m_pDepthStencilState);

	HRESULT hr = ZDx.GetDev()->CreateDepthStencilState(&m_Desc, &m_pDepthStencilState);
	if (FAILED(hr)) return false;
	return true;
}

void ZDepthStencilState::SetState()
{
	// オブジェクトが未作成の場合は、作成する
	if (m_pDepthStencilState == nullptr)
	{
		if (Create() == false)return;
	}

	ZDx.GetDevContext()->OMSetDepthStencilState(m_pDepthStencilState, m_StencilRef);
}


#include "PCH/pch.h"
#include "ZStates.h"

using namespace EzLib;

//===================================================
// サンプラーステート
//===================================================
void ZSamplerState::SetAll_GetStatePS(UINT StartSlot)
{
	Safe_Release(m_pSampler);
	ZDx.GetDevContext()->PSGetSamplers(StartSlot, 1, &m_pSampler);
	m_pSampler->GetDesc(&m_Desc);
}
void ZSamplerState::SetAll_GetStateVS(UINT StartSlot)
{
	Safe_Release(m_pSampler);
	ZDx.GetDevContext()->VSGetSamplers(StartSlot, 1, &m_pSampler);
	m_pSampler->GetDesc(&m_Desc);
}
void ZSamplerState::SetAll_GetStateGS(UINT StartSlot)
{

	Safe_Release(m_pSampler);
	ZDx.GetDevContext()->GSGetSamplers(StartSlot, 1, &m_pSampler);
	m_pSampler->GetDesc(&m_Desc);
}
void ZSamplerState::SetAll_GetStateCS(UINT StartSlot)
{
	Safe_Release(m_pSampler);
	ZDx.GetDevContext()->CSGetSamplers(StartSlot, 1, &m_pSampler);
	m_pSampler->GetDesc(&m_Desc);
}

void ZSamplerState::SetAll_Standard()
{
	Safe_Release(m_pSampler);
	ZeroMemory(&m_Desc, sizeof(m_Desc));
	m_Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	m_Desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	m_Desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	m_Desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	//	m_Desc.MipLODBias = -1.0f;
	m_Desc.MipLODBias = 0;
	m_Desc.MaxAnisotropy = 16;
	m_Desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	m_Desc.BorderColor[0] = m_Desc.BorderColor[1] = m_Desc.BorderColor[2] = m_Desc.BorderColor[3] = 0;
	m_Desc.MinLOD = -D3D11_FLOAT32_MAX;
	m_Desc.MaxLOD = D3D11_FLOAT32_MAX;
}

void ZSamplerState::Set_Wrap()
{
	Safe_Release(m_pSampler);
	m_Desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	m_Desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	m_Desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
}

void ZSamplerState::Set_Clamp()
{
	Safe_Release(m_pSampler);
	m_Desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	m_Desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	m_Desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
}

void EzLib::ZSamplerState::Set_Mirror()
{
	Safe_Release(m_pSampler);
	m_Desc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	m_Desc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	m_Desc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
}

void ZSamplerState::Set_Border(const ZVec4& borderColor)
{
	Safe_Release(m_pSampler);
	m_Desc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	m_Desc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	m_Desc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	m_Desc.BorderColor[0] = borderColor.x;
	m_Desc.BorderColor[1] = borderColor.y;
	m_Desc.BorderColor[2] = borderColor.z;
	m_Desc.BorderColor[3] = borderColor.w;
}

void ZSamplerState::Set_Filter(D3D11_FILTER filter, int maxAnisotropy)
{
	Safe_Release(m_pSampler);
	m_Desc.Filter = filter;
	m_Desc.MaxAnisotropy = maxAnisotropy;
}

void ZSamplerState::Set_Filter_Linear()
{
	Safe_Release(m_pSampler);
	m_Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
}

void ZSamplerState::Set_Filter_Point()
{
	Safe_Release(m_pSampler);
	m_Desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
}

void ZSamplerState::Set_Filter_Anisotropy(UINT MaxAnisotropy)
{
	Safe_Release(m_pSampler);
	m_Desc.Filter = D3D11_FILTER_ANISOTROPIC;
	m_Desc.MaxAnisotropy = MaxAnisotropy;
}

void ZSamplerState::Set_ComparisonMode(D3D11_COMPARISON_FUNC compFunc)
{
	Safe_Release(m_pSampler);
	m_Desc.ComparisonFunc = compFunc;
	m_Desc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
}

bool ZSamplerState::Create()
{
	Safe_Release(m_pSampler);

	HRESULT hr = ZDx.GetDev()->CreateSamplerState(&m_Desc, &m_pSampler);
	if (FAILED(hr))return false;
	return true;
}

void ZSamplerState::SetStatePS(UINT StartSlot)
{
	// オブジェクトが未作成の場合は、作成する
	if (m_pSampler == nullptr)
	{
		if (Create() == false)return;
	}

	ZDx.GetDevContext()->PSSetSamplers(StartSlot, 1, &m_pSampler);
}

void ZSamplerState::SetStateVS(UINT StartSlot)
{
	// オブジェクトが未作成の場合は、作成する
	if (m_pSampler == nullptr)
	{
		if (Create() == false)return;
	}

	ZDx.GetDevContext()->VSSetSamplers(StartSlot, 1, &m_pSampler);
}

void ZSamplerState::SetStateGS(UINT StartSlot)
{
	// オブジェクトが未作成の場合は、作成する
	if (m_pSampler == nullptr)
	{
		if (Create() == false)return;
	}

	ZDx.GetDevContext()->GSSetSamplers(StartSlot, 1, &m_pSampler);
}

void ZSamplerState::SetStateCS(UINT StartSlot)
{
	// オブジェクトが未作成の場合は、作成する
	if (m_pSampler == nullptr)
	{
		if (Create() == false)return;
	}

	ZDx.GetDevContext()->CSSetSamplers(StartSlot, 1, &m_pSampler);
}

//================================================
// ラスタライザステートクラス
//================================================
void ZRasterizeState::SetAll_GetState()
{
	Safe_Release(m_pRasterize);
	ZDx.GetDevContext()->RSGetState(&m_pRasterize);
	m_pRasterize->GetDesc(&m_Desc);
}

void ZRasterizeState::SetAll_Standard()
{
	Safe_Release(m_pRasterize);
	ZeroMemory(&m_Desc, sizeof(m_Desc));
	m_Desc.FillMode = D3D11_FILL_SOLID;    // ポリゴン面描画
	m_Desc.CullMode = D3D11_CULL_BACK;     // 裏面を描画しない
	m_Desc.FrontCounterClockwise = FALSE;   // 時計回りを表面
	m_Desc.DepthBias = 0;
	m_Desc.DepthBiasClamp = 0;
	m_Desc.SlopeScaledDepthBias = 0;
	m_Desc.DepthClipEnable = TRUE;
	m_Desc.ScissorEnable = FALSE;          // シザー矩形無効

	// スワップチェーンのマルチサンプリングの設定にあわせる
	DXGI_SWAP_CHAIN_DESC swapDesc;
	ZDx.GetSwapChain()->GetDesc(&swapDesc);
	if (swapDesc.SampleDesc.Count > 1)
	{
		m_Desc.MultisampleEnable = TRUE;
	}
	else
	{
		m_Desc.MultisampleEnable = FALSE;
	}

	m_Desc.AntialiasedLineEnable = FALSE;

}

// 現在のDescの内容を元にサンプラ作成
bool ZRasterizeState::Create()
{
	Safe_Release(m_pRasterize);

	HRESULT hr = ZDx.GetDev()->CreateRasterizerState(&m_Desc, &m_pRasterize);
	if (FAILED(hr))return false;
	return true;
}

void ZRasterizeState::SetState()
{
	// オブジェクトが未作成の場合は、作成する
	if (m_pRasterize == nullptr)
	{
		if (Create() == false)return;
	}
	ZDx.GetDevContext()->RSSetState(m_pRasterize);
}


//===================================================
// レンダーターゲット、深度ステンシルデータ
//===================================================
void ZRenderTargets::RT(int rtNo, ZTexture& rtTex)
{
	Safe_Release(m_pRTs[rtNo]);
	if (rtTex.GetRTTex())
	{
		m_pRTs[rtNo] = rtTex.GetRTTex();	// 登録
		if (m_pRTs[rtNo])m_pRTs[rtNo]->AddRef();				// 参照カウンタ増加

		// rtNoが0のやつのビューポートを使用
		if (rtNo == 0)
		{
			m_VP.Width = (float)rtTex.GetInfo().Width;
			m_VP.Height = (float)rtTex.GetInfo().Height;
			m_VP.MinDepth = 0.0f;
			m_VP.MaxDepth = 1.0f;
			m_VP.TopLeftX = 0;
			m_VP.TopLeftY = 0;
		}
	}
}

void ZRenderTargets::RT(int rtNo, ID3D11RenderTargetView* rtView)	// View指定Ver
{
	Safe_Release(m_pRTs[rtNo]);
	if (rtView)
	{
		m_pRTs[rtNo] = rtView;	// 登録
		if (m_pRTs[rtNo])m_pRTs[rtNo]->AddRef();				// 参照カウンタ増加

		// rtNoが0のやつのビューポートを使用
		if (rtNo == 0)
		{
			ID3D11Resource* res;
			rtView->GetResource(&res);
			ID3D11Texture2D* tex2D;
			HRESULT hr = res->QueryInterface(IID_ID3D11Texture2D, (void **)&tex2D);
			res->Release();
			if (SUCCEEDED(hr))
			{
				D3D11_TEXTURE2D_DESC desc;
				tex2D->GetDesc(&desc);
				m_VP.Width = (float)desc.Width;
				m_VP.Height = (float)desc.Height;
				m_VP.MinDepth = 0.0f;
				m_VP.MaxDepth = 1.0f;
				m_VP.TopLeftX = 0;
				m_VP.TopLeftY = 0;

				tex2D->Release();
			}
		}
	}

}

void ZRenderTargets::RT(int rtNo, ZRenderTargets& rtdata)
{
	RT(rtNo, rtdata.m_pRTs[rtNo]);
}

bool ZRenderTargets::GetRTDesc(int rtNo, D3D11_TEXTURE2D_DESC& outDesc)
{
	if (m_pRTs[rtNo] == nullptr)return false;

	ID3D11Resource* res;
	m_pRTs[rtNo]->GetResource(&res);
	ID3D11Texture2D* tex2D;
	HRESULT hr = res->QueryInterface(IID_ID3D11Texture2D, (void **)&tex2D);
	res->Release();
	if (SUCCEEDED(hr))
	{
		tex2D->GetDesc(&outDesc);
		tex2D->Release();
		return true;
	}
	return false;
}

void ZRenderTargets::Depth(ZTexture& depthTex)
{
	Safe_Release(m_pDepth);
	if (depthTex.GetDepthTex())
	{
		m_pDepth = depthTex.GetDepthTex();	// 登録
		if (m_pDepth)m_pDepth->AddRef();					// 参照カウンタ増加
	}
}

void ZRenderTargets::Depth(ID3D11DepthStencilView* depthView)	// View指定Ver
{
	Safe_Release(m_pDepth);
	if (depthView)
	{
		m_pDepth = depthView;	// 登録
		if (m_pDepth)m_pDepth->AddRef();					// 参照カウンタ増加
	}

}

void ZRenderTargets::Depth(ZRenderTargets& rtdata)
{
	Depth(rtdata.m_pDepth);
}

void ZRenderTargets::GetNowAll()
{
	Release();

	ZDx.GetDevContext()->OMGetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, m_pRTs, &m_pDepth);

	UINT pNumVierports = 1;
	ZDx.GetDevContext()->RSGetViewports(&pNumVierports, &m_VP);
}

void ZRenderTargets::GetNowRTs()
{

	for (int i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
	{
		Safe_Release(m_pRTs[i]);
	}

	ZDx.GetDevContext()->OMGetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, m_pRTs, nullptr);

	UINT pNumVierports = 1;
	ZDx.GetDevContext()->RSGetViewports(&pNumVierports, &m_VP);
}

void ZRenderTargets::GetNowDepth()
{
	Safe_Release(m_pDepth);
	ZDx.GetDevContext()->OMGetRenderTargets(0, nullptr, &m_pDepth);
}

void ZRenderTargets::GetNowTop()
{
	Release();

	ZDx.GetDevContext()->OMGetRenderTargets(1, m_pRTs, &m_pDepth);

	UINT pNumVierports = 1;
	ZDx.GetDevContext()->RSGetViewports(&pNumVierports, &m_VP);
}

void ZRenderTargets::SetToDevice(int setRTNum)
{
	if (setRTNum < 0)setRTNum = D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT;

	// RTとDepthをセット
	ZDx.GetDevContext()->OMSetRenderTargets(setRTNum, m_pRTs, m_pDepth);
	// ビューポートも切り替える
	ZDx.GetDevContext()->RSSetViewports(1, &m_VP);
}

void ZRenderTargets::Release()
{
	for (int i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
	{
		Safe_Release(m_pRTs[i]);
	}
	Safe_Release(m_pDepth);
}

void ZRenderTargets::operator = (const ZRenderTargets& src)
{
	// 現在の物を解放
	Release();

	// RTの参照をコピーし参照カウンタ増加
	for (int i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
	{
		m_pRTs[i] = src.m_pRTs[i];
		if (m_pRTs[i])m_pRTs[i]->AddRef();
	}

	// Depthの参照をコピーし参照カウンタ増加
	m_pDepth = src.m_pDepth;
	if (m_pDepth)m_pDepth->AddRef();

	// ビューポートコピー
	m_VP = src.m_VP;
}
