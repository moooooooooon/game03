//===============================================================
//   Direct3D関係のクラス
// 
//===============================================================

#ifndef ZDirectXGraphics_h
#define ZDirectXGraphics_h

namespace EzLib
{

	class ZResourceStorage;
	
	//================================================
	//   Direct3Dクラス
	// 
	//  Direct3D11の基本的な機能をまとめたクラス
	// 
	//  @ingroup Graphics_Important
	//================================================
	class ZDirectXGraphics
	{
	public:
		ZDirectXGraphics();
		~ZDirectXGraphics();

		//==============================================================
		// 取得
		//==============================================================
		//   デバイス取得。リソースの作成およびディスプレイ アダプターの機能の列挙などで使用。
		//  ※参照カウンタは加算していません
		ID3D11Device*			GetDev() { return m_pDevice; }

		//   デバイスコンテキスト取得。描画やステートの変更などで使用。
		//  ※参照カウンタは加算していません
		ID3D11DeviceContext*	GetDevContext() { return m_pDeviceContext; }

		//   スワップチェイン取得。
		//  ※参照カウンタは加算していません
		IDXGISwapChain*			GetSwapChain() { return m_pGISwapChain; }

		//   ファクトリ取得
		IDXGIFactory*			GetFactory() { return m_pGIFactory; }

		//   スワップチェインディスク取得
		const DXGI_SWAP_CHAIN_DESC& GetSwapChainDesc() { return m_DXGISwapChainDesc; }

		//   X解像度取得
		int						GetRezoW() { return (int)m_DXGISwapChainDesc.BufferDesc.Width; }

		//   Y解像度取得
		int						GetRezoH() { return (int)m_DXGISwapChainDesc.BufferDesc.Height; }

		//   バックバッファ取得。
		ZSP<ZTexture>			GetBackBuffer() { return m_texBackBuffer; }

		//   Zバッファ取得。
		ZSP<ZTexture>		GetDepthStencil() { return m_texDepthStencil; }

		//   1x1 白テクスチャ取得
		ZSP<ZTexture>			GetWhiteTex() { return m_texWhite; }

		//   1x1 黒テクスチャ取得
		ZSP<ZTexture>			GetBlackTex() { return m_texBlack; }

		//   1x1 Z方向法線マップ取得
		ZSP<ZTexture>			GetNormalTex() { return m_texNormal; }

		//   通常トゥーンテクスチャ取得
		ZSP<ZTexture>			GetToonTex() { return m_texToon; }

		//   リソース倉庫取得
		ZResourceStorage*		GetResStg() { return m_pResStg; }


		//   DirectX9のデバイスを取得。
		//  ※参照カウンタは加算していません
	//	LPDIRECT3DDEVICE9		GetD3D9Dev(){ return m_pD3D9Device; }

		//   アンチエイリアス用定数
		enum MSAA
		{
			MSAA_NONE = 1,
			MSAA_x2 = 2,
			MSAA_x4 = 4,
			MSAA_x8 = 8,
		};

		//==============================================================
		// 初期化・解放
		//==============================================================

		//--------------------------------------------------------------------------
		//   初期設定
		//  Direct3D11を初期化する
		//  	hWnd		… ウィンドウのハンドル
		//  	w			… 解像度 X
		//  	h			… 解像度 Y
		//  	resStg		… 登録するリソース管理クラス　EzLib内でテクスチャなどのリソースを読み込む際には、こいつが使用される
		//  	msaa		… アンチエイリアスを使うか　テクスチャの形式とかいろいろ面倒になるので使用しないこと推奨　AAは別途シェーダでやるほうがいいと思う
		//  @param[out]	errMsg		… エラーメッセージが返る
		//  @return 成功 … true
		//--------------------------------------------------------------------------
		bool Init(HWND hWnd, int w, int h, ZResourceStorage* resStg, MSAA msaa = MSAA_NONE, ZString* errMsg = nullptr);

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();


		//--------------------------------------------------------------------------
		//   標準的なステート設定をする
		//--------------------------------------------------------------------------
		void SetDefaultStates();

		//--------------------------------------------------------------------------
		//   フルスクリーン切り替え
		//  	bFullScreen	… true:フルスクリーン　false:ウィンドウ
		//--------------------------------------------------------------------------
		void SetFullScreen(bool bFullScreen)
		{
			m_pGISwapChain->SetFullscreenState((BOOL)bFullScreen, NULL);
		}

		//--------------------------------------------------------------------------
		//   解像度変更
		//  	w		… 解像度 X
		//  	h		… 解像度 Y
		//--------------------------------------------------------------------------
		bool ResizeResolution(size_t w, size_t h);

		//--------------------------------------------------------------------------
		//   ビューポート設定
		// 
		//  w,hに0を指定すると、バックバッファの大きさで設定される。
		// 
		//  	w … 幅
		//  	h … 高さ
		//--------------------------------------------------------------------------
		void SetViewport(float w = 0, float h = 0);


		//---------------------------------------------------------------------------
		//   VSync設定
		//---------------------------------------------------------------------------
		void SetVSync(bool flg);

		//==============================================================
		// ラスタライザステート関係
		//==============================================================

		//   カリングモードの変更
		//  ※ラスタライザステートのカリング以外の項目は基本的なものに設定されます
		//  	mode … カリングモード
		// 						- D3D11_CULL_NONE … 両面描画
		// 						- D3D11_CULL_FRONT … 裏面のみ表示
		// 						- D3D11_CULL_BACK … 表面のみ表示
		void RS_CullMode(D3D11_CULL_MODE mode);

		//==============================================================
		// 深度ステンシルステート関係
		//  詳細はZDepthStencilStateクラスを使用してください
		//==============================================================

		//   Z判定やZ書き込みモードのON/OFF
		//  ※深度ステンシルステートのその他の項目は基本的なものに設定されます
		void DS_ZSetting(bool ZEnable, bool ZWriteEnable);

		//==============================================================
		// 描画関係
		//==============================================================
		//   2D四角形描画
		//  x,y,w,hはピクセル座標系で指定  ※頂点形式は、座標とUV(ZVertex_Pos_UV)で描画される
		void DrawQuad(int x, int y, int w, int h);

		void Begin(ZVec4& clearColor, bool isDepth, bool isStencl, float depth = 1.0f, UINT stencil = 0);

		void End();

		void RemoveTextureVS(int slotNo = -1);		// 頂点シェーダの指定スロットのテクスチャを解除 -1で全てのスロット解除
		void RemoveTexturePS(int slotNo = -1);		// ピクセルシェーダの指定スロットのテクスチャを解除 -1で全てのスロット解除
		void RemoveTextureGS(int slotNo = -1);		// ジオメトリシェーダの指定スロットのテクスチャを解除 -1で全てのスロット解除
		void RemoveTextureCS(int slotNo = -1);		// コンピュートシェーダの指定スロットのテクスチャを解除 -1で全てのスロット解除
		void RemoveUavCS(int slotNo);			// コンピュートシェーダの指定スロットのUAテクスチャを解除

		//   バックバッファを画面に表示
		HRESULT Present(UINT SyncInterval, UINT Flags);

	private:
		ID3D11Device* m_pDevice;					// DirectX11の中心になるクラス。全体の管理とバッファ、シェーダ、テクスチャなどのリソース作成などを行う。DX9とは違って、このクラスは描画関係のメンバ関数を持たない。
		ID3D11DeviceContext* m_pDeviceContext;		// 描画処理を行うクラス。
													// 内部的には、レンダリングコマンドと呼ばれるバイナリデータを作成し、GPUに送る。GPUがレンダリングコマンドを解析することで実際の描画処理が行われる。
													// 複数作成することができるため、レンダリングコマンドを事前に作成したり、スレッドでの並列処理が可能？

		IDXGIAdapter* m_pGIAdapter;					// ディスプレイ サブシステム(1つまたは複数のGPU、DAC、およびビデオ メモリー)。
		IDXGIFactory* m_pGIFactory;					// フルスクリーン切り替えなどで使用。
		IDXGISwapChain* m_pGISwapChain;				// ウインドウへの表示やダブルバッファリングなどを行うクラス。マルチサンプリング、リフレッシュレートなどの設定もできるみたい。
		DXGI_SWAP_CHAIN_DESC m_DXGISwapChainDesc;	// スワップチェーンの設定データ
		ZSP<ZTexture> m_texBackBuffer;				// バックバッファ
		ZSP<ZTexture> m_texDepthStencil;			// Zバッファ

		ZSP<ZTexture> m_texWhite;					// 1x1 白テクスチャ マテリアルのテクスチャが無い時用
		ZSP<ZTexture> m_texBlack;					// 1x1 黒テクスチャ フェードイン・アウト用
		ZSP<ZTexture> m_texNormal;					// 1x1 Z方向法線マップ 法線マップが無い時用
		ZSP<ZTexture> m_texToon;					// data\Texture\toon.png
		
		ZResourceStorage* m_pResStg;				// リソース倉庫への参照

		ZSamplerState m_ssLinear_Wrap;				// Linear補間 Wrap 
		ZSamplerState m_ssLinear_Clamp;				// Linear補間 Clamp 

		ID3D11ShaderResourceView* m_srvReset[D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT];	// 全てnullptr　Shader Resource Viewのリセット用

		// DrawQuad用
		ZPolygon m_polyQuad;

		bool m_UseVSync;
	};

	


}
#endif
