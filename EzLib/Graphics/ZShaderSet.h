#ifndef __ZSHADER_SET_H__
#define __ZSHADER_SET_H__

namespace EzLib
{
	// VertexShaderとPixelShaderをまとめただけ
	class ZShaderSet
	{
	public:
		ZSP<ZVertexShader> GetVS(const ZString& name);
		ZSP<ZPixelShader> GetPS(const ZString& name);

		void SetVS(const ZString& name,ZSP<ZVertexShader> vs);
		void SetPS(const ZString& name,ZSP<ZPixelShader> ps);
		
		void RemoveAllShaders();

	private:
		ZUnorderedMap<ZString,ZSP<ZVertexShader>> m_VSMap;
		ZUnorderedMap<ZString,ZSP<ZPixelShader>>  m_PSMap;
	};
}



#endif