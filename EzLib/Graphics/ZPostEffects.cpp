#include "PCH/pch.h"

namespace EzLib
{
	ZPostEffectPass::ZPostEffectPass()
		: m_IsEnable(true),m_pPostEffects(nullptr)
	{
	}

	ZPostEffectPass::~ZPostEffectPass()
	{
	}

	ZPostEffects::ZPostEffects()
	{
	}

	ZPostEffects::~ZPostEffects()
	{
		Release();
	}

	void ZPostEffects::Init()
	{
		m_TexCompleted = Make_Shared(ZTexture, sysnew);
		m_TexCompleted->CreateRT(ZDx.GetRezoW(), ZDx.GetRezoH());
	}

	void ZPostEffects::Release()
	{
		if (m_PassMap.empty())
			return;

		for (auto& pass : m_PassMap)
		{
			pass.second->Release();
			SAFE_DELPTR(pass.second);
		}

		m_PassMap.clear();
		m_Passes.clear();
	}

	void ZPostEffects::LoadSettingFromJson(const char* filename)
	{
		std::string error;
		m_SettingJsonObject = LoadJsonFromFile(filename, error);
		if(error.empty() == false)
		{
			assert(false);
			DebugLog_Line("Failed : PostEffect::LoadJsonFromFile()");
		}
	}

	void ZPostEffects::SaveSettingToJson(const char* filename)
	{
		std::ofstream ss(filename);
		cereal::JSONOutputArchive o_archive(ss);

		// 有効フラグ
		{
			EnableFlgArchiver enableFlgArchiver;
			for (auto& pass : m_PassMap)
				enableFlgArchiver.FlgMap[pass.first.c_str()] = pass.second->IsEnable();
			o_archive(cereal::make_nvp("Flgs", enableFlgArchiver));
		}

		// 各種パラメータ
		{
			for (auto& pass : m_Passes)
				pass->SaveSettingToJson(o_archive);
		}
	}

	void ZPostEffects::RenderPostEffects(ZSP<ZTexture> source)
	{
		// 完成画面用テクスチャが作成されてない
		if (m_TexCompleted == nullptr) return;
		
		m_RTSave.GetNowAll();
		
		m_TexCompleted->ClearRT(ZVec4(0));

		// 完成画面用テクスチャに一応コピー
		PresentToCompletedTexture(source);
		
		for (auto& pass : m_Passes)
		{
			if(pass->IsEnable())
				pass->RenderPass(source);
		}
	
		Present();
	}


	void ZPostEffects::RemovePassForPassList(ZPostEffectPass* pass)
	{
		for (auto it = m_Passes.begin(); it != m_Passes.end(); it++)
		{
			if (*it != pass)
				continue;
			m_Passes.erase(it);
			break;
		}
	}
}
