//===============================================================
//   ステートクラス群、レンダーターゲット切り替えクラス
// 
//  ・ZBlendStateクラス		… 半透明や加算合成などを切り替える時に使う		\n
//  ・ZDepthStencilStateクラス	… Zバッファやステンシルバッファの動作設定		\n
//  ・ZSamplerStateクラス		… テクスチャの詳細な使用設定					\n
//  ・ZRasterizeStateクラス	… 3Dから2Dへのレンダリング時の動作設定			\n
//  ・ZRenderTargetsクラス		… MRT(マルチレンダーターゲット)も考慮したRT、Depth操作を行う	\n
// 
//===============================================================
#ifndef ZStates_h
#define ZStates_h

namespace EzLib
{

	//  @defgroup Graphics_States グラフィック関係(ステート、レンダーターゲット系)
	//  @{

	//================================================
	//   ブレンドステートクラス
	// 
	// 	半透明や加算合成などを切り替える時に使う
	//================================================
	class ZBlendState
	{
	public:
		//===============================================
		// パラメータ設定系
		//  ※まだデバイスへはセットされません。SetState()でセットされます。
		//  ※また、これらの関数はステートオブジェクトを解放します(再作成が必要なため)
		//===============================================
		//   現在のパイプラインから取得した情報を使う
		void SetAll_GetState();

		//    ブレンド無効　α値も上書きされるモード
		//  	RT_No　			… 設定するレンダーターゲット番号。-1で全RT(0〜7)にセットする。
		void Set_Disable(int RT_No = 0);

		//    α無し αは無視される
		//  	RT_No　			… 設定するレンダーターゲット番号。-1で全RT(0〜7)にセットする。
		//  	bOverrideAlpha　… 描画先のアルファ値を上書きするか？ trueにすると描画先のアルファに描画元のアルファを上書きする(RTがアルファ値を持ってるテクスチャのみ有効)。
		void Set_NoAlpha(int RT_No = 0, bool bOverrideAlpha = false);

		//    半透明合成 No … -1で全RT(0〜7)にセットする
		void Set_Alpha(int RT_No = 0);

		//    加算合成 No … -1で全RT(0〜7)にセットする
		void Set_Add(int RT_No = 0);

		//    各チャンネルの書き込みマスクを設定
		//  	RT_No　	… 設定するレンダーターゲット番号。-1で全RT(0〜7)にセットする。
		//  	r		… 赤成分を書き込むかどうか
		//  	g		… 緑成分を書き込むかどうか
		//  	b		… 青成分を書き込むかどうか
		//  	a		… アルファ成分を書き込むかどうか
		void Set_WriteMask(int RT_No = 0, bool r = true, bool g = true, bool b = true, bool a = true);

		//   アルファトゥカバレッジの設定。αテストみたいなもの。
		void Set_AlphaToCoverageEnable(bool bEnable)
		{
			Safe_Release(m_pBlendState);
			m_Desc.AlphaToCoverageEnable = bEnable ? TRUE : FALSE;
		}

		//   MRTで各レンダーターゲットのブレンドステートの設定を個別に設定できる
		//   bEnable	…	false:0番目のみが使用される
		// 							true:0〜7個別で使用される
		void Set_IndependentBlendEnable(bool bEnable)
		{
			Safe_Release(m_pBlendState);
			m_Desc.IndependentBlendEnable = bEnable ? TRUE : FALSE;
		}

		//   ブレンディング係数設定。RGBA の成分。
		// 　※D3D11_BLEND_BLEND_FACTOR指定時のみ使用。
		void Set_BlendFactor(const float factor[4])
		{
			Safe_Release(m_pBlendState);
			for (int i = 0; i < 4; i++)
			{
				m_BlendFactor[i] = factor[i];
			}
		}

		//   すべてのアクティブなレンダーターゲットでどのサンプルが更新されるかが判別されるマスクを設定
		void Set_SampleMask(UINT mask)
		{
			Safe_Release(m_pBlendState);
			m_SampleMask = mask;
		}

		//   descの内容をコピーする
		void Set_FromDesc(const D3D11_BLEND_DESC& desc)
		{
			Safe_Release(m_pBlendState);
			m_Desc = desc;
		}

		//===============================================
		// 作成・セット
		//===============================================
		//   現在の設定内容を元に、ステートオブジェクトを作成のみ行う
		//  @return 作成成功:true
		bool Create();

		//   ブレンドステートをデバイスへセットする
		//  ステートオブジェクトが作成されていない場合は、作成する
		void SetState();

		//   解放
		void Release()
		{
			Safe_Release(m_pBlendState);
		}

		//===============================================
		// 設定情報取得
		//===============================================
		//   設定情報取得
		const D3D11_BLEND_DESC&	GetDesc() { return m_Desc; }
		//   ステートオブジェクト取得
		ID3D11BlendState*		GetState() { return m_pBlendState; }
		//   BlendFactor取得
		float*					GetBlendFactor() { return m_BlendFactor; }
		//   SampleMasl取得
		UINT					GetSampleMask() { return m_SampleMask; }

		//===============================================
		ZBlendState();
		~ZBlendState()
		{
			Release();
		}

	private:
		// ブレンドステート
		D3D11_BLEND_DESC	m_Desc;				// 設定情報
		ID3D11BlendState*	m_pBlendState;		// ブレンドステートオブジェクト

		float				m_BlendFactor[4];	// BlendFactor
		UINT				m_SampleMask;		// SampleMask

	private:
		// コピー禁止用
		ZBlendState(const ZBlendState& src) {}
		void operator=(const ZBlendState& src) {}
	};


	//================================================
	//   深度ステンシルステートクラス
	// 
	// 　Zバッファやステンシルバッファの動作設定
	//================================================
	class ZDepthStencilState
	{
	public:
		//===============================================
		// 作成・セット
		//  ※まだデバイスへはセットされません。SetState()でセットされます。
		//  ※また、これらの関数はステートオブジェクトを解放します(再作成が必要なため)
		//===============================================

		//----------------------------------------------------------
		//   基本的な設定をセット
		//----------------------------------------------------------
		void SetAll_Standard();

		//----------------------------------------------------------
		//   現在のパイプラインから取得した情報を使う
		//  ※ある条件下では、すべての項目が取得できないことがあります
		//----------------------------------------------------------
		void SetAll_GetState();

		//----------------------------------------------------------
		//   Z判定のON/OFF
		void Set_ZEnable(bool enable)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc.DepthEnable = enable ? TRUE : FALSE;
		}
		//   Z書き込みのON/OFF
		void Set_ZWriteEnable(bool enable)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc.DepthWriteMask = enable ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
		}
		//   Z判定比較方法の設定 通常はD3D11_COMPARISON_LESS_EQUALでOK
		void Set_ZFunc(D3D11_COMPARISON_FUNC funcFlag = D3D11_COMPARISON_LESS_EQUAL)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc.DepthFunc = funcFlag;
		}

		//   ステンシルテストのON/OFF
		void Set_StencilEnable(bool enable)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc.StencilEnable = enable ? TRUE : FALSE;
		}
		//   ステンシルバッファを読み取る部分のマスク設定
		void Set_StencilReadMask(unsigned char mask)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc.StencilReadMask = mask;
		}
		//   ステンシルバッファを書き込む部分のマスク設定
		void Set_StencilWriteMask(unsigned char mask)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc.StencilWriteMask = mask;
		}
		//   深度ステンシル テストの実行時に基準として使用される参照値設定
		//  	stencilRef	… ステンシル値
		void Set_StencilRef(UINT stencilRef)
		{
			Safe_Release(m_pDepthStencilState);
			m_StencilRef = stencilRef;
		}

		//   descの内容をコピーする
		void Set_FromDesc(const D3D11_DEPTH_STENCIL_DESC& desc)
		{
			Safe_Release(m_pDepthStencilState);
			m_Desc = desc;
		}
		//----------------------------------------------------------

		//===============================================
		// 作成・セット
		//===============================================
		//   現在のDescの内容を元にステートオブジェクトを作成のみ行う
		//  @return 作成成功:true
		bool Create();

		//   深度ステンシルステートをデバイスへセット
		// 	ステートオブジェクトが作成されていない場合は、作成する
		void SetState();

		//   解放
		void Release()
		{
			Safe_Release(m_pDepthStencilState);
		}

		//===============================================
		// 設定情報取得
		//===============================================
		//   設定情報取得
		const D3D11_DEPTH_STENCIL_DESC&	GetDesc() { return m_Desc; }
		//   深度ステンシルステートオブジェクト取得
		ID3D11DepthStencilState*		GetState() { return m_pDepthStencilState; }
		//   シテンシル値取得
		UINT							GetStencilRef() { return m_StencilRef; }

		//===============================================
		ZDepthStencilState() : m_pDepthStencilState(0), m_StencilRef(0)
		{
		}
		~ZDepthStencilState()
		{
			Release();
		}

	private:
		D3D11_DEPTH_STENCIL_DESC	m_Desc;					// 設定情報
		ID3D11DepthStencilState*	m_pDepthStencilState;	// ステートオブジェクト
		UINT						m_StencilRef;			// ステンシル値

	private:
		// コピー禁止用
		ZDepthStencilState(const ZDepthStencilState& src) {}
		void operator=(const ZDepthStencilState& src) {}
	};

	//================================================
	//   サンプラーステートクラス
	// 
	// 　テクスチャの詳細な使用設定
	//================================================
	class ZSamplerState
	{
	public:
		//===============================================
		// 作成・セット
		//  ※まだデバイスへはセットされません。SetState()でセットされます。
		//  ※また、これらの関数はステートオブジェクトを解放します(再作成が必要なため)
		//===============================================

		//----------------------------------------------------------
		//   標準的なサンプラ作成
		//----------------------------------------------------------
		void SetAll_Standard();

		//----------------------------------------------------------
		//   ピクセルシェーダパイプラインから現在の設定情報を取得
		//  	StartSlot	… 設定を取得したいスロット番号
		void SetAll_GetStatePS(UINT StartSlot);
		//   頂点シェーダパイプラインから現在の設定情報を取得
		//  	StartSlot	… 設定を取得したいスロット番号
		void SetAll_GetStateVS(UINT StartSlot);
		//   ジオメトリシェーダパイプラインから現在の設定情報を取得
		//  	StartSlot	… 設定を取得したいスロット番号
		void SetAll_GetStateGS(UINT StartSlot);
		//   コンピュータシェーダパイプラインから現在の設定情報を取得
		//  	StartSlot	… 設定を取得したいスロット番号
		void SetAll_GetStateCS(UINT StartSlot);
		//----------------------------------------------------------

		//----------------------------------------------------------
		//   テクスチャ アドレッシングモードをWRAPに設定
		void Set_Wrap();

		//   テクスチャ アドレッシングモードをCLAMPに設定
		void Set_Clamp();

		//    テクスチャ アドレッシングモードをMIRRORに設定
		void Set_Mirror();

		//   テクスチャ アドレッシングモードをBORDERに設定
		void Set_Border(const ZVec4& borderColor);

		//----------------------------------------------------------

		//----------------------------------------------------------
		//   フィルタ設定
		//  	filter	… フィルタ
		void Set_Filter(D3D11_FILTER filter, int maxAnisotropy = 1);
		void Set_Filter_Linear();		// MIN MAG MIP を全てLinearとして設定
		void Set_Filter_Point();		// MIN MAG MIP を全てPointとして設定
		void Set_Filter_Anisotropy(UINT MaxAnisotropy = 16);	// 異方性フィルタ設定
		//----------------------------------------------------------

		//   比較モード設定
		//  Percent Closer Filter(PCF)のソフトシャドウ表現が可能になる。\n
		//  ※HLSL側のサンプラはSamplerComparisonStateで用意し、Texture2DのSample()ではなく、SampleCmp()かSampleCmpLevelzero()を使うこと。
		//  	compFunc	… D3D11_COMPARISON系のフラグを指定
		void Set_ComparisonMode(D3D11_COMPARISON_FUNC compFunc = D3D11_COMPARISON_LESS_EQUAL);

		//   ミップマップレベルのバイアスを設定する
		//  ミップマップをよりかからないようにしたい場合はマイナスの値を指定する。より強くかけたい場合はプラス値を指定する。
		//  	bias … 計算されたミップマップ レベルからのオフセット
		void Set_MipLODBias(float bias)
		{
			m_Desc.MipLODBias = bias;
		}
		//   ミップマップレベルの範囲を設定
		//  例えばミップマップをかけたくない場合は、fMin=0 fMax=0)とかにする。
		//  	fMin	… ミップマップ範囲の下限
		//  	fMax	… ミップマップ範囲の上限
		void Set_MipLOD(float fMin, float fMax)
		{
			m_Desc.MinLOD = fMin;
			m_Desc.MaxLOD = fMax;
		}

		//   descの内容をコピーする
		void Set_FromDesc(const D3D11_SAMPLER_DESC& desc)
		{
			Safe_Release(m_pSampler);
			m_Desc = desc;
		}


		//===============================================
		// 作成・セット
		//===============================================
		//   現在のDescの内容を元にサンプラ作成
		//  @return 作成成功:true
		bool Create();

		//   サンプラをピクセルシェーダーのパイプライン ステージにセットする。
		//  ステートオブジェクトが作成されていない場合は、作成する
		//  	StartSlot	… セットするスロット番号
		void SetStatePS(UINT StartSlot = 0);

		//   サンプラを頂点シェーダーのパイプライン ステージにセットする。
		//  ステートオブジェクトが作成されていない場合は、作成する
		//  	StartSlot	… セットするスロット番号
		void SetStateVS(UINT StartSlot = 0);

		//   サンプラをジオメトリシェーダーのパイプライン ステージにセットする。
		//  ステートオブジェクトが作成されていない場合は、作成する
		//  	StartSlot	… セットするスロット番号
		void SetStateGS(UINT StartSlot = 0);

		//   サンプラをコンピュートシェーダーのパイプライン ステージにセットする。
		//  ステートオブジェクトが作成されていない場合は、作成する
		//  	StartSlot	… セットするスロット番号
		void SetStateCS(UINT StartSlot = 0);

		//===============================================
		//   解放
		//===============================================
		void Release()
		{
			Safe_Release(m_pSampler);
		}

		//===============================================
		// 設定情報取得
		//===============================================
		//   設定情報取得
		const D3D11_SAMPLER_DESC&	GetDesc() { return m_Desc; }
		//   ステートオブジェクト取得
		ID3D11SamplerState*			GetState() { return m_pSampler; }

		//===============================================
		ZSamplerState() : m_pSampler(0)
		{
		}
		~ZSamplerState()
		{
			Release();
		}

	private:
		D3D11_SAMPLER_DESC		m_Desc;			// 設定情報
		ID3D11SamplerState*		m_pSampler;		// ステートオブジェクト

	private:
		// コピー禁止用
		ZSamplerState(const ZSamplerState& src) {}
		void operator=(const ZSamplerState& src) {}
	};

	//================================================
	//   ラスタライザステートクラス
	// 
	// 　3Dから2Dへのレンダリング時の動作設定
	//================================================
	class ZRasterizeState
	{
	public:
		//===============================================
		// 作成・セット
		//  ※まだデバイスへはセットされません。SetState()でセットされます。
		//  ※また、これらの関数はステートオブジェクトを解放します(再作成が必要なため)
		//===============================================

		//----------------------------------------------------------
		//   標準的な情報作成
		//----------------------------------------------------------
		void SetAll_Standard();

		//----------------------------------------------------------
		//   現在のパイプラインから取得した情報を使う
		//----------------------------------------------------------
		void SetAll_GetState();

		//----------------------------------------------------------
		//   カリングモード設定：表面のみ描画
		void Set_CullMode_Back()
		{
			Safe_Release(m_pRasterize);
			m_Desc.CullMode = D3D11_CULL_BACK;
		}
		//   カリングモード設定：裏面のみ描画
		void Set_CullMode_Front()
		{
			Safe_Release(m_pRasterize);
			m_Desc.CullMode = D3D11_CULL_FRONT;
		}
		//   カリングモード設定：両面描画
		void Set_CullMode_None()
		{
			Safe_Release(m_pRasterize);
			m_Desc.CullMode = D3D11_CULL_NONE;
		}
		//   カリングモード設定
		void Set_CullMode(D3D11_CULL_MODE mode)
		{
			Safe_Release(m_pRasterize);
			m_Desc.CullMode = mode;
		}

		//   通常
		void Set_FillMode_Solid()
		{
			Safe_Release(m_pRasterize);
			m_Desc.FillMode = D3D11_FILL_SOLID;
		}
		//   ワイヤーフレーム
		void Set_FillMode_Wireframe()
		{
			Safe_Release(m_pRasterize);
			m_Desc.FillMode = D3D11_FILL_WIREFRAME;
		}

		//   descの内容をコピーする
		void Set_FromDesc(const D3D11_RASTERIZER_DESC& desc)
		{
			Safe_Release(m_pRasterize);
			m_Desc = desc;
		}
		//----------------------------------------------------------

		//===============================================
		// 作成・セット
		//===============================================
		//   現在のDescの内容を元にサンプラ作成
		//  @return 作成成功:true
		bool Create();

		//   ラスタライザーステートをデバイスへセット
		//   ステートオブジェクトが作成されていない場合は、作成する
		void SetState();

		//   解放
		void Release()
		{
			Safe_Release(m_pRasterize);
		}

		//===============================================
		// 設定情報取得
		//===============================================
		//   設定情報取得
		const D3D11_RASTERIZER_DESC&	GetDesc() { return m_Desc; }
		//   ラスタライザーステートオブジェクト取得
		ID3D11RasterizerState*			GetState() { return m_pRasterize; }

		//===============================================
		ZRasterizeState() : m_pRasterize(0)
		{
		}
		~ZRasterizeState()
		{
			Release();
		}

	private:
		D3D11_RASTERIZER_DESC	m_Desc;			// 設定情報
		ID3D11RasterizerState*	m_pRasterize;	// ステートオブジェクト

	private:
		// コピー禁止用
		ZRasterizeState(const ZRasterizeState& src) {}
		void operator=(const ZRasterizeState& src) {}
	};

	//================================================================
	//   レンダーターゲット、Zバッファの切り替え操作クラス
	// 
	// 	MRT(マルチレンダーターゲット)も考慮したRT、Depth操作を行う
	// 
	//================================================================
	class ZRenderTargets
	{
	public:
		//====================================================
		// 設定
		//====================================================

		//----------------------------------------------------------
		// 登録系

		//   指定Noのレンダーターゲットとして登録(デバイスにはまだセットしない)
		//  	rtNo	… セットするレンダーターゲット番号
		//  	rtTex	… このテクスチャ内のRenderTargerViewを登録する
		void RT(int rtNo, ZTexture& rtTex);

		//   指定Noのレンダーターゲットとして登録(デバイスにはまだセットしない)
		//  	rtNo	… セットするレンダーターゲット番号
		//  	rtView	… このRenderTargerViewを登録する
		void RT(int rtNo, ID3D11RenderTargetView* rtView);	// View指定Ver

		//   rtdataに登録されている、指定Noのレンダーターゲットを取得し登録(デバイスにはまだセットしない)
		void RT(int rtNo, ZRenderTargets& rtdata);

		//   深度ステンシルを登録(デバイスにはまだセットしない)
		//  	depthTex	… このテクスチャ内のDepthStencilViewを登録する
		void Depth(ZTexture& depthTex);

		//   深度ステンシルを登録(デバイスにはまだセットしない)
		//  	depthView	… このDepthStencilViewを登録する
		void Depth(ID3D11DepthStencilView* depthView);	// View指定Ver

		//   深度ステンシルを登録(デバイスにはまだセットしない)
		//  	rtdata	… このZRenderTargetsに登録されているDepthを登録
		void Depth(ZRenderTargets& rtdata);
		//----------------------------------------------------------

		//----------------------------------------------------------
		// デバイスから取得系

		//   現在デバイスにセットされている全てのRTとDepthStencilを、このクラスに取得する
		void GetNowAll();

		//   現在デバイスにセットされている全てのRTを、このクラスに取得する
		void GetNowRTs();

		//   現在デバイスにセットされているDepthStencilを、このクラスに取得する
		void GetNowDepth();

		//	現在デバイスにセットされている0番目のRTとDepthStencilを取得する
		void GetNowTop();

		//----------------------------------------------------------

		//   登録されている情報を全て解除
		void Release();

		//====================================================
		// デバイスへセット
		//====================================================

		//   このクラスに登録されているRT､Depthを、実際にデバイスへセットする(RT、Depthを実際に切り替えること)
		//  ※0番目のRTのサイズでビューポートも変更しています。
		//   setRTNum		: セットするRTの数。-1ですべてセット(8枚)。例えば 2 だと、0と1の2枚がセットされる。
		void SetToDevice(int setRTNum = -1);
		//====================================================
		// 取得
		//====================================================

		//   現在このクラスに登録されているRTを取得(デバイスのRT取得ではありません)
		//  ※戻り値のビューは、参照カウンタをAddRefしません
		//   rtNo	… RT番号(0〜7)
		//  @return このクラスに登録されているrtNo番目のレンダーターゲットビュー
		ID3D11RenderTargetView* GetRT(int rtNo) { return m_pRTs[rtNo]; }

		//   現在このクラスに登録されているDepthを取得(デバイスのDepth取得ではありません)
		//  ※戻り値のビューは、参照カウンタをAddRefしません
		//  @return このクラスに登録されているidx番目のデプスステンシルビュー
		ID3D11DepthStencilView* GetDepth() { return m_pDepth; }

		//   指定RTの情報取得
		//  	rtNo	… 情報を取得したいRT番号
		//  @param[out]	outDesc	… 情報を入れるための変数
		//  @return true:正常に取得できた
		bool GetRTDesc(int rtNo, D3D11_TEXTURE2D_DESC& outDesc);


		//====================================================

		// 
		ZRenderTargets()
		{
			for (auto& rt : m_pRTs)
				rt = nullptr;
			m_pDepth = nullptr;
		};
		// 
		~ZRenderTargets()
		{
			Release();
		}

		// コピー
		ZRenderTargets(const ZRenderTargets& src) : ZRenderTargets()
		{
			*this = src;
		}
		void operator=(const ZRenderTargets& src);

	private:
		ID3D11RenderTargetView* m_pRTs[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];	// RT設定情報記憶用
		ID3D11DepthStencilView*		m_pDepth;										// Depth設定情報記憶用
		D3D11_VIEWPORT				m_VP;											// ビューポート取得用

	};

	//================================================================
	//   現在のレンダーターゲットやZバッファを記憶し、インスタンス消滅時にセットし復元するクラス
	//================================================================
	class ZRenderTarget_BackUpper
	{
	public:
		//   最初に現在の状態を全バックアップ
		ZRenderTarget_BackUpper()
		{
			m_RTs.GetNowAll();
		}

		//   消える直前に、バックアップしたRTとZをセットしなおす
		~ZRenderTarget_BackUpper()
		{
			SetToDevice(true);
		}

		//  @bried 意図的にバックアップしたRTとZに戻す
		//  	bDeleteBackUp	… バックアップデータを解放する？
		void SetToDevice(bool bDeleteBackUp = true)
		{
			m_RTs.SetToDevice();
			if (bDeleteBackUp)m_RTs.Release();
		}

		ZRenderTargets m_RTs;
	};
	// 例)	void ABC(){
	//
	//			ZRenderTarget_BackUpper a;	// ここで現在のRTやZバッファを記憶する
	//
	//			< RTやZバッファを切り替えて描画などを行う >
	//
	//		}	// aローカル変数aが消えるので、記憶されていたRTとZバッファに戻す
	//


	//  @}

}

#endif
