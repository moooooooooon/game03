#ifndef __ZPOST_EFFECTS_H__
#define __ZPOST_EFFECTS_H__

namespace EzLib
{
	class ZPostEffects;

	class ZPostEffectPass
	{
	public:
		ZPostEffectPass();
		virtual ~ZPostEffectPass();
		
		virtual void Init() = 0;
		virtual void Release() = 0;

		virtual void ImGui(){}

		virtual void LoadSettingFromJson(const json11::Json& jsonObj) {};

		virtual void SaveSettingToJson(cereal::JSONOutputArchive& archive) {};

		virtual ZString GetPassName() { return "No Name Pass"; }
		virtual void RenderPass(ZSP<ZTexture> source) = 0;

		bool IsEnable()const;
		void SetEnable(bool flg);
		void SetPostEffcts(ZPostEffects* pPostEffects);

	protected:
		ZPostEffects* m_pPostEffects;
		bool m_IsEnable;
	};

	// ↑のPassは全てこのクラスで管理する
	class ZPostEffects
	{
	public:
		static constexpr char* PostEffectWindowName = "PostEffect";
		static constexpr char* TexDebugWindowName = "PostEffectTextures";
		static constexpr char* TexDebugDockTabName = "PostEffectTextures_Tab";

		struct EnableFlgArchiver
		{
		public:
			template<class Archive>
			void serialize(Archive& archive)
			{
				if (FlgMap.empty())
					return;
				for (auto& f : FlgMap)
					archive(cereal::make_nvp(f.first.c_str(), f.second));
			}

		public:
			ZUnorderedMap<ZString, bool> FlgMap;
		};

	public:
		ZPostEffects();
		virtual ~ZPostEffects();
		
		virtual void Init();
		virtual void Release();

		// EffectPass追加
		void AddEffectPass(const ZString& passName,ZPostEffectPass* pass);
		// EffectPass取得
		ZPostEffectPass* GetEffectPass(const ZString& passName);
		// EffectPassの数取得
		size_t GetNumEffectPass();
		// EffectPass削除
		void RemoveEffectPass(const ZString& passName);

		// PostEffectで使うリソースセット
		void SetTextureResource(const ZString& name, ZSP<ZTexture>& texture);
		// PostEffectで使うリソース取得
		ZSP<ZTexture> GetSourceTexture(const ZString& name);
		// 全テクスチャリソース削除
		void RemoveAllTextureResources();

		// 完成イメージテクスチャ取得
		ZSP<ZTexture> GetCompletedTexture();

		virtual void ImGui(){}
		
		// Jsonファイルを読み込みオブジェクトを保持
		// EffectPassが追加された際にJsonオブジェクトが空でなければ
		// EffectPassのLoadSettingFromJson()関数を呼び出す
		void LoadSettingFromJson(const char* filename);

		void SaveSettingToJson(const char* filename);

		// PostEffect処理開始
		void RenderPostEffects(ZSP<ZTexture> source);

		// 完成イメージへ描画
		virtual void PresentToCompletedTexture(ZSP<ZTexture> source) = 0;
	
	protected:
		virtual void Present() = 0; // 完成イメージをバックバッファへ

	private:
		void RemovePassForPassList(ZPostEffectPass* pass);

	protected:
		ZSP<ZTexture> m_TexCompleted; // 完成イメージ
		ZUnorderedMap<ZString, ZSP<ZTexture>> m_TextureResources;
		ZUnorderedMap<ZString,ZPostEffectPass*> m_PassMap;
		ZVector<ZPostEffectPass*> m_Passes; // 追加した順に実行するため(要素へのアクセスなどは↑のMapでする)
		ZRenderTargets m_RTSave;// 保持用
		
		json11::Json m_SettingJsonObject; // エフェクトパス設定読み込み用
	};

#include "ZPostEffects.inl"

}

#endif