#ifndef ZBillBoard_h
#define ZBillBoard_h

namespace EzLib
{

	//=======================================================================
	// 
	//   四角形ポリゴンオブジェクトクラス
	// 
	//  初期では、幅x高:1x1 UV:0〜１ 色:白　になる	\n
	//   あとはSet系の関数で変更する				\n
	// 　[主な機能]									\n
	// 　・テクスチャ								\n
	// 　・UVアニメーション							\n
	// 　・色										\n
	// 
	//=======================================================================
	class ZBillBoard
	{
	public:

		//=======================================================================
		// 頂点設定
		//=======================================================================

		//------------------------------------------------------------------------
		//   頂点座標設定
		//  X,Y平面に頂点を配置されます。Zは0。	\n
		//  3D座標系で考えること(Yは上)
		//  	x	… X座標
		//  	y	… Y座標
		//  	w	… 幅
		//  	z	… 高さ
		//------------------------------------------------------------------------
		void SetVertex(float x, float y, float w, float h);


		//   色取得
		ZVec4&		GetColor() { return m_Color; }

		//   色設定
		//  	color	… セットする色
		void SetColor(const ZVec4& color)
		{
			m_Color = color;
		}

		//   テクスチャセットを取得
		ZSP<ZTextureSet>&	GetTexSet() { return m_TexSet; }

		//   テクスチャセットをセットする
		//  	texSet	… セットするTextureSet
		void SetTex(const ZSP<ZTextureSet>& texSet)
		{
			m_TexSet = texSet;
		}

		//   単品のテクスチャをセット
		//  	tex	… セットするテクスチャ
		void SetTex(const ZSP<ZTexture>& tex)
		{
			if (m_TexSet == nullptr)m_TexSet = Make_Shared(ZTextureSet,appnew);
			*m_TexSet = tex;
		}

		//=======================================================================
		// UVアニメーション関係
		//=======================================================================

		//   UVアニメーター取得
		ZUVAnimator&	GetAnime() { return m_Anime; }

		//=======================================================================
		// 描画
		//=======================================================================

		//   描画
		void Draw();



		//
		ZBillBoard()
		{

			SetVertex(-0.5f, -0.5f, 1, 1);
			SetColor(ZVec4(1, 1, 1, 1));

			for (int i = 0; i < 4; i++)
			{
				m_V[i].Color = ZVec4(1, 1, 1, 1);
			}

			m_TexSet = Make_Shared(ZTextureSet,appnew);
		}

		~ZBillBoard()
		{
		}

	private:

		ZVertex_Pos_UV_Color	m_V[4];						// 頂点データ

		ZVec4				m_Color = ZVec4(1, 1, 1, 1);	// 色

		ZSP<ZTextureSet>	m_TexSet;						// テクスチャセット


		// アニメーション用
		int					m_xCnt = 1, m_yCnt = 1;		// アニメーションのX,Y分割数
		int					m_AnimeNum = 0;				// アニメーションする数
		float				m_fAnimePos = 0;			// 現在のアニメの位置
		ZUVAnimator		m_Anime;

		//
		ZSP<ZPolygon>		m_pPoly;

		//  作業用ポリゴンデータ(全てのZLaserで使いまわします)
		static ZWP<ZPolygon>	s_wpPolyRectangle;

		ZSP<ZRingDynamicVB>			m_pRingBufPoly;

		static ZWP<ZRingDynamicVB>	s_wpRingVB;
	};

}
#endif
