template<typename T, typename U>
inline void _EnableSharedFromThis(const ZSP<T>& sp, U* p, std::false_type)
{
	// ZEnableSharedFromThis���p�����Ă��Ȃ�
}

template<typename T, typename U>
inline void _EnableSharedFromThis(const ZSP<T>& sp, U* p, std::true_type)
{
	if (p && p->m_WP.IsActive() == false)
		p->m_WP = ZSP<std::remove_cv_t<U>>(sp, const_cast<std::remove_cv_t<U>*>(p));
}

template<typename T, typename U>
inline void EnableSharedFromThis(const ZSP<T>& sp, U* p)
{
	_EnableSharedFromThis(sp, p, std::bool_constant<std::conjunction_v<
						  std::negation<std::is_array<T>>,
						  std::negation<std::is_volatile<U>>,
						  IsEnableShared<U>>>{});
}

#pragma region Unique Pointer

template<typename T>
inline ZUP<T>::ZUP()
{
	m_Ptr = nullptr;
}

template<typename T>
inline ZUP<T>::ZUP(T* p)
{
	m_Ptr = p ? p : nullptr;
}

template<typename T>
inline ZUP<T>::ZUP(ZUP<T>&& p) : m_Ptr(nullptr)
{
	m_Ptr = p.m_Ptr;
	p.m_Ptr = nullptr;
}

template<typename T>
template<typename U>
inline ZUP<T>::ZUP(ZUP<U>&& p) : m_Ptr(nullptr)
{
	m_Ptr = (T*)p.m_Ptr;
	p.m_Ptr = nullptr;
}

template<typename T>
inline ZUP<T>::~ZUP()
{
	Release();
}

template<typename T>
inline T& ZUP<T>::operator*()const
{
	return *m_Ptr;
}

template<typename T>
inline T* ZUP<T>::operator->()const
{
	return m_Ptr;
}

template<typename T>
inline ZUP<T>& ZUP<T>::operator=(T* p)
{
	if (p == m_Ptr)return (*this);

	Release();

	m_Ptr = p;

	return (*this);
}

template<typename T>
inline ZUP<T>& ZUP<T>::operator=(ZUP<T>&& p)
{
	if (p.m_Ptr == m_Ptr)return (*this);

	Release();
	m_Ptr = p.m_Ptr;
	p.m_Ptr = nullptr;

	return (*this);
}

template<typename T>
inline ZUP<T>& ZUP<T>::operator=(nullptr_t nullval)
{
	Release();
	return (*this);
}

template<typename T>
template<typename U>
inline ZUP<T>& ZUP<T>::operator=(U* p)
{
	if (m_Ptr == p)return (*this);
	
	Release();
	m_Ptr = p;

	return (*this);
}

template<typename T>
template<typename U>
inline ZUP<T>& ZUP<T>::operator=(ZUP<U>&& p)
{
	if (m_Ptr == p.m_Ptr)return (*this);

	Release();
	m_Ptr = (T*)p.m_Ptr;
	p.m_Ptr = nullptr;

	return (*this);
}

template<typename T>
inline bool ZUP<T>::operator==(T* val)const
{
	if (m_Ptr == val)return true;
	return false;
}

template<typename T>
inline bool ZUP<T>::operator==(nullptr_t nullval)const
{
	return m_Ptr == nullptr;
}

template<typename T>
inline bool ZUP<T>::operator!=(T* val)const
{
	if (m_Ptr != val)return true;
	return false;
}

template<typename T>
inline bool ZUP<T>::operator!=(nullptr_t nullval)const
{
	return m_Ptr != nullptr;
}

template<typename T>
inline T& ZUP<T>::operator[](size_t index) const
{
	return m_Ptr[index];
}

template<typename T>
inline ZUP<T>::operator bool() const
{
	return m_Ptr != nullptr;
}

template<typename T>
inline bool ZUP<T>::operator!() const
{
	return m_Ptr == nullptr;
}

template<typename T>
inline T* ZUP<T>::GetPtr()const
{
	return m_Ptr;
}

template<typename T>
inline void ZUP<T>::Release()
{
	if (m_Ptr == nullptr)return;

	// ���
	SAFE_DELPTR(m_Ptr);
}

template<typename T>
template<typename U>
inline bool ZUP<T>::DownCast(ZUP<U>& p)
{
	T* castPtr = dynamic_cast<T*>(p.GetPtr());
	if (castPtr == nullptr)return false;

	p.m_Ptr = nullptr;
	Release();

	m_Ptr = castPtr;
	return true;
}

template<typename T>
inline bool ZUP<T>::IsActive()const
{
	if (m_Ptr == nullptr)return false;

	return ZMemoryAllocator::IsActivePtr(m_Ptr);
}

template<typename T>
inline void ZUP<T>::Swap(ZUP<T>& up)
{
	if (up.m_Ptr == m_Ptr)return;

	T* tmpPtr = up.m_Ptr;
	up.m_Ptr = m_Ptr;
	m_Ptr = tmpPtr;
}

#pragma endregion

#pragma region Shared Pointer

template<typename T>
template<typename U>
inline ZSP<T>::ZSP(U* p)
{
	m_pRefCountObj = sysnew(ZRefCountObject<U>,p);
	m_Ptr = (T*)p;
	EnableSharedFromThis(*this, p);
}

template<typename T>
template<typename U>
inline ZSP<T>::ZSP(const ZSP<U>& sp, elemType* p)
{
	if(sp.m_pRefCountObj != nullptr)
		sp.m_pRefCountObj->AddSPRef();

	m_pRefCountObj = sp.m_pRefCountObj;
	m_Ptr = p;
}

template<typename T>
inline ZSP<T>::ZSP(const ZSP<T>& sp)
{
	if (sp.m_pRefCountObj != nullptr)
		sp.m_pRefCountObj->AddSPRef();

	m_Ptr = sp.m_Ptr;
	m_pRefCountObj = sp.m_pRefCountObj;
}

template<typename T>
template<typename U>
inline ZSP<T>::ZSP(const ZSP<U>& sp)
{
	if (sp.m_pRefCountObj != nullptr)
		sp.m_pRefCountObj->AddSPRef();

	m_Ptr = (T*)sp.m_Ptr;
	m_pRefCountObj = sp.m_pRefCountObj;
}

template<typename T>
template<typename U>
inline ZSP<T>::ZSP(const ZWP<U>& p)
{
	if (p.m_pRefCountObj && p.m_pRefCountObj->TryAddSPRef())
	{
		m_pRefCountObj = p.m_pRefCountObj;
		m_Ptr = (T*)p.m_Ptr;
		return;
	}

	assert(false);
}

template<typename T>
inline ZSP<T>::ZSP(ZSP<T>&& sp)
{
	m_pRefCountObj = sp.m_pRefCountObj;
	m_Ptr = sp.m_Ptr;

	sp.m_pRefCountObj = nullptr;
	sp.m_Ptr = nullptr;
}

template<typename T>
template<typename U>
inline ZSP<T>::ZSP(ZSP<U>&& sp)
{
	m_pRefCountObj = sp.m_pRefCountObj;
	m_Ptr = (T*)sp.m_Ptr;

	sp.m_pRefCountObj = nullptr;
	sp.m_Ptr = nullptr;
}

template<typename T>
inline ZSP<T>::~ZSP()
{
	if(m_pRefCountObj)m_pRefCountObj->SubSPRef();
	m_pRefCountObj = nullptr;
}

template<typename T>
inline ZSP<T>& ZSP<T>::operator=(const ZSP<T>& sp)
{
	ZSP(sp).SwapPtr(*this);

	return (*this);
}

template<typename T>
template<typename U>
inline ZSP<T>& ZSP<T>::operator=(const ZSP<U>& sp)
{
	ZSP(sp).SwapPtr(*this);

	return (*this);
}

template<typename T>
inline ZSP<T>& ZSP<T>::operator=(ZSP<T>&& sp)
{
	ZSP(std::move(sp)).SwapPtr(*this);
	
	return (*this);
}

template<typename T>
template<typename U>
inline ZSP<T>& ZSP<T>::operator=(ZSP<U>&& sp)
{
	ZSP(std::move(sp)).SwapPtr(*this);

	return (*this);
}

template<typename T>
template<class U,
	std::enable_if_t<!std::disjunction_v<std::is_array<U>, std::is_void<U>>, int> >
inline U& ZSP<T>::operator*()const
{
	return *m_Ptr;
}

template<typename T>
inline T* ZSP<T>::operator->()const
{
	return m_Ptr;
}

template<typename T>
inline bool ZSP<T>::operator==(T* val)const
{
	return m_Ptr == val;
}

template<typename T>
inline bool ZSP<T>::operator==(nullptr_t nullval)const
{
	return m_Ptr == nullptr;
}

template<typename T>
inline bool ZSP<T>::operator!=(T* val)const
{
	return m_Ptr != val;
}

template<typename T>
inline bool ZSP<T>::operator!=(nullptr_t nullval)const
{
	return m_Ptr != nullptr;
}

template<typename T>
inline ZSP<T>::operator bool() const
{
	return m_Ptr != nullptr;
}

template<typename T>
inline bool ZSP<T>::operator!() const
{
	return m_Ptr == nullptr;
}

template<typename T>
template<class U,class Elem,std::enable_if_t<std::is_array_v<U>, int>>
inline Elem& ZSP<T>::operator[](size_t index) const
{
	return m_Ptr[index];
}

template<typename T>
inline void ZSP<T>::Reset()
{
	ZSP().SwapPtr(*this);
}

template<typename T>
inline void ZSP<T>::Reset(T* p)
{
	ZSP(p).SwapPtr(*this);
}

template<typename T>
inline T* ZSP<T>::GetPtr()const
{
	return m_Ptr;
}

template<typename T>
inline size_t ZSP<T>::GetRefCnt()const
{
	return m_pRefCountObj->GetSPRefCnt();
}

template<typename T>
template<typename U>
inline bool ZSP<T>::DownCast(ZSP<U>& p)
{
	T* castPtr = dynamic_cast<T*>(p.GetPtr());

	if (castPtr == nullptr)
		return false;
	
	if (p.m_pRefCountObj != nullptr)
		p.m_pRefCountObj->AddSPRef();

	m_pRefCountObj = p.m_pRefCountObj;
	m_Ptr = p;
	
	return true;
}

template<typename T>
template<typename U>
inline ZSP<U> ZSP<T>::DownCast()
{
	U* castPtr = dynamic_cast<U*>(m_Ptr);
	if (castPtr == nullptr)return ZSP<U>();

	return ZSP<U>(*this,castPtr);
}

template<typename T>
template<typename U>
inline ZSP<U> ZSP<T>::Cast()
{
	U* castPtr = static_cast<U*>(m_Ptr);

	return ZSP<U>(*this, castPtr);
}

template<typename T>
inline bool ZSP<T>::IsActive()const
{
	if(m_Ptr == nullptr)return false;
	if (m_pRefCountObj == nullptr)return false;

	return m_pRefCountObj->GetSPRefCnt() != 0;
}

template<typename T>
inline void ZSP<T>::SwapPtr(ZSP<T>& sp)
{
	std::swap(m_Ptr, sp.m_Ptr);
	std::swap(m_pRefCountObj, sp.m_pRefCountObj);
}

template<typename T,typename U>
inline bool operator==(const ZSP<T>& left, const ZSP<U>& right)
{
	return (left.GetPtr() == right.GetPtr());
}

template<typename T, typename U>
inline bool operator!=(const ZSP<T>& left, const ZSP<U>& right)
{
	return (left.GetPtr() != right.GetPtr());
}

template<typename T, typename U>
inline bool operator<(const ZSP<T>& left, const ZSP<U>& right)
{
	return (left.GetPtr() < right.GetPtr());
}

template<typename T, typename U>
inline bool operator>=(const ZSP<T>& left, const ZSP<U>& right)
{
	return (left.GetPtr() >= right.GetPtr());
}

template<typename T, typename U>
inline bool operator>(const ZSP<T>& left, const ZSP<U>& right)
{
	return (left.GetPtr() > right.GetPtr());
}

template<typename T, typename U>
inline bool operator<=(const ZSP<T>& left, const ZSP<U>& right)
{
	return (left.GetPtr() <= right.GetPtr());
}

template<typename T>
inline bool operator==(const ZSP<T>& left, nullptr_t)
{
	return (left.GetPtr() == nullptr);
}

template<typename T>
inline bool operator==(nullptr_t, const ZSP<T>& right)
{
	return (nullptr == right.GetPtr());
}

template<typename T>
inline bool operator!=(const ZSP<T>& left, nullptr_t)
{
	return (left.GetPtr() != nullptr);
}

template<typename T>
inline bool operator!=(nullptr_t, const ZSP<T>& right)
{
	return (nullptr != right.GetPtr());
}

template<typename T>
inline bool operator<(const ZSP<T>& left, nullptr_t)
{
	return (left.GetPtr() < static_cast<typename ZSP<T>::elemType*>(nullptr));
}

template<typename T>
inline bool operator<(nullptr_t, const ZSP<T>& right)
{
	return (static_cast<typename ZSP<T>::elemType*>(nullptr) < right.GetPtr());
}

template<typename T>
inline bool operator>=(const ZSP<T>& left, nullptr_t)
{
	return (left.GetPtr() >= static_cast<typename ZSP<T>::elemType*>(nullptr));
}

template<typename T>
inline bool operator>=(nullptr_t, const ZSP<T>& right)
{
	return (static_cast<typename ZSP<T>::elemType*>(nullptr) >= right.GetPtr());
}

template<typename T>
inline bool operator>(const ZSP<T>& left, nullptr_t)
{
	return (left.GetPtr() > static_cast<typename ZSP<T>::elemType*>(nullptr));
}

template<typename T>
inline bool operator>(nullptr_t, const ZSP<T>& right)
{
	return (static_cast<typename ZSP<T>::elemType*>(nullptr) > right.GetPtr());
}

template<typename T>
inline bool operator<=(const ZSP<T>& left, nullptr_t)
{
	return (left.GetPtr() <= static_cast<typename ZSP<T>::elementTpe*>(nullptr));
}

template<typename T>
inline bool operator<=(nullptr_t, const ZSP<T>& right)
{
	return (static_cast<typename ZSP<T>::elemType*>(nullptr) <= right.GetPtr());
}

#pragma endregion

#pragma region Weak Pointer


template<typename T>
inline ZWP<T>::ZWP(const ZWP<T>& p)
{
	if (p.m_pRefCountObj)
		p.m_pRefCountObj->AddWPRef();
	m_pRefCountObj = p.m_pRefCountObj;
	m_Ptr = p.m_Ptr;
}


template<typename T>
template<typename U>
inline ZWP<T>::ZWP(const ZSP<U>& p)
{
	if (p.m_pRefCountObj)
		p.m_pRefCountObj->AddWPRef();
	m_pRefCountObj = p.m_pRefCountObj;
	m_Ptr = p.m_Ptr;
}

template<typename T>
template<typename U>
inline ZWP<T>::ZWP(const ZWP<U>& p)
{
	ZSP<U>sp = p.Lock();
	if (sp.m_pRefCountObj)
		sp.m_pRefCountObj->AddWPRef();
	m_pRefCountObj = sp.m_pRefCountObj;
	m_Ptr = sp.m_Ptr;
}

template<typename T>
inline ZWP<T>::ZWP(ZWP<T>&& p)
{
	m_pRefCountObj = p.m_pRefCountObj;
	m_Ptr = p.m_Ptr;
	p.m_pRefCountObj = nullptr;
	p.m_Ptr = nullptr;
}

template<typename T>
template<typename U>
inline ZWP<T>::ZWP(ZWP<U>&& p)
{
	{
		ZSP<U>sp = p.Lock();
		if (sp.m_pRefCountObj)
			sp.m_pRefCountObj->AddWPRef();
		m_pRefCountObj = sp.m_pRefCountObj;
		m_Ptr = sp.m_Ptr;
	}
	p.Reset();
}


template<typename T>
inline ZWP<T>::~ZWP()
{
	if (m_pRefCountObj)
		m_pRefCountObj->SubWPRef();
}

template<typename T>
inline ZWP<T>& ZWP<T>::operator=(const ZWP<T>& p)
{
	ZWP(p).SwapPtr(*this);
	return *this;
}

template<typename T>
template<typename U>
inline ZWP<T>& ZWP<T>::operator=(const ZWP<U>& p)
{
	ZWP(p).SwapPtr(*this);

	return *this;
}

template<typename T>
inline ZWP<T>& ZWP<T>::operator=(ZWP<T>&& p)
{
	ZWP(std::move(p)).SwapPtr(*this);
	return (*this);
}

template<typename T>
template<typename U>
inline ZWP<T>& ZWP<T>::operator=(ZWP<U>&& p)
{
	ZWP(p).SwapPtr(*this);
	return *this;
}

template<typename T>
template<typename U>
inline ZWP<T>& ZWP<T>::operator=(const ZSP<U>& p)
{
	ZWP(p).SwapPtr(*this);

	return *this;
}


//template<typename T>
//inline T& ZWP<T>::operator*()const
//{
//	assert(m_Ptr);
//	assert(ZMemoryAllocator::IsActivePtr(m_Ptr));
//
//	return *m_Ptr;
//}
//
//template<typename T>
//inline T* ZWP<T>::operator->()const
//{
//	assert(m_Ptr);
//	assert(ZMemoryAllocator::IsActivePtr(m_Ptr));
//
//	return m_Ptr;
//}

template<typename T>
inline bool ZWP<T>::operator==(T* val)
{
	return m_Ptr == val;
}

template<typename T>
inline bool ZWP<T>::operator==(nullptr_t nullval)
{
	return m_Ptr == nullptr;
}

template<typename T>
inline bool ZWP<T>::operator!=(T* val)
{
	return m_Ptr != val;
}

template<typename T>
inline bool ZWP<T>::operator!=(nullptr_t nullval)
{
	return m_Ptr != nullptr;
}

template<typename T>
inline ZWP<T>::operator bool() const
{
	return m_Ptr != nullptr;
}

template<typename T>
inline bool ZWP<T>::operator!() const
{
	return m_Ptr == nullptr;
}

template<typename T>
inline T* ZWP<T>::GetPtr()const
{
	return m_Ptr;
}

template<typename T>
inline ZSP<T> ZWP<T>::Lock()const
{
	ZSP<T> p(*this);
	
	return p;
}

template<typename T>
inline bool ZWP<T>::IsActive()const
{
	if (m_Ptr == nullptr)return false;

	if (m_pRefCountObj == nullptr)return false;

	return m_pRefCountObj->GetSPRefCnt() != 0;
}

template<typename T>
inline void ZWP<T>::SwapPtr(ZWP<T>& p)
{
	std::swap(m_Ptr, p.m_Ptr);
	std::swap(m_pRefCountObj, p.m_pRefCountObj);
}

template<typename T>
inline void ZWP<T>::Reset()
{
	ZWP().SwapPtr(*this);
}

template<typename T, typename U>
inline bool operator==(const ZUP<T>& left, const ZUP<U>& right)
{
	return (left.GetPtr() == right.GetPtr());
}

template<typename T, typename U>
inline bool operator!=(const ZUP<T>& left, const ZUP<U>& right)
{
	return (left.GetPtr() != right.GetPtr());
}

template<typename T, typename U>
inline bool operator<(const ZUP<T>& left, const ZUP<U>& right)
{
	return (left.GetPtr() < right.GetPtr());
}

template<typename T, typename U>
inline bool operator>=(const ZUP<T>& left, const ZUP<U>& right)
{
	return (left.GetPtr() >= right.GetPtr());
}

template<typename T, typename U>
inline bool operator>(const ZUP<T>& left, const ZUP<U>& right)
{
	return (left.GetPtr() > right.GetPtr());
}

template<typename T, typename U>
inline bool operator<=(const ZUP<T>& left, const ZUP<U>& right)
{
	return (left.GetPtr() <= right.GetPtr());
}

template<typename T>
inline bool operator==(const ZUP<T>& left, nullptr_t)
{
	return (left.GetPtr() == nullptr);
}

template<typename T>
inline bool operator==(nullptr_t, const ZUP<T>& right)
{
	return (nullptr == right.GetPtr());
}

template<typename T>
inline bool operator!=(const ZUP<T>& left, nullptr_t)
{
	return (left.GetPtr() != nullptr);
}

template<typename T>
inline bool operator!=(nullptr_t, const ZUP<T>& right)
{
	return (nullptr != right.GetPtr());
}

template<typename T>
inline bool operator<(const ZUP<T>& left, nullptr_t)
{
	return (left.GetPtr() < static_cast<typename ZUP<T>::elemType*>(nullptr));
}

template<typename T>
inline bool operator<(nullptr_t, const ZUP<T>& right)
{
	return (static_cast<typename ZUP<T>::elemType*>(nullptr) < right.GetPtr());
}

template<typename T>
inline bool operator>=(const ZUP<T>& left, nullptr_t)
{
	return (left.GetPtr() >= static_cast<typename ZUP<T>::elemType*>(nullptr));
}

template<typename T>
inline bool operator>=(nullptr_t, const ZUP<T>& right)
{
	return (static_cast<typename ZUP<T>::elemType*>(nullptr) >= right.GetPtr());
}

template<typename T>
inline bool operator>(const ZUP<T>& left, nullptr_t)
{
	return (left.GetPtr() > static_cast<typename ZUP<T>::elemType*>(nullptr));
}

template<typename T>
inline bool operator>(nullptr_t, const ZUP<T>& right)
{
	return (static_cast<typename ZUP<T>::elemType*>(nullptr) > right.GetPtr());
}

template<typename T>
inline bool operator<=(const ZUP<T>& left, nullptr_t)
{
	return (left.GetPtr() <= static_cast<typename ZUP<T>::elementTpe*>(nullptr));
}

template<typename T>
inline bool operator<=(nullptr_t, const ZUP<T>& right)
{
	return (static_cast<typename ZUP<T>::elemType*>(nullptr) <= right.GetPtr());
}


#pragma endregion


template<typename T>
inline ZSP<T> ZEnable_Shared_From_This<T>::SharedFromThis()
{
	return ZSP<T>(m_WP);
}

template<typename T>
inline ZSP<const T> ZEnable_Shared_From_This<T>::SharedFromThis()const
{
	return ZSP<const T>(m_WP);
}

template<typename T>
inline ZWP<T> ZEnable_Shared_From_This<T>::WeakFromThis()
{
	return m_WP;
}

template<typename T>
inline ZWP<const T> ZEnable_Shared_From_This<T>::WeakFromThis()const
{
	return m_WP;
}

template<typename T>
inline ZEnable_Shared_From_This<T>::ZEnable_Shared_From_This(const ZEnable_Shared_From_This&) : m_WP()
{
}

template<typename T>
inline ZEnable_Shared_From_This<T>& ZEnable_Shared_From_This<T>::operator=(const ZEnable_Shared_From_This<T>&)
{
	return *this;
}
