#include "PCH/pch.h"
#include "SubSystems.h"

namespace EzLib
{
namespace ZECS
{
	EntityComponentSystem::EntityComponentSystem()
		: m_CompRefrection(&m_CompMemories),
		m_UseMultiThread(false)
	{
	}

	EntityComponentSystem::~EntityComponentSystem()
	{
		m_CompRefrection.Release();
		RemoveAllEntity();

		for (auto& d : m_CompMemories)
			d.second.clear();

		__ECSComponentBase::Release();

		m_CompMemories.clear();
	}

	ZSP<ECSEntity> EntityComponentSystem::MakeEntityFromJson(const json11::Json & jsonObj)
	{
		// エンティティ作成
		auto entity = ECS.MakeEntity();
		// UUID
		{
			auto& jsonUUID = jsonObj["UUID"];
			if (jsonUUID.is_null() == false)
				entity->SetUUID(ZUUID(jsonUUID.string_value().c_str()));
		}

		auto jsonComps = jsonObj["CompList"].array_items();
		for (auto& jsonComp : jsonComps)
		{
			// 文字列からクラスインスタンスを生成する（ClassReflectionクラス使用）
			auto comp = ECS.InstantiateComponent(jsonComp.string_value().c_str());
			// コンポーネント追加
			ECS.AddComponent(entity, comp);

			auto& initJson = jsonObj[jsonComp.string_value()];
			if (initJson.is_null() == false)
			{
				auto pComp = ECS.GetComponent(entity->m_EntityHandle, comp.ComponentID);
				pComp->InitFromJson(initJson);
			}
		}

		
		// リスナーに通知
		for (auto& listener : m_ECSListeners)
		{
			if (listener->NotifyOnAllEntityOperations())
			{
				listener->OnMadeEntityFromJson(entity->m_EntityHandle);
				continue;
			}

			ComponentBitSet tmpBit = listener->GetComponentBitSet();
			tmpBit &= entity->m_CompBitSet;
			if (tmpBit == 0) continue;

			listener->OnMadeEntityFromJson(entity->m_EntityHandle);
		}

		return entity;
	}

	void EntityComponentSystem::RemoveEntity(ECSEntity& entity)
	{
		std::lock_guard<std::mutex> lg(m_EntityMtx);
		_RemoveEntityComps(entity.m_EntityHandle);
		entity.m_EntityHandle = NULL_ENTITY_HANDLE;
	}

	void EntityComponentSystem::RemoveAllEntity()
	{
		std::lock_guard<std::mutex> lg(m_EntityMtx);
		for(size_t i = m_Entities.size()-1;m_Entities.size() > 0; i = m_Entities.size() - 1)
			_RemoveEntityComps(i);
		
		m_Entities.resize(0);
	}

	void EntityComponentSystem::UpdateSystems(ECSSystemList& systems,const float delta, bool notUseLateUpdate)
	{
		if (systems.Size() <= 0) return;

		std::lock_guard<std::mutex> lg(m_ECSMtx);
	
		for (uint32 updateCnt = 0; updateCnt < 2; updateCnt++)
		{
			if(updateCnt == 1 && notUseLateUpdate == true)
				return;

			for (uint32 updateIndex = 0; updateIndex < systems.Size(); updateIndex++)
			{
				if (systems[updateIndex]->IsActive() == false)
					continue;

				auto& compTypes = systems[updateIndex]->GetComponentTypes();
				
				// 要求されたコンポーネントが0個でも更新
				if(compTypes.size() == 0)
				{
					if (updateCnt == 0)
						systems[updateIndex]->UpdateComponents(delta, nullptr);
					else
						systems[updateIndex]->LateUpdateComponents(delta, nullptr);
					continue;
				}

				UpdateComponentPackageList compPackages = CollectComponentPackages(systems[updateIndex]);
				bool isLateUpdate = (updateCnt == 1);
				_UpdateSystem(systems[updateIndex], delta, compPackages, isLateUpdate);
			
			}	//	for (uint32 updateIndex = 0; updateIndex < systems.Size(); updateIndex++)
		
		} // for (uint32 updateCnt = 0; updateCnt < 2; updateCnt++)
	}

	void EntityComponentSystem::EnableUseMultiThread()
	{
		std::lock_guard<std::mutex> lg(m_ECSMtx);
		m_UseMultiThread = true;
	}

	void EntityComponentSystem::DisableUseMultiThread()
	{
		std::lock_guard<std::mutex> lg(m_ECSMtx);
		m_UseMultiThread = false;
	}

	void EntityComponentSystem::_RemoveEntityComps(EntityHandle entityHandle)
	{
		if (entityHandle == NULL_ENTITY_HANDLE)
			return;
		
		auto& entityComps = HandleToEntityComps(entityHandle);
		
		// リスナーに通知
		for (auto& listener : m_ECSListeners)
		{
			if (listener->NotifyOnAllEntityOperations())
			{
				listener->OnRemoveEntity(entityHandle);
				continue;
			}

			ComponentBitSet tmpBit = listener->GetComponentBitSet();
			tmpBit &= HandleToEntitySptr(entityHandle)->m_CompBitSet;
			if (tmpBit == 0) continue;

			listener->OnRemoveEntity(entityHandle);
		}

		for(auto comp : entityComps)
			DeleteComponent(comp.first, comp.second);

		uint32 destIndex = entityHandle;
		uint32 srcIndex = m_Entities.size() - 1;
		
		m_Entities[destIndex] = m_Entities[srcIndex];
		m_Entities.pop_back();
		
		if (destIndex >= m_Entities.size())
			return;
		m_Entities[destIndex].first->m_EntityHandle = destIndex;
	}

	void EntityComponentSystem::DeleteComponent(uint32 componentID,uint32 compIndex)
	{
		ComponentMemory& compArray = m_CompMemories[componentID];
		ECSComponentFreeFunction freeFunc = __ECSComponentBase::GetTypeFreeFunction(componentID);
		size_t typeSize = __ECSComponentBase::GetTypeSize(componentID);
		uint32 srcIndex = compArray.size() - typeSize;

		auto* destComponent = (__ECSComponentBase*)(&compArray[compIndex]);
		auto* srcComponent = (__ECSComponentBase*)(&compArray[srcIndex]);
		freeFunc(destComponent);
		
 		if(compIndex == srcIndex)
		{
			compArray.resize(srcIndex);
			return;
		}
		memcpy(destComponent, srcComponent, typeSize);

		// 一応登録されているかチェック
		srcComponent->m_MemoryIndex = compIndex;

		auto& srcComponents = HandleToEntityComps(srcComponent->m_Entity->m_EntityHandle);
		
		auto& it = srcComponents.find(componentID);

		if (it != srcComponents.end() && (*it).second == srcIndex)
			srcComponents[componentID] = compIndex;

		compArray.resize(srcIndex);
	}

	EntityComponentSystem::UpdateComponentPackageList EntityComponentSystem::CollectComponentPackages(ZSP<ECSSystemBase>& system)
	{
		UpdateComponentPackageList packageList;
		auto compTypes = system->GetComponentTypes();
		
		if (compTypes.size() == 0) return packageList;

		// 単一のコンポーネントなら
		else if (compTypes.size() == 1)
		{
			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[0]);
			ComponentMemory& compMemArray = m_CompMemories[compTypes[0]];
			size_t numComps = compMemArray.size() / typeSize;
			packageList.resize(numComps);
			for (uint32 i = 0; i < compMemArray.size(); i += typeSize)
			{
				size_t entityIndex = i / typeSize;
				__ECSComponentBase* comp = (__ECSComponentBase*)&compMemArray[i];
				packageList[entityIndex].push_back(comp);
			}
			return packageList;
		}
		// 複数のコンポーネントなら
		else if(compTypes.size() > 1)
		{
			auto& bitSet = system->GetBitSet();

			// systemのコンポーネントフラグ取得
			const ZSVector<uint32>& compFlags = system->GetComponentFlags();

			ComponentArray compMemArray(compTypes.size());
			for (uint32 i = 0; i < compTypes.size(); i++)
				compMemArray[i] = &m_CompMemories[compTypes[i]];

			// 要求された中で一番インスタンスの少ないコンポーネントタイプのインデックスを取得
			uint32 minSizeIndex = FindLeastCommonComponent(compTypes, compFlags);

			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[minSizeIndex]);
			ComponentMemory& compArray = *compMemArray[minSizeIndex]; // 一番少ないコンポーネントの配列
			size_t numConpnents = compArray.size() / typeSize;
			packageList.reserve(numConpnents);
			
			for (uint32 i = 0; i < compArray.size(); i += typeSize)
			{
				// コンポーネントをもつエンティティがシステムの要求するコンポーネントを全て持っているか
				auto comp = (__ECSComponentBase*)(&(compArray[i]));
				auto tmpbit = bitSet & comp->m_Entity->m_CompBitSet;
				if (tmpbit != bitSet) continue;
				
				packageList.emplace_back();
				UpdateComponentPackage& package = packageList.back();
				package.resize(compTypes.size());
				package[minSizeIndex] = (__ECSComponentBase*)(&(compArray[i]));

				// コンポーネントからEntity取得

				auto& entityComponents = HandleToEntityComps(comp->m_Entity->m_EntityHandle);

				// 取得したEntityから残りのコンポーネント取得
				for (uint32 j = 0; j < compTypes.size(); j++)
				{
					if (j == minSizeIndex) continue;
					package[j] = _GetComponent(entityComponents, *compMemArray[j], compTypes[j]);
				}

			} // for (uint32 i = 0; i < compArray.size(); i += typeSize)

		} // else if(compTypes.size() > 1)

		return packageList;
	}

	void EntityComponentSystem::_UpdateSystem(ZSP<ECSSystemBase> updateSystem, float delta, UpdateComponentPackageList& pacageList, bool isLateUpdate)
	{
		// ECS自体、またはシステムのマルチスレッドでの更新が有効でなければシングルスレッドで更新
		if (m_UseMultiThread == false || updateSystem->UseMultiThread() == false)
		{
			for (auto& compPackage : pacageList)
			{
				if (isLateUpdate == false)
					updateSystem->UpdateComponents(delta, &compPackage[0]);
				else
					updateSystem->LateUpdateComponents(delta, &compPackage[0]);
			}
			return;
		}

		auto execTask =
			[this, &pacageList, isLateUpdate, updateSystem, delta](ZThreadPool::ZThreadTaskData data)
		{
			if (isLateUpdate == false)
				updateSystem->UpdateComponents(delta, &pacageList[data.TaskIndex][0]);
			else
				updateSystem->LateUpdateComponents(delta, &pacageList[data.TaskIndex][0]);
		};
		
		size_t numTask = pacageList.size();
		THPOOL.AddTask(numTask, max(1, numTask / (THPOOL.GetNumThreads()-1)), execTask);
		THPOOL.WaitForAllTasksFinish();
	}

	uint32 EntityComponentSystem::FindLeastCommonComponent(const ZSVector<uint32>& compTypes, const ZSVector<uint32>& compFlags)
	{
		// 指定されたコンポーネント一覧の中で一番保持している数が少ないコンポーネントのIDを取得
		
		uint32 minSize = (uint32)-1; // オーバーフロー -> uint32の最大値
		uint32 minIndex = (uint32)-1; // オーバーフロー -> uint32の最大値

		for(uint32 i = 0;i<compTypes.size();i++)
		{
			if ((compFlags[i]& ECSSystemBase::FLAG_OPTIONAL) != 0)
				continue;

			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[i]);

			uint32 size = m_CompMemories[compTypes[i]].size()/ typeSize;
			if (size > minSize) continue;
			minSize = size;
			minIndex = i;
		}

		return minIndex;
	}

}
}