#ifndef __ECS_H__
#define __ECS_H__

#include "PCH/pch.h"
#include "ECSCommon.h"
#include "ZComponentRefrection.h"

namespace EzLib
{
namespace ZECS
{
	class ECSEntity;

	class EntityComponentSystem
	{
	private:
		using UpdateComponentPackageList = ZVector<UpdateComponentPackage>;

	public:
		// コピー禁止
		EntityComponentSystem(EntityComponentSystem&) = delete;
		EntityComponentSystem& operator=(EntityComponentSystem&) = delete;

		void AddListener(ECSListener* listener);
		void RemoveListener(ECSListener* listener);
		void RemoveAllListener();

		#pragma region Entity Functions

		ZSP<ECSEntity> MakeEntity(__ECSComponentBase** components, const uint32* componentIDs, size_t numComponents);
		template<typename ...Args>
		ZSP<ECSEntity> MakeEntity(Args* ...args);
		
		// JsonからEntity作成
		ZSP<ECSEntity> MakeEntityFromJson(const json11::Json& jsonObj);

		void RemoveEntity(ECSEntity& handle);
		void RemoveAllEntity();

		uint32 GetNumEntities()const;

		#pragma endregion

		#pragma region Component Functoins

		template<class Component>

		void AddComponent(ZSP<ECSEntity>& entity, const Component* component);
		void AddComponent(ZSP<ECSEntity>& entity, const InstanceData& instanceData);

		template<class Component>
		bool RemoveComponent(EntityHandle entityHandle);

		template<class Component>
		Component* GetComponent(EntityHandle entityHandle);

		__ECSComponentBase* GetComponent(EntityHandle entityHandle, uint32 compID);

		template<class Component>
		size_t GetNumComponent();
		
		template<class Component>
		Component* MakeComponent();

		template<class Class>
		void RegisterClassReflection(const ZString& className);

		InstanceData InstantiateComponent(const ZString& className);

		#pragma endregion

		#pragma region System Function

		void UpdateSystems(ECSSystemList& systems,const float delta,bool notUseLateUpdate = false);

		#pragma endregion

		void EnableUseMultiThread();

		void DisableUseMultiThread();

	#pragma region Singleton

	public:
		static EntityComponentSystem& GetInstance()
		{
			static EntityComponentSystem m_Instance;
			return m_Instance;
		}

	private:
		EntityComponentSystem();
		~EntityComponentSystem();

	#pragma endregion

	private:
		ZSP<ECSEntity>& HandleToEntitySptr(EntityHandle entityHandle);
		EntityComps& HandleToEntityComps(EntityHandle entityHandle);
		
		void _RemoveEntityComps(EntityHandle entityHandle);
		void DeleteComponent(uint32 componentID, uint32 compIndex);
		bool _RemoveComponent(EntityHandle entityHandle, uint32 componentID);
		void _AddComponent(ZSP<ECSEntity>& entity, EntityComps& entityComps, uint32 componentID,__ECSComponentBase* component);
		__ECSComponentBase* _GetComponent(EntityComps& entityComponents, ComponentMemory& compMemory, uint32 componentID);

		UpdateComponentPackageList CollectComponentPackages(ZSP<ECSSystemBase>& system);
		void _UpdateSystem(ZSP<ECSSystemBase> updateSystem, float delta, UpdateComponentPackageList& pacageList, bool isLateUpdate);

		uint32 FindLeastCommonComponent(const ZSVector<uint32>& compTypes, const ZSVector<uint32>& compFlags);

	private:
		bool m_UseMultiThread;	// マルチスレッドを使うか
		
		ComponentMemoriesByID m_CompMemories;		// id & components raw data

		ZAVector<EntityCompsPair> m_Entities;	// ZSP<ECSEntity> & (compID & compIndex)
		ZAVector<ECSListener*> m_ECSListeners;
		ZComponentRefrection m_CompRefrection;		// コンポーネント用クラスリフレクション
		
		// スレッドセーフのためのMutex
		std::mutex m_EntityMtx;
		std::mutex m_CompMtx;
		std::mutex m_ECSMtx;

	};

}
}

#include "ECSEntity.h"
#include "ECSComponent.h"
#include "ECSSystem.h"
#include "ECSListener.h"

namespace EzLib
{
namespace ZECS
{
#include "ECS.inl"
}
}

#endif