#ifndef __ECS_ENTITY_H__
#define __ECS_ENTITY_H__

#ifndef ECS
#define ECS EzLib::ZECS::EntityComponentSystem::GetInstance()
#endif

#include "ECSCommon.h"
#include "Utils/DebugLog.h"

namespace EzLib
{
namespace ZECS
{
	// AddComponent関数の都合上ECSEntityのインスタンスはshared_ptrで管理されている必要あり
	class ECSEntity : public ZEnable_Shared_From_This<ECSEntity>
	{
	private:
		// 統括クラスのみm_EntityHandleを直にいじれるように
		friend class EntityComponentSystem;

	public:
		ECSEntity(EntityHandle handle = NULL_ENTITY_HANDLE)
			: m_EntityHandle(handle), m_UUID(EzLib::CreateUUID())
		{
		}

		ECSEntity(ECSEntity&) = delete;

		virtual ~ECSEntity();
		
		// ※ ECSではエンティティは生成順にvectorに追加され、
		//   エンティティ削除時はvectorの最後尾に移動させた後にpop_backしているので、
		//   vectorのclear関数で一括削除すると存在しないインデックスを参照してしまい、
		//   エラー落ちするので一括でエンティティを削除する際はECSEntityクラスに用意した
		//   専用の関数を使って削除する
		static void RemoveAllEntity(ZAVector<ZSP<ECSEntity>>& entityList);

		void Remove();

		// コンポーネント追加
		template<typename Component>
		void AddComponent();

		// コンポーネント取得
		template<typename Component>
		Component* GetComponent();
		
		// 指定コンポーネントを持っているか
		template<typename Component>
		bool HasComponent();

		// コンポーネント削除
		template<typename Component>
		void RemoveComponent();

		// 生きているか?
		bool IsActive();

		// UUID取得
		const ZUUID& GetUUID()const;
		// UUID設定
		void SetUUID(const ZUUID& uuid);
		
		static void MakeEntityFromJsonFile(const ZString& fileName,ZAVector<ZSP<ECSEntity>>* list);
		static void MakeEntityFromJson(const json11::Json& jsonObj, ZAVector<ZSP<ECSEntity>>* list);

	private:
		EntityHandle m_EntityHandle;
		ComponentBitSet m_CompBitSet;
		ZUUID m_UUID;
		
	};

}
}

namespace EzLib
{
namespace ZECS
{
#include "ECSEntity.inl"
}
}

#endif