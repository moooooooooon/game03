#include "ECSListener.h"

namespace EzLib
{
namespace ZECS
{
	ECSListener::ECSListener()
		: m_IsNotifyOnAllComponentOperations(false),
		  m_IsNotifyOnAllEntityOperations(false)
	{
	}

	ECSListener::~ECSListener()
	{
	}

	void ECSListener::OnMakeEntity(EntityHandle handle)
	{
	}

	void ECSListener::OnMadeEntityFromJson(EntityHandle handle)
	{
	}

	void ECSListener::OnRemoveEntity(EntityHandle hadle)
	{
	}

	void ECSListener::OnAddComponent(EntityHandle handle, uint32 id)
	{
	}

	void ECSListener::OnRemoveComponent(EntityHandle handle, uint32 id)
	{
	}
}
}