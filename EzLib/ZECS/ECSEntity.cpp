#include "PCH/pch.h"
#include "ECSEntity.h"
#include "SubSystems.h"

namespace EzLib
{
namespace ZECS
{
	ECSEntity::~ECSEntity()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	void ECSEntity::RemoveAllEntity(ZAVector<ZSP<ECSEntity>>& entityList)
	{
		if (entityList.empty())return;

		ZSVector<ZSP<ECSEntity>> removeTargetEntities;
		removeTargetEntities.reserve(entityList.size() / 2);
	
		// Handleがnullptrの要素を削除
		for (size_t i = 0; i < entityList.size(); i++)
		{
			if (entityList[i]->m_EntityHandle != NULL_ENTITY_HANDLE)
				removeTargetEntities.push_back(entityList[i]);
		}
	
		entityList.clear();

		// 昇順ソート
		std::sort(removeTargetEntities.begin(), removeTargetEntities.end(),
			[](ZSP<ECSEntity>& l, ZSP<ECSEntity>& r)
		{
			return l->m_EntityHandle < r->m_EntityHandle;
		});
	
		for (uint32 i = 0; i < removeTargetEntities.size(); i++)
			ECS.RemoveEntity(*removeTargetEntities[i].GetPtr());
	
		removeTargetEntities.clear();
		removeTargetEntities.shrink_to_fit();
	
	}

	void ECSEntity::Remove()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	void ECSEntity::MakeEntityFromJsonFile(const ZString & fileName, ZAVector<ZSP<ECSEntity>>* list)
	{
		// json ファイル読み込み
		std::string error;
		json11::Json json = LoadJsonFromFile(fileName.c_str(), error);
		if (error.size() != 0)
		{
			DW_SCROLL(2, "json 読み込みエラー(%s)", fileName.c_str());
			return;
		}

		MakeEntityFromJson(json,list);
	}

	void ECSEntity::MakeEntityFromJson(const json11::Json & jsonObj, ZAVector<ZSP<ECSEntity>>* list)
	{
		auto& jsonEntities = jsonObj["Objects"].array_items();
		
		// エンティティ作成
		for (auto& jsonEntity : jsonEntities)
			list->push_back(ECS.MakeEntityFromJson(jsonEntity));
	}

}
}
