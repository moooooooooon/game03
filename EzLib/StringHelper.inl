inline ZVector<ZString> SplitString(const ZString& string, const ZString& delimiters)
{
	size_t start = 0;
	size_t end = string.find_first_of(delimiters);

	ZVector<ZString> result;
	while (end <= ZString::npos)
	{
		ZString s = string.substr(start, end - start);
		if (s.empty() == false)
			result.push_back(s);

		if (end == ZString::npos) break;

		start = end + 1;
		end = string.find_first_of(delimiters, start);
	}
	return result;
}

inline ZVector<ZString> SplitString(const ZString& string, const char delimiter)
{
	return SplitString(string, ZString(1, delimiter));
}

inline ZVector<ZString> Tokenize(const ZString& string)
{
	return SplitString(string, " \t\n");
}

inline const char* FindToken(const char* str, const ZString& token)
{
	const char* t = str;
	while (t = strstr(t, token.c_str()))
	{
		bool left = str == t || isspace(t[-1]);
		bool right = !(t[token.size()]) || isspace(t[token.size()]);

		if (left && right) return t;

		t += token.size();
	}

	return nullptr;
}

inline const char* FindToken(const ZString& string, const ZString& token)
{
	return FindToken(string.c_str(), token);
}

inline int32 FindStringPosition(const ZString& string, const ZString& search, uint offset)
{
	const char* str = string.c_str() + offset;
	const char* found = strstr(str, search.c_str());
	if (found == nullptr) return -1;
	return (int32)(found - str) + offset;
}

inline ZString StringRange(const ZString& string, uint start, uint length)
{
	return string.substr(start, length);
}

inline ZString RemoveStringRange(const ZString& string, uint start, uint length)
{
	ZString result = string;
	return result.erase(start, length);
}

inline ZString GetBlock(const char* str, const char** outPosition)
{
	const char* end = strstr(str, "}");
	if (!end) return ZString(str);

	if (outPosition)*outPosition = end;
	uint length = end - str + 1;
	return ZString(str, length);
}

inline ZString GetBlock(const ZString& string, uint offset)
{
	const char* str = string.c_str() + offset;
	return GetBlock(str);
}

inline ZString GetStatement(const char* str, const char** outPosition)
{
	const char* end = strstr(str, ";");
	if (!end)return ZString(str);
	if (outPosition) *outPosition = end;
	uint length = end - str + 1;
	return ZString(str, length);
}

inline bool StringContains(const ZString& string, const ZString& chars)
{
	return string.find(chars) != ZString::npos;
}

inline bool StartsWith(const ZString& string, const ZString& start)
{
	return string.find(start) == 0;
}

inline int32 NextInt(const ZString& string)
{
	const char* str = string.c_str();
	for (uint i = 0; i < string.size(); i++)
	{
		if (isdigit(string[i]))
			return atoi(&string[1]);
	}
	return -1;
}
