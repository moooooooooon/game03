//===============================================================
//   サウンド関係
// 
// 	ZSoundManagerクラス	… 再生中のサウンド管理など。\n
// 	ZSoundDataクラス		… サウンドの読み込みや、実際に再生するインスタンスの再生など。\n
// 	ZSoundクラス			… 実際再生されるサウンドインスタンス。ZSoundDataクラスから生成される。\n
// 
//===============================================================
#ifndef ZSound_h
#define ZSound_h

namespace EzLib
{

	class ZSoundData;

	//=======================================================
	//   再生用サウンドインスタンスクラス
	// 
	//   実際に再生するサウンドオブジェクト				\n
	//   ZSoundDataクラスのCreateInstance()で作成可能	\n
	// 
	//  @ingroup Sound
	//=======================================================
	class ZSound : public ZEnable_Shared_From_This<ZSound>
	{
		friend class ZSoundData;
	public:
		//==================================================================
		// 再生
		//==================================================================
		//   2D再生
		//  	loop	… ループ再生するか？
		void Play(bool loop = false);

		//   3D再生　※モノラルサウンドのみ3D再生可能です
		//  	pos		… 座標
		//  	loop	… ループ再生するか？
		void Play3D(const ZVec3& pos, bool loop = false,float rad=1);

		//==================================================================
		// 停止
		//==================================================================
		//   停止
		void Stop()
		{
			if (m_Inst)m_Inst->Stop();
		}

		//   一時停止
		void Pause()
		{
			if (m_Inst)m_Inst->Pause();
		}

		//   再開
		void Resume()
		{
			if (m_Inst)m_Inst->Resume();
		}

		void Update();
		
		//==================================================================
		// 設定
		//==================================================================
		//   ボリューム設定
		//  	vol	… ボリューム設定(1.0が100%)
		void SetVolume(float vol);

		float GetVolume() { return m_Volume; }

		//   3Dサウンド座標設定
		//  	pos		… 座標
		void SetPos(const ZVec3& pos);
		
		void SetRadius(float rad) {
			m_Rad = rad;
		}


		//==================================================================
		// 取得
		//==================================================================
		//   再生中か？
		//  @return 再生中ならtrue
		bool IsPlay();

		//==================================================================
		~ZSound();

	private:
		// 
		ZSound()
		{
			m_Rad = m_Volume = 1.f;
		}

	private:

		uptr<DirectX::SoundEffectInstance>		m_Inst;
		ZSP<ZSoundData>							m_srcData;
		bool									m_b3D = false;
		float									m_Volume;
		ZVec3									m_SoundPos;
		float									m_Rad;
	};

	//=======================================================
	//   サウンドデータクラス
	// 
	// 　サウンドの元データとなるクラス				\n
	// 　これ自体では再生できません					\n
	// 　CreateInstance()で実際に再生するサウンドZSoundクラスのインスタンスを生成	\n
	// 
	//  @ingroup Sound
	//=======================================================
	class ZSoundData : public ZIResource
	{
		friend class ZSound;
	public:
		//==================================================================
		//   WAVEサウンド読み込み
		//  	filename	… ファイル名
		//==================================================================
		bool LoadWaveFile(const ZString& fileName);

		//==================================================================
		//   再生用のサウンドインスタンス作成
		//  	b3D			… 3Dサウンドにするか？
		//  @return 新たに作成した再生用サウンドインスタンス
		//==================================================================
		ZSP<ZSound> CreateInstance(bool b3D);

		//==================================================================
		//   リソースタイプ取得
		//==================================================================
		virtual ZString GetTypeName() const override
		{
			ZString str = "ZSoundData : ";
			str += m_FileName;
			return str;
		}

		//==================================================================

		ZSoundData()
		{
		}

		~ZSoundData()
		{
			m_SoundEffect = nullptr;
			m_FileName = "";
		}


	private:
		ZUP<DirectX::SoundEffect> m_SoundEffect;

		ZString					m_FileName;


	};

	//================================================
	// 
	//   サウンド管理 シングルトンクラス
	// 
	//  
	//  @ingroup Sound
	//================================================
	class ZSoundManager
	{
		friend class ZSound;
	public:
		//==================================================================
		// 取得系
		//==================================================================
		//   AudioEngine取得
		ZUP<DirectX::AudioEngine>& GetAudioEngine() { return m_AudioEng; }

		//   3Dリスナー(聞き手)取得
		DirectX::AudioListener&	GetListener() { return m_Listener; }

		//   初期化されているか？
		bool	IsInit() { return (m_AudioEng != nullptr); }

		//   現在再生中のサウンド数取得
		int		GetNumPlaying() { return (int)m_PlayList.size(); }

		//==================================================================
		// 初期化・解放
		//==================================================================

		//   初期化
		bool Init();

		//   解放
		void Release();

		//==================================================================
		// 共通
		//==================================================================
		//   すべて停止
		void StopAll();

		//==================================================================
		// 処理
		//==================================================================
		//   処理　定期的に実行してください
		//  再生中のサウンドの管理やリスナーの設定繁栄などを行っています
		void Update();

		//==================================================================
		~ZSoundManager();

	private:

		ZUP<DirectX::AudioEngine>				m_AudioEng;			// AudioEngine


		DirectX::AudioListener					m_Listener;			// 3Dサウンド用リスナー

		ZSMap<UINT, ZSP<ZSound>>			m_PlayList;			// 現在再生中のサウンドリスト

	private:
		// 複製リスト追加。実際に再生されているものになる。
		void AddPlayList(const ZSP<ZSound>& snd)
		{
			if (snd == nullptr)return;
			m_PlayList[(UINT)snd.GetPtr()] = snd;
		}

		//===================================================================
		// シングルトン
	private:
		ZSoundManager();

	public:
		static ZSoundManager &GetInstance()
		{
			static ZSoundManager Instance;
			return Instance;
		}
		//===================================================================
	};


}

#endif
