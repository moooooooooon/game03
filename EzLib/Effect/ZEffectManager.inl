inline Effekseer::Manager* ZEffectManager::GetManager()const
{
	return m_ESManager;
}

inline EffekseerRenderer::Renderer* ZEffectManager::GetRenderer()const
{
	return m_ESRenderer;
}

inline EffekseerSound::Sound* ZEffectManager::GetSound()const
{
	return m_ESSound;
}

inline uint16 ZEffectManager::GetServerPort() const
{
	return m_ServerPort;
}

inline ZEffectManager::EffectHandle ZEffectManager::_SubmitPlayEffect(Effekseer::Effect* effect, const ZVec3& pos,const OnPlayEffectFunction& onPlayEffectFunc)
{
	EffectHandle handle = NullEffectHandle;
	if (effect == nullptr) return handle;

	handle = m_PlayEffectCounter++;
	
	m_PlayRequests.emplace_back();
	auto& data = m_PlayRequests.back();
	data.handle = handle;
	data.pEffect = effect;
	data.Pos = std::move(pos);
	data.OnPlayEffectFunc = std::move(onPlayEffectFunc);
	m_ESHandleMap[handle] = NullEffectHandle;

	return handle;
}

inline Effekseer::Handle ZEffectManager::EffectHandleToEffekseerHandle(const EffectHandle& handle)const
{
	Effekseer::Handle effekseerHandle = NullEffectHandle;

	try
	{
		effekseerHandle = m_ESHandleMap.at(handle);
	}
	catch (std::out_of_range&)
	{
	}

	return effekseerHandle;
}

inline ZEffectManager::EffectHandle ZEffectManager::EffekseerHandleToEffectHandle(const Effekseer::Handle& esHandle)const
{
	EffectHandle handle = NullEffectHandle;

	try
	{
		handle = m_EffectHandleMap.at(esHandle);
	}
	catch (std::out_of_range&)
	{
	}

	return handle;
}