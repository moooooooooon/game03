//===============================================================
//   データ管理関係
// 
//===============================================================
#ifndef ZDataManager_h
#define ZDataManager_h

namespace EzLib
{

	//================================================================================
	//   汎用データ保管クラス
	// 
	//  (Singleton無しのFlyWeightパターンのような感じ)	\n
	//   weak_ptr管理:作成はshared_ptrで返す。所有者がいなくなった時点でデータは自動解放される(weak_ptrなので)。		\n
	// 	             map内にノードは残るが、AddData()やGetData()したときに、解放済みのデータがあればmapから解除する。	\n
	// 				 またはOptimize()でも可能。																			\n
	// 
	//   ・マルチスレッド対応(LockTypeに排他制御クラスの型を指定する)
	// 
	//  @ingroup Etc
	//================================================================================
	template<typename T, typename LockType>
	class ZDataWeakStorage
	{
	private:
		using LockGuard = ZLockGuard<LockType>;

	public:
		//==========================================================================
		//   データ追加
		// 
		//  指定キー名のデータが存在しなければ指定型Xを新規追加し返す	\n
		//  既に存在する場合はそれを返す								\n
		//  ※生成する型指定Ver											\n
		// 
		//  	Name		… キー名
		//  	pbCreate	… 生成した場合はtrue　既に存在する場合はfalseが入る
		//  @return 存在する：既存のデータ　存在しない：新規作成したデータ
		//==========================================================================
		template<typename X>
		ZSP<X> AddData_Type(const ZString& Name, bool* pbCreate = nullptr);

		//==========================================================================
		//   データ追加
		// 
		//  指定キー名のデータが存在しなければ型Tを新規追加し返す	\n
		//  既に存在する場合はそれを返す							\n
		// 
		//  	Name		… キー名
		//  	pbCreate	… 生成した場合はtrue　既に存在する場合はfalseが入る
		//  @return 存在する：既存のデータ　存在しない：新規作成したデータ
		//==========================================================================
		ZSP<T> AddData(const ZString& Name, bool* pbCreate = nullptr)
		{
			return AddData_Type<T>(Name, pbCreate);
		}

		//=======================================================
		//   指定名のデータ取得
		//  @return 存在する：データ　存在しない：nullptr
		//=======================================================
		ZSP<T> GetData(const ZString& Name);

		//=======================================================
		//   指定キーのデータを管理マップから削除
		//  	Name		… キー名
		//=======================================================
		void DelData(const ZString& Name);

		//=======================================================
		//   解放
		//  全てのデータを消す
		//=======================================================
		void Release();

		//=======================================================
		//   最適化
		// 	破壊済みのノードを消す
		//=======================================================
		void Optimize();

		//=======================================================
		//   ノード数取得
		//=======================================================
		UINT GetNodeNum()
		{
			// 排他制御
			ZLockGuard<LockType> lock(m_Lock);

			return m_DataMap.size();
		}

		//=======================================================
		//   全リソースマップを取得
		//  ※マップ自体は排他制御でコピーして返しているが、内部データはマルチスレッドアクセス時には注意
		//=======================================================
		ZAUnorderedMap<ZString, const ZSP<T>> GetDataMap()
		{
			// 排他制御
			ZLockGuard<LockType> lock(m_Lock);

			return (ZAUnorderedMap<ZString,const ZSP<T>>&)m_DataMap;
		}


	public:

		// 
		ZDataWeakStorage() : m_Cnt(0)
		{
		}
		// 
		virtual ~ZDataWeakStorage()
		{
			Release();
		}

	private:
		ZAUnorderedMap<ZString, ZSP<T> >		m_DataMap;		// 登録map
		std::atomic<int>						m_Cnt;		// 定期的な最適化用カウンタ
		static const int m_OptimizeCycle;


		// マルチスレッド用排他制御オブジェクト
		//  UtilityのZLocking.hのクラスを指定する
		LockType									m_Lock;


	private:
		// コピー禁止用
		ZDataWeakStorage(const ZDataWeakStorage<T, LockType>& src) {}
		void operator=(const ZDataWeakStorage<T, LockType>& src) {}
	};

	#include "ZDataStorage.inl"


}
#endif