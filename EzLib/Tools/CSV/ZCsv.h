//===============================================================
//   Csv形式のファイル関係
// 
//===============================================================
#ifndef ZCsv_h
#define ZCsv_h

namespace EzLib
{

	//===============================================
	//   CSVファイル入出力クラス
	//  ・日本語文字がカンマや改行と勘違いしないように対応もしてます
	//  @ingroup Etc
	//===============================================
	class ZCsv
	{
	public:
		//========================================================
		//  brief CSVデータ
		//========================================================
		ZVector<ZVector<ZString>> m_Tbl;

		//========================================================
		//  brief CSVファイルから、m_Tblに読み込む
		//  	filename	… CSVファイル名
		//  	bDelTitle	… 最初の１行目は省略するか？
		//========================================================
		bool Load(const ZString& filename, bool bDelTitle = false);

		//========================================================
		//  brief CSV形式文字列から、m_Tblに読み込む
		//  	text		… CSV形式の文字列
		//  	bDelTitle	… 最初の１行目は省略するか？
		//========================================================
		bool LoadFromText(const ZString& text, bool bDelTitle = false);

		//========================================================
		//  brief m_Tblをファイルへ保存
		//  	filename	… CSVファイル名
		//========================================================
		bool Save(const ZString& filename) const;

		//========================================================
		//  brief 指定した列に指定文字列があるか、全行を検索する
		//  	nCol	… 指定列
		//  	str		… 検索文字列
		//  @return 発見：行Index　無い：-1
		//========================================================
		int Search_Col(int nCol, const ZString& str) const
		{
			for (int i = 0; i < (int)m_Tbl.size(); i++)
			{
				if (nCol >= (int)m_Tbl[i].size())continue;

				if (str == m_Tbl[i][nCol])
				{
					return i;
				}
			}
			return -1;
		}

		//========================================================
		//  brief 指定した行に指定文字列があるか、全列を検索する
		//  	nRow	… 指定行
		//  	str		… 検索文字列
		//  @return 発見：列Index　無い：-1
		//========================================================
		int Search_Row(int nRow, const ZString& str) const
		{
			if (nRow >= (int)m_Tbl.size())return -1;

			for (int i = 0; i < (int)m_Tbl[nRow].size(); i++)
			{
				if (str == m_Tbl[nRow][i])
				{
					return i;
				}
			}
			return -1;
		}

		//========================================================
		//  brief 0行目をタグ文字として使い、tagと一致する文字列が何行を求め、nRow行目のtag列目の文字列を返す
		//  	nRow	… 指定行
		//  	tag		… 列を判定するための文字列
		//  @return nRow行目のtag列目の文字列を返す　tagの列が存在しない場合は、""が返る
		//========================================================
		ZString GetCol(int nRow, const ZString& tag) const
		{
			int col = Search_Row(0, tag);
			if (col == -1)return "";

			return m_Tbl[nRow][col];
		}

		//========================================================
		//   0列目をタグ文字として使い縦方向に検索する。
		// 
		//  tagと一致する行があれば、tag列以外のコピーを返す。
		// 
		//   tag		… 行を判定するための文字列
		//  @return 発見した行のコピーを返す。ただしタグ列は含めない。
		//========================================================
		ZVector<ZString> GetRowAll(const ZString& tag) const
		{
			// 0列目をタグとして考え、一致する文字列が何行目にあるかを検索
			int row = Search_Col(0, tag);
			if (row != -1)
			{	// 発見
				// タグ以外をコピーして返す
				ZVector<ZString> ret;
				if (m_Tbl[row].size() == 0)return ret;

				// タグ以外をコピーして返す
				ret.reserve(m_Tbl[row].size());
				for (UINT i = 1; i < m_Tbl[row].size(); i++)
				{
					ret.push_back(m_Tbl[row][i]);
				}
				return std::move(ret);
			}

			return ZVector<ZString>();
		}

		//   データをクリアする
		void Clear()
		{
			m_Tbl.clear();
		}

		/*
		//   配列のようにアクセスできるようにするためオペレータ
		ZVector<ZString>& operator[](int rowIdx) {
			return m_Tbl[rowIdx];
		}
		//   配列のようにアクセスできるようにするためオペレータ
		const ZVector<ZString>& operator[](int rowIdx) const {
			return m_Tbl[rowIdx];
		}
		*/


	};

}
#endif