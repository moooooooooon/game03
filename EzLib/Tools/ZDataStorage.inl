//===================================================================
//
//
// ZDataWeakStorageテンプレートクラスのメンバ関数の実装部分
//
//
//===================================================================

template<typename T,typename LockType>
const int ZDataWeakStorage<T, LockType>::m_OptimizeCycle = 100; // 数値は適当

// データ追加
template<typename T, typename LockType>
template<typename X>
ZSP<X> ZDataWeakStorage<T, LockType>::AddData_Type(const ZString& Name, bool* pbCreate)			// 生成
{

	// 定期的に不要ノードを消す
	m_Cnt++;
	if (m_Cnt % m_OptimizeCycle == 0)
		Optimize();

	ZSP<X> lpData;
	ZString sKey = Name;

	// 排他制御
	LockGuard lock(m_Lock);

	//===============================
	// すでに登録されているかをCheck
	//===============================
	auto p = m_DataMap.find(sKey);
	if (m_DataMap.end() != p)
	{
		// 存在
		if (pbCreate)*pbCreate = false;
		return p->second.DownCast<X>();
	}

	//===========================
	// データを作成
	//===========================
	lpData = Make_Shared(X,appnew);

	m_DataMap[sKey] = lpData;

	if (pbCreate)*pbCreate = true;

	return lpData;
}

// 指定名のデータ取得
template<typename T, typename LockType>
ZSP<T> ZDataWeakStorage<T, LockType>::GetData(const ZString& Name)
{
	// 定期的に不要ノードを消す
	m_Cnt++;
	if (m_Cnt % m_OptimizeCycle == 0)
		Optimize();
	
	ZString sKey;
	if (Name.empty())
		sKey = "";
	else
		sKey = Name;

	// 排他制御
	LockGuard lock(m_Lock);

	// 存在確認
	auto p = m_DataMap.find(sKey);
	if (p == m_DataMap.end())return nullptr;

	// 破壊確認
	if (p->second.GetRefCnt() <= 1) // 参照カウントが1以下 -> 自分しか参照していない
	{
		m_DataMap.erase(p);
		return nullptr;
	}

	// 存在するならreturn
	return p->second.Lock();
}

// 指定キーのデータを管理マップから削除
template<typename T, typename LockType>
void ZDataWeakStorage<T, LockType>::DelData(const ZString& Name)
{
	// 排他制御
	{
		LockGuard lock(m_Lock);

		// 存在確認
		auto p= m_DataMap.find(Name);
		if (p == m_DataMap.end())return;	// 存在しないなら終了

		// 削除
		m_DataMap.erase(p);
	}

	Optimize();
}

// 解放
template<typename T, typename LockType>
void ZDataWeakStorage<T, LockType>::Release()
{
	// 排他制御
	LockGuard lock(m_Lock);

	auto p = m_DataMap.begin();
	while (p != m_DataMap.end())
	{
		p->second.Reset();
		p = m_DataMap.erase(p);
	}
}

// 最適化
template<typename T, typename LockType>
void ZDataWeakStorage<T, LockType>::Optimize()
{
	// 排他制御
	LockGuard lock(m_Lock);

	auto p = m_DataMap.begin();
	while (p != m_DataMap.end())
	{
		if (p->second.GetRefCnt() <= 1)
			p = m_DataMap.erase(p);
		else
			++p;
	}
}
