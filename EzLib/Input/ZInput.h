#ifndef __ZINPUT_H__
#define __ZINPUT_H__

//===========================================================
//
// キーボードやマウスの入力などを管理する
//
//===========================================================
class ZInput
{
public:
	static constexpr uint NumKeys = 256;
	static constexpr uint NumMouseButtons = 3;

public:
	//===============================
	// 共通
	//===============================
	// 定数
	// マウスボタン番号
	enum MOUSEBUTTON
	{
		BUTTON_L   = 0,
		BUTTON_R   = 1,
		BUTTON_MID = 2
	};

	enum KEYFLAG
	{
		Enter = 1 << 0,		// 押した瞬間
		Stay  = 1 << 1,		// 押してる状態
		Exit  = 1 << 2		// 離した瞬間
	};

	// マウス & キーボードの入力情報をすべて0に
	void ClearAllKeyState();
	
	//===============================
	// キーボード
	//===============================
public:
	void UpdateKey();		// 仮想キーの状態を取得し、Keyを更新。

	// キーボード判定
	// KeyNo	… 判定するキーコード
	// flag		… どのタイミングで判定するか
	//   ・Stay		… 押している間
	//   ・Enter	… 押した最初の1回目のみ
	//   ・Exit		… 離した最初の1回目のみ
	inline bool KeyCheck(unsigned char KeyNo, KEYFLAG flag)
	{
		if (m_Key[KeyNo] & flag)return true;
		return false;
	}
	// キーボード判定(押した最初の1回目のみ)
	// KeyNo	… 判定するキーコード
	inline bool KeyEnter(unsigned char KeyNo)
	{
		return KeyCheck(KeyNo, Enter);
	}
	// キーボード判定(押している間)
	// KeyNo	… 判定するキーコード
	inline bool KeyStay(unsigned char KeyNo)
	{
		return KeyCheck(KeyNo, Stay);
	}
	// キーボード判定(離した最初の1回目のみ)
	// KeyNo	… 判定するキーコード
	inline bool KeyExit(unsigned char KeyNo)
	{
		return KeyCheck(KeyNo, Exit);
	}

	// 全キーステート取得
	inline const BYTE* GetKeys()const
	{
		return m_Key;
	}

	// 全キーステート設定
	inline void SetKeys(BYTE* keys)
	{
		memcpy(m_Key, keys, NumKeys);
	}


private:
	BYTE	m_KeyBuffer[NumKeys];	// キーバッファ
	BYTE	m_Key[NumKeys];			// キーボード用フラグ
	
//===============================
// マウス
//===============================
public:
	// FPSゲーム用マウスモード
	//  マウス非表示、座標固定でm_MouseMoveValue(getMouseMoveValue関数)に移動量が入る
	void SetFPSMode(HWND hWnd, bool enable);

	// マウスデータ更新
	void UpdateMouseData(HWND hWnd);

	inline void SetInitMousePos(POINT& pos) {
		m_MousePos = pos;
	}

	// マウスボタン判定
	// button	… 判定するマウスボタン MOUSE_? で指定
	// flag		… どのタイミングで判定するか
	//   ・Stay		… 押している間
	//   ・Enter	… 押した最初の1回目のみ
	//   ・Exit		… 離した最初の1回目のみ
	inline bool MouseCheck(MOUSEBUTTON button, KEYFLAG flag)
	{
		BYTE MouseButton[3] = { VK_LBUTTON, VK_RBUTTON, VK_MBUTTON };
		return KeyCheck(MouseButton[button], flag);
	}
	inline bool MouseEnter(MOUSEBUTTON button)
	{
		return MouseCheck(button, Enter);
	}
	inline bool MouseStay(MOUSEBUTTON button)
	{
		return MouseCheck(button, Stay);
	}
	inline bool MouseExit(MOUSEBUTTON button)
	{
		return MouseCheck(button, Exit);
	}

	// マウスホイールスクロール値設定
	inline void	SetMouseWheelValue(int Val) { m_MouseWheel = Val; }

	// FPSマウス操作モードのON/OFF取得
	inline bool	GetMouseFPSMode() { return m_MouseFPSMode; }
	// マウス座標
	inline const POINT&	GetMousePos() { return m_MousePos; }
	// １つ前のマウス座標
	inline const POINT&	GetMouseOldPos() { return m_MouseOldPos; }
	// マウスが動いた値(ピクセル)
	inline const POINT&	GetMouseMoveValue() { return m_MouseMoveValue; }
	// ホイールを動かした値
	inline int		GetMouseWheel() { return m_MouseWheel; }

	// マウス移動量セット
	inline void SetMouseMoveValue(const POINT& moveVal)
	{
		m_MouseMoveValue = moveVal;
	}

	void MoveMouseCursor(const POINT& moveVal,HWND hWnd);

private:
	// Mouse
	POINT	m_MousePos;						// マウス座標
	POINT	m_MouseOldPos;					// １つ前のマウス座標
	POINT	m_MouseDown[NumMouseButtons];	// マウスを押した位置
	int		m_MouseWheel;					// ホイールを動かした値
	POINT	m_MouseMoveValue;				// マウスが動いた値
	bool	m_MouseFPSMode = false;

	//------------------------------------------------------------------
	// シングルトン
private:
	ZInput();
public:
	static ZInput& GetInstance()
	{
		static ZInput instance;
		return instance;
	}
	//------------------------------------------------------------------
};

#define INPUT ZInput::GetInstance()

#endif