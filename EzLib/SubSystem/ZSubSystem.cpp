#include "PCH/pch.h"

namespace EzLib
{
	bool ZSubSystemContainer::Init()
	{
		return true;
	}

	void ZSubSystemContainer::Release()
	{
		if (m_Orders.empty())return;

		for (auto it = m_Orders.rbegin(); it != m_Orders.rend(); it++)
		{
			auto findIt = m_SubSystems.find(*it);
			assert(findIt != m_SubSystems.end());

			findIt->second.Reset();
			m_SubSystems.erase(findIt);
		}

		m_Orders.clear();
	}

	namespace SubSystemDetails
	{
		InternalStatus& Status()
		{
			static InternalStatus s_Status = InternalStatus::IDLE;
			return s_Status;
		}

		void Release()
		{
			// 使用されていない
			if (Status() != InternalStatus::RUNNING)
				return;

			GetConteiner().Release();
			GetConteiner() = {};
			Status() = InternalStatus::RELEASED;
		}

		ZSubSystemContainer& GetConteiner()
		{
			static ZSubSystemContainer s_Conteiner;
			return s_Conteiner;
		}

		bool Init()
		{
			// 初期化済み
			if (Status() == InternalStatus::RUNNING)
				return false;
			// 初期化失敗(現時点ではあり得ない)
			if (GetConteiner().Init() == false)
				return false;
			Status() = InternalStatus::RUNNING;
			return true;
		}

	}


}