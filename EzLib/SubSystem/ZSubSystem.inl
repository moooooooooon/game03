template<typename T,typename... Args>
inline T& ZSubSystemContainer::AddSubSystem(Args&&... args)
{
	auto index = rtti::TypeID<T>().hash_code();

	// すでに同じサブシステムが存在している
	assert(!HasSubSystem<T>());

	m_Orders.push_back(index);
	m_SubSystems[index] = Make_Shared(T, sysnew, std::forward<Args>(args)...);
	return GetSubSystem<T>();
}

template<typename T>
inline T& ZSubSystemContainer::GetSubSystem()
{
	// サブシステムが存在しない
	assert(HasSubSystem<T>());

	const auto index = rtti::TypeID<T>().hash_code();
	return *(T*)(m_SubSystems[index].GetPtr());
}

template<typename T>
inline void ZSubSystemContainer::RemoveSubSystem()
{
	// サブシステムが存在しない
	assert(HasSubSystem<T>());

	const auto index = rtti::TypeID<T>().hash_code();
	m_SubSystems.erase(index);
	m_Orders.erase(std::remove_if(std::begin(m_Orders),std::end(m_Orders),
		[index](const auto& elem)
	{
		return index == elem;
	},std::end(m_Orders)));
}

template<typename T>
inline bool ZSubSystemContainer::HasSubSystem()const
{
	const auto index = rtti::TypeID<T>().hash_code();
	return m_SubSystems.find(index) != m_SubSystems.end();
}

template<typename T,typename U,typename... Args>
inline bool ZSubSystemContainer::HasSubSystem()const
{
	return HasSubSystem<T>() && HasSubSystem<U,Args...>();
}

template<typename T>
inline T& GetSubSystem()
{
	// 初期化されていない
	assert(SubSystemDetails::Status() == SubSystemDetails::InternalStatus::RUNNING);

	return SubSystemDetails::GetConteiner().GetSubSystem<T>();
}

template<typename T,typename... Args>
inline T& AddSubSystem(Args&&... args)
{
	// 初期化されていない
	assert(SubSystemDetails::Status() == SubSystemDetails::InternalStatus::RUNNING);
	return SubSystemDetails::GetConteiner().AddSubSystem<T>(std::forward<Args>(args)...);
}

template<typename T>
inline void RemoveSubSystem()
{
	// 初期化されていない
	if (SubSystemDetails::Status() != SubSystemDetails::InternalStatus::RUNNING)
		return;
	
	SubSystemDetails::GetConteiner().RemoveSubSystem<T>();
}

template<typename... Args>
inline bool HasSubSystem()
{
	if (SubSystemDetails::Status() != SubSystemDetails::InternalStatus::RUNNING)
		return false;

	return SubSystemDetails::GetConteiner().HasSubSystem<Args...>();
}