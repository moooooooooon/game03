#ifndef __ZCLASS_REFLECTON_H__
#define __ZCLASS_REFLECTON_H__

namespace EzLib
{
	class ZClassReflection
	{
	public:
		~ZClassReflection() { Release(); }

		template<typename T>
		void Register(const ZString& name)
		{
			m_ReflectionMap[name] = [] { return (void*)sysnew(T); };
		}

		template<typename T>
		ZSP<T> New(const ZString& name)
		{
			assert(m_ReflectionMap.find(name) != m_ReflectionMap.end());

			return ZSP<T>(m_ReflectionMap[name]());
		}

		template<typename T>
		ZUP<T> New_Unique(const ZString& name)
		{
			assert(m_ReflectionMap.find(name) != m_ReflectionMap.end());
			return ZUP<T>((T*)m_ReflectionMap[name]());
		}

		void Release()
		{
			m_ReflectionMap.clear();
		}

	private:
		ZSUnorderedMap<ZString, std::function<void*()>> m_ReflectionMap;
	};
}

#endif