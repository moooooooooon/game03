#ifndef __PATHUTIL_H__
#define __PATHUTIL_H__

#include <string>
#include <vector>

namespace EzLib
{
	class ZPathUtil
	{
	public:
		// インスタンス化禁止
		ZPathUtil() = delete;
		ZPathUtil(const ZPathUtil& pathUtil) = delete;
		ZPathUtil& operator=(const ZPathUtil& pathUtil) = delete;
	
		// カレントディレクトリ取得
		static ZString GetCurrentDir();
		
		// 実行可能ファイル取得
		static ZString GetExecutablePath();
		
		// 結合
		static ZString Combine(const std::vector<ZString>& parts);
		static ZString Combine(const ZString& a,const ZString& b);
	
		// パスからディレクトリ名取得
		static ZString GetDirName(const ZString& path);
	
		// パスからファイル名を取得
		static ZString GetFileName(const ZString& path);
	
		// ファイル名から拡張子以外を取得
		static ZString GetFileName_NonExt(const ZString& path);
	
		// ファイル名から拡張子を取得
		static ZString GetExt(const ZString& path);
	
		// 区切り文字(\\)取得
		static ZString GetDelimiter();
	
		// 区切り文字の正規化
		static ZString Normalize(const ZString& path);
	
		// ファイルが存在するか
		static bool FileExists(const ZString& path);

	};

}
#endif