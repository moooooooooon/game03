#ifndef __ZVFS_H__
#define __ZVFS_H__

namespace EzLib
{
	// Vertual File System
	// Mount()で任意の実ディレクトリを任意の仮想ディレクトリとして登録
	//  例: Mount("Model","data/Model")
	// UnMount()で仮想ディレクトリの登録解除
	// ResolvePhysicalPath()で任意の仮想ディレクトリ下のファイルパスから実ディレクトリパスを取得
	//  例: ResolvePhysicalPath("/Model/Box/model.xed",outPath)
	//   ※ 仮想ディレクトリ指定時は先頭に必ず'/'をつけること
	//      ResolvePhysicalPath("Model/Box/model.xed",~~) <- これだと"Model/~~"は実ディレクトリ指定と判断するため
	class ZVFS
	{
	private:
		static ZVFS* s_Instance;

	public:
		static void Init();
		static void Release();
		inline static ZVFS* GetInstance() { return s_Instance; }

	public:
		void Mount(const ZString& virtualPath, const ZString& physicalPath);
		void UnMount(const ZString& path);
		bool ResolvePhysicalPath(const ZString& path, ZString& outPhysicalPath);

	private:
		ZUnorderedMap<ZString, ZVector<ZString>> m_MountPoints;
	};

#define VFS ZVFS::GetInstance()

}

#endif