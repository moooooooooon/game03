#include "PCH/pch.h"

namespace EzLib
{
	ZVFS* ZVFS::s_Instance = nullptr;

	void ZVFS::Init()
	{
		s_Instance = sysnew(ZVFS);
	}

	void ZVFS::Release()
	{
		SAFE_DELPTR(s_Instance);
	}

	void ZVFS::Mount(const ZString& virtualPath, const ZString& physicalPath)
	{
		assert(s_Instance);
		m_MountPoints[virtualPath].push_back(physicalPath);
	}

	void ZVFS::UnMount(const ZString& path)
	{
		assert(s_Instance);
		m_MountPoints[path].clear();
	}

	bool ZVFS::ResolvePhysicalPath(const ZString& path, ZString& outPhysicalPath)
	{
		assert(s_Instance);
		if (path[0] != '/')
		{
			outPhysicalPath = path;
			return ZPathUtil::FileExists(path);
		}

		ZVector<ZString> dirs = SplitString(path, '/');
		const ZString& virtualDir = dirs.front();
		if (m_MountPoints.find(virtualDir) == m_MountPoints.end() ||
			m_MountPoints[virtualDir].empty())
			return false;

		ZString remainder = path.substr(virtualDir.size() + 1, path.size() - virtualDir.size());

		for (const ZString& physicalPath : m_MountPoints[virtualDir])
		{
			ZString path = physicalPath + remainder;
			if (ZPathUtil::FileExists(path))
			{
				outPhysicalPath = path;
				return true;
			}
		}

		return false;
	}

}
