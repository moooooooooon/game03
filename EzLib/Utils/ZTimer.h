#ifndef __ZTIMER_H__
#define __ZTIMER_H__

namespace EzLib
{
	class ZTimer
	{
	public:
		ZTimer(std::chrono::duration<double>& dulationTime)
			: m_FrameDulationTime(dulationTime), m_Stoped(true)
		{
		}

		inline void Start()
		{
			if (m_Stoped == false)
				return;

			m_FrameDulationTime = std::chrono::microseconds(0);
			m_StartTime = std::chrono::high_resolution_clock::now();
			m_Stoped = false;
		}

		inline void Stop()
		{
			if (m_Stoped)
				return;
			m_EndTime = std::chrono::high_resolution_clock::now();
			m_FrameDulationTime = m_EndTime - m_StartTime;
			m_Stoped = true;
		}

	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> m_StartTime;
		std::chrono::time_point<std::chrono::high_resolution_clock> m_EndTime;
		std::chrono::duration<double>& m_FrameDulationTime;
		bool m_Stoped;
	};

}

#endif