#ifndef __ZTHREAD_ID_H__
#define __ZTHREAD_ID_H__

#include <stddef.h>

namespace EzLib
{
	inline size_t GetCurrentThreadID()
	{
		return std::hash<std::thread::id>()(std::this_thread::get_id());
	}
}

#endif