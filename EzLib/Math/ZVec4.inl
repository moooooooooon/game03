#include "ZVec4.h"

namespace EzLib
{

	//------------------------------------------------------------------------------
	// Comparision operators
	//------------------------------------------------------------------------------

	inline bool ZVec4::operator == (const ZVec4& V) const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR v2 = XMLoadFloat4(&V);
		return XMVector4Equal(v1, v2);
	}

	inline bool ZVec4::operator != (const ZVec4& V) const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR v2 = XMLoadFloat4(&V);
		return XMVector4NotEqual(v1, v2);
	}

	//------------------------------------------------------------------------------
	// Assignment operators
	//------------------------------------------------------------------------------

	inline ZVec4& ZVec4::operator+= (const ZVec4& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR v2 = XMLoadFloat4(&V);
		XMVECTOR X = XMVectorAdd(v1, v2);
		XMStoreFloat4(this, X);
		return *this;
	}

	inline ZVec4& ZVec4::operator-= (const ZVec4& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR v2 = XMLoadFloat4(&V);
		XMVECTOR X = XMVectorSubtract(v1, v2);
		XMStoreFloat4(this, X);
		return *this;
	}

	inline ZVec4& ZVec4::operator*= (const ZVec4& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR v2 = XMLoadFloat4(&V);
		XMVECTOR X = XMVectorMultiply(v1, v2);
		XMStoreFloat4(this, X);
		return *this;
	}

	inline ZVec4& ZVec4::operator*= (float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR X = XMVectorScale(v1, S);
		XMStoreFloat4(this, X);
		return *this;
	}

	inline ZVec4& ZVec4::operator/= (float S)
	{
		using namespace DirectX;
		assert(S != 0.0f);
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR X = XMVectorScale(v1, 1.f / S);
		XMStoreFloat4(this, X);
		return *this;
	}

	//------------------------------------------------------------------------------
	// Urnary operators
	//------------------------------------------------------------------------------

	inline ZVec4 ZVec4::operator- () const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(this);
		XMVECTOR X = XMVectorNegate(v1);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}

	//------------------------------------------------------------------------------

	inline float ZVec4::Dot(const ZVec4 &v1, const ZVec4 &v2)
	{
		using namespace DirectX;
		XMVECTOR V1 = XMLoadFloat4(&v1);
		XMVECTOR V2 = XMLoadFloat4(&v2);
		XMVECTOR X = XMVector4Dot(V1, V2);
		return XMVectorGetX(X);
	}

	inline float ZVec4::DotClamp(const ZVec4 &v1, const ZVec4 &v2)
	{
		float retdot = Dot(v1, v2);
		if (retdot < -1.0f)
		{
			retdot = -1.0f;
		}
		if (retdot > 1.0f)
		{
			retdot = 1.0f;
		}
		return retdot;
	}

	inline float ZVec4::PlaneDot(const ZVec4& plane, const ZVec3& v)
	{
		using namespace DirectX;
		XMVECTOR Plane = XMLoadFloat4(&plane);
		XMVECTOR V = XMLoadFloat3(&v);
		XMVECTOR X = XMPlaneDotCoord(Plane, V);
		return XMVectorGetX(X);
	}

	inline void ZVec4::Cross(ZVec4 &vOut, const ZVec4 &v1, const ZVec4 &v2, const ZVec4 &v3)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(&v1);
		XMVECTOR x2 = XMLoadFloat4(&v2);
		XMVECTOR x3 = XMLoadFloat4(&v3);
		XMVECTOR R = XMVector4Cross(x1, x2, x3);
		XMStoreFloat4(&vOut, R);
	}

	inline float ZVec4::Length(const ZVec4 &vSrc)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&vSrc);
		XMVECTOR X = XMVector4Length(v1);
		return XMVectorGetX(X);
	}

	inline void ZVec4::Normalize(ZVec4 &vOut, const ZVec4 &vSrc)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&vSrc);
		XMVECTOR X = XMVector4Normalize(v1);
		XMStoreFloat4(&vOut, X);
	}
	
	inline void ZVec4::PlaneNormalize(ZVec4 &vOut, const ZVec4 &vSrc)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&vSrc);
		XMVECTOR X = XMPlaneNormalize(v1);
		XMStoreFloat4(&vOut, X);
	}

	inline void ZVec4::Lerp(ZVec4 &vOut, const ZVec4 &v1, const ZVec4 &v2, float f)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(&v1);
		XMVECTOR x2 = XMLoadFloat4(&v2);
		XMVECTOR X = XMVectorLerp(x1, x2, f);
		XMStoreFloat4(&vOut, X);
	}

	inline void ZVec4::Hermite(ZVec4 &vOut, const ZVec4 &v1, const ZVec4 &t1, const ZVec4 &v2, const ZVec4 &t2, float f)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(&v1);
		XMVECTOR x2 = XMLoadFloat4(&t1);
		XMVECTOR x3 = XMLoadFloat4(&v2);
		XMVECTOR x4 = XMLoadFloat4(&t2);
		XMVECTOR X = XMVectorHermite(x1, x2, x3, x4, f);
		XMStoreFloat4(&vOut, X);
	}

	inline void ZVec4::CatmullRom(ZVec4 &vOut, const ZVec4 &v0, const ZVec4 &v1, const ZVec4 &v2, const ZVec4 &v3, float f)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(&v0);
		XMVECTOR x2 = XMLoadFloat4(&v1);
		XMVECTOR x3 = XMLoadFloat4(&v2);
		XMVECTOR x4 = XMLoadFloat4(&v3);
		XMVECTOR X = XMVectorCatmullRom(x1, x2, x3, x4, f);
		XMStoreFloat4(&vOut, X);
	}

	inline void ZVec4::Transform(ZVec4 &vOut, const ZVec4 &vSrc, const ZMatrix &mat)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&vSrc);
		XMMATRIX M = XMLoadFloat4x4(&mat);
		XMVECTOR X = XMVector4Transform(v1, M);
		XMStoreFloat4(&vOut, X);
	}

	//------------------------------------------------------------------------------
	// Binary operators
	//------------------------------------------------------------------------------

	inline ZVec4 operator+ (const ZVec4& V1, const ZVec4& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&V1);
		XMVECTOR v2 = XMLoadFloat4(&V2);
		XMVECTOR X = XMVectorAdd(v1, v2);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}

	inline ZVec4 operator- (const ZVec4& V1, const ZVec4& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&V1);
		XMVECTOR v2 = XMLoadFloat4(&V2);
		XMVECTOR X = XMVectorSubtract(v1, v2);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}

	inline ZVec4 operator* (const ZVec4& V1, const ZVec4& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&V1);
		XMVECTOR v2 = XMLoadFloat4(&V2);
		XMVECTOR X = XMVectorMultiply(v1, v2);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}

	inline ZVec4 operator* (const ZVec4& V, float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&V);
		XMVECTOR X = XMVectorScale(v1, S);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}

	inline ZVec4 operator/ (const ZVec4& V1, const ZVec4& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&V1);
		XMVECTOR v2 = XMLoadFloat4(&V2);
		XMVECTOR X = XMVectorDivide(v1, v2);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}

	inline ZVec4 operator* (float S, const ZVec4& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(&V);
		XMVECTOR X = XMVectorScale(v1, S);
		ZVec4 R;
		XMStoreFloat4(&R, X);
		return R;
	}


}
