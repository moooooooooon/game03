//===============================================================
//   3次元ベクトルクラス
// 
//===============================================================
#ifndef ZVec3_h
#define ZVec3_h

namespace EzLib
{

	class ZMatrix;

	//============================================================================
	//   ３次元ベクトルクラス
	// 
	//  @ingroup Math
	//============================================================================
	class ZVec3 : public DirectX::XMFLOAT3
	{
	public:
		ZVec3() : DirectX::XMFLOAT3(0, 0, 0) {}

		ZVec3(float f) : DirectX::XMFLOAT3(f, f, f) {}

		ZVec3(float fx, float fy, float fz) : DirectX::XMFLOAT3(fx, fy, fz) {}

		ZVec3(const DirectX::XMFLOAT3& In) : DirectX::XMFLOAT3(In) {}

		ZVec3(DirectX::FXMVECTOR V) { DirectX::XMStoreFloat3(this, V); }

		void Set(float fx, float fy, float fz)
		{
			x = fx;
			y = fy;
			z = fz;
		}

		//   セット
		template<typename T>
		void Set(const T& v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		//   文字列の配列からセット
		void Set(const std::vector<std::string>& strArray)
		{
			x = std::stof(strArray[0]);
			y = std::stof(strArray[1]);
			z = std::stof(strArray[2]);
		}

		//   Json11データからセット [ x, y, z]の配列形式
		void Set(const json11::Json& jsonAry)
		{
			auto ary = jsonAry.array_items();
			if (ary.size() != 3)return;
			x = (float)ary[0].number_value();
			y = (float)ary[1].number_value();
			z = (float)ary[2].number_value();
		}

		// casting
		operator float* () { return &x; }
		//	operator const float* () const { return &x; }

			//   XMVECTORへ変換
		operator DirectX::XMVECTOR() const { return XMLoadFloat3(this); }
		//   SimpleMath::Vector3へ型のみ変換
		operator DirectX::SimpleMath::Vector3&() const { return *(DirectX::SimpleMath::Vector3*)this; }

		// 比較演算子
		bool operator == (const ZVec3& V) const;
		bool operator != (const ZVec3& V) const;

		// 代入演算子
		ZVec3& operator= (const ZVec3& V) { x = V.x; y = V.y; z = V.z; return *this; }
		ZVec3& operator+= (const ZVec3& V);
		ZVec3& operator-= (const ZVec3& V);
		ZVec3& operator*= (const ZVec3& V);
		ZVec3& operator*= (float S);
		ZVec3& operator/= (float S);

		ZVec3& operator=(const DirectX::XMVECTOR& v)
		{
			XMStoreFloat3(this, v);
			return *this;
		}

		template<typename T>
		ZVec3& operator= (const T& V) { x = V.x; y = V.y; z = V.z; return *this; }
		template<>
		ZVec3& operator=<float>(const float& f) { x = f; y = f; z = f; return *this; }

		// Urnary operators
		ZVec3 operator+ () const { return *this; }
		ZVec3 operator- () const;

		// 要素アクセス
		float& operator[](int idx)
		{
			return (&x)[idx];
		}

		// 定数ベクトル
		static const ZVec3 Zero;	// 全要素0ベクトル
		static const ZVec3 One;	// 全要素1ベクトル
		static const ZVec3 Up;		// 上ベクトル
		static const ZVec3 Down;	// 下ベクトル
		static const ZVec3 Left;	// 左ベクトル
		static const ZVec3 Right;	// 右ベクトル
		static const ZVec3 Front;	// 前方ベクトル
		static const ZVec3 Back;	// 後方ベクトル

		//-------------------------------------------------------------
		//   内積
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  @return 内積値
		//-------------------------------------------------------------
		static float Dot(const ZVec3 &v1, const ZVec3 &v2);			// 静的関数

		//-------------------------------------------------------------
		//   内積 自分とv1
		//  	v1		… ベクトル１
		//  @return 内積値
		//-------------------------------------------------------------
		float Dot(const ZVec3 &v1) const
		{
			return Dot(*this, v1);
		}

		//-------------------------------------------------------------
		//   内積(結果を-1〜1の範囲にする)
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  @return 内積値
		//-------------------------------------------------------------
		static float DotClamp(const ZVec3 &v1, const ZVec3 &v2);			// 静的関数 必ず -1<=dot<=1 で返す


		//-------------------------------------------------------------
		//   内積(結果を-1〜1の範囲にする) 自分とv1
		//  	v1		… ベクトル１
		//  @return 内積値
		//-------------------------------------------------------------
		float DotClamp(const ZVec3 &v1) const
		{
			return DotClamp(*this, v1);
		}

		//-------------------------------------------------------------
		//   外積
		//  @param[out]	vOut	… 計算結果のベクトルが入る
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//-------------------------------------------------------------
		static void Cross(ZVec3 &vOut, const ZVec3 &v1, const ZVec3 &v2);// 静的関数

		static ZVec3 Cross(const ZVec3 &v1, const ZVec3 &v2);

		//-------------------------------------------------------------
		//   外積 自分 x vBack
		//  @param[out]	vOut	… 計算結果のベクトルが入る
		//  	vBack	… 対称のベクトル
		//-------------------------------------------------------------
		void CrossBack(ZVec3 &vOut, const ZVec3 &vBack) const
		{
			Cross(vOut, *this, vBack);
		}

		//-------------------------------------------------------------
		//   外積 vFront x 自分
		//  @param[out]	vOut	… 計算結果のベクトルが入る
		//  	vFront	… 対称のベクトル
		//-------------------------------------------------------------
		void CrossFront(ZVec3 &vOut, const ZVec3 &vFront) const
		{
			Cross(vOut, vFront, *this);
		}

		//-------------------------------------------------------------
		//   ベクトルの長さ
		//  	vSrc	… 長さを求めるベクトル
		//-------------------------------------------------------------
		static float Length(const ZVec3 &vSrc);						// 静的関数

		//-------------------------------------------------------------
		//   ベクトルの長さ
		//-------------------------------------------------------------
		float Length() const
		{
			return Length(*this);
		}

		//-------------------------------------------------------------
		//   ベクトルの長さの二乗
		//  	vSrc	… 長さの二乗を求めるベクトル
		//-------------------------------------------------------------
		static float LengthSq(const ZVec3 &vSrc);						// 静的関数

		//-------------------------------------------------------------
		//   ベクトルの長さの二乗
		//-------------------------------------------------------------
		float LengthSq() const
		{
			return LengthSq(*this);
		}

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//  @param[out]	vOut	… 正規化したベクトルが入る
		//  	vSrc	… 処理の基となるベクトル
		//-------------------------------------------------------------
		static void Normalize(ZVec3 &vOut, const ZVec3 &vSrc);			// 静的関数

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//-------------------------------------------------------------
		ZVec3& Normalize()
		{
			Normalize(*this, *this);
			return *this;
		}

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//  @param[out]	vOut	… 正規化したベクトルが入る
		//-------------------------------------------------------------
		void Normalize(ZVec3 &vOut) const
		{
			Normalize(vOut, *this);
		}

		ZVec3 Normalized()const
		{
			ZVec3 v;
			Normalize(v);
			return v;
		}

		//-------------------------------------------------------------
		//   ベクトルをクランプする
		//   vmin		… 最小ベクトル
		//   vmax		… 最大ベクトル
		//-------------------------------------------------------------
		void Clamp(const ZVec3& vmin, const ZVec3& vmax);


		static void Min(ZVec3& out, const ZVec3& v1, const ZVec3& v2);
		static void Max(ZVec3& out, const ZVec3& v1, const ZVec3& v2);

		static bool NearEqual(const ZVec3& v1, const ZVec3& v2,const ZVec3& eps);

		//-------------------------------------------------------------
		//   線形補間
		// 
		//  座標pV1〜pV2の中間位置f(0〜1)の座標を算出
		// 
		//  @param[out]	vOut		… 補間結果の座標が入る
		//  	v1			… 開始座標
		//  	v2			… 終了座標
		//  	f			… 補間位置(0〜1)  例えば0なら開始座標(v1)となり、1なら終了座標(v2)となる　0.5ならv1とv2の中間座標となる
		//-------------------------------------------------------------
		static void Lerp(ZVec3 &vOut, const ZVec3 &v1, const ZVec3 &v2, float f);

		//-------------------------------------------------------------
		//   エルミネートスプライン補間
		//-------------------------------------------------------------
		static void Hermite(ZVec3 &vOut, const ZVec3 &v1, const ZVec3 &t1, const ZVec3 &v2, const ZVec3 &t2, float f);

		//-------------------------------------------------------------
		//   Catmull-Romスプライン補間
		// 
		//  座標pV1〜pV2の中間位置f(0〜1)の座標をスプライン補間で算出(曲線的な感じの結果になる)
		// 
		//  @param[out]	vOut		… 補間結果の座標が入る
		//  	pV0			… pV1の１つ前の座標
		//  	pV1			… 開始座標
		//  	pV2			… 終了座標
		//  	pV3			… pV2の次の座標
		//  	f			… 補間位置(0〜1)  例えば0なら開始座標(pV0)となり、1なら終了座標(pV1)となる　0.5ならpV0とpV1の中間座標となる
		//-------------------------------------------------------------
		static void CatmullRom(ZVec3 &vOut, const ZVec3 &v0, const ZVec3 &v1, const ZVec3 &v2, const ZVec3 &v3, float f);

		//-------------------------------------------------------------
		//   行列で変換
		//  @param[out]	vOut	… 変換後のベクトル
		//  	vSrc	… 処理の基となるベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		static void Transform(ZVec3 &vOut, const ZVec3 &vSrc, const ZMatrix &mat);	// 静的関数

		//-------------------------------------------------------------
		//   行列で変換
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void Transform(const ZMatrix &mat)
		{
			Transform(*this, *this, mat);
		}
		//-------------------------------------------------------------
		//   行列で変換
		//  @param[out]	vOut	… 変換後のベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void Transform(ZVec3 &vOut, const ZMatrix &mat) const
		{
			Transform(vOut, *this, mat);
		}

		//-------------------------------------------------------------
		//   行列で変換(回転のみ)
		//  @param[out]	vOut	… 変換後のベクトル
		//  	vSrc	… 処理の基となるベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		static void TransformNormal(ZVec3 &vOut, const ZVec3 &vSrc, const ZMatrix &mat);	// 静的関数

		//-------------------------------------------------------------
		//   行列で変換(回転のみ)
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void TransformNormal(const ZMatrix &mat)
		{
			TransformNormal(*this, *this, mat);
		}
		//-------------------------------------------------------------
		//   行列で変換(回転のみ)
		//  @param[out]	vOut	… 変換後のベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void TransformNormal(ZVec3 &vOut, const ZMatrix &mat) const
		{
			TransformNormal(vOut, *this, mat);
		}

		//-------------------------------------------------------------
		//   指定方向に指定角度だけ回転。(正反対でも曲がる)
		//  	vTargetDir	… 向きたいの方向
		//  	MaxAng		… 最大回転角度　この角度以上は回転しない
		//-------------------------------------------------------------
		void Homing(const ZVec3& vTargetDir, float MaxAng);

		//-------------------------------------------------------------
		//   三角形のAABB求める
		//  	v1		… 頂点１の座標
		//  	v2		… 頂点２の座標
		//  	v3		… 頂点３の座標
		//  @param[out]	outMinP	… AABBの最少座標
		//  @param[out]	outMaxP	… AABBの最大座標
		//-------------------------------------------------------------
		static void CreateAABB(const ZVec3 &v1, const ZVec3 &v2, const ZVec3 &v3, ZVec3 &outMinP, ZVec3 &outMaxP);

		//-------------------------------------------------------------
		//   線分のAABBを算出
		//  	v1		… 座標１
		//  	v2		… 座標２
		//  @param[out]	outMinP	… AABBの最少座標
		//  @param[out]	outMaxP	… AABBの最大座標
		//-------------------------------------------------------------
		static void CreateAABBFromLineSegment(const ZVec3 &v1, const ZVec3 &v2, ZVec3 &outMinP, ZVec3 &outMaxP);


		//-------------------------------------------------------------
		//   ZVec2として扱う
		//-------------------------------------------------------------
		ZVec2& Vec2()
		{
			return *(ZVec2*)this;
		}

		//   文字列として返す
		std::string GetUUIDStr()
		{
			std::string str = "( ";
			//		str += std::to_string(x) + " , ";
			//		str += std::to_string(y) + " , ";
			//		str += std::to_string(z) + " )";
			str += std::_Floating_to_string("%.3f", x) + " , ";
			str += std::_Floating_to_string("%.3f", y) + " , ";
			str += std::_Floating_to_string("%.3f", z) + " )";
			return std::move(str);
		}


	};

	/*
	ZVec3 operator+ (const ZVec3& V1, const ZVec3& V2);
	ZVec3 operator- (const ZVec3& V1, const ZVec3& V2);
	ZVec3 operator* (const ZVec3& V1, const ZVec3& V2);
	ZVec3 operator* (const ZVec3& V, float S);
	ZVec3 operator/ (const ZVec3& V1, const ZVec3& V2);
	ZVec3 operator/ (const ZVec3& V1, float S);
	ZVec3 operator* (float S, const ZVec3& V);
	*/

}

#endif
