#include "MainFrame/ZMainFrame.h"
#include "Camera/LightCamera.h"
#include "Shader/Light/LightManager.h"
#include "Shader/Renderer/ModelRenderer/ModelRenderingPipeline.h"
#include "Shader/ShaderManager.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/Fade/Fade.h"
#include "Shader/PostEffect/EffectPass//EffectPasses.h"
#include "ECSComponents/Components.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "StateMachines/Hacker/HackerStates.h"
#include "StateMachines/Hacker/HackerStateMachine.h"

#include "GameWorld.h"

#include "CommonSubSystems.h"

GameWorld::InitializeParams::InitializeParams()
{
	PostEffectInitializeFilePath = "data/Json/PostEffect/PostEffect.json";
	BackImageTextureFilePath = "data/UI/title3.png";
	LoadingEffectFilePath = "data/Effect/Load/Load.efk";
}

GameWorld::GameWorld() :
	m_NowCamera(nullptr),
	m_FrameCnt(0),
	m_LoadEffect(nullptr),
	m_IsLoading(false),
	m_IsDrawHalfFPSMode(false)
{
	m_LoadEffectHandle = ZEffectManager::NullEffectHandle;
}

void GameWorld::Init(const InitializeParams& initParams)
{

	// リフレクション登録
	RegisterComponentReflection();
	RegisterClassReflection();

	// コントローラーコンフィグ読み込み
	LoadControllerConfig();
	
	// 共通サブシステム
	InitializeSubSystems();

	// ロードエフェクト読み込み
	{
		GW.m_LoadEffect = EFFECT.LoadEffect(initParams.LoadingEffectFilePath);
		m_TexBackImg = Make_Shared(ZTexture, appnew);
		m_TexBackImg->LoadTexture(initParams.BackImageTextureFilePath);
	}

	//	ポストエフェクトを作成
	m_PostEffects.Init();
	m_PostEffects.LoadSettingFromJson(initParams.PostEffectInitializeFilePath.c_str());
	// EffectPass追加
	{
		auto* setupPass = appnew(SetupPass);
		auto* hePass = appnew(HEPass);
		auto* ssaoPass = appnew(SSAOPass);
		auto* dofPass = appnew(DOFPass);
		auto* xRayPass = appnew(XRayPass);
		auto* lightBloomPass = appnew(LightBloomPass);
		setupPass->Init();
		hePass->Init();
		ssaoPass->Init();
		dofPass->Init();
		xRayPass->Init();
		lightBloomPass->Init();
		m_PostEffects.AddEffectPass("Setup", setupPass);
		m_PostEffects.AddEffectPass("HE", hePass);
		m_PostEffects.AddEffectPass("SSAO", ssaoPass);
		m_PostEffects.AddEffectPass("DoF", dofPass);
		m_PostEffects.AddEffectPass("XRay", xRayPass);
		m_PostEffects.AddEffectPass("LightBloom", lightBloomPass);
	}

	// 物理エンジンのデバッグ描画用関数オブジェクトセット

#if _DEBUG
	auto& debugDraw = PHYSICS.GetBulletDebugDraw();
	if (debugDraw != nullptr)
	{
		debugDraw->SetSubmitLineFunction1([](const ZVec3& p1, const ZVec3& p2, const ZVec4& col1, const ZVec4& col2)
		{
			auto& lr = ShMgr.GetRenderer<LineRenderer>();
			lr.Submit(p1, p2, col1, col2);
		});

		debugDraw->SetSubmitLineFunction2([](const ZVec3& p1, const ZVec3& p2, const ZVec4& col)
		{
			auto& lr = ShMgr.GetRenderer<LineRenderer>();
			lr.Submit(p1, p2, col);
		});
	}
#endif

	// シーン初期化
	m_SceneMgr.Init();
}

void GameWorld::Release()
{
	m_SceneMgr.Release();

	if (m_LoadEffect)
	{
		EFFECT.ReleaseEffect(m_LoadEffect);
		m_LoadEffect = nullptr;
	}

	m_PostEffects.Release();
	if (m_Entities.empty() == false)
		ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.clear();
	m_NowCamera = nullptr;
}

void GameWorld::Update()
{
	if (m_SceneMgr.Start() == false)
	{
		_PreUpdate();
		_Update();
	}
	INPUT.SetMouseWheelValue(0);
}

void GameWorld::_PreUpdate()
{
	BOXINPUT.StateUpdate();

	ColEng.ClearList();

	UpdateInputs();

	// 衝突判定前更新
	m_SceneMgr.PreUpdate();
}

void GameWorld::_Update()
{
	// 衝突判定
	ColEng.Run();
	PHYSICS.StepSimulation(APP.m_DeltaTime);

	// シーン更新
	m_SceneMgr.Update();

	CTMgr.Update();
	FadeIO.Update();
	EFFECT.Update();

#ifdef SHOW_DEBUG_WINDOW
	// デバッグウィンドウ表示切り替え
	if (INPUT.KeyStay(VK_CONTROL) && INPUT.KeyEnter('D'))
		DEBUG_WINDOW.ToggleWindow();
#endif
}

void GameWorld::TickUpdate()
{
	m_SceneMgr.TickUpdate();
}

void GameWorld::Draw()
{
	// フレームレートの半分のレートで描画を行うなら
	if (m_IsDrawHalfFPSMode && APP.GetFrameCounter() % 2 != 0)
		return; // 偶数フレーム目以外は描画なし

	// 描画前準備(バックバッファ& Zバッファクリア)
	// バックバッファをクリアする。
	ZDx.Begin(ZVec4(0, 0, 0, 1), true, true);
	{
		// シーン描画
		m_SceneMgr.Draw();

#ifdef SHOW_DEBUG_WINDOW
		// メモリ詳細出力
		constexpr int beginMemoryScrollIndex = 10;
		ZAllocator::DisplayAllMemoryInfo(beginMemoryScrollIndex);
		m_SceneMgr.ImGuiUpdate();
		DEBUG_WINDOW.Update();
		DEBUG_WINDOW.Draw();
#endif
		FadeIO.Draw();
		
	}
	ZDx.End();

}

void GameWorld::StartLoadEffect()
{
	// 再生中ならそのまま再生
	if (m_IsLoading == true) return;

	m_IsLoading = true;
	m_LoadEffectHandle = EFFECT.SubmitPlayEffect(m_LoadEffect,ZVec3(0,-2,0));
	EFFECT.RegistOnStopedEffectFunction(m_LoadEffectHandle, // ループ用
		[this](const Effekseer::Handle, bool isRemovingManager)
	{
		// ロード中でかつエフェクトが停止した要因がマネージャーの削除でなければ
		if (m_IsLoading && isRemovingManager == false)
			m_LoadEffectHandle = EFFECT.SubmitPlayEffect(m_LoadEffect);
	});
}

void GameWorld::StopLoadEffect()
{
	// 停止されているなら何もしない
	if (m_IsLoading == false) return;

	m_IsLoading = false;
	EFFECT.StopEffect(m_LoadEffectHandle);
	m_LoadEffectHandle = ZEffectManager::NullEffectHandle;
}

void GameWorld::ChangeScene(const ZString& sceneName,bool useFade, float fadeTime)
{
	if (useFade == false)
	{
		m_SceneMgr.ChangeScene(sceneName);
		return;
	}

	// フェードアウト->ChangeScene->フェードイン
	FadeIO.ChangeFadeTime(fadeTime);
	FadeIO.StartFadeOut();
	FadeIO.SetExecuteEndFunction(
		[sceneName]()
	{
		GW.m_SceneMgr.NowChangeScene(sceneName);
		FadeIO.StartFadeIn(); // 非同期読み込みする際はロード完了時に呼ぶべき
	});
}

void GameWorld::RegisterComponentReflection()
{
	ECS.RegisterClassReflection<GameModelComponent>("GameModel");
	ECS.RegisterClassReflection<ModelBoneControllerComponent>("ModelBoneController");
	ECS.RegisterClassReflection<AnimatorComponent>("Animator");
	ECS.RegisterClassReflection<TransformComponent>("Transform");
	ECS.RegisterClassReflection<CameraComponent>("Camera");
	ECS.RegisterClassReflection<VelocityComponent>("Velocity");
	ECS.RegisterClassReflection<GravityComponent>("Gravity");
	ECS.RegisterClassReflection<PlayerComponent>("Player");
	ECS.RegisterClassReflection<PlayerControllerComponent>("PlayerController");
	ECS.RegisterClassReflection<HackerComponent>("Hacker");
	ECS.RegisterClassReflection<SecurityComponent>("Security");
	ECS.RegisterClassReflection<SecurityTerminalComponent>("SecurityTerminal");
	ECS.RegisterClassReflection<ServerTerminalComponent>("Server");
	ECS.RegisterClassReflection<ColliderComponent>("Collider");
	ECS.RegisterClassReflection<StateMachineComponent>("StateMachine");
	ECS.RegisterClassReflection<RadioButtonComponent>("RadioButton");
	ECS.RegisterClassReflection<SoundSetComponent>("SoundSet");
}

void GameWorld::RegisterClassReflection()
{
	// ステートマシン関係
	ClassRef.Register<HackerStateMachine>("HackerStateMachine");
	ClassRef.Register<HackerState_Wait>("HackerState_Wait");
	ClassRef.Register<HackerState_Walk>("HackerState_Walk");
	ClassRef.Register<HackerState_Run>("HackerState_Run");
	ClassRef.Register<HackerState_SquatWait>("HackerState_SquatWait");
	ClassRef.Register<HackerState_SquatWalk>("HackerState_SquatWalk");
	ClassRef.Register<HackerState_Fall>("HackerState_Fall");
	ClassRef.Register<HackerState_DownWait>("HackerState_DownWait");
	ClassRef.Register<HackerState_DownWalk>("HackerState_DownWalk");
	ClassRef.Register<HackerState_AccessStart>("HackerState_AccessStart");
	ClassRef.Register<HackerState_Access>("HackerState_Access");
	ClassRef.Register<HackerState_AccessEnd>("HackerState_AccessEnd");
	ClassRef.Register<HackerState_Cure>("HackerState_Cure");
	ClassRef.Register<HackerState_GetUp>("HackerState_GetUp");
	ClassRef.Register<HackerState_DestroyObj>("HackerState_DestroyObjStart");
	ClassRef.Register<HackerState_DestroyObj>("HackerState_DestroyObj");
	ClassRef.Register<HackerState_DestroyObjEnd>("HackerState_DestroyObjEnd");
}

void GameWorld::LoadControllerConfig()
{
	std::string error;
	auto controllerConfig = LoadJsonFromFile("controller.ini", error);
	for (int i = 0; i < 2; i++)
	{
		m_ControllerVerticalMoveInputSensitivity[i] = DefaultVerticalMoveInputSensitivity;
		m_ControllerHorizontalMoveInputSensitivity[i] = DefaultHorizontalMoveInputSensitivity;
		m_FlipVertical[i] = false;
	}
	if (error.empty() == false)return;
	
	// 上下感度設定読み込み
	{
		auto VSensitivity = controllerConfig["VerticalSensitivity"];
		auto ary = VSensitivity.array_items();
		if (ary.size() == 1)
		{
			for (int i = 0; i < 2; i++)
				m_ControllerVerticalMoveInputSensitivity[i] = ary[0].int_value();
		}
		else if (ary.size() >= 2)
		{
			for (int i = 0; i < 2; i++)
				m_ControllerVerticalMoveInputSensitivity[i] = ary[i].int_value();
		}
	}

	// 左右感度設定読み込み
	{
		auto HSensitivity = controllerConfig["HorizontalSensitivity"];
		auto ary = HSensitivity.array_items();
		if (ary.size() == 1)
		{
			for (int i = 0; i < 2; i++)
				m_ControllerHorizontalMoveInputSensitivity[i] = ary[0].int_value();
		}
		else if (ary.size() >= 2)
		{
			for (int i = 0; i < 2; i++)
				m_ControllerHorizontalMoveInputSensitivity[i] = ary[i].int_value();
		}
	}

	// 上下操作反転設定読み込み
	{
		auto FlipVertical = controllerConfig["FlipVertical"];
		auto ary = FlipVertical.array_items();
		if (ary.size() == 1)
		{
			for (int i = 0; i < 2; i++)
				m_FlipVertical[i] = ary[0].bool_value();
		}
		else if (ary.size() >= 2)
		{
			for (int i = 0; i < 2; i++)
				m_FlipVertical[i] = ary[i].bool_value();
		}

	}	
}

void GameWorld::InitializeSubSystems()
{
	AddSubSystem<ClosureTaskManager>();
	AddSubSystem<Fade>();
	auto& colEng = AddSubSystem<CollisionEngine>();
	auto& shMgr = AddSubSystem<ShaderManager>();
	auto& liMgr = AddSubSystem<LightManager>();
	auto& renderingPipeline = AddSubSystem<ModelRenderingPipeline>();
	shMgr.Init();
	liMgr.Init();
	renderingPipeline.Init();
}

void GameWorld::UpdateInputs()
{
	m_InputAxis[2]; // コントローラー2つ分
	m_InputButton[2]; // コントローラー2つ分

	// 値をリセット
	for(int i = 0;i<2;i++)
	{
		m_InputAxis[i].Set(0, 0, 0);
		m_InputButton[i] = 0;
	}

	// キーボード入力データ
	if (INPUT.KeyStay('W'))	m_InputAxis[0].z =  1;
	if (INPUT.KeyStay('S'))	m_InputAxis[0].z = -1;
	if (INPUT.KeyStay('A'))	m_InputAxis[0].x = -1;
	if (INPUT.KeyStay('D'))	m_InputAxis[0].x =  1;

	// マウス移動量
	m_MoveInputValue[0] = INPUT.GetMouseMoveValue();

	// ボタン
	if (INPUT.KeyEnter(VK_LBUTTON)) m_InputButton[0] |= KeyMap::Access;
	if (INPUT.KeyStay(VK_LBUTTON))	m_InputButton[0] |= KeyMap::Access_Stay;
	if (INPUT.KeyStay(VK_SHIFT))	m_InputButton[0] |= KeyMap::Run;
	if (INPUT.KeyStay(VK_CONTROL))	m_InputButton[0] |= KeyMap::Squat;
	if (INPUT.KeyStay('E'))			m_InputButton[0] |= KeyMap::Skill;
	if (INPUT.KeyEnter(VK_RBUTTON)) m_InputButton[0] |= KeyMap::Action;
	if (INPUT.KeyStay(VK_RBUTTON))	m_InputButton[0] |= KeyMap::Action_Stay;
	if (INPUT.KeyEnter(VK_RETURN))	m_InputButton[0] |= KeyMap::START;


	for (int i = 0; i < 2;i++)
	{
		if (BOXINPUT.IsConnected(i) == false)continue;

		// 移動入力
		{
			ZVec3 lStickVal;
			lStickVal.x = BOXINPUT.GetStateThumb(i, "LX");
			lStickVal.z = BOXINPUT.GetStateThumb(i, "LY");
			if (m_InputAxis[i].Length() <= 0 && lStickVal.Length() <= 0)
			{
				m_OnInputAxis[i] = false;
				m_OnStayInputAxis[i] = false;
			}
			else
			{
				if (m_OnStayInputAxis[i])m_OnInputAxis[i] = false;
				else m_OnInputAxis[i] |= true;
				m_OnStayInputAxis[i] = true;
			}
			m_InputAxis[i] += lStickVal;
			m_InputAxis[i] *= 0.5f;
		}

		// 視点移動入力
		{
			ZVec3 rStickVal;
			rStickVal.x = BOXINPUT.GetStateThumb(i, "RX");
			rStickVal.z = BOXINPUT.GetStateThumb(i, "RY");
			
			if (m_FlipVertical[i]) // 上下反転 
				rStickVal.z *= -1;

			rStickVal.x *= m_ControllerHorizontalMoveInputSensitivity[i];
			rStickVal.z *= m_ControllerVerticalMoveInputSensitivity[i];

			POINT p;
			p.x = (LONG)rStickVal.x;
			p.y = (LONG)rStickVal.z;
			if (abs(p.x) > 0 && abs(p.y) > 0)
			{
				INPUT.MoveMouseCursor(p, APP.m_Window->GetWindowHandle()); // カーソル移動
				INPUT.SetMouseMoveValue(p); // マウス移動量上書き
			}

			m_MoveInputValue[i] = p;
		}

		// 左クリック
		bool lbp = BOXINPUT.ButtonPressed(i, "LB");
		bool lbh = BOXINPUT.GetStateButton(i, "LB");

		// 右クリック
		bool rbp = BOXINPUT.ButtonPressed(i, "RB");
		bool rbh = BOXINPUT.GetStateButton(i, "RB");

		// Shift
		bool rth = BOXINPUT.GetStateTrigger(i, "RT") > 0.1f;
		
		// Crtl
		bool lth = BOXINPUT.GetStateTrigger(i, "LT") > 0.1f;

		bool ap = BOXINPUT.ButtonPressed(i, "A");
		bool bp = BOXINPUT.ButtonPressed(i, "B");
		bool xp = BOXINPUT.ButtonPressed(i, "X");
		bool yp = BOXINPUT.ButtonPressed(i, "Y");


		if (lbp) m_InputButton[i] |= KeyMap::Access;
		if (lbh) m_InputButton[i] |= KeyMap::Access_Stay;
		if (rth) m_InputButton[i] |= KeyMap::Run;
		if (lth) m_InputButton[i] |= KeyMap::Squat;
		if (rbp) m_InputButton[i] |= KeyMap::Action;
		if (rbh) m_InputButton[i] |= KeyMap::Action_Stay;
		if (ap)	 m_InputButton[i] |= KeyMap::A;
		if (bp)	 m_InputButton[i] |= KeyMap::B;
		if (xp)	 m_InputButton[i] |= KeyMap::X;
		if (yp)	 m_InputButton[i] |= KeyMap::Y;
	}

}

