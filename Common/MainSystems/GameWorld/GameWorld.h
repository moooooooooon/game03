#ifndef __GAME_WORLD_H__
#define __GAME_WORLD_H__

#include "Shader/PostEffect/PostEffects.h"
#include "XBoxCon/XboxConInput.h"

struct CameraComponent;

enum KeyMap
{
	Access = 0x00000001,
	Run = 0x00000002,
	Squat = 0x00000004,
	Skill = 0x00000008,
	Action = 0x00000010,
	Access_Stay = 0x00000020,
	Action_Stay = 0x00000040,
	// コントローラ スキルチェック用-------
	A = 0x00000080,
	B = 0x00000100,
	X = 0x00000200,
	Y = 0x00000400,
	//--------------------------------
	START = 0x00000100,
};

class GameWorld
{
public:
	struct InitializeParams
	{
		InitializeParams();

		ZString PostEffectInitializeFilePath;
		ZString BackImageTextureFilePath;
		ZString LoadingEffectFilePath;
	};

	static constexpr int DefaultVerticalMoveInputSensitivity = 7;
	static constexpr int DefaultHorizontalMoveInputSensitivity = 10;

public:
	~GameWorld() { Release(); }
	
	void Init(const InitializeParams& initParams = InitializeParams());
	void Release();

	void Update();
	void TickUpdate(); // 毎秒
	void Draw();

	void StartLoadEffect();
	void StopLoadEffect();

	void ChangeScene(const ZString& sceneName,bool useFade = true,float fadeTime = 0.5f);

#pragma region Singleton

	static GameWorld& GetInstance()
	{
		static GameWorld instance;
		return instance;
	}
	
	// コピー禁止
	void operator=(const GameWorld&) = delete;
	void operator=(GameWorld&&) = delete;

private:

	GameWorld();
	
#pragma endregion

	void _PreUpdate();
	void _Update();

private:
	void RegisterComponentReflection();
	void RegisterClassReflection();
	void LoadControllerConfig();
	void InitializeSubSystems();
	void UpdateInputs();

public:
	ZAVector<ZSP<ECSEntity>> m_Entities;
	PostEffects m_PostEffects;
	CameraComponent* m_NowCamera;
	Effekseer::Effect* m_LoadEffect;
	ZEffectManager::EffectHandle m_LoadEffectHandle;
	ZSP<ZTexture> m_TexBackImg;
	ZSceneManager m_SceneMgr;

	ZVec3 m_InputAxis[2]; // コントローラー2つ分
	int m_InputButton[2]; // コントローラー2つ分
	POINT m_MoveInputValue[2]; // 視点移動入力値(コントローラー2つ分)
	bool m_OnInputAxis[2]{false};
	bool m_OnStayInputAxis[2]{false};

	int m_ControllerVerticalMoveInputSensitivity[2]; // コントローラーの上下の視点移動入力の感度(コントローラー2つ分)
	int m_ControllerHorizontalMoveInputSensitivity[2]; // コントローラーの左右の視点移動入力の感度(コントローラー2つ分)
	bool m_FlipVertical[2];								// コントローラーの上下の反転(コントローラー2つ分)

	uint m_FrameCnt;
	bool m_IsLoading;
	bool m_IsDrawHalfFPSMode; // 2フレームに一度描画
	

	/*-----------------------------------------------------------------*/
	struct GameState {
		bool HackerWin		= false;
		bool SecurityWin	= false;
		bool AccesServer	= false;	//	サーバーにアクセスできるか

		void InitGame() {
			AccesServer = false;
			SecurityWin = false;
			HackerWin = false;
		}
	};
	GameState	m_GameState;
	/*-----------------------------------------------------------------*/
};

#define GW GameWorld::GetInstance()

#endif