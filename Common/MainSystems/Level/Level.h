#ifndef __LEVEL_H__
#define __LEVEL_H__

class Level
{
public:
	Level();
	~Level();

	void Init(const char* dataFilePath);
	void Release();

	void Update();
	void Draw();

private:
	ZAVector<ZSP<ECSEntity>> m_Entities;
	ZString m_DataFilePath;
	bool m_IsEnable;
};

#endif