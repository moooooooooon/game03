#ifndef __COLLISION_ENGINE_H__
#define __COLLISION_ENGINE_H__

class ColliderBase;

// 当たり判定用定数
namespace HitGroups
{
	enum
	{
		_0	= 0x00000001,
		_1	= 0x00000002,
		_2	= 0x00000004,
		_3	= 0x00000008,
		_4	= 0x00000010,
		_5	= 0x00000020,
		_6	= 0x00000040,
		_7	= 0x00000080,
		_8	= 0x00000100,
		_9	= 0x00000200,
		_10	= 0x00000400,
		_11	= 0x00000800,
		_12	= 0x00001000,
		_13	= 0x00002000,
		_14	= 0x00004000,
		_15	= 0x00008000,
		//		:  以下略　最大 _31まで

		_ALL = 0xFFFFFFFF
	};
}

namespace HitShapes
{
	enum
	{
		SPHERE		= 0x00000001,	// 球
		RAY			= 0x00000002,	// レイ
		BOX			= 0x00000004,	// 立方体
		MESH		= 0x00000010,	// メッシュ
		COMPOUND	= 0x00000020,	// 複合

		ALL = 0xFFFFFFFF	// すべて
	};
}

namespace HitStates
{
	enum
	{
		ENTER	= 0x00000001,
		STAY	= 0x00000002,
		EXIT	= 0x00000004
	};
}

// ヒット結果データ(当たり判定の結果)
struct HitResData
{
public:
	// メッシュの三角形とのヒット面情報
	struct FaceHitInfo
	{
	public:
		FaceHitInfo(int meshNo, int faceIndex, const ZVec3& nearHitPos);

	public:
		int MeshNo;			// メッシュ番号
		int FaceIndex;		// 面番号
		ZVec3 vNearHitPos;	// ヒットした最近接座標
	};

	// ヒット詳細情報
	struct Node
	{
	public:
		Node();

	public:
		const ColliderBase* MyHitObj;	// 自分のHitObj
		const ColliderBase* YouHitObj;	// ヒットした相手のHitObj

		ZVec3 vNearHitPos;				// ヒットした最近接座標
		ZVec3 vPush;					// (球、メッシュ用)押されているベクトル ぶつかり系の処理に使用

		float RayDist = FLT_MAX;		// (レイ用)当たった距離

		ZSVector<FaceHitInfo> Mesh_FaceHitTbl;

	};

public:
	void Init();

public:
	// ヒットデータリスト
	ZSVector<Node> HitDataList;

};

// 基本コライダークラス
class ColliderBase
{
private:
	friend class CollisionEngine;

public:
	ColliderBase();
	virtual ~ColliderBase();

	// 形状情報取得
	int GetShape()const;

	// 基本設定
	// atkFilter		… (判定する側)グループフィルタ。HitGroups::〜で指定する。
	// atkShapeFilter	… (判定する側)どの形状と判定するか。HitShapes::〜で指定する。
	// defFilter		… (判定される側)グループフィルタ。HitGroups::〜で指定する。
	void Init(size_t id = 0,
		int atkFilter = 0xFFFFFFFF,
		int atkShapeFilter = 0xFFFFFFFF,
		int defFilter = 0xFFFFFFFF);

	virtual const ZAABB& GetAABB()const = 0;

	// 当たり判定
	// obj		: 相手の判定オブジェクト
	// resData1	: 判定結果を入れる変数　自分側
	// resData2	: 判定結果を入れる変数　相手側
	virtual bool HitTest(const ColliderBase* obj, HitResData* resData1, HitResData* resData2) = 0;

	// 質量比計算
	float CalcMassRatio(float mass)const;

	// 無視リストにID追加
	void AddIgnoreID(size_t id);

	// デバッグ描画
	virtual void DebugDraw(float alpha)const;

	// m_UserMapのname項目をした型へ変換し取得
	// ※型判定なし 存在しない場合はnullptr
	template<typename T = void>
	ZSP<T> GetUserMap(const ZString& name)const;

	// 自分とobjが判定しても良いかチェック
	bool IsTest(const ColliderBase* obj)const;

	// 指定型へキャスト
	// 変換出来ない時はnullptr
	template<typename T>
	T* Cast()const;

protected:
	virtual void ReCalcAABB()
	{
	}

public:
	// 基本データ
	size_t				m_ID;			// 判定用ID 無視リスト判定等で使用
	ZSVector<size_t> m_IgnoreIDs;	// (判定する側専用)判定を無視するID

	int m_AtkFilter;	// (判定する側専用)判定する側のときのグループフィルタ HitGroup使用
	int m_DefFilter;	// (判定される側専用)判定される側の時のグループフィルタ HitGroup使用
	int m_ShapeFilter;	// (判定する側専用)どの形状と判定するかのフィルタ HitGroup使用

	std::function<bool(const ColliderBase*)> m_BroadPhaseChack;	// (判定する側専用)汎用事前チェック関数

protected:
	// 形状情報
	int m_Shape;		// 自身の形状(型キャスト時の識別用)
	ZAABB m_AABB;

private:
	int m_NowLineNo;	// (判定する側専用)判定時のラインNo(CollisionEngineクラスから変更)

public:
	// その他 好きなデータ格納用

	ZSUnorderedMap<ZString, ZWP<void>> m_UserMap;

	// 結果用データ
	int m_HitState;							// (判定する側専用)ヒット結果の状態フラグ HitStatesを参照
	ZSVector<HitResData> m_HitResTbl;	// ヒット結果リスト 当たり判定結果格納場所

	// 判定結果通知関数
	// Hitした1回目のみ呼ばれる関数オブジェクト
	std::function<void(const ZSP<ColliderBase>&)> m_OnHitEnter;
	// Hitしている間呼ばれる関数オブジェクト
	std::function<void(const ZSP<ColliderBase>&)> m_OnHitStay;
	// Hitしなくなった1回目のみ呼ばれる関数オブジェクト
	std::function<void(const ZSP<ColliderBase>&)> m_OnHitExit;
	// Hitに関係なく、最後に絶対に実行される関数オブジェクト
	std::function<void(const ZSP<ColliderBase>&)> m_OnTerminal;

	// マルチスレッド実行時に使用　上記のOnHit系の関数と違い、マルチスレッド実行時に呼ばれるので注意
	// 必要だと感じた人はコメント解除(cpp側も)して使ってください
	//	std::function<void(const ColliderBase*)>	m_OnHitEnter_Parallel;
	//	std::function<void(const ColliderBase*)>	m_OnHitStay_Parallel;
	//	std::function<void(const ColliderBase*)>	m_OnHitExit_Parallel;

	// その他データ
	float m_Mass;	// ぶつかり時に使用 質量0で固定物

	// デバッグ描画色
	ZVec4 m_Debug_Color;

};

class Collider_Sphere : public ColliderBase
{
public:
	Collider_Sphere();

	virtual const ZAABB& GetAABB()const override;

	// 当たり判定計算関数
	virtual bool HitTest(const ColliderBase* obj, HitResData* resData_Sphere, HitResData* resData2)override;

	virtual void DebugDraw(float alpha)const override;

	// 基本情報設定
	void Set(const ZVec3& pos, float rad);

	// 座標のみ設定
	void SetPos(const ZVec3& pos);

	void SetRadius(float rad);

	// このクラスの形状
	static int sShape()
	{
		return HitShapes::SPHERE;
	}

protected:
	virtual void ReCalcAABB()override;

public:
	// データ
	DirectX::BoundingSphere	m_Sphere;

};

// 立方体の当たり判定用
class Collider_Box : public ColliderBase
{
public:
	Collider_Box();

	virtual const ZAABB& GetAABB()const override;

	const DirectX::BoundingOrientedBox& GetBox()const;
	const ZVec3& GetDir(int index)const;

	// 当たり判定計算
	virtual bool HitTest(const ColliderBase* obj, HitResData* resData_Box, HitResData* resData2)override;

	// デバッグ描画
	virtual void DebugDraw(float alpha)const override;

	// 基本情報設定

	// ローカルのOBB座標と行列から設定
	void Set(const ZVec3& vLocalCenterPos, const ZVec3& vHalfSize, const ZMatrix& mat);

	// ローカルの最小座標、最大座標を行列からデータセット
	void Set_MinMax(const ZVec3& vlocalMin, const ZVec3& vlocalMax, const ZMatrix& mat);

	// 行列設定
	void SetMatrix(const ZMatrix& mat);

	static int sShape()
	{
		return HitShapes::BOX;
	}

protected:
	virtual void ReCalcAABB()override;

private:
	DirectX::BoundingOrientedBox m_Box;
	ZVec3 m_Dir[3];		// 中心からの軸
	
};

class Collider_Ray : public ColliderBase
{
public:
	Collider_Ray();

	virtual const ZAABB& GetAABB()const override;

	// 当たり判定計算
	virtual bool HitTest(const ColliderBase* obj, HitResData* resData_Ray, HitResData* resData2)override;

	// デバッグ描画
	virtual void DebugDraw(float alpha)const override;

	// 基本情報設定
	void Set(const ZVec3& pos1, const ZVec3& pos2);

	const ZVec3& GetPos1()const;

	const ZVec3& GetPos2()const;

	const ZVec3& GetDir()const;

	float GetRayLen()const;

	static int sShape()
	{
		return HitShapes::RAY;
	}

protected:
	virtual void ReCalcAABB()override;

private:
	// データ Start ~ Endの座標間でレイ判定
	ZVec3 m_vPos1;	// レイ座標1(start)
	ZVec3 m_vPos2;	// レイ座標2(end)
	ZVec3 m_vDir;	// 方向 m_vPos2 - m_vPos1
	float m_RayLen;

};

class Collider_Mesh : public ColliderBase
{
public:
	Collider_Mesh();

	virtual const ZAABB& GetAABB()const override;

	const ZAABB& GetBaseAABB()const { return m_BaseAABB; }

	// 当たり判定計算
	virtual bool HitTest(const ColliderBase* obj, HitResData* resData_Mesh, HitResData* resData2)override;

	// デバッグ描画
	virtual void DebugDraw(float alpha)const override;

	// 基本情報設定
	void Set(const ZMatrix& mat);

	// メッシュを追加登録
	void AddMesh(ZSP<ZGameModel> pModel);

	void ClearMesh();

	static int sShape()
	{
		return HitShapes::MESH;
	}

protected:
	virtual void ReCalcAABB()override;

public:
	ZSVector<ZSP<ZMesh>> m_MeshTbl;
	ZMatrix m_Mat;		// 変換行列

private:
	ZAABB m_BaseAABB;

};

class Collider_Compound : public ColliderBase
{
public:
	Collider_Compound();

	virtual const ZAABB& GetAABB()const override;

	// 当たり判定計算
	virtual bool HitTest(const ColliderBase* obj, HitResData* resData1_Comp, HitResData* resData2)override;

	// デバッグ描画
	virtual void DebugDraw(float alpha)const override;

	// ヒットオブジェクトを登録する
	void AddHitObj(ColliderBase* hitObj);

	// 登録ヒットオブジェクトを全て登録解除する
	void ClearHitObj();

	static int sShape()
	{
		return HitShapes::COMPOUND;
	}

public:
	ZSVector<ColliderBase*> m_Colliders;
	bool m_IsAll = false;
};


// 当たり判定マネージャ
class CollisionEngine
{
private:
	static constexpr size_t NumAtkLines = 20;

public:
	CollisionEngine();
	~CollisionEngine();

	void Init(uint octreeAreaSplitLevel = 5, const ZAABB& octreeArea = ZAABB(-1000, 1000));

	// 判定する側として登録
	//  lineNo … 0〜19の合成20個グループがあり、0〜19の順番で判定が行われる
	//            先に判定したいものほど、若い番号のグループに登録すること
	//            (例)レイ判定を行い、ヒットした座標で球判定をする : レイ判定は(0)、球判定は(1)で登録しておく

	void AddAtk(ZSP<ColliderBase> obj, size_t lineNo = 0);

	// 判定される側として登録
	void AddDef(ZSP<ColliderBase> obj);

	// 当たり判定を実行し、各Objへ通知する
	// 全m_AtkList vs 全m_DefListの判定を行い、全m_AtkListへ通知する
	// ヒット時は、その攻タイプのm_OnHit系の関数が実行される
	void Run();

	// 任意のAABBと衝突のの可能性のあるCollider取得
	ZVector<ColliderBase*> GetCollisionObjects(ZAABB& testAabb);

	// atkObj vs 全m_DefListの判定を行う
	void Test(ColliderBase* atkObj);

	// リストをクリア
	void ClearList();

	// デバッグ描画
	void DebugDraw(float alpha);

	// マルチスレッドで判定 On/Off
	void EnableMultiThread(bool flg);

	inline auto& GetOctree()const { return m_Octree; }

private:
	// Colliderリスト

	// 判定する側

	std::array<ZSVector<ZSP<ColliderBase>>, NumAtkLines> m_AtkLists;
	
	// 判定される側
	ZSVector<ZSP<ColliderBase>> m_DefList;

	// 8分木空間分割
	ZOctree<ColliderBase> m_Octree;


	int m_CpuCnt;
	bool m_IsMultiThread;
	bool m_IsInitialized;

};

#include "CollisionEngine.inl"

#include "HitTests.h"

#endif