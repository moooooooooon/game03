#include "MainFrame/ZMainFrame.h"
#include "DOF_Shader.h"

bool DOF_Shader::Init()
{
	m_Shaders.SetVS("DOF", APP.m_ResStg.LoadVertexShader("Shader/DOF_VS.cso"));
	m_Shaders.SetPS("DOF", APP.m_ResStg.LoadPixelShader("Shader/DOF_PS.cso"));
	m_Shaders.SetPS("Extraction", APP.m_ResStg.LoadPixelShader("Shader/DOF_ExtractionPS.cso"));
	
	m_RingBuf.Create(ZVertex_Pos_UV::GetVertexTypeData(), 4);

	m_View.Create(0);

	return true;
}

void DOF_Shader::Release()
{
	m_View.Release();
	m_Shaders.RemoveAllShaders();
	m_RingBuf.Release();
}

void DOF_Shader::Render(ZSP<ZTexture> base, ZSP<ZTexture> blur, ZSP<ZTexture> dof, ZSP<ZTexture> out,const ZVec2& size)
{
	ZRenderTarget_BackUpper bu;
	{
		UINT pNumViewports = 1;
		D3D11_VIEWPORT vp;
		ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);

		ZMatrix p2DMat;
		p2DMat.CreateOrthoLH(size.x, size.y, 0, 1);
		p2DMat.Move(-1, -1, 0);
		p2DMat.Scale(1, -1, 1);
		m_View.m_Data.view = p2DMat;
		m_View.WriteData();
	}
	//	変換行列作成
	{
	//	UINT pNumViewports = 1;
	//	D3D11_VIEWPORT vp;
	//	ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);

	//	ZMatrix p2DMat;
	//	p2DMat.CreateOrthoLH(vp.Width, vp.Height, 0, 1);
	//	p2DMat.Move(-1, -1, 0);
	//	p2DMat.Scale(1, -1, 1);
	//	m_View.m_Data.view = p2DMat;
	//	m_View.WriteData();
	}

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, out->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	//	レンダリングステート保存
	ZDepthStencilState	DSBackUp;
	ZBlendState			BSBackUp;
	{
	// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();

		// ブレンドステート
		ZBlendState bs;
		bs.Set_Alpha();
		bs.SetState();
	}

	//	シェーダセット
	m_Shaders.GetVS("DOF")->SetShader();
	m_Shaders.GetPS("DOF")->SetShader();
	m_View.SetVS();
	m_View.SetPS();

	//	テクスチャセット
	dof->SetTexturePS(0);
	base->SetTexturePS(1);
	blur->SetTexturePS(2);



	// 頂点作成
	//const int	 numVtx = 4;
	//ZVertex_Pos_UV vertices[numVtx] =
	//{
	//	{ ZVec3( 0.f, (float)APP.m_Window->GetHeight(),	0)								,	ZVec2(0,1), },
	//	{ ZVec3( 0.f,0.f, 0)															,	ZVec2(0,0), },
	//	{ ZVec3((float)APP.m_Window->GetWidth(),(float)APP.m_Window->GetHeight(), 0)	,	ZVec2(1,1), },
	//	{ ZVec3((float)APP.m_Window->GetWidth(),0.f,	0)								,	ZVec2(1,0), }
	//};
	
	const int	 numVtx = 4;
	ZVertex_Pos_UV vertices[numVtx] =
	{
	{ ZVec3(0.f, size.y,0) 	 ,	ZVec2(0,1), },
	{ ZVec3(0.f,0.f, 0)      ,	ZVec2(0,0), },
	{ ZVec3(size.x,size.y, 0),	ZVec2(1,1), },
	{ ZVec3(size.x,0.f,	0)	 ,	ZVec2(1,0), }
	};


	//	描画
	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);


	//	テクスチャ解除
	ZDx.RemoveTexturePS(0);
	ZDx.RemoveTexturePS(1);
	ZDx.RemoveTexturePS(2);


	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();
}

void DOF_Shader::GenerateDOFMap(ZSP<ZTexture> base, ZSP<ZTexture> out,const ZVec2& size)
{	
	ZRenderTarget_BackUpper bu;

	//	変換行列作成
	{
		UINT pNumViewports = 1;
		D3D11_VIEWPORT vp;
		ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);

		ZMatrix p2DMat;
		p2DMat.CreateOrthoLH(size.x, size.y, 0, 1);
		p2DMat.Move(-1, -1, 0);
		p2DMat.Scale(1, -1, 1);
		m_View.m_Data.view = p2DMat;
		m_View.WriteData();
	}

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, out->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	//	レンダリングステート保存
	ZDepthStencilState	DSBackUp;
	ZBlendState			BSBackUp;
	{
		// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();

		// ブレンドステート
		ZBlendState bs;
		bs.Set_Alpha();
		bs.SetState();
	}

	//	シェーダセット
	m_Shaders.GetVS("DOF")->SetShader();
	m_Shaders.GetPS("Extraction")->SetShader();
	m_View.SetVS();
	m_View.SetPS();

	//	テクスチャセット
	base->SetTexturePS(1);


	// 頂点作成
	const int	 numVtx = 4;
	ZVertex_Pos_UV vertices[numVtx] =
	{
	{ ZVec3(0.f, size.y,0) 	 ,	ZVec2(0,1), },
	{ ZVec3(0.f,0.f, 0)      ,	ZVec2(0,0), },
	{ ZVec3(size.x,size.y, 0),	ZVec2(1,1), },
	{ ZVec3(size.x,0.f,	0)	 ,	ZVec2(1,0), }
	};


	//	描画
	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);


	//	テクスチャ解除
	ZDx.RemoveTexturePS(1);


	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();
}
