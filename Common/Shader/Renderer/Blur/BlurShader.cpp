#include "MainFrame/ZMainFrame.h"
#include "SubSystems.h"
#include "CommonSubSystems.h"
#include "Shader/ShaderManager.h"
#include "BlurShader.h"

const float BlurShader::DefaultDisp = 5.f;
const int BlurShader::DefaultLoop	= 3;

BlurShader::BlurShader()
{
}

bool BlurShader::Init()
{
	m_Shaders.SetVS("Pass1", APP.m_ResStg.LoadVertexShader("Shader/BlurVS_1.cso"));
	m_Shaders.SetPS("Pass1", APP.m_ResStg.LoadPixelShader("Shader/BlurPS_1.cso"));
	m_Shaders.SetVS("Pass2", APP.m_ResStg.LoadVertexShader("Shader/BlurVS_2.cso"));
	m_Shaders.SetPS("Pass2", APP.m_ResStg.LoadPixelShader("Shader/BlurPS_2.cso"));
	m_Shaders.SetVS("HIE", APP.m_ResStg.LoadVertexShader("Shader/HIE_VS.cso"));
	m_Shaders.SetPS("HIE", APP.m_ResStg.LoadPixelShader("Shader/HIE_PS.cso"));

	//	ターゲット作成
	if (!CreateRenderTarget()) { return false; }

	//	リングバッファ作成
	m_RingBuf.Create(ZVertex_Pos_UV::GetVertexTypeData(), 4);

	//	定数バッファ作成
	m_Data.Create(0);
	m_View.Create(1);

	//	ガウスの重み計算
	ComputeGaussWeights(pow(DefaultDisp, 2));

	//	デバッグ描画用
#if _DEBUG
	m_DebugSave = Make_Shared(ZTextureSet,appnew);
	m_DebugSave->CreateRTSet(640, 360,10);
#endif
	m_pSr = &ShMgr.GetRenderer<SpriteRenderer>();

	return true;
}

void BlurShader::Release()
{
	m_Data.Release();
	m_View.Release();
	m_Shaders.RemoveAllShaders();
	m_RingBuf.Release();

	ReleaseRenderTarget();
	DeleteMipTarget();

#if _DEBUG
	m_DebugSave->Release(); 
#endif
}

void BlurShader::CreateMipTarget(ZVec2 size)
{

	//	すでに作成済みなら戻る
	if (m_MipFlg) return;

	DeleteMipTarget();

	//	一旦解放
	m_MipTargets.clear();

	float ReducionVal = 1.f;

	//	オリジナルサイズでのMipTargetは実装しないので初期値に
	for (int i = Original; i <= Sixty; i++)
	{
		m_MipTargets[i] = Make_Shared(ZTextureSet,appnew);
		//	ターゲットを作成
		m_MipTargets[i]->CreateRTSet((int)(size.x*ReducionVal), (int)(size.y*ReducionVal),2);
		//	半分にする
		ReducionVal*=0.5f;
	}
	m_MipFlg = true;
}

void BlurShader::DeleteMipTarget()
{
	m_MipTargets.clear();
	m_MipFlg = false;
}

void BlurShader::ChangeTarget()
{
	float ReducionVal = CalcReductionVal(m_SizeFlg);

	//	ターゲットのサイズ
	m_RenderSize = ZVec2(APP.m_Window->GetWidth()*ReducionVal, APP.m_Window->GetHeight()*ReducionVal);

	//	テクセルサイズセット
	SetTargetSize(m_RenderSize.x, m_RenderSize.y);

	m_BlurTex->Release();

	m_BlurTex->SetTex(0, m_MipTargets[m_SizeFlg]->GetTexZSP(0));
	m_BlurTex->SetTex(1, m_MipTargets[m_SizeFlg]->GetTexZSP(1));

}

void BlurShader::ChangeRenderTarget(BlurRenderFlgs flg)
{
	m_SizeFlg = flg;

	//	作成済みの場合はポインターを切り替えるだけ
	if (m_MipFlg)
	{
		ChangeTarget();
		return;
	}
	
	//	Mipが作成されていない時はターゲットの作成から
	ReleaseRenderTarget();
	CreateRenderTarget();
	
}

void BlurShader::SaveRTBegin()
{
#ifdef _DEBUG
	if(m_DebugSaveFlg == false) m_DebugSave->AllClearRT();
	m_DebugSaveCnt = 0;
	m_DebugSaveFlg = true;
#endif
}

void BlurShader::SaveRTEnd()
{
#ifdef _DEBUG
	m_DebugSaveFlg = false;
#endif
}

void BlurShader::GenerateBlur(ZSP<ZTexture> tex, UINT loop, float disp)
{
	ComputeGaussWeights(pow(disp, 2));

	ZRenderTarget_BackUpper bu;

	//	クリア
	m_BlurTex->AllClearRT();

	//	
	m_RT.GetNowTop();
	m_RT.Depth(nullptr);

	m_RT.RT(0, m_BlurTex->GetTex(1)->GetRTTex());
	m_RT.SetToDevice();

	/**
	*	ReSize
	**/
	m_pSr->Begin(false, true);
	m_pSr->Draw2D(tex->GetTex(), 0, 0, m_RenderSize.x, m_RenderSize.y);
	m_pSr->End();

	/**
	*	Blur
	**/
	m_Data.m_Data.Size		= m_RenderSize;
	m_Data.m_Data.Offset	= ZVec2(16.f / m_RenderSize.x, 16.f / m_RenderSize.y);
	m_Data.SetVS();
	m_Data.SetPS();
	m_Data.WriteData();

	Begin();
	{
		for (UINT i = 0; i < loop; i++)
		{
			// Pass1
			m_RT.RT(0, *m_BlurTex->GetTex(0));
			m_RT.SetToDevice();
			Draw2D(m_BlurTex->GetTexZSP(1), ZVec2::zero, m_RenderSize, *m_Shaders.GetVS("Pass1"), *m_Shaders.GetPS("Pass1"));

			// Pass2
			m_RT.RT(0, *m_BlurTex->GetTex(1));
			m_RT.SetToDevice();
			Draw2D(m_BlurTex->GetTexZSP(0), ZVec2::zero, m_RenderSize, *m_Shaders.GetVS("Pass2"), *m_Shaders.GetPS("Pass2"));
		}
	}
	End();

#if _DEBUG
	if (m_DebugSaveFlg)
	{
		if (m_DebugSaveCnt < 10)
		{
			m_RT.Depth(nullptr);
			m_RT.RT(0, m_DebugSave->GetTex(m_DebugSaveCnt)->GetRTTex());
			m_RT.SetToDevice();

			m_pSr->Begin(false, true);
			m_pSr->Draw2D(m_BlurTex->GetTexZSP(1)->GetTex(), 0, 0, 640, 360);
			m_pSr->End();

			++m_DebugSaveCnt;
		}
	}
#endif

}

void BlurShader::HIE(ZSP<ZTexture> base, ZSP<ZTexture> out, const ZVec2 & size)
{
	ZRenderTarget_BackUpper bu;

	//	変換行列作成
	{
		UINT pNumViewports = 1;
		D3D11_VIEWPORT vp;
		ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);

		ZMatrix p2DMat;
		p2DMat.CreateOrthoLH(size.x, size.y, 0, 1);
		p2DMat.Move(-1, -1, 0);
		p2DMat.Scale(1, -1, 1);
		m_View.m_Data.view = p2DMat;
		m_View.WriteData();
	}

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, out->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	//	レンダリングステート保存
	ZDepthStencilState	DSBackUp;
	ZBlendState			BSBackUp;
	{
		// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();

		// ブレンドステート
		ZBlendState bs;
		bs.Set_Alpha();
		bs.SetState();
	}

	//	シェーダセット
	m_Shaders.GetVS("HIE")->SetShader();
	m_Shaders.GetPS("HIE")->SetShader();
	m_View.SetVS();
	m_View.SetPS();

	//	テクスチャセット
	base->SetTexturePS(0);

	// 頂点作成
	const int	 numVtx = 4;
	ZVertex_Pos_UV vertices[numVtx] =
	{
		{ ZVec3(0.f, size.y,0) 	 ,	ZVec2(0,1)	},
		{ ZVec3(0)				 ,	ZVec2(0)	},	
		{ ZVec3(size.x,size.y, 0),	ZVec2(1)	},	
		{ ZVec3(size.x,0.f,	0)	 ,	ZVec2(1,0)	}
	};

	//	描画
	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);

	//	テクスチャ解除
	ZDx.RemoveTexturePS(0);

	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();
}

bool BlurShader::CreateRenderTarget()
{
	float ReductionVal = CalcReductionVal(m_SizeFlg);

	//	ターゲットのサイズ
	m_RenderSize = ZVec2(APP.m_Window->GetWidth()*ReductionVal, APP.m_Window->GetHeight()*ReductionVal);

	//	テクセルサイズセット
	SetTargetSize(m_RenderSize.x, m_RenderSize.y);


	//	ブラー用の作業用テクスチャ作成
	{
		m_BlurTex = Make_Shared(ZTextureSet,appnew,2);
		auto b1 = Make_Shared(ZTexture,appnew);
		if (!b1->CreateRT((int)m_RenderSize.x, (int)m_RenderSize.y))
			return false;
		m_BlurTex->SetTex(0, b1);

		auto b2 = Make_Shared(ZTexture, appnew);
		if (b2->CreateRT((int)m_RenderSize.x, (int)m_RenderSize.y) == false)
			return false;
		m_BlurTex->SetTex(1, b2);
	}

	return true;
}

void BlurShader::ReleaseRenderTarget()
{
	m_BlurTex = nullptr;
}

void BlurShader::ComputeGaussWeights(float disp)
{
	const int	NumWeight	= 8;
	float		total		= 0.f;

	for (int i = 0; i < NumWeight; i++)
	{
		float pos = 1.f + 2.f * (float)i;
		m_Data.m_Data.weights[i] = expf(-0.5f*(pos*pos) / disp);
		total += 2.f*m_Data.m_Data.weights[i];
	}

	float invTotal = 1.0f / total;
	for (int i = 0; i < NumWeight; i++)
		m_Data.m_Data.weights[i] *= invTotal;
}

void BlurShader::Begin()
{
	/**
	*	2D射影作成
	**/

	UINT pNumViewports = 1;
	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);
	m_Proj2DMat.CreateOrthoLH(vp.Width, vp.Height, 0, 1);
	m_Proj2DMat.Move(-1, -1, 0);
	m_Proj2DMat.Scale(1, -1, 1);
	m_View.m_Data.view = m_Proj2DMat;
	m_View.WriteData();

	/**
	*	レンダリングステート
	**/

	// 現状の各種ステート記憶
	m_Save.DSBackUp.SetAll_GetState();
	m_Save.BSBackUp.SetAll_GetState();

	// Z書き込み、判定無効化
	ZDepthStencilState ds;
	ds.Set_FromDesc(m_Save.DSBackUp.GetDesc());
	ds.Set_ZEnable(false);
	ds.Set_ZWriteEnable(false);
	ds.SetState();	//	セット

	// ブレンドステート
	ZBlendState bs;
	bs.Set_Alpha();
	bs.SetState();

}

void BlurShader::End()
{
	// ステート復元
	m_Save.BSBackUp.SetState();
	m_Save.DSBackUp.SetState();
}

void BlurShader::Draw2D(ZSP<ZTexture> tex, ZVec2 pos, ZVec2 size, ZVertexShader& vs, ZPixelShader& ps)
{

	// テクスチャ
	if (!tex)return;

	vs.SetShader();
	ps.SetShader();
	SetConstantBuffers();


	tex->SetTexturePS(0);

	// 頂点作成
	const int	 numVtx = 4;
	float		x2		= pos.x + size.x;
	float		y2		= pos.y + size.y;

	ZVertex_Pos_UV vertices[numVtx] =
	{
		{ ZVec3( pos.x,		y2,	0),ZVec2(0,1),	},
		{ ZVec3( pos.x,	 pos.y,	0),ZVec2(0),	},
		{ ZVec3(	x2,		y2,	0),ZVec2(1),	},
		{ ZVec3(	x2,	 pos.y,	0),ZVec2(1,0),	}
	};

	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);

	// セットされているテクスチャの解除(しないとダメ)
	ZDx.RemoveTexturePS(0);
}

void BlurShader::SetConstantBuffers()
{
	m_View.SetVS();
	m_View.SetPS();
}

void BlurShader::SetTargetSize(float w, float h)
{
	m_Data.m_Data.Texel = ZVec2(1.f / w, 1.f / h);
}

void BlurShader::ImGui()
{
#if _DEBUG

	auto imGuiFunc = [this]
	{
		auto dockID = ImGui::GetID("Blurs_Tab");
		if (ImGui::Begin("Blurs") == false)
		{
			ImGui::End();
			return;
		}
		ImGui::DockSpace(dockID);
		ImGui::End();

		//for (int i = 0; i < m_DebugSave->GetListSize(); i++) {
		for (int i = 0; i < m_DebugSaveCnt; i++) 
		{
			std::string name = "Blur_" + std::to_string(i);
			ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
			if (ImGui::Begin(name.c_str()))
			{
				if (m_DebugSave->GetTex(i)->GetTex())
					ImGui::Image(m_DebugSave->GetTex(i)->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::End();
		}
		
	};

	DW_IMGUI_FUNC(imGuiFunc);
#endif
}

float BlurShader::CalcReductionVal(BlurRenderFlgs flg)
{
	switch (flg)
	{
		case Original:
			return 1.f;
		case Harf:
			return 0.5f;
		case Quarter:
			return 0.25f;
		case Eighth:
			return 0.125f;
		case Sixteen:
			return 0.0625;
		case ThirtyOne:
			return 0.03125;
		case Sixty:
			return 0.015625;
	}

	//	よくわからない値が来たらハーフテクスチャで
	m_SizeFlg = Harf;
	return 0.5f;
	
}