#ifndef __BLUR_SHADER_H__
#define __BLUR_SHADER_H__

#include "../RendererBase.h"
#include "Shader/ContactBufferStructs.h"

class SpriteRenderer;

class BlurShader : public RendererBase
{
private:
	/*========================================================================*/
	//	Structure
	/*========================================================================*/

	//	レンダリング用データの保存用
	struct SaveData
	{
		ZDepthStencilState	DSBackUp;
		ZBlendState			BSBackUp;

		~SaveData()
		{
			DSBackUp.Release();
			BSBackUp.Release();
		}
	};


	/*========================================================================*/
	//	Const Variables
	/*========================================================================*/

	static const float DefaultDisp;
	static const int   DefaultLoop;

public:
	//	ブラーのレンダーサイズ
	enum BlurRenderFlgs
	{
		Original	=	1,
		Harf		=	2,
		Quarter		=	3,
		Eighth		=	4,
		Sixteen		=	5,
		ThirtyOne	=	6,
		Sixty		=	7,
	};
	
	static constexpr size_t BlurRenderCnt = 7;

public:
	BlurShader();
	~BlurShader() { Release(); }

	bool Init();
	void Release();
	
	/*-----------------------------------------------------------------------*/
	//	複数サイズのレンダーターゲットを作る(1/1から始まり1/16までを作成)
	/*-----------------------------------------------------------------------*/
	void CreateMipTarget(ZVec2 size);
	void DeleteMipTarget();
	ZSP<ZTexture> GetMipTex(BlurRenderFlgs f) { return m_MipTargets[f]->GetTexZSP(1); }

	/*-----------------------------------------------------------------------*/
	//	作業用テクスチャのサイズを変更(速度を上げたいときはQuarter以上を)
	/*-----------------------------------------------------------------------*/
	void ChangeRenderTarget(BlurRenderFlgs flg = Harf);

	void GenerateBlur(ZSP<ZTexture> tex,UINT loop = DefaultLoop,float disp = DefaultDisp);

	//	高輝度抽出（High Intensity Extraction)
	void HIE(ZSP<ZTexture> base, ZSP<ZTexture> out, const ZVec2& size);

	ZSP<ZTexture>	GetTexture() { return m_BlurTex->GetTexZSP(1); }

	//	Debug
	void SaveRTBegin();
	void SaveRTEnd();

	void ImGui();

private:
	void Begin();
	void End();
	void Draw2D(ZSP<ZTexture> tex,ZVec2 pos, ZVec2 size, ZVertexShader& vs, ZPixelShader& ps);
	void SetConstantBuffers();

	void SetTargetSize(float w, float h);
	
	bool CreateRenderTarget();
	void ReleaseRenderTarget();
	void ComputeGaussWeights(float disp);
	void ChangeTarget();

	float CalcReductionVal(BlurRenderFlgs flg);

private:
	//	C_Buffer
	ZConstantBuffer<cbGauss> m_Data;
	ZConstantBuffer<cbView> m_View;

	//	RenderSizeFlg
	BlurRenderFlgs	m_SizeFlg = Harf;

	//	RenderSave
	SaveData		m_Save;
	
	//	2D描画用データ
	ZRingDynamicVB	m_RingBuf;
	bool			m_IsBegined;
	ZMatrix			m_Proj2DMat;
	
	//	現在のレンダーサイズ
	ZVec2			m_RenderSize;
	
	//	レンダーターゲットの設定用
	ZRenderTargets		m_RT;
	ZSP<ZTextureSet>	m_BlurTex;

	//	縮小バッファの事前作成フラグ
	bool				m_MipFlg = false;
	ZMap<int, ZSP<ZTextureSet>>	m_MipTargets;

#if _DEBUG
	//	デバッグ用にブラーの履歴を保存する(最大10個)
	bool m_DebugSaveFlg = true;
	int  m_DebugSaveCnt	= 0;
	ZSP<ZTextureSet>	m_DebugSave;	//	デバッグ用
#endif

	SpriteRenderer* m_pSr;

};

#endif