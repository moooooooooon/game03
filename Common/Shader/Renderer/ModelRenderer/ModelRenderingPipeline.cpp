#include "MainFrame/ZMainFrame.h"
#include "Shader/Light/LightManager.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "SkinMeshRenderer.h"
#include "DebugColliderMeshRenderer.h"
#include "MeshRenderer.h"
#include "ModelRenderingPipeline.h"
#include "CommonSubSystems.h"

bool ModelRenderingPipeline::Init()
{
	Release();

	bool successed;

	m_SkinMeshRenderer				= Make_Shared(SkinMeshRenderer,sysnew);
	m_DebugColliderMeshRenderer		= Make_Shared(DebugColliderMeshRenderer,sysnew);
	m_StaticModelRenderer			= Make_Shared(MeshRenderer, sysnew);

	successed &= m_SkinMeshRenderer->Init();
	successed &= m_DebugColliderMeshRenderer->Init();
	successed &= m_StaticModelRenderer->Init();

	m_RenderTargets = Make_Shared(ZTextureSet,appnew,RenderTargetCnt);
	
	//	シャドウマップの書き込みテクスチャ作成
	m_LightDepthTexSize.Set(1024);
	m_TexLightScene = Make_Shared(ZTexture, sysnew);
	m_TexLightScene->CreateRT((int)m_LightDepthTexSize.x, (int)m_LightDepthTexSize.y, DXGI_FORMAT_R32_FLOAT);
	m_TexLightDepth = Make_Shared(ZTexture, sysnew);
	m_TexLightDepth->CreateDepth((int)m_LightDepthTexSize.x, (int)m_LightDepthTexSize.y);
	
	// ライトマネージャーにシャドウマップ用テクスチャのサイズをセット
	LiMgr.SetTexLightDepthSize(m_LightDepthTexSize);

	DXGI_FORMAT	fmt[] =
	{
		DXGI_FORMAT_R16G16B16A16_FLOAT,
		DXGI_FORMAT_R32_FLOAT,
		DXGI_FORMAT_R32_FLOAT,
		DXGI_FORMAT_R16G16B16A16_FLOAT,
		DXGI_FORMAT_R16G16_FLOAT
	};
	m_RenderTargets->CreateRTSet(APP.m_Window->GetWidth(), APP.m_Window->GetHeight(), RenderTargetCnt, fmt);

	return successed;
}

void ModelRenderingPipeline::Release()
{
	m_TexLightDepth = nullptr;
	m_TexLightScene = nullptr;
	m_SkinMeshRenderer = nullptr;
	m_DebugColliderMeshRenderer = nullptr;
	m_StaticModelRenderer = nullptr;

	m_RenderTargets = nullptr;
}

void ModelRenderingPipeline::Begin3DRendering()
{
	m_BackUp.GetNowAll();

	m_RenderTargets->AllClearRT(ZVec4(0,0,0,1));
	m_RenderTargets->GetTex(1)->ClearRT(ZVec4(1));
	m_RenderTargets->GetTex(4)->ClearRT(ZVec4(0.5f, 0.5f, 1.f, 1.f));

	m_RT.GetNowTop();

	for (int cnt = 0; cnt < m_RenderTargets->GetListSize(); cnt++)
		m_RT.RT(cnt, m_RenderTargets->GetTex(cnt)->GetRTTex());

	m_RT.SetToDevice();
}

void ModelRenderingPipeline::End3DRendering()
{
	m_BackUp.SetToDevice();
}

void ModelRenderingPipeline::Draw()
{
	m_TexLightScene->SetTexturePS(19);

	if (m_ZPreFlg)
	{
		m_SkinMeshRenderer->Flash();
		Z_Prepass();
	}
	else
		m_SkinMeshRenderer->Flash();
	
	
	m_StaticModelRenderer->Flash();

	ZDx.GetWhiteTex()->SetTexturePS(19);
}

void ModelRenderingPipeline::DrawShadow()
{
	ZRenderTarget_BackUpper b;

	m_TexLightScene->ClearRT(ZVec4(1, 1, 1, 1));
	m_TexLightDepth->ClearDepth();

	ZRenderTargets rt;
	rt.RT(0, m_TexLightScene->GetRTTex());
	rt.Depth(m_TexLightDepth->GetDepthTex());
	
	ZDx.SetViewport(m_LightDepthTexSize.x, m_LightDepthTexSize.y);
	rt.SetToDevice();

	m_StaticModelRenderer->FlashShadowObject();
	m_SkinMeshRenderer->FlashShadowObject();
}

void ModelRenderingPipeline::Z_Prepass() 
{
	m_StaticModelRenderer->Z_Prepass();
}

void ModelRenderingPipeline::ImGui()
{
#if _DEBUG

	static const ZString title[] =
	{
		"BasePass",
		"CameraDepth",
		"CharaDepth",
		"RefPow",
		"Normal"
	};

	auto Targets = [this]
	{
		auto dockID = ImGui::GetID("RenderTextures_Tab");
		if (ImGui::Begin("RenderTextures") == false)
		{
			ImGui::End();
			return;
		}
		ImGui::Checkbox("Use::Z-PrePass", &m_ZPreFlg);
		ImGui::DockSpace(dockID);
		ImGui::End();

		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("LightDepth"))
		{
			if (m_TexLightScene)
				ImGui::Image(m_TexLightScene->GetTex(), ImGui::GetWindowSize());
		}
		ImGui::End();

		for (int i = 0; i < m_RenderTargets->GetListSize(); i++)
		{
			ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
			if (ImGui::Begin(title[i].c_str()))
			{
				if (m_RenderTargets->GetTex(i)->GetTex())
					ImGui::Image(m_RenderTargets->GetTex(i)->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::End();
		}
		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("depth"))
			ImGui::Image(ZDx.GetDepthStencil()->GetTex(), ImGui::GetWindowSize());
		ImGui::End();
	};

	DW_IMGUI_FUNC(Targets);

#endif

}