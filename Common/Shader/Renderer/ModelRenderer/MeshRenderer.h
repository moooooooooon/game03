#ifndef __MODEL_RENDERER_H__
#define __MODEL_RENDERER_H__

#include "../ModelRenderer/ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

struct GameModelComponent;

class MeshRenderer : public BatchRenderer<ZMatrix*,GameModelComponent*>
{
public:
	MeshRenderer();
	virtual ~MeshRenderer() { Release(); }

	virtual bool Init()override;
	bool Init(int maxBufferSize);

	// 解放
	virtual void Release()override;

	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, GameModelComponent* pModel)override;
	void Submit_Shadow(ZMatrix* mat, GameModelComponent* pModel);

	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画 ↓の3つの関数一括呼び出し)
	virtual void Flash()override;

	// 本描画(影描画オブジェクト)
	void FlashShadowObject();

	// 本描画(通常描画オブジェクト)
	void FlashNormalObjects();

	// 本描画(半透明描画オブジェクト)
	void FlashSemiTransparentObjects();

	// 本描画(XRay描画オブジェクト)
	void FlashXRayObjects();

	void Z_Prepass();


#pragma region 描画設定

	// ライティング ON/OFF
	void SetLightEnable(bool flag);

	// 距離フォグ設定
	// fogDensity ... フォグの密度
	void SetDistanceFog(bool flag, const ZVec3* fogColor = nullptr, float fogDensity = -1);

	void SetMulColor(const ZVec4* color = &ZVec4::one);

#pragma endregion

private:
	// 描画処理の共通部分(バッファクリアなし)
	void _Flash(ZUnorderedMap<GameModelComponent*, ZMatrix*>& renderingObjects,
				ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>>& renderingModelComps,
				std::function<void(ZMaterial&)> prevDrawSubsetFunction,bool writeColorBuffers = true);

	virtual void SetConstantBuffers()override;
	bool CreateInstancingBuffer(int maxBufferSize);

	void SetMaterial(ZMaterial& mate);
	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;
	ZInstancingBuffer m_WorldMatrixBuffer;
	ZInstancingBuffer m_XRayColorBuffer;
	ZInstancingBuffer m_MulColorBuffer;
	size_t m_MatrixBufferSize;
	std::mutex m_Mtx;

	// 描画時に使用
	ZMatrix* m_MatrixArray;
	ZVec4* m_XRayColorArray;
	ZVec4* m_MulColorArray;

	// 影描画オブジェクト
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_ShadowRenderingObjects;
	// 通常描画オブジェクト
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_NormalRenderingObjects;
	// XRay描画オブジェクト
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_XRayRenderingObjects;
	// 半透明描画
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_SemiTransparentRenderingObjects;

	// メッシュをキーとしたコンポーネントリストの
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_ShadowRenderingModelComps;			// 影描画
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_NormalRenderingModelComps;			// 通常描画
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_XRayRenderingModelComps;				// XRay描画
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_SemiTransparentRenderingModelComps;	// 半透明描画

};

#endif