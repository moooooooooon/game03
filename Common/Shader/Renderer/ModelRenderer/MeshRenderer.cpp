#include "MainFrame/ZMainFrame.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "MeshRenderer.h"

MeshRenderer::MeshRenderer()
	: m_MatrixArray(nullptr), m_XRayColorArray(nullptr), m_MulColorArray(nullptr), m_MatrixBufferSize(0)
{
}

bool MeshRenderer::Init()
{
	Release();

	m_Shaders.SetVS("Inst", APP.m_ResStg.LoadVertexShader("Shader/Model_InstancingVS.cso"));
	m_Shaders.SetPS("Inst", APP.m_ResStg.LoadPixelShader("Shader/Model_PS.cso"));

	//	Z-プリパス用
	m_Shaders.SetPS("ZPre", APP.m_ResStg.LoadPixelShader("Shader/Model_MonoPS.cso"));

	// 影
	m_Shaders.SetVS("InstShadow", APP.m_ResStg.LoadVertexShader("Shader/ShadowInstancingVS.cso"));
	m_Shaders.SetPS("InstShadow", APP.m_ResStg.LoadPixelShader("Shader/ShadowPS.cso"));
	
	m_Shaders.SetPS("Chara", APP.m_ResStg.LoadPixelShader("Shader/Model_CharaPS.cso"));


	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);
	
	bool successed;
	// ワールド行列用バッファ作成
#ifdef _DEBUG
	successed &= CreateInstancingBuffer(5000); //数値は適当
#endif
#ifdef NDEBUG
	successed &= CreateInstancingBuffer(50000); // 数値は適当
#endif

	return successed;
}

bool MeshRenderer::Init(int maxBufferSize)
{
	Release();

	bool successed;

	m_Shaders.SetVS("Inst", APP.m_ResStg.LoadVertexShader("Shader/Model_InstancingVS.cso"));
	m_Shaders.SetPS("Inst", APP.m_ResStg.LoadPixelShader("Shader/Model_PS.cso"));

	//	Z-プリパス用
	m_Shaders.SetPS("ZPre", APP.m_ResStg.LoadPixelShader("Shader/Model_MonoPS.cso"));

	// 影
	m_Shaders.SetVS("InstShadow", APP.m_ResStg.LoadVertexShader("Shader/ShadowInstancingVS.cso"));
	m_Shaders.SetPS("InstShadow", APP.m_ResStg.LoadPixelShader("Shader/ShadowPS.cso"));

	m_Shaders.SetPS("Chara", APP.m_ResStg.LoadPixelShader("Shader/Model_CharaPS.cso"));

	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);

	// ワールド行列用バッファ作成
	successed &= CreateInstancingBuffer(maxBufferSize);

	return successed;
}

void MeshRenderer::Release()
{
	m_Shaders.RemoveAllShaders();

	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();

	m_WorldMatrixBuffer.Release();

	SAFE_DELPTR(m_MatrixArray);
	SAFE_DELPTR(m_XRayColorArray);
	SAFE_DELPTR(m_MulColorArray);

	m_MatrixBufferSize = 0;
}

void MeshRenderer::Submit(ZMatrix* mat, GameModelComponent* pModel)
{
	if (pModel->Model == nullptr)return;
	std::lock_guard<std::mutex> lg(m_Mtx);

	// フラグによって振り分け
	auto& pRenderFlg = pModel->RenderFlg;
	if (pRenderFlg.Character) // XRay対象オブジェクトか
	{
		m_XRayRenderingObjects[pModel] = mat;
		m_XRayRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
	}
	else if (pRenderFlg.SemiTransparent) // 半透明対象オブジェクトか
	{
		m_SemiTransparentRenderingObjects[pModel] = mat;
		m_SemiTransparentRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
	}
	else // 通常描画オブジェクトか
	{
		m_NormalRenderingObjects[pModel] = mat;
		m_NormalRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
	}
}

void MeshRenderer::Submit_Shadow(ZMatrix * mat, GameModelComponent * pModel)
{
	if (pModel->Model == nullptr)return;
	std::lock_guard<std::mutex> lg(m_Mtx);
	m_ShadowRenderingObjects[pModel] = mat;
	m_ShadowRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
}

void MeshRenderer::Flash()
{	
	FlashXRayObjects();
	FlashNormalObjects();
	FlashSemiTransparentObjects();
}

void MeshRenderer::FlashShadowObject()
{
	if (m_ShadowRenderingObjects.empty())
		return;

	m_Shaders.GetVS("InstShadow")->SetShader();
	m_Shaders.GetPS("InstShadow")->SetShader();

	m_WorldMatrixBuffer.SetToSlot(sizeof(ZMatrix), 0);

	_Flash(m_ShadowRenderingObjects, m_ShadowRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		// テクスチャをセット
		if (mate.TexSet->GetTex(0))
			mate.TexSet->GetTex(0)->SetTexturePS(0);
		else
			ZDx.GetWhiteTex()->SetTexturePS(0);
	}, false);

	m_ShadowRenderingObjects.clear();
	m_ShadowRenderingModelComps.clear();
}

void MeshRenderer::FlashNormalObjects()
{
	m_Shaders.GetVS("Inst")->SetShader();
	m_Shaders.GetPS("Inst")->SetShader();

	SetConstantBuffers();
	m_cb1_PerObject.WriteData();

	_Flash(m_NormalRenderingObjects, m_NormalRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		SetMaterial(mate);
		SetTextures(mate);

		m_cb2_PerMaterial.WriteData();
	});

	m_NormalRenderingModelComps.clear();
	m_NormalRenderingObjects.clear();
}

void MeshRenderer::FlashSemiTransparentObjects()
{
	m_Shaders.GetVS("Inst")->SetShader();
	m_Shaders.GetPS("Inst")->SetShader();

	SetConstantBuffers();
	m_cb1_PerObject.WriteData();
	
	// この処理はSystemにしたほうがよさげ
	for (auto& param : m_SemiTransparentRenderingObjects)
	{
		auto& pRenderFlg = param.first->RenderFlg;
		if (pRenderFlg.Delete)
		{
			pRenderFlg.Alpha -= 0.01f;
			if (pRenderFlg.Alpha < 0.5f) pRenderFlg.Alpha = 0.5f;
		}
	}

	_Flash(m_SemiTransparentRenderingObjects,m_SemiTransparentRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		SetMaterial(mate);
		SetTextures(mate);

		m_cb2_PerMaterial.WriteData();
	});

	
	m_SemiTransparentRenderingModelComps.clear();
	m_SemiTransparentRenderingObjects.clear();
}

void MeshRenderer::FlashXRayObjects()
{
	m_Shaders.GetVS("Inst")->SetShader();
	m_Shaders.GetPS("Chara")->SetShader();
	
	SetConstantBuffers();
	m_cb1_PerObject.WriteData();

	_Flash(m_XRayRenderingObjects,m_XRayRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		SetMaterial(mate);
		SetTextures(mate);

		m_cb2_PerMaterial.WriteData();
	});

	m_XRayRenderingObjects.clear();
	m_XRayRenderingModelComps.clear();
}

void MeshRenderer::Z_Prepass()
{
	m_Shaders.GetVS("Inst")->SetShader();
	m_Shaders.GetPS("ZPre")->SetShader();

	m_cb2_PerMaterial.SetPS();

	m_WorldMatrixBuffer.SetToSlot(sizeof(ZMatrix), 0);

	if (m_XRayRenderingObjects.empty() == false)
	{
		_Flash(m_XRayRenderingObjects,m_XRayRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			SetMaterial(mate);

			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			m_cb2_PerMaterial.WriteData();

		},false);
	}

	if (m_NormalRenderingObjects.empty() == false)
	{
		_Flash(m_NormalRenderingObjects, m_NormalRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			SetMaterial(mate);

			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			m_cb2_PerMaterial.WriteData();

		}, false);
	}

	if (m_SemiTransparentRenderingObjects.empty() == false)
	{
		_Flash(m_SemiTransparentRenderingObjects, m_SemiTransparentRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			SetMaterial(mate);

			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			m_cb2_PerMaterial.WriteData();

		}, false);
	}

}

void MeshRenderer::SetLightEnable(bool flag)
{
	m_cb1_PerObject.m_Data.LightEnable = flag ? 1 : 0;
}

void MeshRenderer::SetDistanceFog(bool flag, const ZVec3 * fogColor, float fogDensity)
{
	m_cb1_PerObject.m_Data.DistanceFogDensity = flag ? 1.0f : 0.0f;
	if (fogColor)
		m_cb1_PerObject.m_Data.DistanceFogColor = *fogColor;
	if (fogDensity > 0)
		m_cb1_PerObject.m_Data.DistanceFogDensity = fogDensity;
}

void MeshRenderer::SetMulColor(const ZVec4 * color)
{
	if (color == nullptr)
		m_cb1_PerObject.m_Data.MulColor = ZVec4::one;
	else
		m_cb1_PerObject.m_Data.MulColor = *color;
}

void MeshRenderer::_Flash( ZUnorderedMap<GameModelComponent*, ZMatrix*>& renderingObjects,
											ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>>& renderingModelComps,
											std::function<void(ZMaterial&)> prevDrawSubsetFunction,
											bool writeColorBuffers)
{
	// 描画するオブジェクトがないなら
	if (renderingObjects.empty() || renderingModelComps.empty())
		return;

	if (!prevDrawSubsetFunction)
		prevDrawSubsetFunction = [](ZMaterial&) {};

	for (auto& param : renderingModelComps)
	{
		auto& gmodel = param.first; // <- 描画対象(GameModel)

		// パラメータ(行列,合成色,XRayの色)をインスタンシング用バッファに格納
		auto mulColor = m_cb1_PerObject.m_Data.MulColor;
		uint numInstance = param.second.size();
		// バッファにも上限があるので一回にインスタンス描画する数の制御処理を書いたほうがいいが面倒なので省略
		for (uint i = 0; i < numInstance; i++)
		{
			auto& comp = param.second[i];
			auto& mat = renderingObjects[comp];
			m_MatrixArray[i] = (*mat);
			
			if(writeColorBuffers)
			{
				auto& pRenderFlg = comp->RenderFlg;
				mulColor.w = pRenderFlg.Alpha;
				m_MulColorArray[i] = mulColor;
				m_XRayColorArray[i] = pRenderFlg.XRayCol;
			}
		}

		m_WorldMatrixBuffer.WriteData(m_MatrixArray,sizeof(ZMatrix) * numInstance);
		if(writeColorBuffers)
		{
			m_MulColorBuffer.WriteData(m_MulColorArray, sizeof(ZVec4) * numInstance);
			m_XRayColorBuffer.WriteData(m_XRayColorArray, sizeof(ZVec4) * numInstance);
		}

		for (auto& smodel : gmodel->GetModelTbl_Static())
		{
			auto& mesh = smodel->GetMesh();
			mesh->SetDrawData();
			
			auto& materials = smodel->GetMaterials();
			uint numSubset = materials.size();

			// マテリアルループ
			for (uint currentSubset = 0; currentSubset < numSubset; currentSubset++)
			{
				if (mesh->GetSubset(currentSubset)->FaceCount == 0)
					continue;

				ZMaterial& mate = materials[currentSubset];

				prevDrawSubsetFunction(mate);
				
				mesh->DrawSubsetInstance(currentSubset, numInstance);
			}
		}

	} // for(auto& param : renderingModelComps)

}

void MeshRenderer::SetConstantBuffers()
{
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
	m_cb2_PerMaterial.SetPS();

	m_WorldMatrixBuffer.SetToSlot(sizeof(ZMatrix), 0);
	m_XRayColorBuffer.SetToSlot(sizeof(ZVec4), 0);
	m_MulColorBuffer.SetToSlot(sizeof(ZVec4), 0);

}

bool MeshRenderer::CreateInstancingBuffer(int maxBufferSize)
{
	// Matrix
	uint slot = 1;
	bool s = m_WorldMatrixBuffer.Create(sizeof(ZMatrix) * maxBufferSize, slot);
	assert(s);

	// XRay
	slot = 2;
	s &= m_XRayColorBuffer.Create(sizeof(ZVec4) * maxBufferSize, slot);
	assert(s);
	
	// MulColor
	slot = 3;
	s &= m_MulColorBuffer.Create(sizeof(ZVec4) * maxBufferSize, slot);
	assert(s);

	if(s)
	{
		m_MatrixBufferSize = maxBufferSize;
		m_MatrixArray = sysnewArray(ZMatrix, maxBufferSize);
		m_XRayColorArray = sysnewArray(ZVec4, maxBufferSize);
		m_MulColorArray = sysnewArray(ZVec4, maxBufferSize);
		return true;
	}

	auto m = EFFECT.GetManager();

	return false;
}

void MeshRenderer::SetMaterial(ZMaterial& mate)
{
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.AttackPower;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;
}

void MeshRenderer::SetTextures(ZMaterial& mate)
{
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);
	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);
	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}