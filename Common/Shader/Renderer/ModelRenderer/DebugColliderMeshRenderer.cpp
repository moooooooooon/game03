#include "MainFrame/ZMainFrame.h"
#include "DebugColliderMeshRenderer.h"

bool DebugColliderMeshRenderer::Init()
{
	Release();

	bool successed = true;

	// シェーダ読み込み
	m_Shaders.SetVS("WireModel", APP.m_ResStg.LoadVertexShader("Shader/Model_VS.cso"));
	m_Shaders.SetPS("WireModel", APP.m_ResStg.LoadPixelShader("Shader/Model_MonoPS.cso"));

	successed &= m_cb0_Matrix.Create(0);
	successed &= m_cb1_PerObject.Create(1);
	successed &= m_cb2_PerMaterial.Create(2);

	// ワイヤフレーム描画用ラスタライズステート設定
	m_WireframeState.SetAll_Standard();
	m_WireframeState.Set_FillMode_Wireframe();

	return false;
}

void DebugColliderMeshRenderer::Release()
{
	m_Shaders.RemoveAllShaders();
	m_cb0_Matrix.Release();
	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();
	m_Save.Release();
	m_WireframeState.Release();
}

void DebugColliderMeshRenderer::Begin()
{
	m_Save.SetAll_GetState();
	m_WireframeState.SetState();
}

void DebugColliderMeshRenderer::End()
{
	m_Save.SetState();
}

void DebugColliderMeshRenderer::DrawMeshWireframe(ZMesh& mesh,const ZMatrix& mat, const ZVec4 * color)
{
	SetConstantBuffers();

	SetWorldMat(mat);
	// オブジェクト単位のデータを定数バッファへ書き込み
	m_cb0_Matrix.WriteData();
	m_cb1_PerObject.WriteData();

	m_Shaders.GetVS("WireModel")->SetShader();
	m_Shaders.GetPS("WireModel")->SetShader();

	// マテリアル単位のデータを定数バッファへ書き込み
	if (color)
		m_cb2_PerMaterial.m_Data.Diffuse.Set(*color);
	m_cb2_PerMaterial.m_Data.Emissive.Set(0, 0, 0, 0);
	m_cb2_PerMaterial.WriteData();

	// メッシュ描画情報セット
	mesh.SetDrawData();

	// マテリアル(サブセットループ)
	for (size_t i = 0; i < mesh.GetSubset().size(); i++)
		mesh.DrawSubset(i);

}

void DebugColliderMeshRenderer::SetConstantBuffers()
{
	// オブジェクト単位のデータ
	m_cb0_Matrix.SetVS();
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();

	// マテリアル単位のデータ
	m_cb2_PerMaterial.SetPS();
}
