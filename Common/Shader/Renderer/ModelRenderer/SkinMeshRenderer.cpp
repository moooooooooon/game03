#include "MainFrame/ZMainFrame.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "SkinMeshRenderer.h"

bool SkinMeshRenderer::Init()
{
	Release();

	bool successed = true;
	 
	m_Shaders.SetVS("Skin", APP.m_ResStg.LoadVertexShader("Shader/Model_SkinVS.cso"));
	m_Shaders.SetPS("Skin", APP.m_ResStg.LoadPixelShader("Shader/Model_PS.cso"));
	
	m_Shaders.SetVS("SkinShadow", APP.m_ResStg.LoadVertexShader("Shader/ShadowSkinVS.cso"));
	m_Shaders.SetPS("SkinShadow", APP.m_ResStg.LoadPixelShader("Shader/ShadowPS.cso"));
	
	m_Shaders.SetPS("Chara", APP.m_ResStg.LoadPixelShader("Shader/Model_CharaPS.cso"));

	// コンスタントバッファー作成
	m_cb0_Matrix.Create(0);
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);

	return successed;
}

void SkinMeshRenderer::Release()
{
	m_Shaders.RemoveAllShaders();

	m_cb0_Matrix.Release();
	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();

}

void SkinMeshRenderer::Submit(ZMatrix* mat, GameModelComponent* modelComp, ModelBoneControllerComponent* boneComp)
{
	if (modelComp->Model == nullptr) return;

	// スキンメッシュのモデルがなければ
	if (modelComp->Model->GetModelTbl_Skin().empty())
		return;

	if (mat == nullptr)return;

	m_RenderBuffer[std::make_tuple(modelComp,boneComp)].push_back(mat);
}

void SkinMeshRenderer::Flash()
{
	m_Shaders.GetVS("Skin")->SetShader();
	SetConstantBuffers();

	for (auto& param : m_RenderBuffer)
	{
		auto& modelComp = std::get<0>(param.first);
		auto& boneComp = std::get<1>(param.first);
		auto& gModel = modelComp->Model;
		auto& bc = boneComp->BoneController;
		auto& rFlg = modelComp->RenderFlg;

		if (rFlg.Character)
		{
			m_cb1_PerObject.m_Data.X_Col = rFlg.XRayCol;
			m_Shaders.GetPS("Chara")->SetShader();
		}
		else 
			m_Shaders.GetPS("Skin")->SetShader();
	
		// ボーン行列セット
		bc.GetBoneConstantBuffer().SetVS(3);

		for (auto pModel : gModel->GetModelTbl_Skin())
		{
			// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
			pModel->GetMesh()->SetDrawData();

			auto& materials = pModel->GetMaterials();
			for (auto mat : param.second)
			{
				// 行列セット
				SetMatrix(*mat);
				m_cb0_Matrix.WriteData();
				m_cb1_PerObject.WriteData();

				for (UINT i = 0; i < materials.size(); i++)
				{
					if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
						continue;

					ZMaterial& mate = materials[i];
					SetMaterial(mate);

					m_cb2_PerMaterial.WriteData();

					SetTextures(mate);

					pModel->GetMesh()->DrawSubset(i);

				}
			}

		}

	}

	m_RenderBuffer.clear();
}

void SkinMeshRenderer::SetConstantBuffers()
{
	// オブジェクト単位のデータ
	m_cb0_Matrix.SetVS();
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
	
	// マテリアル単位のデータ
	m_cb2_PerMaterial.SetPS();
}

void SkinMeshRenderer::SetMatrix(const ZMatrix & mat)
{
	m_cb0_Matrix.m_Data.Mat = mat;
}

void SkinMeshRenderer::SetXColor(const ZVec4& col)
{
	m_cb1_PerObject.m_Data.X_Col = col;
}

void SkinMeshRenderer::SetMaterial(ZMaterial& mate)
{
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.AttackPower;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;
}

void SkinMeshRenderer::SetTextures(ZMaterial& mate)
{
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);

	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);
	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}


void SkinMeshRenderer::FlashShadowObject()
{
	m_Shaders.GetVS("SkinShadow")->SetShader();
	m_Shaders.GetPS("SkinShadow")->SetShader();

	m_cb0_Matrix.SetVS();

	for (auto& param : m_RenderBuffer)
	{
		auto& modelComp = std::get<0>(param.first);
		auto& boneComp = std::get<1>(param.first);
		auto& gModel = modelComp->Model;
		auto& bc = boneComp->BoneController;
		
		// ボーン行列セット
		bc.GetBoneConstantBuffer().SetVS(2);

		for (auto& pModel : gModel->GetModelTbl_Skin())
		{
			// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
			pModel->GetMesh()->SetDrawData();

			auto& materials = pModel->GetMaterials();
			for (auto mat : param.second)
			{
				// 行列セット
				SetMatrix(*mat);
				m_cb0_Matrix.WriteData();
				m_cb1_PerObject.WriteData();

				for (UINT i = 0; i < materials.size(); i++)
				{
					if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
						continue;

					ZMaterial& mate = materials[i];
					SetMaterial(mate);
					m_cb2_PerMaterial.WriteData();

					// テクスチャをセット
					if (mate.TexSet->GetTex(0))
						mate.TexSet->GetTex(0)->SetTexturePS(0);
					else
						ZDx.GetWhiteTex()->SetTexturePS(0);

					pModel->GetMesh()->DrawSubset(i);

				}
			}

		} // for (auto& pModel : gModel->GetModelTbl_Skin())

	} // for (auto& param : m_RenderBuffer)

}