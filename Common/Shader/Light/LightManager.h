#ifndef __LIGHT_MANAGER_H__
#define __LIGHT_MANAGER_H__

#include "Lights.h"
#include "Shader/ContactBufferStructs.h"

class LightCamera;

// ライト管理クラス
//  シェーダの定数バッファのb5,b6,b7を使用
class LightManager
{
public:
	LightManager();

	// 初期化
	void Init();

	// 解放
	//  各ライトの管理リストは解放しない
	//  定数バッファなどが解放する
	void Release();

	// 平行光源取得
	ZSP<DirLight> GetDirLight();

	// ポイントライト追加
	//  点光源を追加する場合はこの関数を呼ぶ 誰も保持しなくなった時点で光源は消滅する
	ZSP<PointLight> AddPointLight();

	// スポットライト追加
	//  点光源を追加する場合はこの関数を呼ぶ 誰も保持しなくなった時点で光源は消滅する
	ZSP<SpotLight> AddSpotLight();

	// 平行光源視点のカメラ取得
	ZSP<LightCamera> GetLightCamera();

	// 環境光の色設定
	void SetAmbientLightColor(const ZVec3& color);

	// シャドウマップ用の深度テクスチャサイズをセット
	void SetTexLightDepthSize(const ZVec2& size);

	// シャドウマップ用カメラ位置設定
	void SetShadowCamTargetPoint(const ZVec3& pos);

	// 更新処理
	//  全てのライト情報を定数バッファに書き込む
	void Update();

	void ImGui();

public:
	// 環境光
	ZVec3 m_AmbientLightColor;

	// 平行光源
	ZSP<DirLight>		m_DirLight;
	
	// 点光源
	ZSList<ZWP<PointLight>>	m_PointLightList;	// ポイントライト管理リスト

	// スポット光源
	ZSList<ZWP<SpotLight>> m_SpotLightList;		// スポットライト管理リスト

	// ModelRenderers用ライト定数バッファ(通常は１フレーム単位での更新)
	ZConstantBuffer<cbSampleShaderLight> m_cb12_Light;

	//	ディレクショナルライト光源のカメラ
	ZSP<LightCamera>		m_Cam;
};


#endif