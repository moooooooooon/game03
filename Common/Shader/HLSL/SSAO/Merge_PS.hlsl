#include "inc_SSAO.hlsli"

float4 main(VS_OUT In) : SV_Target0
{
	float4 diffuse = DiffuseTex.Sample(LinerClampSmp,In.UV);
	float4 ssao = SSAOTex.Sample(LinerClampSmp, In.UV);
	float4 result = diffuse * ssao;
	result.a = 1;
	return result;
}