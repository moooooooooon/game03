#include "inc_SSAO.hlsli"

VS_OUT main(float4 pos : POSITION,
			float2 uv : TEXCOORD0)
{
	VS_OUT Out = (VS_OUT)0;

	// ���_���W���ˉe�ϊ�
	Out.Pos = mul(pos, g_mTransform);
	// UV
	Out.UV = uv;

	return Out;
}