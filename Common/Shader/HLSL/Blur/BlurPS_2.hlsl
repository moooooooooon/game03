#include "inc_Blur.hlsli"

PsOut main(PsGaussIn In)
{
    PsOut Out = (PsOut) 0;

    Out.Col += Weights0.x * (ColorTex.Sample(ColorSmp, In.texcoord0) + ColorTex.Sample(ColorSmp, In.texcoord7 + float2(0.f, Offset.y)));
    Out.Col += Weights0.y * (ColorTex.Sample(ColorSmp, In.texcoord1) + ColorTex.Sample(ColorSmp, In.texcoord6 + float2(0.f, Offset.y)));
    Out.Col += Weights0.z * (ColorTex.Sample(ColorSmp, In.texcoord2) + ColorTex.Sample(ColorSmp, In.texcoord5 + float2(0.f, Offset.y)));
    Out.Col += Weights0.w * (ColorTex.Sample(ColorSmp, In.texcoord3) + ColorTex.Sample(ColorSmp, In.texcoord4 + float2(0.f, Offset.y)));
  															 															
    Out.Col += Weights1.x * (ColorTex.Sample(ColorSmp, In.texcoord4) + ColorTex.Sample(ColorSmp, In.texcoord3 + float2(0.f, Offset.y)));
    Out.Col += Weights1.y * (ColorTex.Sample(ColorSmp, In.texcoord5) + ColorTex.Sample(ColorSmp, In.texcoord2 + float2(0.f, Offset.y)));
    Out.Col += Weights1.z * (ColorTex.Sample(ColorSmp, In.texcoord6) + ColorTex.Sample(ColorSmp, In.texcoord1 + float2(0.f, Offset.y)));
    Out.Col += Weights1.w * (ColorTex.Sample(ColorSmp, In.texcoord7) + ColorTex.Sample(ColorSmp, In.texcoord0 + float2(0.f, Offset.y)));
	
    return Out;
}
