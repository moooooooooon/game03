#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"

// g_Diffuseを単色で出力するピクセルシェーダ
PsOut main(PsIn In)
{
    float4 MateCol = 1; 
    float4 texCol = MeshTex.Sample(WrapSmp, In.UV); 
    MateCol = g_Diffuse * texCol * In.MulColor; 
	clip(MateCol.a - 0.01f);
    PsOut Out = (PsOut)0;
    Out.Color = MateCol;
    return Out;
}
