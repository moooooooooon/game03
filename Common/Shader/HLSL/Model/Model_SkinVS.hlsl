#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"

//===============================================
// スキンメッシュ用頂点シェーダー
//===============================================
VsOut main(VsSkinningIn In)
{
	//----------------------------------------------------------------------	
	// スキニング処理　最大4つのボーン行列をブレンドし、座標や法線などを変換する。
	//----------------------------------------------------------------------	
	row_major float4x4 mBones = 0; // 最終的なワールド行列
	[unroll]
    for (int i = 0; i < 4; i++)
	{
        mBones += g_mBones[In.blendIndex[i]] * In.blendWeight[i];
	}
    
	// ボーン行列で変換(まだローカル座標系)
	In.pos			= mul(In.pos, mBones);
	In.tangent		= mul(In.tangent, (float3x3) mBones);
	In.binormal		= mul(In.binormal, (float3x3) mBones);
	In.normal		= mul(In.normal, (float3x3) mBones);
	//----------------------------------------------------------------------	


    VsOut Out = (VsOut)0;

	//-------------------------------------
	// 座標変換
	//-------------------------------------
	Out.Pos		= mul(In.pos, g_mW);   // ワールド変換
	Out.wPos	= Out.Pos.xyz;     // ワールド座標を憶えておく
	Out.Pos		= mul(Out.Pos, g_mV);   // ビュー変換
	Out.wvPos	= Out.Pos.xyz;    // ビュー座標を憶えておく
    Out.Depth = mul(Out.Pos, g_mPO).zw;
  
	Out.Pos		= mul(Out.Pos, g_mP);   // 射影変換
 
	//-------------------------------------
	// UV
	//-------------------------------------
	Out.UV		= In.uv;

	//-------------------------------------
	// ３種の法線
	//-------------------------------------
	float3x3 mW = mul((float3x3)g_mW, mBones);
	Out.wT		= normalize(mul(In.tangent, mW));
	Out.wB		= normalize(mul(In.binormal,mW));
	Out.wN		= normalize(mul(In.normal,  mW));
	
	// 加工なしの法線
	Out.DefN	= In.normal;

	// XRayColor
	Out.XRayColor = g_XColor;
	Out.MulColor = g_MulColor;

	return Out;
}
