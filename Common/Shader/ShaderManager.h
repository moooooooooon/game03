#ifndef __SHADER_MANAGER_H__
#define __SHADER_MANAGER_H__

#include "Renderer/Renderers.h"
#include "RendererSet/RendererSet.h"

/*------------------------------------------------------------------*/
// 定数バッファ
/*------------------------------------------------------------------*/
//  cbuffer(b0〜b8)		… 自由

//	cbuffer(b9)			…（固定）DoFで使用
//  cbuffer(b10)		… (固定)シャドウマップで使用予定
//  cbuffer(b11)		… (固定)カメラデータで使用
//  cbuffer(b12)		… (固定)ライトデータで使用
//  cbuffer(b13)		… (固定)デバッグ用で使用予定

/*------------------------------------------------------------------*/
// サンプラステート
/*------------------------------------------------------------------*/
//  s0					… Linear補間のWrapサンプラ
//  s1					… Linear補間のClampサンプラ
//  s2					… Point補間のWrapサンプラ
//  s3					… Point補間のClampサンプラ
//  s4〜9				… 固定用の予約
//  s10					… ShadowMap用 Comparisonサンプラ

//  s11〜s15			… 自由

struct ShaderManager
{
public:
	ShaderManager();

	void Init();
	void Release();

	// シェーダ(定数バッファ)のカメラのデータを更新
	void UpdateCamera(const ZCamera* camera = &ZCamera::LastCam);

	template<typename T>
	inline T& GetRenderer()
	{
		return m_RendererSet.GetRenderer<T>();
	}

public:
	// ステート
	ZBlendState		m_bsAlpha;				// 半透明合成モード
	ZBlendState		m_bsAdd;				// 加算合成モード
	ZBlendState		m_bsNoAlpha;			// アルファ無効モード
	ZBlendState		m_bsAlpha_AtoC;			// 半透明合成モード(AlphaToCoverage付き)

	// サンプラ
	ZSamplerState	m_smp0_Linear_Wrap;		// s0	Linear補間& Wrapモード用サンプラ
	ZSamplerState	m_smp1_Linear_Clamp;	// s1	Linear補間& Clampモード用サンプラ
	ZSamplerState	m_smp2_Point_Wrap;		// s2	Point補間& Wrapモード用サンプラ
	ZSamplerState	m_smp3_Point_Clamp;		// s3	Point補間& Clampモード用サンプラ
	ZSamplerState	m_smp10_Shadow;			// s10	シャドウマップ用サンプラ
	ZSamplerState	m_smp11_Point_Mirror;	// s11	Point補間& Mirrorモード用サンプラ

	// カメラ用定数バッファ
	ZConstantBuffer<cbCamera>	m_cb11_Camera;

	// デバッグ用定数バッファ
	ZConstantBuffer<cbDebug>	m_cb13_Debug;

	// その他
	ZTexture m_TexCubeMap;	// キューブマップテクスチャ(環境マッピング用)

private:
	// レンダラー群
	RendererSet		m_RendererSet;
	
};


#endif