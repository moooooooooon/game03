#include "PCH/pch.h"
#include "RendererSet.h"

RendererSet::~RendererSet()
{
	Release();
}

void RendererSet::Release()
{
	if (m_Orders.empty())return;

	for (auto it = m_Orders.rbegin(); it != m_Orders.rend(); it++)
	{
		auto findIt = m_Renderers.find(*it);
		assert(findIt != m_Renderers.end());

		findIt->second.Reset();
		m_Renderers.erase(findIt);
	}

	m_Orders.clear();
}
