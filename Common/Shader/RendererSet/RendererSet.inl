template<typename T,typename... Args>
inline T& RendererSet::AddRenderer(Args&&... args)
{
	// レンダラーがすでに存在している
	assert(!HasRenderer<T>());

	auto index = rtti::TypeID<T>().hash_code();
	m_Orders.push_back(index);
	m_Renderers[index] = Make_Shared(T, sysnew, std::forward<Args>(args)...);
	return GetRenderer<T>();
}

template<typename T>
inline T& RendererSet::GetRenderer()
{
	// レンダラーが存在しない
	assert(HasRenderer<T>());
	auto index = rtti::TypeID<T>().hash_code();
	return *(T*)(m_Renderers[index].GetPtr());
}

template<typename T>
inline bool RendererSet::HasRenderer()
{
	const auto index = rtti::TypeID<T>().hash_code();
	return m_Renderers.find(index) != m_Renderers.end();
}

template<typename T>
inline void RendererSet::RemoveRenderer()
{
	// レンダラーが存在しない
	assert(HasRenderer<T>());

	const auto index = rtti::TypeID<T>().hash_code();
	m_Renderers.erase(index);
	m_Orders.erase(std::remove_if(std::begin(m_Orders), std::end(m_Orders),
		[index](const auto& elem)
	{
		return index == elem;
	}, std::end(m_Orders)));

}
