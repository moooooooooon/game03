#ifndef __DOF_PASS_H__
#define __DOF_PASS_H__

#include "Shader/ContactBufferStructs.h"
#include "Shader/Renderer/DOF/DOF_Shader.h"

class BlurShader;

class DOFPass : public ZPostEffectPass
{
public:
	DOFPass();

	void Init();
	void Release();

	void ImGui()override;

	void LoadSettingFromJson(const json11::Json& jsonObj)override;
	void SaveSettingToJson(cereal::JSONOutputArchive& archive)override;

	ZString GetPassName()override;
	void RenderPass(ZSP<ZTexture> source);

private:
	DOF_Shader m_DOF;
	ZSP<ZTexture> m_TexDOF;
	ZConstantBuffer<cbDoFParam> m_cb9_DOFParam;
	BlurShader* m_pBs;
};

#endif