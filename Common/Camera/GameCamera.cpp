#include "MainFrame/ZMainFrame.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "Shader/ShaderManager.h"
#include "GameCamera.h"
#include "CommonSubSystems.h"

GameCamera::GameCamera()
{
	// カメラデータ初期化
	m_IsFPSMode = false;
	m_LocalMat.CreateMove(0, 10, -15);
	SetPerspectiveFovLH(60, (float)ZDx.GetRezoW() / (float)ZDx.GetRezoH(), 0.01f, 200);
}

void GameCamera::Init(float rx, float ry, float camZoom, bool isFpsMode)
{
	m_LocalMat.CreateRotateX(rx);
	m_LocalMat.RotateY(ry);
	m_LocalMat.Move_Local(0, 0, camZoom);
	m_BaseMat.CreateMove(0, 0, 0);
	m_IsFPSMode = isFpsMode;
	UpdateFrustumPlanes();
	UpdateCameraAABB();
}

void GameCamera::Init(const ZVec3 & BaseTrans, const ZVec3 & LocalTrans, bool isFpsMode)
{
	m_BaseMat.CreateMove(BaseTrans);
	m_LocalTransMat.CreateMove(LocalTrans);
	m_IsFPSMode = isFpsMode;
	UpdateFrustumPlanes();
	UpdateCameraAABB();
	INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), m_IsFPSMode);
}

void GameCamera::ChangeAspect(float aspect)
{
	SetPerspectiveFovLH(60, aspect, 0.01f, 200);
}

void GameCamera::Update(const POINT& moveVal)
{
	// カメラ操作	
	if ((APP.m_Window->IsWindowActive() && m_IsFPSMode) == false)
		return;
	
	// 回転
	
	m_RotAngle.x += moveVal.y * m_RotRatio;
	m_RotAngle.y += moveVal.x * m_RotRatio;

	// 角度制限
	if (m_RotAngle.x > m_XRotAngleLimit.x)m_RotAngle.x = m_XRotAngleLimit.x;
	if (m_RotAngle.x < m_XRotAngleLimit.y)m_RotAngle.x = m_XRotAngleLimit.y;

	// 回転行列
	ZMatrix m_LocalRot;
	ZMatrix mx, my;
	mx.CreateRotateX(m_RotAngle.x);
	my.CreateRotateY(m_RotAngle.y);
	m_LocalRot = mx * my;
	m_LocalMat = m_LocalTransMat * m_LocalRot;

	// 視錐台平面更新
	UpdateFrustumPlanes();
	UpdateCameraAABB();
}

void GameCamera::SetCamera()
{
	// カメラ設定
	// 射影行列設定
	//SetProj(mProj);

	// 最終的なカメラ行列を求める
	//m_mCam = m_LocalMat * m_BaseMat;

	// カメラ行列からビュー行列を作成
	CameraToView();

	ZCamera::LastCam = *this;

	// シェーダー側の定数バッファに書き込む
	ShMgr.UpdateCamera(this);
}