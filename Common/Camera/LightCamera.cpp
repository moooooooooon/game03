#include "MainFrame/ZMainFrame.h"
#include "LightCamera.h"

LightCamera::LightCamera()
{
	m_Far = 100.f;
	m_Near = 0.1f;
	SetOrthoLH(ProjW, ProjH, m_Near, m_Far);
	m_mCam.CreateMove(0, 0, 0);

	m_TarPos.Set(ZVec3::Zero);
	m_Direction.Set(ZVec3::Up);
	m_Distance = 50.f;
}

void LightCamera::Update()
{
	SetOrthoLH(ProjW, ProjH, m_Near, m_Far);
	m_TarPos.y = 0.f;
	m_mCam.CreateMove(m_TarPos + (m_Direction*m_Distance));
	m_mView.CreateViewMatrixPos(m_mCam.GetPos(), m_TarPos);
	UpdateFrustumPlanes();
	UpdateCameraAABB();
}

void LightCamera::ImGui()
{
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("DirLightCamParam") == false) {
			ImGui::End();
			return;
		}

		ImGui::DragFloat("Near", &m_Near, 1.f, 0.1f, 100.f);
		ImGui::DragFloat("Far", &m_Far, 1.f, 10.f, 1000.f);

		ImGui::DragFloat("Width", &ProjW, 10.f, 10.f, 2000.f);
		ImGui::DragFloat("Height", &ProjH, 10.f, 10.f, 2000.f);

		ImGui::DragFloat("Distance", &m_Distance, 1.f, 1.f, 500.f);
		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);
}
