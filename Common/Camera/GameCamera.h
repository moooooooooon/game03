#ifndef __GAME_CAMERA_H__
#define __GAME_CAMERA_H__

// ゲーム用にある程度機能実装したカメラ
class GameCamera : public ZCamera
{
public:
	GameCamera();

	// 初期設定
	void Init(float rx, float ry, float camZoom, bool isFpsMode = false);
	void Init(const ZVec3& BaseTrans, const ZVec3& LocalTrans, bool isFpsMode = true);
	
	void ChangeAspect(float aspect);

	// 更新
	void Update(const POINT& moveVal);
	
	// セット
	void SetCamera();

public:
	ZMatrix m_LocalMat;
	ZMatrix m_BaseMat;
	ZMatrix m_LocalTransMat;
	
	ZVec2 m_RotAngle;			// カメラの回転角度
	ZVec2 m_XRotAngleLimit;		// 上下回転の制限（x:下 y:上）
	float m_RotRatio = 0.5;		// カメラの回転量

	bool m_IsFPSMode = true;
};


#endif
