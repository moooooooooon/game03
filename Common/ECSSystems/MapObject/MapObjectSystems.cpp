#include "PCH/pch.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "MapObjectSystems.h"
#include "CommonSubSystems.h"
#include "MainSystems/GameWorld/GameWorld.h"

SecurityTerminalUpdateSystem::SecurityTerminalUpdateSystem()
{
	Init();
	m_DebugSystemName = "SecurityTerminalUpdateSystem";
}

void SecurityTerminalUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto securityComp = GetCompFromUpdateParam(SecurityTerminalComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// 無効時
	if (!securityComp->Enable) return;
	
	// アクセスがある場合
	if (securityComp->NumAccess > 0)
	{
		// アクセス数分ゲージ増加
		securityComp->Gage += delta * securityComp->NumAccess;
	}
	
	// アクセス数リセット
	securityComp->NumAccess = 0;

	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController.GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "アクセス")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Box)continue;	// 箱以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Box, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_3, // 判定する側のフィルタ
				HitShapes::MESH | HitShapes::BOX,
				HitGroups::_3  // 判定される側のフィルタ
			);

			// ボックス情報
			hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize, transComp->Transform);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);
			//hitObj->GetAABB();
			// 登録
			ColEng.AddDef(hitObj);	// 判定される側

		}
	}

}

void SecurityTerminalUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{

	auto securityComp = GetCompFromUpdateParam(SecurityTerminalComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);

	// セキュリティ解除
	if (securityComp->Enable &&
		securityComp->Gage >= SecurityTerminalComponent::MaxGage)
	{
		securityComp->Enable = false;
		animatorComp->Enable = true;
		animatorComp->Animator->ChangeAnimeSmooth("Lifted(Loop)", 0, 10, true);
	}
}

ServerUpdateSystem::ServerUpdateSystem()
{
	Init();
	m_DebugSystemName = "ServerUpdateSystem";
}

void ServerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto serverComp = GetCompFromUpdateParam(ServerTerminalComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	//auto soundComp = GetCompFromUpdateParam(SoundSetComponent, components);

	// 無効時
	//if (!serverComp->Enable) 
	//{ 
	//	soundComp->Stop("securityTerminal");
	//	return; 
	//}

	//if (!soundComp->IsPlay("securityTerminal")) {
	//	soundComp->Play3D("securityTerminal", transComp->Transform.GetPos());
	//}


	// アクセスがある場合
	if (serverComp->NumAccess > 0)
	{
		// アクセス数分ゲージ増加
		serverComp->Gage += serverComp->NumAccess;

	}


	// アクセス数リセット
	serverComp->NumAccess = 0;


	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController.GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "アクセス")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Box)continue;	// 箱以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();

			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Box, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_3, // 判定する側のフィルタ
				HitShapes::MESH | HitShapes::BOX,
				HitGroups::_3  // 判定される側のフィルタ
			);

			// ボックス情報
			hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize, transComp->Transform);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);

			// 登録
			ColEng.AddDef(hitObj);	// 判定される側

		}
	}
}

void ServerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{

	auto serverComp = GetCompFromUpdateParam(ServerTerminalComponent, components);

	/*-----------------------------------------------------------------------------------------*/

	DW_STATIC(8, "ServerGage: %d", serverComp->Gage);
	//	デバッグ用のサーバーアクセス
	if (GW.m_GameState.AccesServer) {
		if (INPUT.KeyStay(VK_SPACE)) {
			serverComp->Gage++;
		}
	}
	/*-----------------------------------------------------------------------------------------*/

	
	// ダウンロード完了
	if (serverComp->Enable &&
		serverComp->Gage >= ServerTerminalComponent::MaxGage)
	{

		serverComp->Enable = false;
		
		//	ゲームオーバーじゃなければクリア
		if (!GW.m_GameState.SecurityWin)
		{
			GW.m_GameState.HackerWin = true;
		}

	}


}
