#ifndef __MAPOBJECT_SYSTEMS_H__
#define __MAPOBJECT_SYSTEMS_H__


class SecurityTerminalUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent,
		AnimatorComponent,TransformComponent,
		ColliderComponent,SecurityTerminalComponent)
public:
	SecurityTerminalUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

};

class ServerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent,
		TransformComponent, ColliderComponent, ServerTerminalComponent
		//SoundSetComponent,
		)
public:
	ServerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;
};

#endif