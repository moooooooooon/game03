#include "PCH/pch.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "PlayerUpdateSystem.h"


PlayerUpdateSystem::PlayerUpdateSystem()
{
	Init();
	m_DebugSystemName = "PlayerUpdateSystem";
}

void PlayerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);

	if (INPUT.KeyEnter('P'))
		cameraComp->Cam.m_IsFPSMode = !cameraComp->Cam.m_IsFPSMode;
	

	playerContComp->ControllerConnected = BOXINPUT.IsConnected(playerContComp->ControllerID);
	
	int index = playerContComp->ControllerConnected ? playerContComp->ControllerID : 0;
	
	// カメラ更新
	if (cameraComp->Enable || playerContComp->ControllerConnected)
		cameraComp->Cam.Update(GW.m_MoveInputValue[index]);

	// リセット
	playerContComp->Axis.Set(0, 0, 0);
	playerContComp->Button = 0;

	// 無効時
	if (!playerContComp->Enable && !playerContComp->ControllerConnected)return;

	// 値取得
	playerContComp->Axis = GW.m_InputAxis[index];
	playerContComp->Button = GW.m_InputButton[index];
}
