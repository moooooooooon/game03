#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "ECSComponents/Components.h"
#include "StateMachines/Hacker/HackerStateMachine.h"

#include "SubSystems.h"
#include "CommonSubSystems.h"

#include "HackerUpdateSystem.h"

HackerPreUpdateSystem::HackerPreUpdateSystem()
{
	Init();
	m_DebugSystemName = "HackerPreUpdateSystem";
}

void HackerPreUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	hackerStateMachine->m_InputAxis = playerContComp->Axis;
	hackerStateMachine->m_InputButton = playerContComp->Button;

	// しゃがみ
	if (playerContComp->Button & KeyMap::Squat)
		hackerStateMachine->m_SquatFlag = true;
	else
		hackerStateMachine->m_SquatFlag = false;

	velocityComp->Velocity = ZVec3(0);
}

HackerMoveSystem::HackerMoveSystem()
{
	Init();
	m_DebugSystemName = "HackerMoveSystem";
}

void HackerMoveSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = camComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	if (vTar.Length() > 0)
	{
		//float speed = playerComp->Speed * delta;
		//mat.Move(vTar * speed);
		// カメラの向いている方向へキャラ回転
		ZVec3 vZ = mat.GetZAxis();
		vZ.Homing(vTar, 20);
		mat.SetLookTo(vZ, ZVec3::Up);
		velocityComp->Velocity += vTar * playerComp->Speed * delta;
	}

	mat.Move(velocityComp->Velocity);

}

HackerUpdateSystem::HackerUpdateSystem()
{
	Init();
	m_DebugSystemName = "HackerUpdateSystem";
	m_DestroyEffect = EFFECT.LoadEffect("data/Effect/Obj_Break/Destroy.efk");
}

void HackerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	// オブジェクト破壊モード
	if (hackerStateMachine->m_DestroyModeFlg)
		DestroyObjMode(components);

	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::ALL,
			HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 0.5f, 0),	// レイの開始位置
			pos + ZVec3(0, -100, 0));		// レイの終了位置

		ColEng.Test(hitObj.GetPtr());
		
		if (hitObj->m_HitState | HitStates::STAY)
		{
			float nearestDist = FLT_MAX;

			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (transComp->m_Entity == entity)continue;

					if (nearestDist > node.RayDist)
						nearestDist = node.RayDist;
				}
			}

			
			if (nearestDist < 0.5f && velocityComp->Velocity.y <= 0)
			{
				transComp->Transform._42 += (0.5f - nearestDist);
				playerComp->IsSky = false;
			}
			else if (nearestDist > 0.5f + 0.3f)
				playerComp->IsSky = true;
		}
	}

	// カメラ座標更新
	ZMatrix m;
	m = camComp->Cam.m_BaseMat;
	m.Move(transComp->Transform.GetPos());

	camComp->Cam.m_mCam = camComp->Cam.m_LocalMat * m;
	camComp->Cam.m_mView = camComp->Cam.m_mCam.Inversed();
	camComp->Cam.UpdateFrustumPlanes();


	// 負傷していなければエフェクトなし
	if (hackerStateMachine->m_IsInjury == false)
		return;
	
	// 負傷時のエフェクト表示 改良予定
	hackerComp->DamageEffectTime++;
	if (hackerComp->DamageEffectTime > 50) //
	{
		hackerComp->DamageEffectTime = 0;

		auto model = bcComp->BoneController.GetGameModel();

		auto effectFunc = [this, hackerComp, transComp, model](const Effekseer::Handle handle, Effekseer::Manager* manager)mutable
		{
			ZVec3 effectPos = ZVec3(
				RAND.GetFloat(-model->GetAABB_HalfSize().x, model->GetAABB_HalfSize().x),
				RAND.GetFloat(-model->GetAABB_HalfSize().y, model->GetAABB_HalfSize().y),
				RAND.GetFloat(-model->GetAABB_HalfSize().z, model->GetAABB_HalfSize().z));

			effectPos *= ZVec3(0.8f, 1, 0.8f);

			hackerComp->DamageEffectPos[handle] = effectPos;
			effectPos.Transform(transComp->Transform);
			manager->SetLocation(handle, Effekseer::Vector3D(effectPos.x, effectPos.y, effectPos.z));
		};

		auto h = EFFECT.SubmitPlayEffect(hackerComp->InjuryEffect, effectFunc);

	}
	
}



#if 0
void HackerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	// HPが0以下 -> ゲームオーバー -> 操作不可
	if (hackerComp->HP <= 0)return;

	// しゃがみ
	if (playerContComp->Button & KeyMap::Squat)
		hackerComp->SquatFlag = true;
	else
		hackerComp->SquatFlag = false;

	// ダメージ
	switch (hackerComp->DamageState)
	{
		case HackerComponent::Damage::DAMAGE:
		{
			auto transComp = GetCompFromUpdateParam(TransformComponent, components);
			hackerComp->DamageEffectTime++;

			// エフェクト
			if (hackerComp->DamageEffectTime > 50)
			{
				hackerComp->DamageEffectTime = 0;
				auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);


				auto effectFunc = [this, hackerComp, transComp, colliderComp](const Effekseer::Handle handle, Effekseer::Manager* manager)mutable
				{
					ZVec3 effectPos = ZVec3(
						RAND.GetFloat(colliderComp->HitObj->GetBaseAABB().Min.x, colliderComp->HitObj->GetBaseAABB().Max.x),
						RAND.GetFloat(colliderComp->HitObj->GetBaseAABB().Min.y, colliderComp->HitObj->GetBaseAABB().Max.y),
						RAND.GetFloat(colliderComp->HitObj->GetBaseAABB().Min.z, colliderComp->HitObj->GetBaseAABB().Max.z));

					effectPos *= ZVec3(0.8f, 1, 0.8f);

					hackerComp->DamageEffectPos[handle] = effectPos;
					effectPos.Transform(transComp->Transform);
					manager->SetLocation(handle, Effekseer::Vector3D(effectPos.x, effectPos.y, effectPos.z));
				};

				auto h = EFFECT.SubmitPlayEffect(hackerComp->InjuryEffect, effectFunc);
			}


			Effekseer::Manager* manager = EFFECT.GetManager();

			for (auto& effect : hackerComp->DamageEffectPos)
			{
				ZVec3 pos = effect.second;

				pos.Transform(transComp->Transform);

				//	manager->SetLocation(effect.first,
				//		Effekseer::Vector3D(pos.x,pos.y,pos.z));
			}

		}
		break;

		case HackerComponent::Damage::DOWN:
		{
			hackerComp->HP--;
			if (hackerComp->HP <= 0)
			{
				/*死亡*/
			}
		}
		break;
	}

	switch (playerComp->ActionState)
	{
		case HackerComponent::WAIT:
		Action_Wait(components);
		break;
		case HackerComponent::WALK:
		Action_Walk(components, delta);
		break;
		case HackerComponent::RUN:
		Action_Run(components, delta);
		break;
		case HackerComponent::SQUAT_WAIT:
		Action_SquatWait(components);
		break;
		case HackerComponent::SQUAT_WALK:
		Action_SquatWalk(components, delta);
		break;
		case HackerComponent::FALL:
		Action_Fall(components);
		break;
		case HackerComponent::DOWN_WAIT:
		Action_DownWait(components);
		break;
		case HackerComponent::DOWN_WALK:
		Action_DownWalk(components, delta);
		break;
		case HackerComponent::ACCESS_START:
		Action_AccessStart(components);
		break;
		case HackerComponent::ACCESS:
		Action_Access(components);
		break;
		case HackerComponent::ACCESS_END:
		Action_AccessEnd(components);
		break;
		case HackerComponent::CURE:
		Action_Cure(components);
		break;
		case HackerComponent::GETUP:
		Action_GetUp(components);
		break;
		case HackerComponent::DESTORY_OBJ_START:
		Action_DestoryObjStart(components);
		break;
		case HackerComponent::DESTORY_OBJ:
		Action_DestoryObj(components);
		break;
		case HackerComponent::DESTORY_OBJ_END:
		Action_DestoryObjEnd(components);
		break;
		default:
		break;
	}

	if (hackerComp->DestroyObjMode)
		DestroyObjMode(components);

	Move(delta, transComp, camComp, playerComp, playerContComp);
}

void HackerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{

	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);

	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController.GetGameModel()->GetPhysicsDataSetList()[9];


	for (auto& rb : phyData.RigidBodyDataTbl)
	{

		if (rb.RigidBodyName == "ぶつかり下")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			SetCollision_Hit(components, rb);
		}
		else if (rb.RigidBodyName == "治療")
		{
			if (hackerComp->AccessFlag || hackerComp->DestroyObjMode)
				continue;

			SetCollision_Cure(components, rb);
		}

		// ダウンしている場合やられとアクセスは判定しない
		if (hackerComp->DamageState >= HackerComponent::DOWN)continue;


		if (rb.RigidBodyName == "ぶつかり上")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			SetCollision_Hit(components, rb);
		}
		else if (rb.RigidBodyName == "やられ")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();
			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Sphere, appnew);

			// 基本設定
			hitObj->Init(0,
						 HitGroups::_5, // 判定する側のフィルタ
						 HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
						 HitGroups::_5  // 判定される側のフィルタ
			);

			// スフィア情報
			hitObj->Set(m.GetPos(), rb.ShapeSize.x);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);

			// 登録
			ColEng.AddDef(hitObj);	// 判定される側

		}
		else if (rb.RigidBodyName == "アクセス")
		{
			if (hackerComp->SkillCheckTime == 0)
			{
				bool cureFlag = playerComp->ActionState == HackerComponent::CURE;

				if (!(playerContComp->Button & KeyMap::Access) &&
					!hackerComp->AccessFlag ||
					!(playerContComp->Button & KeyMap::Access_Stay) ||
					cureFlag ||
					hackerComp->DestroyObjMode)
				{
					// アクセスフラグをオフ
					hackerComp->AccessFlag = false;
					continue;
				}
			}

			// アクセスフラグをオフ
			hackerComp->AccessFlag = false;
			if((playerContComp->Button & KeyMap::Access) || (playerContComp->Button & KeyMap::Access_Stay))
				SetCollision_Access(components, rb);

		}
	}



	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
					 HitGroups::_0,
					 HitShapes::ALL,
					 HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 0.5f, 0),	// レイの開始位置
					pos + ZVec3(0, -100, 0));		// レイの終了位置

		ColEng.AddAtk(hitObj);
		hitObj->m_OnHitStay = [this, playerComp, transComp](const ZSP<ColliderBase>& hitObj)
		{
			float nearestDist = FLT_MAX;

			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					//if (youEntity == nullptr)continue;
					//// 自キャラは無視
					//if (colliderComp->m_Entity == youEntity)continue;
					// 近い？
					if (nearestDist > node.RayDist)
					{
						nearestDist = node.RayDist;
						//nearestObj = youGameObj;
					}
				}
			}

			// 空中時
			if (playerComp->IsSky)	// めりこんでる
			{
				if (nearestDist < 0.5f && playerComp->Velocity.y <= 0)
				{
					transComp->Transform._42 += 0.5f - nearestDist; // 押し戻す
					playerComp->Velocity.y = 0;
					playerComp->IsSky = false;
				}
			}
			else
			{
				if (nearestDist > 0.5f + 0.3f || playerComp->Velocity.y > 0)
				{
					playerComp->IsSky = true;
				}
				else
				{
					transComp->Transform._42 += 0.5f - nearestDist;
					playerComp->Velocity.y = 0;
				}
			}

		};

		// 最後に呼ばれる
		hitObj->m_OnTerminal = [this, transComp, cameraComp](const ZSP<ColliderBase>& hitObj)
		{
			// キャラの座標を使用
			ZMatrix m;
			m = cameraComp->Cam.m_BaseMat;
			m.Move(transComp->Transform.GetPos());

			cameraComp->Cam.m_mCam = cameraComp->Cam.m_LocalMat * m;
			cameraComp->Cam.m_mView = cameraComp->Cam.m_mCam.Inversed();
			cameraComp->Cam.UpdateFrustumPlanes();
		};
	}

}

#endif


void HackerUpdateSystem::ResolveCollision(TransformComponent * transComp, const ZSP<ColliderBase>& hitObj)
{
	for (auto& res : hitObj->m_HitResTbl)
	{
		for (auto& node : res.HitDataList)
		{
			float F = hitObj->CalcMassRatio(node.YouHitObj->m_Mass);
			transComp->Transform.Move(node.vPush*F);
		}
	}

}

void HackerUpdateSystem::Action_Wait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Button & KeyMap::Action)
	{
		hackerComp->DestroyObjMode = !hackerComp->DestroyObjMode;
	}
		
	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{

		if (hackerComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = HackerComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->Speed = hackerComp->SquatWalkSpeed;
		}
		else if (playerContComp->Button & KeyMap::Run)
		{
			// 走り
			playerComp->ActionState = HackerComponent::RUN;
			animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
			playerComp->Speed = hackerComp->RunSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = HackerComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->Speed = hackerComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (hackerComp->SquatFlag)
		{
			// しゃがみ
			playerComp->ActionState = HackerComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_Walk(UpdateCompParams components, float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Button & KeyMap::Action)
	{
		hackerComp->DestroyObjMode = !hackerComp->DestroyObjMode;
	}

	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		if (hackerComp->SquatFlag)
		{
			// しゃがみ
			playerComp->ActionState = HackerComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		}
		else
		{
			// 待機
			playerComp->ActionState = HackerComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		}
		return;
	}
	else if (hackerComp->SquatFlag)
	{
		// しゃがみ歩き
		playerComp->ActionState = HackerComponent::SQUAT_WALK;
		animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
		playerComp->Speed = hackerComp->SquatWalkSpeed;
		return;
	}
	else if (playerContComp->Button & KeyMap::Run)
	{
		// 走り
		playerComp->ActionState = HackerComponent::RUN;
		animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
		playerComp->Speed = hackerComp->RunSpeed;
		return;
	}

}

void HackerUpdateSystem::Action_Run(UpdateCompParams components, float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Button & KeyMap::Action)
	{
		hackerComp->DestroyObjMode = !hackerComp->DestroyObjMode;
	}

	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		if (hackerComp->SquatFlag)
		{
			// しゃがみ
			playerComp->ActionState = HackerComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		}
		else
		{
			playerComp->ActionState = HackerComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		}
		return;
	}
	else if (!(playerContComp->Button & KeyMap::Run))
	{
		if (hackerComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = HackerComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->Speed = hackerComp->SquatWalkSpeed;
		}
		else
		{
			playerComp->ActionState = HackerComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->Speed = hackerComp->WalkSpeed;
		}
		return;
	}


}

void HackerUpdateSystem::Action_SquatWait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Button & KeyMap::Action)
	{
		hackerComp->DestroyObjMode = !hackerComp->DestroyObjMode;
	}

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		if (hackerComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = HackerComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->Speed = hackerComp->SquatWalkSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = HackerComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->Speed = hackerComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (!hackerComp->SquatFlag)
		{
			// 待機
			playerComp->ActionState = HackerComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
			return;
		}
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_SquatWalk(UpdateCompParams components, float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Button & KeyMap::Action)
	{
		hackerComp->DestroyObjMode = !hackerComp->DestroyObjMode;
	}

	if (!hackerComp->SquatFlag)
	{
		if (playerContComp->Axis.x == 0 &&
			playerContComp->Axis.z == 0)
		{
			// 待機
			playerComp->ActionState = HackerComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		}
		else
		{
			// 歩き
			playerComp->ActionState = HackerComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->Speed = hackerComp->WalkSpeed;
		}
		return;

	}
	else
	{
		if (playerContComp->Axis.x == 0 &&
			playerContComp->Axis.z == 0)
		{
			// しゃがみ
			playerComp->ActionState = HackerComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


}

void HackerUpdateSystem::Action_Fall(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationEnd())
	{
		playerComp->ActionState = HackerComponent::DOWN_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around Wait", 0, 25, true);

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_DownWait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{

		// 歩き
		playerComp->ActionState = HackerComponent::DOWN_WALK;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around", 0, 10, true);
		playerComp->Speed = hackerComp->DownWalkSpeed;

		return;
	}


	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_DownWalk(UpdateCompParams components, float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);


	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		// 待機
		playerComp->ActionState = HackerComponent::DOWN_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around Wait", 0, 10, true);
		return;
	}

	
}

void HackerUpdateSystem::Action_AccessStart(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (!hackerComp->AccessFlag)
	{
		// アクセス終了
		playerComp->ActionState = HackerComponent::ACCESS_END;
		animatorComp->Animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
		return;
	}

	if (animatorComp->Animator->IsAnimationEnd())
	{
		// アクセス
		playerComp->ActionState = HackerComponent::ACCESS;
		animatorComp->Animator->ChangeAnimeSmooth("Hacking", 0, 10, true);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_Access(UpdateCompParams components)
{

	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (!hackerComp->AccessFlag)
	{
		// アクセス終了
		playerComp->ActionState = HackerComponent::ACCESS_END;
		animatorComp->Animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_AccessEnd(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);

	if (animatorComp->Animator->IsAnimationEnd())
	{
		// 待機
		playerComp->ActionState = HackerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_Cure(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	// 治療中にボタンを離すか治療完了していたら
	if (!(playerContComp->Button & KeyMap::Access_Stay) || hackerComp->FinishCure)
	{
		// しゃがみ
		playerComp->ActionState = HackerComponent::SQUAT_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		hackerComp->FinishCure = false;
		return;
	}

	// 治療中に移動、立ち上がった場合治療中断
	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		if (hackerComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = HackerComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->Speed = hackerComp->SquatWalkSpeed;
		}
		else if (playerContComp->Button & KeyMap::Run)
		{
			// 走り
			playerComp->ActionState = HackerComponent::RUN;
			animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
			playerComp->Speed = hackerComp->RunSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = HackerComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->Speed = hackerComp->WalkSpeed;
		}
		return;
	}
	else if (!hackerComp->SquatFlag)
	{
		// 待機
		playerComp->ActionState = HackerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_GetUp(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);

	if (animatorComp->Animator->IsAnimationEnd())
	{
		// 待機
		playerComp->ActionState = HackerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_DestoryObjStart(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);


	// 消去中断
	if (!(playerContComp->Button & KeyMap::Access_Stay))
	{
		auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

		// 待機
		playerComp->ActionState = HackerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		hackerComp->DestroyObjMode = true;
		return;
	}

	if (animatorComp->Animator->IsAnimationEnd())
	{
		playerComp->ActionState = HackerComponent::DESTORY_OBJ;
		animatorComp->Animator->ChangeAnimeSmooth("Hacking", 0, 10, false);
		return;
	}


	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_DestoryObj(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);


	// 消去中断
	if (!(playerContComp->Button & KeyMap::Access_Stay))
	{
		auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

		// 待機
		playerComp->ActionState = HackerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		hackerComp->DestroyObjMode = true;
		return;
	}

	// 消去
	if (animatorComp->Animator->IsAnimationEnd())
	{
		auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

		if (hackerComp->DestroyObj.IsActive())
		{
			auto destroyObj = hackerComp->DestroyObj.Lock();
			auto model = destroyObj->GetComponent<GameModelComponent>();

			model->RenderFlg.Delete = true;
			auto transComp = destroyObj->GetComponent<TransformComponent>();
			auto pos = transComp->Transform.GetPos() + ZVec3(0, 0.01f, 0);
			EFFECT.SubmitPlayEffect(m_DestroyEffect,pos);
			
			
			destroyObj->Remove();


			hackerComp->DestroyObjCnt++;
			hackerComp->DestroyObj.Reset();

			hackerComp->DestroyObjMode = false;
			// 待機
			playerComp->ActionState = HackerComponent::DESTORY_OBJ_END;
			animatorComp->Animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
			return;
		}
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::Action_DestoryObjEnd(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// 終了
	if (animatorComp->Animator->IsAnimationEnd())
	{
		// 待機
		playerComp->ActionState = HackerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void HackerUpdateSystem::DestroyObjMode(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	if (playerContComp->Button & KeyMap::Access)
	{
		// 破壊実行
		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		// 基本設定
		hitObj->Init(0,
					 HitGroups::_0, // 判定する側のフィルタ
					 HitShapes::ALL,
					 HitGroups::_0 // 判定される側のフィルタ
		);

		ZVec3 pos = transComp->Transform.GetPos();

		// 向いている方向ベクトル
		ZVec3 front = transComp->Transform.GetZAxis();
		front.Normalize();
		front *= 100;

		ZVec3 startPos = pos + ZVec3(0, 0.5, 0);	// レイの開始位置
		ZVec3 endPos = startPos + front;			// レイの終了位置

		hitObj->Set(startPos, endPos);

		// 自分のEntityのアドレスを仕込んでおく
		//hitObj->m_UserMap["Entity"] = transComp->m_Entity;

		// デバッグ用の色
		hitObj->m_Debug_Color.Set(0.3f, 0.3f, 1, 1);

		// 登録
		ColEng.AddAtk(hitObj);	// 判定する側

		hitObj->m_OnHitStay = [this, playerComp, animatorComp, hackerComp](const ZSP<ColliderBase>& hitObj)
		{
			ZSP<ECSEntity> nearObj = nullptr;
			float nearDist = FLT_MAX;

			// Hitしたやつら全て
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					auto& youObj = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (youObj == nullptr) continue;

					if (nearDist > node.RayDist)
					{
						nearObj = youObj;
						nearDist = node.RayDist;
					}
				}
			}

			if (nearObj != nullptr)
			{
				hackerComp->DestroyObj = nearObj;
				hackerComp->DestroyObjMode = false;
				playerComp->ActionState = HackerComponent::DESTORY_OBJ_START;
				animatorComp->Animator->ChangeAnimeSmooth("HackStart", 0, 10, false);

			}
		};

	}
}

void HackerUpdateSystem::SetCollision_Hit(UpdateCompParams components, const ZGM_RigidBodyData & rb)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// ワールド行列
	ZMatrix m = rb.GetMatrix();
	m *= transComp->Transform;

	auto hitObj = Make_Shared(Collider_Sphere, appnew);

	// 基本設定
	hitObj->Init(0,
				 HitGroups::_0 | HitGroups::_8, // 判定する側のフィルタ
				 HitShapes::ALL,
				 HitGroups::_8 // 判定される側のフィルタ
	);

	// スフィア情報
	hitObj->Set(m.GetPos(), rb.ShapeSize.x);

	// 質量
	hitObj->m_Mass = rb.Mass;

	// 自分のEntityのアドレスを仕込んでおく
	//hitObj->m_UserMap["Entity"] = transComp->m_Entity;

	// デバッグ用の色
	hitObj->m_Debug_Color.Set(0, 0, 1, 1);

	// 登録
	ColEng.AddAtk(hitObj);	// 判定する側
	ColEng.AddDef(hitObj);	// 判定される側

	// ヒット時に実行される
	hitObj->m_OnHitStay = std::bind(&HackerUpdateSystem::ResolveCollision, this, transComp, std::placeholders::_1);
	
}

void HackerUpdateSystem::SetCollision_Cure(UpdateCompParams components, const ZGM_RigidBodyData & rb)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	// ワールド行列
	ZMatrix m = rb.GetMatrix();
	m *= transComp->Transform;

	auto hitObj = Make_Shared(Collider_Sphere, appnew);

	// 基本設定
	hitObj->Init(0,
				 HitGroups::_7, // 判定する側のフィルタ
				 HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
				 HitGroups::_7  // 判定される側のフィルタ
	);

	// スフィア情報
	hitObj->Set(m.GetPos(), rb.ShapeSize.x);

	// 自分のEntityのアドレスを仕込んでおく
	hitObj->m_UserMap["Entity"] = hackerComp->m_Entity;

	// デバッグ用の色
	hitObj->m_Debug_Color.Set(0, 1, 0, 1);

	// 治療する側
	if (hackerComp->SquatFlag &&
		hackerComp->DamageState != HackerComponent::DOWN)
	{
		// ボタンを押した瞬間かすでに治療状態の場合
		if (playerContComp->Button & KeyMap::Access ||
			playerComp->ActionState == HackerComponent::CURE)
			ColEng.AddAtk(hitObj);
	}

	// 治療される側
	if (hackerComp->DamageState != HackerComponent::NORMAL)
	{
		// 負傷状態かつしゃがみもしくはダウン状態であるとき
		if (hackerComp->DamageState == HackerComponent::DAMAGE &&
			hackerComp->SquatFlag ||
			hackerComp->DamageState == HackerComponent::DOWN)
			ColEng.AddDef(hitObj);
	}

	hitObj->m_OnHitEnter = [this, transComp, playerComp, animatorComp, hackerComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// 自分自身は無視
				if (hackerComp->m_Entity == youEntity)continue;


				if (playerComp->ActionState != HackerComponent::CURE)
				{
					// 治療状態に
					playerComp->ActionState = HackerComponent::CURE;
					animatorComp->Animator->ChangeAnimeSmooth("Treatment", 0, 10, true);

					// 相手のいる方向を向く
					auto youTransComp = youEntity->GetComponent<TransformComponent>();
					ZVec3 vZ = youTransComp->Transform.GetPos() - transComp->Transform.GetPos();
					transComp->Transform.SetLookTo(vZ, ZVec3::Up);


					EFFECT.SubmitPlayEffect(hackerComp->TreatmentEffect,
											youTransComp->Transform.GetPos(),
											ZEffectManager::OnPlayEffectFunction());

				}
			}
		}
	};

	// ヒット時に実行される
	hitObj->m_OnHitStay = [this,transComp, animatorComp, playerComp, hackerComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// 自分自身は無視
				if (hackerComp->m_Entity == youEntity)continue;


				auto youSurvivorComp = youEntity->GetComponent<HackerComponent>();
				if (youSurvivorComp == nullptr)continue;
				youSurvivorComp->CureGage++;


				// 治療完了
				if (youSurvivorComp->CureGage >= youSurvivorComp->MaxCure)
				{
					hackerComp->FinishCure = true;

					// リセット
					youSurvivorComp->CureGage = 0;

					if (youSurvivorComp->DamageState == HackerComponent::DAMAGE)
					{
						youSurvivorComp->DamageState = HackerComponent::NORMAL;

					}
					else if (youSurvivorComp->DamageState == HackerComponent::DOWN)
					{
						youSurvivorComp->DamageState = HackerComponent::DAMAGE;

						// シルエット消す
						auto youModelComp = youEntity->GetComponent<GameModelComponent>();
						youModelComp->RenderFlg.Character = false;

						// 起き上がり状態に
						auto youAnimatorComp = youEntity->GetComponent<AnimatorComponent>();
						auto youPlayerComp = youEntity->GetComponent<PlayerComponent>();
						youPlayerComp->ActionState = HackerComponent::GETUP;
						youAnimatorComp->Animator->ChangeAnimeSmooth("Crawls around from Stand", 0, 10, false);
					}
				}
			}
		}
	};
}

void HackerUpdateSystem::SetCollision_Access(UpdateCompParams components, const ZGM_RigidBodyData& rb)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	auto hitObj = Make_Shared(Collider_Box, appnew);

	// 基本設定
	hitObj->Init(0,
				 HitGroups::_0, // 判定する側のフィルタ
				 HitShapes::ALL,
				 HitGroups::_0  // 判定される側のフィルタ
	);

	// ボックス情報
	hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize, transComp->Transform);

	// 自分のEntityのアドレスを仕込んでおく
	hitObj->m_UserMap["Entity"] = playerComp->m_Entity;

	// デバッグ用の色
	hitObj->m_Debug_Color.Set(0, 1, 0, 1);

	// 登録
	ColEng.AddAtk(hitObj);	// 判定する側

	hitObj->m_OnHitEnter = [this, playerComp, animatorComp](const ZSP<ColliderBase>& hitObj)
	{
		if (playerComp->ActionState != HackerComponent::ACCESS_START &&
			playerComp->ActionState != HackerComponent::ACCESS)
		{
			bool hasSecurityTerminal = false;
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					ZSP<ECSEntity> entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					auto securityTerminalComp = entity->GetComponent<SecurityTerminalComponent>();
					if (securityTerminalComp == nullptr)
						continue;
					hasSecurityTerminal = true;
				}
			}
			if (hasSecurityTerminal == false)
				return;

			playerComp->ActionState = HackerComponent::ACCESS_START;
			animatorComp->Animator->ChangeAnimeSmooth("HackStart", 0, 10, false);
		}
	};

	hitObj->m_OnHitStay = [this, hackerComp,playerContComp,playerComp, animatorComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// アクセスフラグオン
				hackerComp->AccessFlag = true;
		
				// サーバー
				auto serverComp = youEntity->GetComponent<ServerTerminalComponent>();
				if (serverComp)
				{
					serverComp->NumAccess++;
					//DW_SCROLL(3, "Serveraccess");
					break;
				}

				// セキュリティ端末
				auto securityComp = youEntity->GetComponent<SecurityTerminalComponent>();
				if (securityComp == nullptr)
					continue;

				securityComp->NumAccess++;
				hackerComp->AccessTime++;

				// 端末アクセスがスキルチェックの発生間隔に達したら
				if (hackerComp->AccessTime >= hackerComp->SkillCheckInterval)
				{
					// 確率でスキルチェック発生
					if (RAND.GetInt(100) < hackerComp->SkillCheckProbability)
					{
						hackerComp->SkillCheckTime = hackerComp->SkillCheckTimeLimit;
						hackerComp->SkillCheckKey = 'A' + RAND.GetInt(26); // キーコード（A~Z）

						auto securityTransComp = youEntity->GetComponent<TransformComponent>();

						// スキルチェック
						auto task = [securityTransComp, hackerComp, securityComp]()mutable
						{
							hackerComp->SkillCheckTime--;
							if (hackerComp->SkillCheckTime <= 0)
							{
								//DW_STATIC(4, "SkillCheck Failed");

								// スキルチェック失敗
								hackerComp->SuccessSkillCheck = false;

								// エフェクト（仮置き）
								auto* testEffect = EFFECT.LoadEffect("data/Effect/Error/Error.efk");
								auto effectHandle = EFFECT.SubmitPlayEffect(testEffect, securityTransComp->Transform.GetPos());

								// 失敗したらゲージを減らす
								securityComp->Gage -= SecurityTerminalComponent::DecreaseGage;
								if (securityComp->Gage < 0)
									securityComp->Gage = 0;
								return false;
							}

							// キーチェック
							if (INPUT.KeyEnter(hackerComp->SkillCheckKey))
							{
								//DW_STATIC(4, "SkillCheck Success");

								// スキルチェック成功
								hackerComp->SuccessSkillCheck = true;
								hackerComp->SkillCheckTime = 0;

								return false;
							}

							return true;
						};

						CTMgr.AddClosure("SkillCheck", task);

					}

					// 端末アクセス時間リセット
					hackerComp->AccessTime = 0;
				}
				break;


			} // for (auto& node : res.HitDataList)


		} //for (auto& res : hitObj->m_HitResTbl)


	};
}

