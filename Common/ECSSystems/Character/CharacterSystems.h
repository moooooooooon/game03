#ifndef __CHARACTER_SYSTEMS_H__
#define __CHARACTER_SYSTEMS_H__

// 多段ヒットカウンター処理
class MultiHitCounterUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(MultiHitCounterComponent)
public:
	MultiHitCounterUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

#include "PlayerUpdateSystem.h"
#include "HackerUpdateSystem.h"
#include "SecurityUpdateSystem.h"

#endif