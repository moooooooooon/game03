#include "PCH/pch.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "CharacterSystems.h"

MultiHitCounterUpdateSystem::MultiHitCounterUpdateSystem()
{
	Init();
}

void MultiHitCounterUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto hitCounterComp = GetCompFromUpdateParam(MultiHitCounterComponent, components);

	if (hitCounterComp->Counter.empty())return;

	// 多段ヒットリストの寿命処理
	auto it = hitCounterComp->Counter.begin();
	while (it != hitCounterComp->Counter.end())
	{
		it->second--;
		if (it->second <= 0)
		{
			it = hitCounterComp->Counter.erase(it);
			continue;
		}
		++it;
	}

}
