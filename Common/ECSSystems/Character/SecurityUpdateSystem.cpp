#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "SubSystems.h"
#include "CommonSubSystems.h"

#include "SecurityUpdateSystem.h"


SecurityUpdateSystem::SecurityUpdateSystem()
{
	Init();
	m_DebugSystemName = "KillerUpdateSystem";
}

void SecurityUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto securityComp = GetCompFromUpdateParam(SecurityComponent, components);


	// 多段ヒットリストの寿命処理
	for (auto it = securityComp->MultiHitMap.begin(); it != securityComp->MultiHitMap.end();)
	{
		it->second--;
		if (it->second <= 0)
		{
			it = securityComp->MultiHitMap.erase(it);
			continue;
		}
		
		++it;
	}

	switch (playerComp->ActionState)
	{
		case SecurityComponent::WAIT:
		Action_Wait(components);
		break;
		case SecurityComponent::WALK:
		Action_Walk(components, delta);
		break;
		case SecurityComponent::ATTACK_SINK:
		Action_Attack_Sink(components);
		break;
		case SecurityComponent::ATTACK:
		Action_Attack(components);
		break;
		case SecurityComponent::ATTACK_NOHIT:
		Action_Attack_NoHit(components);
		break;
		case SecurityComponent::ATTACK_HIT:
		Action_Attack_Hit(components);
		break;
		default:
		break;
	}

	if (securityComp->CreateWallMode == true)
	{
		Action_CreateWall(components);
	}

	// スクリプトキー時に実行される関数
	auto scriptProc = [this, bcComp,transComp,securityComp,colliderComp](ZAnimeKey_Script* scr)
	{

		// 文字列をJsonとして解析
		std::string errorMsg;
		json11::Json jsonObj = json11::Json::parse(scr->Value.c_str(), errorMsg);
		if (errorMsg.size() > 0)
		{
			DW_SCROLL(2, "JsonError（%s）", errorMsg.c_str());
			return;
		}

		auto scriptArray = jsonObj["Scripts"].array_items();
		for (auto& scrItem : scriptArray)
		{
			// スクリプトの種類
			std::string scrType = scrItem["Type"].string_value();
			// 攻撃判定発生
			if (scrType == "Attack")
				Script_Attack(bcComp, transComp, securityComp, colliderComp, scrItem);

		}

	};


	animatorComp->ScriptProc = scriptProc;
}

void SecurityUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);

	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController.GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "ぶつかり")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();
			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Sphere, appnew);

			// 基本設定
			hitObj->Init(0,
						 HitGroups::_0 | HitGroups::_8, // 判定する側のフィルタ
						 HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
						 HitGroups::_8 // 判定される側のフィルタ
			);

			// スフィア情報
			hitObj->Set(m.GetPos(), rb.ShapeSize.x);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			//hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 0, 1, 1);

			// 登録
			ColEng.AddAtk(hitObj);	// 判定する側
			ColEng.AddDef(hitObj);	// 判定される側

			// ヒット時に実行される
			hitObj->m_OnHitEnter = [this, transComp](const ZSP<ColliderBase>& hitObj)
			{
				// Hitしたやつら全て
				for (auto& res : hitObj->m_HitResTbl)
				{
					for (auto& node : res.HitDataList)
					{
						//// 相手のEntity
						//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
						//if (youEntity == nullptr)continue;

						//// 自分自身は無視
						//if (colliderComp->m_Entity == youEntity)continue;

						float F = hitObj->CalcMassRatio(node.YouHitObj->m_Mass);
						transComp->Transform.Move(node.vPush*F);
					}
				}
			};
		}
	}

	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
					 HitGroups::_0,
					 HitShapes::ALL,
					 HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 0.5, 0),	// レイの開始位置
					pos + ZVec3(0, -100, 0));		// レイの終了位置

		ColEng.AddAtk(hitObj);
		hitObj->m_OnHitStay = [this, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
		{
			float nearestDist = FLT_MAX;
			//sptr(GameObject> nearestObj;
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					//if (youEntity == nullptr)continue;
					//// 自キャラは無視
					//if (colliderComp->m_Entity == youEntity)continue;
					// 近い？
					if (nearestDist > node.RayDist)
						nearestDist = node.RayDist;
					
				}
			}

			// 空中時
			if (playerComp->IsSky)	// めりこんでる
			{
				if (nearestDist < 0.5f && playerComp->Velocity.y <= 0)
				{
					transComp->Transform._42 += 0.5f - nearestDist; // 押し戻す
					playerComp->Velocity.y = 0;
					playerComp->IsSky = false;
				}
			}
			else
			{
				if (nearestDist > 0.5f + 0.3f || playerComp->Velocity.y > 0)
					playerComp->IsSky = true;
				else
				{
					transComp->Transform._42 += 0.5f - nearestDist;
					playerComp->Velocity.y = 0;
				}
			}

		};

		// 最後に呼ばれる
		hitObj->m_OnTerminal = [this, bcComp, transComp, cameraComp](const ZSP<ColliderBase>& hitObj)
		{
			// キャラの頭の座標を使用
			auto headLocalMat = bcComp->BoneController.SearchBone("頭")->LocalMat;
			auto headMat = headLocalMat * transComp->Transform;

			ZMatrix m;
			m = cameraComp->Cam.m_BaseMat;
			m.Move(headMat.GetPos());

			cameraComp->Cam.m_mCam = cameraComp->Cam.m_LocalMat * m;
			cameraComp->Cam.m_mView = cameraComp->Cam.m_mCam.Inversed();
			cameraComp->Cam.UpdateFrustumPlanes();
		};
	}


}

void SecurityUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

void SecurityUpdateSystem::Action_Wait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto killerComp = GetCompFromUpdateParam(SecurityComponent, components);


	// 壁生成
	SpawnWall(components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		// 移動へ
		playerComp->ActionState = SecurityComponent::WALK;
		animatorComp->Animator->ChangeAnimeSmooth("Movement", 0, 10, true);
		playerComp->Speed = killerComp->WalkSpeed;
		return;
	}

	if (playerContComp->Button &
		KeyMap::Access &&
		killerComp->CreateWallMode == false)
	{
		// 溜めへ
		playerComp->ActionState = SecurityComponent::ATTACK_SINK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
		return;
	}


	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	//vTar += cam.m_LocalMat.GetXAxis();
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);

	// 重力
	playerComp->Velocity.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);


}

void SecurityUpdateSystem::Action_Walk(UpdateCompParams components, float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto killerComp = GetCompFromUpdateParam(SecurityComponent, components);


	SpawnWall(components);

	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		// 待機へ
		playerComp->ActionState = SecurityComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	if (playerContComp->Button &
		KeyMap::Access &&
		killerComp->CreateWallMode == false)
	{
		// 溜めへ
		playerComp->ActionState = SecurityComponent::ATTACK_SINK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();

	float speed = playerComp->Speed * delta;
	mat.Move(vTar * speed);

	ZVec3 vZ = cam.m_LocalMat.GetZAxis();
	vZ.y = 0;
	//vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->Velocity.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void SecurityUpdateSystem::Action_Attack_Sink(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);

	// アニメーション終了か攻撃ボタンを離したら
	if (animatorComp->Animator->IsAnimationEnd() ||
		!(playerContComp->Button & KeyMap::Access_Stay))
	{
		auto killerComp = GetCompFromUpdateParam(SecurityComponent, components);

		// 溜めに応じて攻撃力を変える
		if (animatorComp->Animator->IsAnimationEnd())
			killerComp->AttackPower = 2;
		else
			killerComp->AttackPower = 1;

		cameraComp->Enable = false;

		// 攻撃へ
		playerComp->ActionState = SecurityComponent::ATTACK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack", 0, 3, false);

		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);


	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void SecurityUpdateSystem::Action_Attack(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationEnd())
	{
		auto killerComp = GetCompFromUpdateParam(SecurityComponent, components);
		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
		cameraComp->Enable = true;

		// 攻撃が当たったか
		if (killerComp->HitAttack)
		{
			playerComp->ActionState = SecurityComponent::ATTACK_HIT;
			animatorComp->Animator->ChangeAnimeSmooth("Attack(Hit)", 0, 3, false);
			killerComp->HitAttack = false;
		}
		else
		{
			playerComp->ActionState = SecurityComponent::ATTACK_NOHIT;
			animatorComp->Animator->ChangeAnimeSmooth("Attack(NoHit)", 0, 3, false);
		}

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void SecurityUpdateSystem::Action_Attack_NoHit(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationEnd())
	{
		// 待機へ
		playerComp->ActionState = SecurityComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void SecurityUpdateSystem::Action_Attack_Hit(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationEnd())
	{
		// 待機へ
		playerComp->ActionState = SecurityComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);

	// 力による移動
	transComp->Transform.Move(playerComp->Velocity);
}

void SecurityUpdateSystem::Action_CreateWall(UpdateCompParams components)
{

	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto killerComp = GetCompFromUpdateParam(SecurityComponent, components);

	// 壁生成
	if (playerContComp->Button & KeyMap::Access)
	{

		// 当たり判定用モデル
		auto wallColliderComp = killerComp->WallEntity->GetComponent<ColliderComponent>();

		int filter = 0;
		filter |= 1 << 0;

		// 基本設定
		wallColliderComp->HitObj->Init(0,
									   filter,// 判定する側時のマスク
									   0xFFFFFFFF, // 形状マスク
									   filter // 判定される側時のマスク
		);

		killerComp->CreateWallCnt++;
		killerComp->WallEntity = nullptr;
		killerComp->CreateWallMode = false;

		return;

	}

	auto wallTransComp = killerComp->WallEntity->GetComponent<TransformComponent>();
	wallTransComp->Transform = transComp->Transform;
	wallTransComp->Transform.Move_Local(0, 0, 1);
}

void SecurityUpdateSystem::SpawnWall(UpdateCompParams components)
{
	auto killerComp = GetCompFromUpdateParam(SecurityComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	if (playerContComp->Button & KeyMap::Action)
	{
		if (killerComp->CreateWallCnt >= SecurityComponent::MaxNumCreateWall)
		{
			killerComp->WallEntityList.front()->Remove();
			killerComp->WallEntityList.pop_front();
			killerComp->CreateWallCnt--;
		}

		killerComp->CreateWallMode = !killerComp->CreateWallMode;

		if (killerComp->CreateWallMode == true &&
			killerComp->WallEntity == nullptr)
		{
			TransformComponent* wallTransComp = ECS.MakeComponent<TransformComponent>();
			wallTransComp->Transform = transComp->Transform;

			GameModelComponent* wallModelComp = ECS.MakeComponent<GameModelComponent>();

			wallModelComp->Model = killerComp->WallModel;
			wallModelComp->RenderFlg.SemiTransparent = true;


			ColliderComponent* wallColliderComp = ECS.MakeComponent<ColliderComponent>();

			// 当たり判定用モデル
			wallColliderComp->HitModel = killerComp->WallModel;
			wallColliderComp->HitObj = Make_Shared(Collider_Mesh, appnew);

			// 基本設定
			wallColliderComp->HitObj->Init(0,
										   0,// 判定する側時のマスク
										   HitShapes::ALL, // 形状マスク
										   HitGroups::_0 || HitGroups::_8 // 判定される側時のマスク
			);

			// メッシュを追加   
			wallColliderComp->HitObj->AddMesh(wallColliderComp->HitModel);


			auto wallEntity = ECS.MakeEntity(wallTransComp, wallModelComp, wallColliderComp);

			GW.m_Entities.push_back(wallEntity);
			killerComp->WallEntity = wallEntity;
			killerComp->WallEntityList.push_back(wallEntity);
		}
		else if (killerComp->CreateWallMode == false &&
				 killerComp->WallEntity != nullptr)
		{
			// 設置せずに戻った場合削除
			if (killerComp->WallEntityList.empty() == false)
			{
				killerComp->WallEntity->Remove();
				killerComp->WallEntity = nullptr;
				killerComp->WallEntityList.pop_back();
			}
			//auto wallModelComp = killerComp->WallEntity->GetComponent<GameModelComponent>();
		}

	}
}

void SecurityUpdateSystem::Script_Attack(ModelBoneControllerComponent* bcComp, TransformComponent* transComp, SecurityComponent* securityComp,
										 ColliderComponent* colliderComp, json11::Json& scrItem)
{

	struct Param
	{
		// 寿命
		int life;
		// ボーンの名前
		ZString boneName;
		// 当たり判定の半径
		float radius;
		// ヒット間隔
		int hitInterval;

	};

	Param param;
	param.life = scrItem["Time"].int_value();
	// ボーンの名前
	param.boneName = scrItem["BoneName"].string_value().c_str();
	// 当たり判定の半径
	param.radius = (float)scrItem["Radius"].number_value();
	// ヒットの間隔
	param.hitInterval = scrItem["HitInterval"].int_value();

	auto task = [this, transComp, bcComp, colliderComp, securityComp, param]()mutable
	{
		param.life--;
		if (param.life <= 0)return false;

		// ボーン
		auto bone = bcComp->BoneController.SearchBone(param.boneName);

		// ボーン行列
		ZMatrix m = bone->LocalMat * transComp->Transform;

		auto hitObj = Make_Shared(Collider_Sphere, appnew);

		// 基本設定
		hitObj->Init(0,
					 HitGroups::_5, // 判定する側のフィルタ
					 HitShapes::SPHERE | HitShapes::MESH,
					 HitGroups::_5  // 判定される側のフィルタ
		);

		// スフィア情報
		hitObj->Set(m.GetPos(), param.radius);

		// 自分のEntityのアドレスを仕込んでおく
		hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

		// デバッグ用の色
		hitObj->m_Debug_Color.Set(1, 0, 0, 1);

		// 登録
		ColEng.AddAtk(hitObj);	// 判定される側

		// ヒット時に実行される
		hitObj->m_OnHitStay = [this, colliderComp, securityComp, param](const ZSP<ColliderBase>& hitObj)
		{
			hitObj->m_HitResTbl;
			// Hitしたやつら全て
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (youEntity == nullptr)continue;

					// 自分自身は無視
					//if (colliderComp->m_Entity == youEntity)continue;

					// すでにヒットしているか
					const ZUUID& entityUUID = youEntity->GetUUID();
					if (securityComp->MultiHitMap.count(entityUUID) != 0)continue;

					//DW_SCROLL(3, "Hit");
					securityComp->MultiHitMap[entityUUID] = param.hitInterval;

					securityComp->HitAttack = true;

					auto youSurvivorComp = youEntity->GetComponent<HackerComponent>();
					if (youSurvivorComp == nullptr)continue;
					auto youPlayerComp = youEntity->GetComponent<PlayerComponent>();
					if (youPlayerComp == nullptr)continue;
					auto youAnimatorComp = youEntity->GetComponent<AnimatorComponent>();
					auto youTransComp = youEntity->GetComponent<TransformComponent>();
					//auto youStateMachineComp = youEntity->GetComponent<StateMachineComponent>();

					// エフェクト（仮置き）
					EFFECT.SubmitPlayEffect(youSurvivorComp->DamageEffect, youTransComp->Transform.GetPos() + ZVec3(0, 0.01f, 0));

					// 攻撃力分ダメージとする
					youSurvivorComp->DamageState += securityComp->AttackPower;

					if (youSurvivorComp->DamageState >= HackerComponent::DOWN)
					{
						// ダウン状態ならシルエットを出す
						auto youModelComp = youEntity->GetComponent<GameModelComponent>();
						youModelComp->RenderFlg.Character = true;

						youSurvivorComp->DamageState = HackerComponent::DOWN;
						youPlayerComp->ActionState = HackerComponent::FALL;

						// しゃがみ
						if (youSurvivorComp->SquatFlag)
							youAnimatorComp->Animator->ChangeAnimeSmooth("SitDown from Down", 0, 10, false);
						else
							youAnimatorComp->Animator->ChangeAnimeSmooth("Stand from Down", 0, 10, false);
					}
				}
			}
		};

		return true;
	};

	CTMgr.AddClosure("Attack", task);
}
