#ifndef __HACKER_UPDATE_SYSTEM_H__
#define __HACKER_UPDATE_SYSTEM_H__

class HackerPreUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(PlayerComponent, PlayerControllerComponent, HackerComponent, StateMachineComponent,VelocityComponent)
public:
	HackerPreUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	
};

class HackerMoveSystem : public ECSSystemBase
{
	DefUseComponentType(HackerComponent,TransformComponent,CameraComponent,VelocityComponent,PlayerComponent,PlayerControllerComponent)
public:
	HackerMoveSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

class HackerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(AnimatorComponent, ModelBoneControllerComponent,
						TransformComponent, PlayerComponent,
						CameraComponent, PlayerControllerComponent,
						ColliderComponent, HackerComponent,VelocityComponent,StateMachineComponent)

public:
	HackerUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	//virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

private:

	// �Փˉ���
	void ResolveCollision(TransformComponent* transComp, const ZSP<ColliderBase>& hitObj);

	void Action_Wait(UpdateCompParams components);
	void Action_Walk(UpdateCompParams components, float delta);
	void Action_Run(UpdateCompParams components, float delta);
	void Action_SquatWait(UpdateCompParams components);
	void Action_SquatWalk(UpdateCompParams components, float delta);
	void Action_Fall(UpdateCompParams components);
	void Action_DownWait(UpdateCompParams components);
	void Action_DownWalk(UpdateCompParams components, float delta);
	void Action_AccessStart(UpdateCompParams components);
	void Action_Access(UpdateCompParams components);
	void Action_AccessEnd(UpdateCompParams components);
	void Action_Cure(UpdateCompParams components);
	void Action_GetUp(UpdateCompParams components);
	void Action_DestoryObjStart(UpdateCompParams components);
	void Action_DestoryObj(UpdateCompParams components);
	void Action_DestoryObjEnd(UpdateCompParams components);

	void SetCollision_Hit(UpdateCompParams components, const ZGM_RigidBodyData& rb);
	void SetCollision_Cure(UpdateCompParams components, const ZGM_RigidBodyData& rb);
	void SetCollision_Access(UpdateCompParams components, const ZGM_RigidBodyData& rb);

	void DestroyObjMode(UpdateCompParams components);

	Effekseer::Effect* m_DestroyEffect;
};


#endif