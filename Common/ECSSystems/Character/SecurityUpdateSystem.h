#ifndef __SECURITY_UPDATE_SYSTEM_H__
#define __SECURITY_UPDATE_SYSTEM_H__

class SecurityUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent,
						AnimatorComponent, TransformComponent,
						PlayerComponent, CameraComponent,
						PlayerControllerComponent, ColliderComponent,
						SecurityComponent)
public:
	SecurityUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	void Action_Wait(UpdateCompParams components);
	void Action_Walk(UpdateCompParams components, float delta);
	void Action_Attack_Sink(UpdateCompParams components);
	void Action_Attack(UpdateCompParams components);
	void Action_Attack_NoHit(UpdateCompParams components);
	void Action_Attack_Hit(UpdateCompParams components);
	void Action_CreateWall(UpdateCompParams components);

	void SpawnWall(UpdateCompParams components);

	void Script_Attack(ModelBoneControllerComponent* bcComp, TransformComponent* transComp, SecurityComponent* securityComp,
					   ColliderComponent* colliderComp, json11::Json& scrItem);

private:

	ZSP<ECSEntity> m_WallEntity;
	GameModelComponent* m_WallModelComponent;
	ColliderComponent* m_WallColliderComponent;
};

#endif