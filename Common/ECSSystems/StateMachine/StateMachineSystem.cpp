#include "PCH/pch.h"
#include "ECSComponents/Components.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "StateMachineSystem.h"

StateMachineSystem::StateMachineSystem()
{
	Init();
	m_DebugSystemName = "StateMachineSystem";
}

void StateMachineSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);

	if (stateMachineComp->pStateMachine->IsSetStartState() == false)
		stateMachineComp->pStateMachine->SetStartState(stateMachineComp->StartState);

	stateMachineComp->pStateMachine->Update(delta);

}