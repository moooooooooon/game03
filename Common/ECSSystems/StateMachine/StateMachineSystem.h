#ifndef __STATE_MACHINE_SYSTEM_H__
#define __STATE_MACHINE_SYSTEM_H__

class StateMachineSystem : public ECSSystemBase
{
	DefUseComponentType(StateMachineComponent)

public:
	StateMachineSystem();

	void UpdateComponents(float delta, UpdateCompParams components)override;

};


#endif