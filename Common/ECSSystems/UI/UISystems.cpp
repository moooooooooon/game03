#include "MainFrame/ZMainFrame.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "MainSystems/Fade/Fade.h"
#include "Shader/ShaderManager.h"
#include "ECSComponents/UI/UIComponents.h"
#include "ECSComponents\MapObject\MapObjectComponents.h"
#include "CommonSubSystems.h"
#include "UISystems.h"

TitleMenuUpdateSystem::TitleMenuUpdateSystem()
{
	Init();
	m_DebugSystemName = "TitleMenuUpdateSystem";
	auto sound = APP.m_ResStg.LoadSound("data/Sound/se/system/decision_low.wav");
	SE = sound->CreateInstance(false);
	m_IsEndOffsetWait = false;
	m_IsPressedEnter = false;
	CTMgr.AddTimedClosure("TitleMenuUpdateOffset",
		[this](float elapsedTime,bool isTimeOver)
	{
		if (isTimeOver) m_IsEndOffsetWait = true;
	},0.5f);
}

void TitleMenuUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto radio_btn = GetCompFromUpdateParam(RadioButtonComponent, components);
	if (radio_btn->GroupName != "TitleMenu") { return; }

	if (FadeIO.IsActive()) return; // フェードイン,アウト中
	if (m_IsEndOffsetWait == false)return;
	
	m_IsPressedEnter |= INPUT.KeyEnter(VK_RETURN);
	bool isKeyUpdate = false;
	
	if (BeforSelectID == UNSELECTED)
	{
		//	前の選択がなければ初期化値を
		BeforSelectID = radio_btn->InitButtonID;
	}
	else
	{
		int KeyState = 0;
		if (GW.m_InputAxis[0].z > 0 && GW.m_OnInputAxis[0])
		{
			isKeyUpdate = true;
			KeyState = UP;
		}
		if (GW.m_InputAxis[0].z < 0 && GW.m_OnInputAxis[0])
		{
			isKeyUpdate = true;
			KeyState = DOWN;
		}
		BeforSelectID += KeyState;
		if (BeforSelectID < 0) { BeforSelectID = radio_btn->ButtonList.size() - 1; }
		if (BeforSelectID >= (int)radio_btn->ButtonList.size())
		{
			BeforSelectID = 0;
		}
	}


	//	キーとマウスならマウスを優先する
	//	あと、重なってるボタンなどは、後の判定を優先する
	ZSP<Button> select_button = nullptr;
	for (auto& btn : radio_btn->ButtonList)
	{
		auto& sprite = btn->Sprite;

		btn->IsSelect = false;

		if (btn->ButtonID == BeforSelectID)
		{
			//	キー入力結果を反映(マウス選択がない時)
			if (select_button == nullptr)
			{
				select_button = btn;
				continue;
			}
		}

		if (INPUT.GetMouseMoveValue().x && INPUT.GetMouseMoveValue().y)
		{
			if (CheckMauseOver(btn->Transform, btn->ButtonSize))
			{
				select_button = btn;
			}
		}
		btn->Transform.SetScale(1.f, 1.f, 1.f);	//	拡大戻す
	}


	if (select_button)
	{
		select_button->Transform.SetScale(1.3f, 1.3f, 1.3f);	//	一応大きく

		bool mouseclick = GW.m_InputButton[0] & KeyMap::A;

		//	マウスの場合はボタンの上にあるときのみ
		if (GW.m_InputButton[0] & KeyMap::Access)
		{
			if (CheckMauseOver(select_button->Transform, select_button->ButtonSize))
				mouseclick = true;
		}
		if ((INPUT.KeyExit(VK_RETURN) && m_IsPressedEnter) | mouseclick)
		{
			switch (select_button->ButtonID)
			{
				case 0:
				{
					GW.ChangeScene("Test");
					SE->Play();
				}
				break;

				case 1:
				{
					APP.ExitGameLoop();
					SE->Play();
				}
				break;
			}
		}

		//	ID更新
		if ((BeforSelectID != select_button->ButtonID) | isKeyUpdate) { SE->Play(); }
		BeforSelectID = select_button->ButtonID;
		select_button->IsSelect = true;
	}

}

bool TitleMenuUpdateSystem::CheckMauseOver(const ZMatrix& mat, const ZVec2& size)
{
	float imageW = size.x * 0.5f;
	float imageH = size.y * 0.5f;
	ZVec3 vertices[2] =
	{
		ZVec3(-imageW,-imageH,0),
		ZVec3(imageW,imageH,0),
	};

	// 変換
	for (auto& v : vertices) { v.Transform(mat); }

	POINT mouse_pos = INPUT.GetMousePos();

	bool sideFlg = false;
	bool verticalFlg = false;

	//	横の判定
	if ((mouse_pos.x >= vertices[0].x) && (mouse_pos.x <= (vertices[1].x)))
	{
		sideFlg = true;
	}

	//	縦の判定
	if ((mouse_pos.y >= vertices[0].y) && (mouse_pos.y <= (vertices[1].y)))
	{
		verticalFlg = true;
	}

	//	縦横のフラグ
	return (sideFlg && verticalFlg);
}


RadioButtonDrawSystem::RadioButtonDrawSystem()
{
	Init();
	m_DebugSystemName = "RadioButtonDrawSystem";
}

void RadioButtonDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto radio_button = GetCompFromUpdateParam(RadioButtonComponent, components);

	auto& sr = ShMgr.GetRenderer<SpriteRenderer>();
	for (auto btn : radio_button->ButtonList)
	{
		if (btn->IsSelect)
		{
			ShMgr.m_bsAdd.SetState();
			for (auto tex : btn->Sprite)
			{
				if (tex->BsAdd == false)
					continue;

				sr.Draw2D(
					tex->Tex->GetTex(),
					tex->Size,
					btn->Transform,
					&tex->Color);

			}
			ShMgr.m_bsAlpha.SetState();
		}
		for (auto tex : btn->Sprite)
		{
			if (tex->BsAdd)
				continue;

			sr.Draw2D(
				tex->Tex->GetTex(),
				tex->Size,
				btn->Transform,
				&tex->Color);

		}

	}
}
