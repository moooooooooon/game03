#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "GameStateCheckSystems.h"



HackerCheckSystem::HackerCheckSystem() {
	Init();
	m_DebugSystemName = "HackerCheckSystem";
}

void HackerCheckSystem::UpdateComponents(float delta, UpdateCompParams components) {
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);

	IsCheck = true;

#if _DEBUG
	if (INPUT.KeyExit('Q')) 
	{
		if (!FrameKill) 
		{
			if (hackerComp->HP > 0) 
			{
				hackerComp->HP = 0;
				FrameKill = true;	//	一体ずつ殺す
			}
		}
	}
#endif


	if (hackerComp->HP > 0) {
		HackerCnt++;
	}

}

void HackerCheckSystem::LateUpdateComponents(float delta, UpdateCompParams components) {

#if _DEBUG
	FrameKill = false;
#endif

	if (!IsCheck)return;

	if (HackerCnt == 0) {
		if (!GW.m_GameState.HackerWin) {
			GW.m_GameState.SecurityWin = true;
		}
	}

	DW_STATIC(6, "Num Alive Hacker: %d", HackerCnt);


	HackerCnt = 0;
	IsCheck = false;

}


SecurityCheckSystem::SecurityCheckSystem() {
	Init();
	m_DebugSystemName = "SecurityCheckSystem";
}

void SecurityCheckSystem::UpdateComponents(float delta, UpdateCompParams components) 
{

	auto SecComp	= GetCompFromUpdateParam(SecurityTerminalComponent, components);
	auto AnimComp	= GetCompFromUpdateParam(AnimatorComponent, components);
	

	IsCheck = true;

#if _DEBUG

	if (INPUT.KeyStay(VK_SPACE))
	{
		if (SecComp->Enable) {
			if (!FrameKill) { //	デバッグ用に一体ずつ無効にする
				
				SecComp->Gage += 20;
				if (SecComp->Gage >= SecurityTerminalComponent::MaxGage) {
					SecComp->Enable = false;
				}
			}
			FrameKill = true;
		}
	}

	if (INPUT.KeyEnter(VK_RETURN)) {
		if (SecComp->Enable) {
			if (!FrameKill) { //	デバッグ用に一体ずつ無効にする
				SecComp->Gage = SecurityTerminalComponent::MaxGage;
				SecComp->Enable = false;
			}
			FrameKill = true;
		}
	}

	if (!SecComp->Enable)
	{
		if (!AnimComp->Enable)
		{
			AnimComp->Enable = true;
			AnimComp->Animator->ChangeAnimeSmooth("Loop", 0, 10, true);
		}
	}
#endif

	if (SecComp->Enable) 
	{
		EnableSecCnt++;
	}
}

void SecurityCheckSystem::LateUpdateComponents(float delta, UpdateCompParams components) 
{

#if _DEBUG
	FrameKill = false;
#endif
	if (!IsCheck)return;

	if (EnableSecCnt == 0)
	{
		GW.m_GameState.AccesServer = true;
	}

	DW_STATIC(7, "AliveSecurity: %d", EnableSecCnt);

	IsCheck = false;
	EnableSecCnt = 0;

}
