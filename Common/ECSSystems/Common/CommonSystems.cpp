#include "MainFrame/ZMainFrame.h"
#include "Camera/LightCamera.h"
#include "Shader/Light/LightManager.h"
#include "Shader/Renderer/ModelRenderer/MeshRenderer.h"
#include "Shader/Renderer/ModelRenderer/SkinMeshRenderer.h"
#include "Shader/Renderer/ModelRenderer/ModelRenderingPipeline.h"
#include "Shader/ShaderManager.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "CommonSystems.h"
#include "CommonSubSystems.h"

AnimationUpdateSystem::AnimationUpdateSystem()
{
	Init();
	m_DebugSystemName = "AnimationUpdateSystem";
}

void AnimationUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* bc = GetCompFromUpdateParam(ModelBoneControllerComponent,components);
	auto* animator = GetCompFromUpdateParam(AnimatorComponent,components);
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);

	float fps = (1.0f / delta);

	if (animator->Enable)
	{
		ZMatrix mDelta;
		//animator->Animator->Animation(60.0f / fps, &mDelta);
		animator->Animator->AnimationAndScript(60.0f / fps, animator->ScriptProc, &mDelta);

		trans->Transform = mDelta * trans->Transform;
	}
	bc->BoneController.CalcBoneMatrix(false);
}

void AnimationUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto* bc = GetCompFromUpdateParam(ModelBoneControllerComponent,components);

	bc->BoneController.CalcBoneMatrix(true);
	bc->BoneController.UpdateBoneConstantBuffer();
}

void AnimationUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

StaticMeshDrawSystem::StaticMeshDrawSystem()
{
	Init();
	m_DebugSystemName = "StaticMeshDrawSystem";
	m_UseMultiThread = true;
}

void StaticMeshDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent, components);
	auto* model = GetCompFromUpdateParam(GameModelComponent, components);

	// モデルが読み込まれていない
	if (model->Model == nullptr)return;

	auto aabbCenter = model->Model->GetAABB_Center();
	auto aabbHalfSize = model->Model->GetAABB_HalfSize();
	ZAABB AABB(aabbCenter - aabbHalfSize, aabbCenter + aabbHalfSize);
	AABB.Transform(trans->Transform);

	bool insideCam = true;
	if(GW.m_NowCamera)
		insideCam = GW.m_NowCamera->Cam.IsInsideTheFrustum(AABB);
	bool insideLightCam = LiMgr.GetLightCamera()->IsInsideTheFrustum(AABB);

	// 通常のカメラの視錐台内に存在するか
	if (insideCam)
		RenderingPipeline.m_StaticModelRenderer->Submit(&trans->Transform, model);
	
	// 平行光源視点のカメラの視錐台内に存在するか
	if (insideLightCam)
		RenderingPipeline.m_StaticModelRenderer->Submit_Shadow(&trans->Transform, model);
}

SkinMeshDrawSystem::SkinMeshDrawSystem()
{
	Init();
	m_DebugSystemName = "SkinMeshDrawSystem";
}

void SkinMeshDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);
	auto* model = GetCompFromUpdateParam(GameModelComponent,components);
	auto* boneController = GetCompFromUpdateParam(ModelBoneControllerComponent,components);

	// モデルが読み込まれていない
	if (model->Model == nullptr)return;

	RenderingPipeline.m_SkinMeshRenderer->Submit(&trans->Transform,model,boneController);
}

void SkinMeshDrawSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

SpriteDrawSystem::SpriteDrawSystem()
{
	Init();
	m_DebugSystemName = "SpriteDrawSystem";
}

void SpriteDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent, components);
	auto* sp = GetCompFromUpdateParam(SpriteComponent, components);

	auto& sr = ShMgr.GetRenderer<SpriteRenderer>();
	if (sp->BsAdd)
	{
		ShMgr.m_bsAdd.SetState();
		sr.Draw2D(sp->Tex->GetTex(), sp->Size, trans->Transform, &sp->Color);
		ShMgr.m_bsAlpha.SetState();
	}
	else
	{
		sr.Draw2D(sp->Tex->GetTex(), sp->Size, trans->Transform, &sp->Color);
	}

}

ColliderDefenseRegisterSystem::ColliderDefenseRegisterSystem()
{
	Init();
	m_DebugSystemName = "ColliderSystem";
}

void ColliderDefenseRegisterSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);

#if 0
	// 当たり判定オブジェクトを登録する
	if (colliderComp->HitObj)
	{
		colliderComp->HitObj->m_UserMap["Entity"] = colliderComp->m_Entity;
		// 当たり判定データの行列を更新
		colliderComp->HitObj->Set(transComp->Transform);
		auto& aabb = colliderComp->HitObj->GetAABB();
		// m_ColEng に、このあたり判定を登録
		ColEng.AddDef(colliderComp->HitObj);
	}
#endif

	if (colliderComp->m_pColliders == nullptr || colliderComp->m_pColliders->empty())return;

	auto it = colliderComp->m_pColliders->begin();
	while (it != colliderComp->m_pColliders->end())
	{
		if (it->Collider == nullptr || it->AutoRegister == false)
		{
			++it;
			continue;
		}

		auto collider = it->Collider;

		auto offsetPos = it->OffsetPos;
		auto offsetRotate = it->OffsetRotate;
		auto size = it->Size;

		ZMatrix mat = offsetRotate.ToMatrix();

		if (it->TargetBoneIndex > 0)
		{
			auto tmp = it->Collider->GetUserMap<ZBoneController::BoneNode>("TargetBone")->LocalMat;
			mat *= tmp;
			mat.Move(offsetPos - tmp.GetPos());
		}
		else
			mat.Move(offsetPos);

		switch (collider->GetShape())
		{
			case HitShapes::BOX:
			{
				auto tmp = transComp->Transform;
				mat *= tmp;
				((ZSP<Collider_Box>)collider)->Set(ZVec3(0),size,mat);
			}
			break;
			case HitShapes::SPHERE:
			{
				auto tmp = transComp->Transform;
				mat *= tmp;
				((ZSP<Collider_Sphere>)collider)->Set(mat.GetPos(), size.x);
			}
			break;
			case HitShapes::MESH:
			{
				mat *= transComp->Transform;
				((ZSP<Collider_Mesh>)collider)->Set(mat);
			}
			break;
		}
		it->OldPos = transComp->Transform.GetPos();

		auto& aabb = collider->GetAABB();
		if (collider->m_AtkFilter != 0)
			ColEng.AddAtk(collider, it->AtkLineNo);
		if (collider->m_DefFilter != 0) ColEng.AddDef(collider);

		auto& life = it->Life;
		if (life != -1) --life;
		// コライダーの寿命がつきたら削除
		if(life == 0)
		{
			it = colliderComp->m_pColliders->erase(it);
			continue;
		}

		++it;
	}


}

GravitySystem::GravitySystem()
{
	Init();
	m_DebugSystemName = "GravitySystem";
}

void GravitySystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto gravityComp = GetCompFromUpdateParam(GravityComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	transComp->Transform.Move(gravityComp->Gravity * delta);
}

ResolveCollisionSystem::ResolveCollisionSystem()
{
	Init();
	m_DebugSystemName = "ResolveCollisionSystem";
}

void ResolveCollisionSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	ZVec3 push;
	for (auto& colliderData : *colliderComp->m_pColliders)
	{
		auto& collider = colliderData.Collider;
		
		for (auto& res : collider->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (colliderComp->m_Entity == entity)continue;
				push += node.vPush - push;
				transComp->Transform.Move(push);
			}
		}
	}

	
}
