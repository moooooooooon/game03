#ifndef __COMMON_SYSTEMS_H__
#define __COMMON_SYSTEMS_H__

// ボーンアニメーション更新
class AnimationUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,ModelBoneControllerComponent,AnimatorComponent)
public:
	AnimationUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
};

// 通常メッシュ描画
class StaticMeshDrawSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, GameModelComponent)

public:
	StaticMeshDrawSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

// スキンメッシュ描画
class SkinMeshDrawSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, GameModelComponent,ModelBoneControllerComponent)
public:
	SkinMeshDrawSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
};

// スプライト描画
class SpriteDrawSystem : public ECSSystemBase 
{
	DefUseComponentType(TransformComponent, SpriteComponent)
public:
	SpriteDrawSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

// コライダー登録
class ColliderDefenseRegisterSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, ColliderComponent)
public:
	ColliderDefenseRegisterSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

// 重力
class GravitySystem : public ECSSystemBase
{
	DefUseComponentType(GravityComponent,TransformComponent)

public:
	GravitySystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

// 衝突解決
class ResolveCollisionSystem : public ECSSystemBase
{
	DefUseComponentType(ColliderComponent,TransformComponent)
public:
	ResolveCollisionSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};


#endif