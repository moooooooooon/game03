﻿#ifndef __XBOXCONINPUT_h__
#define __XBOXCONINPUT_h__

#include <windows.h>
#include <XInput.h>
#include <unordered_map>

#pragma comment(lib, "XInput.lib")

#define MAX_CONTROLLERS 4
#define INPUT_DEADZONE  ( 0.50f * FLOAT(0x7FFF) )



class XboxConInput
{
public:

	struct CONTROLLER_STATE
	{
		XINPUT_STATE state;
		bool bConnected;
	};


	void StateUpdate();
	bool IsConnected(int _playerNumber);
	XINPUT_STATE GetState(int _playerNumber);
	WORD GetStateButton(int _playerNumber);
	bool GetStateButton(int _playerNumber,const char *str);
	bool ButtonPressed(int _playerNumber,const char *str);
	float GetStateThumb(int _playerNumber,const char *str);
	float GetStateTrigger(int _playerNumber,const char *str);


private:
	CONTROLLER_STATE m_Controllers[MAX_CONTROLLERS];
	//bool m_ButtonMap[14];
	std::vector<std::unordered_map<const char *, int>> m_ButtonMap;


//------------------------------------------------------------------
// シングルトン
private:
	XboxConInput()
	{
		m_ButtonMap.resize(MAX_CONTROLLERS);
	}
public:
	static XboxConInput& GetInstance()
	{
		static XboxConInput instance;
		return instance;
	}
//------------------------------------------------------------------

};

#define BOXINPUT XboxConInput::GetInstance()

#endif