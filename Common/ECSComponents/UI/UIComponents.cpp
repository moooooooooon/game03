#include "MainFrame/ZMainFrame.h"
#include "UIComponents.h"

void RadioButtonComponent::InitFromJson(const json11::Json& jsonObj)
{

	ButtonList.clear();

	GroupName = jsonObj["GroupName"].string_value().c_str();
	InitButtonID = jsonObj["InitButtonID"].int_value();

	auto ary = jsonObj["ButtonList"].array_items();
	for (auto btn : ary)
	{

		auto add = Make_Shared(Button, appnew);

		ZVec3 pos;
		pos.Set(btn["Transform"]);
		add->Transform.CreateMove(pos);
		add->ButtonSize.Set(btn["Size"]);
		add->ButtonID = btn["ID"].int_value();


		for (auto b : btn["Button"].array_items())
		{
			auto spdat = Make_Shared(SpriteData, appnew);
			spdat->BsAdd = b["Add"].bool_value();
			spdat->Tex = APP.m_ResStg.LoadTexture(b["Texture"].string_value().c_str());
			spdat->Size.Set(b["Size"]);
			add->Sprite.push_back(spdat);
		}

		ButtonList.push_back(add);
	}
}
