#include "PCH/pch.h"
#include "MapObjectComponents.h"

const float SecurityTerminalComponent::MaxGage = 120; // （秒数*fps）
const float SecurityTerminalComponent::DecreaseGage = 10; // （秒数*fps）

void SecurityTerminalComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = true;
	Gage = (float)jsonObj["Gage"].number_value();
}

const int ServerTerminalComponent::MaxGage = 30; // （秒数*fps）

void ServerTerminalComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = true;
	Gage = (float)jsonObj["Gage"].number_value();
}


