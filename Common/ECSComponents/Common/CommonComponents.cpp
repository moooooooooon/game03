#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "CommonComponents.h"

void GameModelComponent::InitFromJson(const json11::Json & jsonObj)
{
	// モデル読み込み
	Model = APP.m_ResStg.LoadMesh(jsonObj["ModelFileName"].string_value().c_str());
	assert(Model);

	// レンダーフラグ
	RenderFlg.Character = jsonObj["Character"].bool_value();
	RenderFlg.SemiTransparent = jsonObj["SemiTransparent"].bool_value();
	RenderFlg.BaseFrustumOut = jsonObj["BaseFrustumOut"].bool_value();
	RenderFlg.NoneShadowRender = jsonObj["NoneShadowRender"].bool_value();
}

void ColliderComponent::InitFromJson(const json11::Json& jsonObj)
{
#if 0
	// 当たり判定用モデル
	HitModel = APP.m_ResStg.LoadMesh(jsonObj["ModelFileName"].string_value().c_str());
	
	// 当たり判定マスク
	int hitGroups = 0;
	auto& jsonHitGroups = jsonObj["HitGroups"];
	if (jsonHitGroups.is_array()) // 配列で所属グループを列挙しているなら
	{
		auto ary = jsonObj["HitGroups"].array_items();
		for (auto& n : ary)
			hitGroups |= 1 << n.int_value();
	}
	else
		hitGroups = jsonHitGroups.int_value();

	HitObj = Make_Shared(Collider_Mesh,appnew);

	// 基本設定
	HitObj->Init(0,
		hitGroups,
		HitShapes::ALL, // 形状マスク
		hitGroups // 判定される側時のマスク
	);

	// ColliderComponent情報を仕込ませておく
	//HitObj->m_UserMap["ColliderComp"] = HitObj;
	// メッシュを追加   
	HitObj->AddMesh(HitModel);
#endif

	auto& colliderSettings = jsonObj["ColliderSettings"];
	if (colliderSettings.is_null())return;
	if(m_pColliders == nullptr)
		m_pColliders = Make_Unique(ZVector<ColliderData>, sysnew);
	auto& colliders = *m_pColliders;
	for (auto& setting : colliderSettings.array_items())
	{
		ZString name = setting["ColliderName"].string_value().c_str();
		
		auto collider = FindCollider(name);
		if (collider == nullptr)continue;

		int atkGroup = 0;
		int defGroup = HitGroups::_ALL;
		int shapeFilter = HitShapes::ALL;
		size_t atkLineNo = 0;
		bool autoRegister = true;

		// AtkGroup
		{
			auto& atk = setting["AtkGroup"];
			if(atk.is_null() == false)
			{
				if (atk.is_array())
				{
					atkGroup = 0;
					for (auto& group : atk.array_items())
						atkGroup |= 1 << group.int_value();
				}
				else
					atkGroup = atk.int_value();
			}
		}

		// DefGroup
		{
			auto& def = setting["DefGroup"];
			if (def.is_null() == false)
			{
				if (def.is_array())
				{
					defGroup = 0;
					for (auto& group : def.array_items())
						defGroup |= 1 << group.int_value();
				}
				else
					defGroup = def.int_value();
			}
		}

		// ShapeFilter
		{
			auto& shape = setting["ShapeFilter"];
			if(shape.is_null() == false)
			{
				if (shape.is_array())
				{
					shapeFilter = 0;
					for (auto& group : shape.array_items())
						shapeFilter |= 1 << group.int_value();
				}
				else
					shapeFilter = shape.int_value();
			}
		}

		// AtkLineNo
		{
			auto& lineNo = setting["AtkLineNo"];
			if (lineNo.is_null() == false)
				atkLineNo = (size_t)lineNo.int_value();
		}

		// AutoRegister
		{
			auto& ar = setting["AutoRegister"];
			if (ar.is_null() == false)
				autoRegister = ar.bool_value();
		}

		collider->Collider->m_AtkFilter = atkGroup;
		collider->Collider->m_DefFilter = defGroup;
		collider->Collider->m_ShapeFilter = shapeFilter;
		collider->AtkLineNo = atkLineNo;
		collider->AutoRegister = autoRegister;
	}

	// 衝突判定対象にならないコライダー排除
	auto it = colliders.begin();
	while (it != colliders.end())
	{
		auto collider = it->Collider;
		if (collider->m_ShapeFilter == 0 ||
			(collider->m_AtkFilter == 0 && collider->m_DefFilter == 0))
		{
			it = colliders.erase(it);
			continue;
		}
		it++;
	}

}

ColliderComponent::ColliderData* ColliderComponent::FindCollider(const ZString & name)
{
	if (m_pColliders == nullptr)return nullptr;

	auto it = std::find_if(m_pColliders->begin(), m_pColliders->end(),
		[name](const auto& data)
	{
		return data.Name == name;
	});
	if (it != m_pColliders->end())return &(*it);

	return nullptr;
}

void AnimatorComponent::InitFromJson(const json11::Json& jsonObj)
{
	if (Animator == nullptr)
		Animator = Make_Shared(ZAnimator, sysnew);

	Enable = jsonObj["Enable"].bool_value();
	InitAnimeLoop = jsonObj["InitAnimeLoop"].bool_value();
	InitAnimeName = jsonObj["InitAnimeName"].string_value().c_str(); 

	ScriptProc = nullptr;
}

void GravityComponent::InitFromJson(const json11::Json& jsonObj)
{
	auto& gravity = jsonObj["Gravity"];
	if (gravity.is_null() || gravity.is_array() == false)
	{
		Gravity = ZVec3(0, -1, 0);
		return;
	}

	Gravity.Set(gravity);
}


//---------------------

SoundData::SoundData()
{
	Rad = Volume = 1.f;
}

SoundData::~SoundData()
{
	if (Sound == nullptr)
		return;

	if (Sound->IsPlay())
		Sound->Stop();
	Sound = nullptr;
}


bool SoundSetComponent::IsPlay(const ZString& soundName) 
{
#if _DEBUG
	auto it = std::find_if(m_Sounds.begin(),m_Sounds.end(),
		[soundName](const auto& soundData)
	{
		return soundData.SoundName == soundName;
	});
	
	if (it == m_Sounds.end())return false;

	auto& sound = (*it);
#else
	auto it = m_SoundData.find(soundName);
	if (it == m_SoundData.end())return false;
	auto& sound = (*it).second;
#endif

	return sound.Sound->IsPlay();
}

void SoundSetComponent::Play(const ZString& soundName,float vol)
{
#if _DEBUG
	auto it = std::find_if(m_Sounds.begin(), m_Sounds.end(),
		[soundName](const auto& soundData)
	{
		return soundData.SoundName == soundName;
	});
	if (it == m_Sounds.end())return;
	
	auto& sound = (*it);

#else
	auto it = m_SoundData.find(soundName);
	if (it == m_SoundData.end())return;
	auto& sound = (*it).second;
#endif


	if (vol >= 0)
		sound.Sound->SetVolume(vol);
	else
		sound.Sound->SetVolume(sound.Volume);

	sound.Sound->Play(sound.IsLoop);

}

void SoundSetComponent::Play3D(const ZString& soundName, ZVec3& pos, float vol)
{
#if _DEBUG

	auto it = std::find_if(m_Sounds.begin(), m_Sounds.end(),
		[soundName](const auto& soundData)
	{
		return soundData.SoundName == soundName;
	});
	if (it == m_Sounds.end())return;

	auto& sound = (*it);
#else
	auto it = m_SoundData.find(soundName);
	if (it == m_SoundData.end())return;

	auto& sound = (*it).second;
#endif

	//	3Dで作成していなければ2D再生
	if (sound.Is3D == false)
	{
		Play(soundName, vol);
		return;
	}

	//	音量設定されてるならそっちで
	if (vol >= 0.f)
		sound.Sound->SetVolume(vol);
	else
		sound.Sound->SetVolume(sound.Volume);

	sound.Sound->Play3D(pos, sound.IsLoop,sound.Rad);

}

void SoundSetComponent::PlayAuto(const ZString& soundName, ZVec3& pos, float vol) 
{
#if _DEBUG

	auto it = std::find_if(m_Sounds.begin(), m_Sounds.end(),
		[soundName](const auto& soundData)
	{
		return soundData.SoundName == soundName;
	});
	if (it == m_Sounds.end())return;

	auto& sound = (*it);
#else
	auto it = m_SoundData.find(soundName);
	if (it == m_SoundData.end())return;
	auto& sound = (*it).second;
#endif

	//	音量設定されてるならそっちで
	if (vol >= 0.f)
		sound.Sound->SetVolume(vol);
	else
		sound.Sound->SetVolume(sound.Volume);
	
	//
	if (sound.Is3D)
		sound.Sound->Play3D(pos, sound.IsLoop, sound.Rad);
	else
		sound.Sound->Play(sound.IsLoop);
}

void SoundSetComponent::Stop(const ZString& soundName)
{
#if _DEBUG

	auto it = std::find_if(m_Sounds.begin(), m_Sounds.end(),
		[soundName](const auto& soundData)
	{
		return soundData.SoundName == soundName;
	});
	if (it == m_Sounds.end())return;

	auto& sound = (*it);
#else
	auto it = m_SoundData.find(soundName);
	if (it == m_SoundData.end())return;
	auto& sound = (*it).second;
#endif

	if (sound.Sound->IsPlay())
		sound.Sound->Stop();
}

void SoundSetComponent::InitFromJson(const json11::Json& jsonObj)
{
	auto sounds = jsonObj["SoundList"].array_items();

#if _DEBUG
	m_Sounds.clear();
	for (auto& sound : sounds)
	{
		m_Sounds.emplace_back();
		auto sd			= APP.m_ResStg.LoadSound(sound["FileName"].string_value().c_str());
		auto& data		= m_Sounds.back();
		data.SoundName	= sound["SoundName"].string_value().c_str();
#else
	m_SoundData.clear();
	for (auto& sound : sounds)
	{
		ZString soundName = sound["SoundName"].string_value().c_str();
		auto& data	= m_SoundData[soundName];
		auto sd		= APP.m_ResStg.LoadSound(sound["FileName"].string_value().c_str());
		data.SoundName = soundName;
#endif
		data.Is3D = sound["Is3D"].bool_value();
		data.IsLoop = sound["Loop"].bool_value();
		data.Rad = (float)sound["Rad"].number_value();
		data.Volume = (float)sound["Volume"].number_value();
		data.Sound = sd->CreateInstance(data.Is3D);
	}

}

