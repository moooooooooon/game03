#ifndef __COMMON_COMPONENTS_H__
#define __COMMON_COMPONENTS_H__

#include "Shader/ModelRenderFlags.h"

class Collider_Mesh;
class ColliderBase;

// ゲーム用モデルコンポーネント
DefComponent(GameModelComponent)
{
	ZSP<ZGameModel> Model;
	ModelRenderFlgs RenderFlg;
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// 単一メッシュコンポーネント
DefComponent(SingleModelComponent)
{
	ZSP<ZSingleModel> Model;
};

// 当たり判定
DefComponent(ColliderComponent)
{
	struct ColliderData
	{
		ZSP<ColliderBase> Collider;
		ZString Name;
		int TargetBoneIndex{ -1 };
		ZVec3 OffsetPos;
		ZQuat OffsetRotate;
		ZVec3 OldPos; // 登録時の座標
		ZVec3 Size;
		size_t TableIndex;
		size_t AtkLineNo{ 0 };
		bool AutoRegister{ true }; // 自動的にコリジョンエンジンに登録するか
		int Life{ -1 }; // 寿命(-1なら無限)
	};

	ZSP<ZGameModel> HitModel;
	ZSP<Collider_Mesh> HitObj;
	ZUP<ZVector<ColliderData>> m_pColliders;
	virtual void InitFromJson(const json11::Json& jsonObj)override;
	ColliderData* FindCollider(const ZString& name);
};

// ゲーム用モデルのボーンコントローラーコンポーネント
DefComponent(ModelBoneControllerComponent)
{
	ZBoneController BoneController;
};

// アニメーターコンポーネント
DefComponent(AnimatorComponent)
{
	bool Enable;
	ZSP<ZAnimator> Animator;
	bool InitAnimeLoop;
	ZString InitAnimeName;
	std::function<void(ZAnimeKey_Script*)>ScriptProc;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// 移動速度
DefComponent(VelocityComponent)
{
	ZVec3 Velocity;
};

// 重力
DefComponent(GravityComponent)
{
	ZVec3 Gravity;
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

/*-------------------------------------------------------------------------------------------------*/
DefComponent(SpriteComponent) 
{
	SpriteComponent() {
		Color.Set(1, 1, 1, 1);
	}

	ZSP<ZTexture>	Tex;
	ZVec2			Size;	//	wid,hei
	ZVec4			Color;
	bool			BsAdd = false;	//	加算発光
	//virtual void InitFromJson(const json11::Json& jsonObj);
};

struct SoundData
{
	SoundData();
	~SoundData();
	
	ZSP<ZSound>		Sound;
	bool			IsLoop = false;
	float			Volume;
	bool			Is3D = false;
	ZString			SoundName;
	float			Rad;
};

DefComponent(SoundSetComponent)
{
	virtual void InitFromJson(const json11::Json& jsonObj)override;

	bool IsPlay(const ZString& soundName);
	void Play(const ZString& soundName,float vol=-1);
	void Play3D(const ZString& soundName, ZVec3& pos, float vol = -1);
	//	SoundDataをもとに再生
	void PlayAuto(const ZString& soundName, ZVec3& pos, float vol = -1);

	void Stop(const ZString& soundName);
#if _DEBUG
	ZVector<SoundData> m_Sounds;
#else
	ZUnorderedMap<ZString, SoundData> m_SoundData;		//	Debugビルドだと解放時にイテレータか何かが死ぬ。Releaseだと大丈夫っぽい
#endif
};



#endif