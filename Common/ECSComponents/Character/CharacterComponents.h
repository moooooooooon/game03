#ifndef __CHARACTER_COMPONENTS_H__
#define __CHARACTER_COMPONENTS_H__

#include "Camera/GameCamera.h"
#include "State/State.h"

DefComponent(CameraComponent)
{
	GameCamera Cam;

	bool Enable;
	
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// プレイヤー共通
DefComponent(PlayerComponent)
{
	ZString Name;		// 名前
	int ActionState;	// ステート
	float Speed;	// 移動速度
	float Gravity;		// 重力
	bool IsSky;			// 空中か
	ZVec3 Velocity;		// 移動（保存）

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// HP
DefComponent(HPComponent)
{
	uint HP;
};

DefComponent(StateMachineComponent)
{
	ZUP<StateMachine> pStateMachine;
	ZString StartState;
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// 多段ヒット管理用
DefComponent(MultiHitCounterComponent)
{
	ZUnorderedMap<ZUUID, uint> Counter;
};

DefComponent(PlayerControllerComponent)
{
	bool Enable;
	ZVec3 Axis;
	int Button;		// 現在のフレームのボタン入力情報
	bool ControllerConnected;
	int ControllerID;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// セキュリティ(キラー)用
DefComponent(SecurityComponent)
{
	enum Action
	{
		WAIT,
		WALK,
		ATTACK_SINK,
		ATTACK,
		ATTACK_NOHIT,
		ATTACK_HIT,
		CREATE_WALL
	};

	ZUnorderedMap<ZUUID, int> MultiHitMap;	// 多段ヒット防止用
	char AttackPower;						// 攻撃力
	bool HitAttack = false;					// 攻撃が当たったか

	bool CreateWallMode = false;			// 壁生成モード
	int CreateWallCnt;						// 生成した壁の数
	ZSP<ECSEntity> WallEntity;
	ZDeque<ZSP<ECSEntity>> WallEntityList;		// 生成した壁
	ZSP<ZGameModel> WallModel;

	float WalkSpeed;						// 移動速度

	static const int MaxNumCreateWall;		// 生成できる壁の数

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// ハッカー(サバイバー)用
DefComponent(HackerComponent)
{
	// アクションステート用
	enum Action
	{
		WAIT,
		WALK,
		RUN,
		SQUAT_WAIT,
		SQUAT_WALK,
		FALL,
		DOWN_WAIT,
		DOWN_WALK,
		ACCESS_START,
		ACCESS,
		ACCESS_END,
		CURE,
		GETUP,
		DESTORY_OBJ_START,
		DESTORY_OBJ,
		DESTORY_OBJ_END
	};

	// ダメージステート用
	enum Damage
	{
		NORMAL,
		DAMAGE,
		DOWN
	};

	Effekseer::Effect* DamageEffect;
	ZAUnorderedMap<Effekseer::Handle,ZVec3> DamageEffectPos;

	Effekseer::Effect* WarpEffect;
	Effekseer::Effect* TraceEffect;
	Effekseer::Effect* InjuryEffect;
	Effekseer::Effect* DestoryObjEffect;
	Effekseer::Effect* TreatmentEffect;

	int		HP;					// 体力
	int		CureGage = 0;		// 負傷・ダウン時の回復ゲージ
	bool	FinishCure = false;	// 治療完了
	char	DamageState;		// ダメージの状態
	int		DamageEffectTime = 0;
	
	bool	SuccessSkillCheck;	// スキルチェックが成功したかどうか
	char	SkillCheckKey;		// スキルチェックのキー
	float	SkillCheckTime;		// スキルチェックの残り時間

	bool	SquatFlag = false;	// しゃがんでいるか
	bool	AccessFlag = false;	// アクセスしているか
	int		AccessTime = 0;		// 端末にアクセスしている時間

	ZWP<ECSEntity> DestroyObj;
	bool	DestroyObjMode = false;
	int		DestroyObjCnt = 0;	// 破壊したオブジェクト数

	int MaxCure;				// 治療ゲージの最大値

	float SkillCheckTimeLimit;	// スキルチェックの制限時間
	int SkillCheckInterval;		// スキルチェックの間隔
	int SkillCheckProbability;	// スキルチェックの発生確率
	int DestroyObjLimit;		// 破壊できるオブジェクト数

	// 移動スピード関係
	float WalkSpeed;
	float RunSpeed;
	float SquatWalkSpeed;
	float DownWalkSpeed;
	
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

#endif