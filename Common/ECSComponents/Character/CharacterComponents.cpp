#include "MainFrame/ZMainFrame.h"
#include "CharacterComponents.h"

void CameraComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = jsonObj["Enable"].bool_value();

	ZVec3 basePos;
	basePos.Set(jsonObj["BasePosition"]);
	
	ZVec3 localPos;
	localPos.Set(jsonObj["LocalPosition"]);
	
	ZVec3 rotate;
	rotate.Set(jsonObj["Rotation"]);
	Cam.m_RotAngle.x = rotate.x;
	Cam.m_RotAngle.y = rotate.y;

	ZVec2 RotAngleLimit;
	RotAngleLimit.Set(jsonObj["RotAngleLimit"]);
	Cam.m_XRotAngleLimit = RotAngleLimit;

	// ������
	Cam.Init(basePos, localPos);

	if (jsonObj["Aspect"].is_null() == false)
		Cam.ChangeAspect((float)jsonObj["Aspect"].number_value());

}

void PlayerComponent::InitFromJson(const json11::Json & jsonObj)
{
	ActionState = jsonObj["ActionState"].int_value();
	Gravity = (float)jsonObj["Gravity"].number_value();
}

void PlayerControllerComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = jsonObj["Enable"].bool_value();
	ControllerID = jsonObj["ControllerID"].int_value();
}

void StateMachineComponent::InitFromJson(const json11::Json & jsonObj)
{
	constexpr char* StateMachineType	= "StateMachineType";
	constexpr char* StateMachineParam	= "StateMachineParam";
	constexpr char* States				= "States";
	constexpr char* RegisterName		= "RegisterName";
	constexpr char* ClassName			= "ClassName";
	constexpr char* StartState			= "StartState";

	if (jsonObj[StateMachineType].is_null())
		pStateMachine = Make_Unique(StateMachine, sysnew);
	else
	{
		ZString machineType = jsonObj[StateMachineType].string_value().c_str();
		pStateMachine = ClassRef.New_Unique<StateMachine>(machineType);
	}

	if (jsonObj[StateMachineParam].is_null() == false)
		pStateMachine->InitFromJson(jsonObj[StateMachineParam]);

	auto& states = jsonObj[States].array_items();
	for (auto& state : states)
	{
		ZString registrName = state[RegisterName].string_value().c_str();
		ZString className = state[ClassName].string_value().c_str();
		
		pStateMachine->RegisterState(registrName, ClassRef.New<StateBase>(className));
	}

	this->StartState = jsonObj[StartState].string_value().c_str();
}

const int SecurityComponent::MaxNumCreateWall = 3;

void SecurityComponent::InitFromJson(const json11::Json & jsonObj)
{
	WalkSpeed = (float)jsonObj["WalkSpeed"].number_value();
	WallModel = APP.m_ResStg.LoadMesh("data/Model/killer/RedWall.xed");
}

void HackerComponent::InitFromJson(const json11::Json & jsonObj)
{
	HP =					jsonObj["HP"].int_value();
	MaxCure =				jsonObj["MaxCure"].int_value();
	SkillCheckTimeLimit =	(float)jsonObj["SkillCheckTimeLimit"].number_value();
	SkillCheckInterval =	jsonObj["SkilCheckInterval"].int_value();
	SkillCheckProbability = jsonObj["SkillCheckProbability"].int_value();
	DestroyObjLimit =		jsonObj["DestroyObjLimit"].int_value();
	WalkSpeed =				(float)jsonObj["WalkSpeed"].number_value();
	RunSpeed =				(float)jsonObj["RunSpeed"].number_value();
	SquatWalkSpeed =		(float)jsonObj["SquatWalkSpeed"].number_value();
	DownWalkSpeed =			(float)jsonObj["DownWalkSpeed"].number_value();

	DamageEffect =			EFFECT.LoadEffect(jsonObj["DamageEffect"].string_value().c_str());
	TraceEffect =			EFFECT.LoadEffect(jsonObj["TraceEffect"].string_value().c_str());
	InjuryEffect =			EFFECT.LoadEffect(jsonObj["InjuryEffect"].string_value().c_str());
	TreatmentEffect =		EFFECT.LoadEffect(jsonObj["TreatmentEffect"].string_value().c_str());

	if (DamageEffect == nullptr)
		DamageEffect =		EFFECT.LoadEffect("data/Effect/Damages/Damages.efk");
	if (TraceEffect == nullptr)
		TraceEffect =		EFFECT.LoadEffect("data/Effect/Hacker_Trace/Hacker_Trace.efk");
	if (InjuryEffect == nullptr)
		InjuryEffect =		EFFECT.LoadEffect("data/Effect/Injury/Injury.efk");
	if (TreatmentEffect == nullptr)
		TreatmentEffect =	EFFECT.LoadEffect("data/Effect/Treatment/Treatment.efk");
}