#ifndef __HACKER_STATES_H__
#define __HACKER_STATES_H__

#include "State/State.h"

class HackerState : public StateBase
{
public:
	virtual void Start() = 0;
	virtual void Update(float delta) = 0;
	virtual void OnChangeEvent() = 0;
	bool CheckDestroyModeFlag();

protected:
	void SetMoveSpeed(float speed);

};

class HackerState_Wait : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

};

class HackerState_Walk : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	HackerComponent* m_HackerComp;
};

class HackerState_Run : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	HackerComponent* m_HackerComp;
};

class HackerState_SquatWait : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

};

class HackerState_SquatWalk : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	HackerComponent* m_HackerComp;
};

class HackerState_Fall : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

class HackerState_DownWait : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

};

class HackerState_DownWalk : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	HackerComponent* m_HackerComp;
};

class HackerState_AccessStart : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

class HackerState_Access : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

class HackerState_AccessEnd : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

class HackerState_Cure : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

};

class HackerState_GetUp : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

class HackerState_DestroyObjStart : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

class HackerState_DestroyObj : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	HackerComponent* m_HackerComp;
};

class HackerState_DestroyObjEnd : public HackerState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();
};

#endif