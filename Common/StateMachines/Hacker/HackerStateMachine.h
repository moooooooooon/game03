#ifndef __HACKER_STATE_MACHINE_H__
#define __HACKER_STATE_MACHINE_H__

#include "State/State.h"

class HackerStateMachine : public StateMachine
{
public:
	void InitFromJson(const json11::Json& jsonObj)override;

public:
	bool m_IsInjury{ false };							// 負傷状態か
	bool m_IsAlive{ true };								// 生きているか
	bool m_DestroyModeFlg{ false };						// オブジェクト破壊モードへの移行フラグ
	bool m_SquatFlag{ false };							// しゃがんでいるか
	bool m_AccessFlag{ false };							// 端末へアクセス中か
	bool m_FinishCure{ false };							// 治療が完了したか

};


#endif