#include "PCH/pch.h"
#include "ECSComponents/Components.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "HackerStateMachine.h"
#include "HackerStates.h"

bool HackerState::CheckDestroyModeFlag()
{
	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);
	if (stateMachine->m_DestroyModeFlg)
	{
		ChangeState("DestroyObjStart");
		stateMachine->m_DestroyModeFlg = false;
		return true;
	}
	return false;
}

void HackerState::SetMoveSpeed(float speed)
{
	auto entity = m_pStateMachine->GetEntity();
	auto playerComp = entity->GetComponent<PlayerComponent>();
	playerComp->Speed = speed;
}

void HackerState_Wait::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Wait", 0, 10, true);
	SetMoveSpeed(0);
}

void HackerState_Wait::Update(float delta)
{
	if (m_IsNext)return;
	if (CheckDestroyModeFlag())return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);
	
	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if ((button & KeyMap::Access) || (button& KeyMap::Access_Stay)) {
		ChangeState("Access");
	}

	if (axis.x != 0 || axis.z != 0)
	{
		if (stateMachine->m_SquatFlag)
		{
			// しゃがみ歩き状態へ
			ChangeState("SquatWalk");
			return;
		}
		else if (button & KeyMap::Run)
		{
			// 走り状態へ
			ChangeState("Run");
			return;
		}

		// 歩き状態へ
		ChangeState("Walk");
		return;
	}
	else
	{
		if (stateMachine->m_SquatFlag)
		{
			// しゃがみ状態へ
			ChangeState("SquatWait");
			return;

		}
	}
}

void HackerState_Wait::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_Walk::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Walk", 0, 10, true);
	auto entity = m_pStateMachine->GetEntity();
	m_HackerComp = entity->GetComponent<HackerComponent>();

	SetMoveSpeed(m_HackerComp->WalkSpeed);
}

void HackerState_Walk::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;
	
	if (button & KeyMap::Action)
		stateMachine->m_DestroyModeFlg = !stateMachine->m_DestroyModeFlg;

	if (axis.x == 0 && axis.z == 0)
	{
		if (stateMachine->m_SquatFlag)
		{
			// しゃがみ状態へ
			ChangeState("HackerStaet_SquatWait");
			return;
		}
		else
		{
			// 待機状態へ
			ChangeState("Wait");
			return;
		}
		return;
	}
	else if(stateMachine->m_SquatFlag)
	{
		// しゃがみ歩き状態へ
		ChangeState("SquatWalk");
		return;
	}
	else if (button & KeyMap::Run)
	{
		// 走り状態へ
		ChangeState("Run");
		return;
	}

	/*
	// カメラの向いている方向に移動
	ZMatrix& mat = m_TransComp->Transform;
	GameCamera& cam = m_CameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis() * axis.x;
	vTar += cam.m_LocalMat.GetXAxis() * axis.x;
	vTar.y = 0;
	float speed = m_PlayerComp->Speed * delta;
	mat.Move(vTar * speed);
	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 20);
	mat.SetLookTo(vZ, ZVec3::Up);
	*/

}

void HackerState_Walk::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_Run::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Run", 0, 10, true);
	auto entity = m_pStateMachine->GetEntity();
	m_HackerComp = entity->GetComponent<HackerComponent>();
	SetMoveSpeed(m_HackerComp->RunSpeed);
}

void HackerState_Run::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if (axis.x == 0 && axis.z == 0)
	{
		if (stateMachine->m_SquatFlag)
		{
			// しゃがみ状態へ
			ChangeState("SquatWait");
			return;
		}
		else
		{
			// 待機状態へ
			ChangeState("Wait");
			return;
		}
	}
	else if (!(button & KeyMap::Run))
	{
		if (stateMachine->m_SquatFlag)
		{
			// しゃがみ歩き状態へ
			ChangeState("SquatWalk");
			return;
		}
		else
		{
			// 歩き状態へ
			ChangeState("Walk");
			return;
		}
	}
	
	/*
	// カメラの向いている方向に移動
	ZMatrix& mat = m_TransComp->Transform;
	GameCamera& cam = m_CameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis() * axis.x;
	vTar += cam.m_LocalMat.GetXAxis() * axis.x;
	vTar.y = 0;
	float speed = m_PlayerComp->Speed * delta;
	mat.Move(vTar * speed);
	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 20);
	mat.SetLookTo(vZ, ZVec3::Up);
	*/
}

void HackerState_Run::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_SquatWait::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Wait", 0, 10, true);
	SetMoveSpeed(0);
}

void HackerState_SquatWait::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if (axis.x != 0 || axis.z != 0)
	{
		if (stateMachine->m_SquatFlag)
		{
			ChangeState("SquatWalk");
			return;
		}
		else
		{
			ChangeState("Walk");
			return;
		}
	}
	else
	{
		if (stateMachine->m_SquatFlag == false)
		{
			ChangeState("Wait");
			return;
		}
	}

}

void HackerState_SquatWait::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_SquatWalk::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("ShitDownWalk", 0, 10, true);

	auto entity = m_pStateMachine->GetEntity();
	m_HackerComp = entity->GetComponent<HackerComponent>();

	SetMoveSpeed(m_HackerComp->SquatWalkSpeed);
}

void HackerState_SquatWalk::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if (stateMachine->m_SquatFlag == false)
	{
		if (axis.x == 0 && axis.z == 0)
		{
			ChangeState("Wait");
			return;
		}
		else
		{
			ChangeState("Walk");
			return;
		}
	}
	else
	{
		if (axis.x == 0 && axis.z == 0)
		{
			ChangeState("SquatWait");
			return;
		}
	}

}

void HackerState_SquatWalk::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_Fall::Start()
{
	SetMoveSpeed(0);
}

void HackerState_Fall::Update(float delta)
{
	if (m_IsNext)return;

	auto animator = m_pStateMachine->GetAnimator();
	if (animator->IsAnimationEnd())
	{
		ChangeState("DownWait");
		return;
	}

}

void HackerState_Fall::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_DownWait::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Crawls around Wait", 0, 25, true);
	SetMoveSpeed(0);
}

void HackerState_DownWait::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if (axis.x != 0 || axis.z)
	{
		// 這いずり状態へ
		ChangeState("DownWalk");
		return;
	}
}

void HackerState_DownWait::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_DownWalk::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Crawls around", 0, 10, true);
	auto entity = m_pStateMachine->GetEntity();
	m_HackerComp = entity->GetComponent<HackerComponent>();

	SetMoveSpeed(m_HackerComp->DownWalkSpeed);
}

void HackerState_DownWalk::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if (axis.x == 0 || axis.z == 0)
	{
		// ダウン待機状態へ
		ChangeState("DownWait");
		return;
	}
	
	/*
	// カメラの向いている方向に移動
	ZMatrix& mat = m_TransComp->Transform;
	GameCamera& cam = m_CameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis() * axis.x;
	vTar += cam.m_LocalMat.GetXAxis() * axis.x;
	vTar.y = 0;
	float speed = m_PlayerComp->Speed * delta;
	mat.Move(vTar * speed);
	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 20);
	mat.SetLookTo(vZ, ZVec3::Up);
	*/
}

void HackerState_DownWalk::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_AccessStart::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("HackStart", 0, 10, false);
	SetMoveSpeed(0);
}

void HackerState_AccessStart::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);
	if (stateMachine->m_AccessFlag == false)
	{
		// アクセス終了状態へ
		ChangeState("AccessEnd");
		return;
	}
	
	auto animator = m_pStateMachine->GetAnimator();
	if (animator->IsAnimationEnd())
	{
		// アクセス状態へ
		ChangeState("Access");
		return;
	}

}

void HackerState_AccessStart::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_Access::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Hacking", 0, 10, true);
	SetMoveSpeed(0);
}

void HackerState_Access::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);
	if (stateMachine->m_AccessFlag == false)
	{
		ChangeState("AccessEnd");
		return;
	}
}

void HackerState_Access::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_AccessEnd::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
	SetMoveSpeed(0);
}

void HackerState_AccessEnd::Update(float delta)
{
	if (m_IsNext)return;

	auto animator = m_pStateMachine->GetAnimator();
	if (animator->IsAnimationEnd())
	{
		ChangeState("Wait");
		return;
	}

}

void HackerState_AccessEnd::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_Cure::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Treatment", 0, 10, true);

	SetMoveSpeed(0);
}

void HackerState_Cure::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if(!(button & KeyMap::Access_Stay) || stateMachine->m_FinishCure)
	{
		// しゃがみ待機状態へ
		ChangeState("SquatWait");
		stateMachine->m_FinishCure = false;
		return;
	}

	if (axis.x != 0 || axis.z != 0)
	{
		if (stateMachine->m_SquatFlag)
		{
			// しゃがみ歩き状態へ
			ChangeState("SquatWalk");
			return;
		}
		else if(button & KeyMap::Run)
		{
			// 走り状態へ
			ChangeState("Run");
			return;
		}
		else
		{
			// 歩き状態へ
			ChangeState("Walk");
			return;
		}
	}
	else if (!stateMachine->m_SquatFlag)
	{
		// 待ち状態へ
		ChangeState("Wait");
		return;
	}

}

void HackerState_Cure::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_GetUp::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Crawls around from Stand", 0, 10, false);
	SetMoveSpeed(0);
}

void HackerState_GetUp::Update(float delta)
{
	if (m_IsNext)return;

	auto animator = m_pStateMachine->GetAnimator();
	if (animator->IsAnimationEnd())
	{
		// 待ち状態へ
		ChangeState("Wait");
		return;
	}

}

void HackerState_GetUp::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_DestroyObjStart::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("HackStart", 0, 10, false);
	SetMoveSpeed(0);
}

void HackerState_DestroyObjStart::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;

	if (!(button & KeyMap::Access_Stay))
	{
		ChangeState("Wait");
		stateMachine->m_DestroyModeFlg = true;
		return;
	}

	auto animator = m_pStateMachine->GetAnimator();
	if (animator->IsAnimationEnd())
	{
		ChangeState("DestroyObj");
		return;
	}

}

void HackerState_DestroyObjStart::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_DestroyObj::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Hacking", 0, 10, false);
	auto entity = m_pStateMachine->GetEntity();
	m_HackerComp = entity->GetComponent<HackerComponent>();
	SetMoveSpeed(0);
}

void HackerState_DestroyObj::Update(float delta)
{
	if (m_IsNext)return;

	auto stateMachine = ((HackerStateMachine*)m_pStateMachine);

	auto& axis = stateMachine->m_InputAxis;
	auto& button = stateMachine->m_InputButton;
	
	// 削除中断
	if (!(button & KeyMap::Access_Stay))
	{
		// 待機状態へ
		ChangeState("Wait");
		return;
	}

	auto animator = m_pStateMachine->GetAnimator();

	// 削除
	if (animator->IsAnimationEnd())
	{
		{
			auto destroyObj = m_HackerComp->DestroyObj.Lock();
			auto model = destroyObj->GetComponent<GameModelComponent>();
			model->RenderFlg.Delete = true;
			m_HackerComp->DestroyObj.Reset();
		}

		m_HackerComp->DestroyObjCnt++;
		m_HackerComp->DestroyObjMode = true;
		
		// オブジェクト破壊完了状態へ
		ChangeState("DestroyObjEnd");
		stateMachine->m_DestroyModeFlg = false;
	}

}

void HackerState_DestroyObj::OnChangeEvent()
{
	m_IsNext = false;
}

void HackerState_DestroyObjEnd::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
	SetMoveSpeed(0);
}

void HackerState_DestroyObjEnd::Update(float delta)
{
	if (m_IsNext)return;

	auto animator = m_pStateMachine->GetAnimator();
	if (animator->IsAnimationEnd())
	{
		// 待機へ
		ChangeState("Wait");
		return;
	}

}

void HackerState_DestroyObjEnd::OnChangeEvent()
{
	m_IsNext = false;
}

