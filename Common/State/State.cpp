#include "PCH/pch.h"
#include "State.h"

StateBase::StateBase() : m_IsNext(false),m_pStateMachine(nullptr)
{
}

void StateBase::ChangeState(const ZString& nextState)
{
	m_IsNext = true;
	m_NextStateName = nextState;
}

StateMachine::StateMachine()
{
}

StateMachine::~StateMachine()
{
	m_StateMap.clear();
	m_NowState = nullptr;
}

void StateMachine::Update(float delta)
{
	if (m_NowState == nullptr || m_IsSetStartState == false)return;

	m_NowState->Update(delta);
	if (m_NowState->IsNext() == false)return;

	auto it = m_StateMap.find(m_NowState->m_NextStateName);
	if (it == m_StateMap.end())
	{
		assert(false);
		return;
	}
	// ステートの切り替えを現在のステートに通知
	m_NowState->OnChangeEvent();
	
	// ステート切り替え
	m_NowStateName = m_NowState->m_NextStateName;
	m_NowState = it->second;
	m_NowState->Start();
}

void StateMachine::RegisterState(const ZString& stateName, const ZSP<StateBase> state)
{
	m_StateMap[stateName] = state;
	m_StateMap[stateName]->m_pStateMachine = this;
}

void StateMachine::SetStartState(const ZString& stateName)
{
	auto it = m_StateMap.find(stateName);
	if (it == m_StateMap.end())return;
	
	m_NowStateName = stateName;
	m_NowState = it->second;
	m_NowState->Start();
	m_IsSetStartState = true;
}

void StateMachine::UnRegisterState(const ZString& stateName)
{
	auto it = m_StateMap.find(stateName);
	if (it == m_StateMap.end())return;
	it->second->m_pStateMachine = nullptr;
	m_StateMap.erase(it);
}

void StateMachine::AllUnRegisterState()
{
	for (auto& state : m_StateMap)
		state.second->m_pStateMachine = nullptr;
	m_StateMap.clear();
}

ZString StateMachine::GetNowStateName() const
{
	return m_NowStateName;
}

void StateMachine::SetAnimator(ZSP<ZAnimator> animator)
{
	m_Animator = animator;
}

ZSP<ZAnimator> StateMachine::GetAnimator()
{
	return m_Animator;
}

void StateMachine::SetEntity(ZSP<ECSEntity> entity)
{
	m_Entity = entity;
}

ZSP<ECSEntity> StateMachine::GetEntity()
{
	return m_Entity;
}

bool StateMachine::IsSetStartState()const
{
	return m_IsSetStartState;
}
