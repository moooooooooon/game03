#ifndef __STATE_BASE_H__
#define __STATE_BASE_H__

class StateMachine;

class StateBase
{
public:
	StateBase();

	virtual void Start() = 0;
	virtual void Update(float delta) = 0;
	virtual void OnChangeEvent() = 0;
	inline bool IsNext()const { return m_IsNext; }

protected:
	void ChangeState(const ZString& nextState);
	
public:
	ZString m_NextStateName;
	StateMachine* m_pStateMachine;
	
protected:
	bool m_IsNext;

};

class StateMachine
{
public:
	StateMachine();
	virtual ~StateMachine();

	virtual void InitFromJson(const json11::Json& jsonObj) {}

	virtual void Update(float delta);

	virtual void RegisterState(const ZString& stateName, const ZSP<StateBase> state);

	virtual void SetStartState(const ZString& stateName);
	
	virtual void UnRegisterState(const ZString& name);

	virtual void AllUnRegisterState();

	ZString GetNowStateName()const;

	void SetAnimator(ZSP<ZAnimator> animator);

	ZSP<ZAnimator> GetAnimator();

	void SetEntity(ZSP<ECSEntity> entity);
	 
	ZSP<ECSEntity> GetEntity();

	bool IsSetStartState()const;

protected:
	ZUnorderedMap<ZString, ZSP<StateBase>> m_StateMap;
	ZSP<StateBase> m_NowState;
	ZString m_NowStateName;
	ZSP<ZAnimator> m_Animator;
	ZSP<ECSEntity> m_Entity;
	bool m_IsSetStartState;

public:
	ZVec3 m_InputAxis;
	int m_InputButton;

};


#endif