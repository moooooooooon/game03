#include "PCH/pch.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "CommonListeners.h"

ModelColliderListener::ModelColliderListener()
{
	AddComponentID(GameModelComponent::ID);
	AddComponentID(ColliderComponent::ID);
	AddComponentID(ModelBoneControllerComponent::ID);
}

void ModelColliderListener::OnAddComponent(EntityHandle handle,uint32 id)
{
	auto modelComp = ECS.GetComponent<GameModelComponent>(handle);
	auto colliderComp = ECS.GetComponent<ColliderComponent>(handle);

	if (modelComp == nullptr || colliderComp == nullptr)
		return;

	if (colliderComp->m_pColliders == nullptr)
		colliderComp->m_pColliders = Make_Unique(ZVector<ColliderComponent::ColliderData>, sysnew);

	if (colliderComp->m_pColliders->empty() == false)return;

	// モデルデータに組み込まれている剛体情報抜き出し

	auto& physicsDataSetList = modelComp->Model->GetPhysicsDataSetList();
	auto& colliders = *colliderComp->m_pColliders;

	for (size_t i = 0; i < physicsDataSetList.size(); i++)
	{
		auto& physicsDataSet = physicsDataSetList[i];
		for (auto& rigidBody : physicsDataSet.RigidBodyDataTbl)
		{
			if (rigidBody.Shape != ZBP_RigidBody::shape::Box && rigidBody.Shape != ZBP_RigidBody::shape::Sphere)
				continue; // カプセルは無視

			colliders.emplace_back();
			ColliderComponent::ColliderData& data = colliders.back();
			
			switch (rigidBody.Shape)
			{
				case ZBP_RigidBody::shape::Box:
				{
					auto box = Make_Shared(Collider_Box, sysnew);
					int def = (~rigidBody.UnCollisionGroup);
					box->Init(0, 0, HitShapes::ALL, def);
					box->m_UserMap["Entity"] = colliderComp->m_Entity;
					data.Collider = box;
				}
				break;

				case ZBP_RigidBody::shape::Sphere:
				{
					auto sphere = Make_Shared(Collider_Sphere, sysnew);
					int def = (~rigidBody.UnCollisionGroup);
					sphere->Init(0, 0, HitShapes::ALL, def);
					sphere->m_UserMap["Entity"] = colliderComp->m_Entity;
					data.Collider = sphere;
				}
				break;
			}

			// 各種データ設定
			data.Name = rigidBody.RigidBodyName;
			data.OffsetPos = rigidBody.Translate;
			data.Size = rigidBody.ShapeSize;
			ZMatrix offsetMat = rigidBody.GetMatrix();
			offsetMat.ToQuaternion(data.OffsetRotate, true);
			data.TargetBoneIndex = rigidBody.BoneIndex;
			data.TableIndex = i;

		}
	}

	// 剛体情報が入っていなかった場合メッシュコライダー使用
	if (colliders.empty())
	{
		auto mesh = Make_Shared(Collider_Mesh, sysnew);
		mesh->Init(0, 0, HitGroups::_ALL, HitGroups::_0);
		mesh->AddMesh(modelComp->Model);

		colliders.emplace_back();
		auto& data = colliders.back();
		data.Collider = mesh;
		data.Name = modelComp->Model->GetFileName();
		data.OffsetPos = ZVec3::Zero;
		data.Size = ZVec3::One;
		data.TableIndex = 0;
	}
}

void ModelColliderListener::OnMadeEntityFromJson(EntityHandle handle)
{
	auto modelComp = ECS.GetComponent<GameModelComponent>(handle);
	auto colliderComp = ECS.GetComponent<ColliderComponent>(handle);
	auto bcComp = ECS.GetComponent<ModelBoneControllerComponent>(handle);

	if(modelComp == nullptr || colliderComp == nullptr || bcComp == nullptr)
		return;

	if (bcComp->BoneController.GetBoneTree().empty())return;

	if (colliderComp->m_pColliders == nullptr)return;
	if (colliderComp->m_pColliders->empty())return;

	for (auto& colData : *colliderComp->m_pColliders)
	{
		if (colData.TargetBoneIndex < 0)continue;
		auto index = colData.TargetBoneIndex;
		colData.Collider->m_UserMap["TargetBone"] = bcComp->BoneController.GetBoneTree()[index];
	}

}

ModelBoneControllerListener::ModelBoneControllerListener()
{
	AddComponentID(ModelBoneControllerComponent::ID);
	AddComponentID(AnimatorComponent::ID);
	AddComponentID(GameModelComponent::ID);
}

void ModelBoneControllerListener::OnAddComponent(EntityHandle handle, uint32 id)
{
	auto bcComp = ECS.GetComponent<ModelBoneControllerComponent>(handle);
	auto animComp = ECS.GetComponent<AnimatorComponent>(handle);
	auto modelComp = ECS.GetComponent<GameModelComponent>(handle);

	if (bcComp)
	{
		if (modelComp)
		{
			bcComp->BoneController.SetModel(modelComp->Model);
			bcComp->BoneController.AddAllPhysicsObjToPhysicsWorld(PHYSICS);
		}

		if (animComp)
		{
			if (animComp->Animator == nullptr)
				animComp->Animator = Make_Shared(ZAnimator, sysnew);
			bcComp->BoneController.InitAnimator(*animComp->Animator);
			animComp->Animator->ChangeAnime(animComp->InitAnimeName, animComp->InitAnimeLoop);
			animComp->Animator->EnableRootMotion(true);
		}
	}

}

ColliderListener::ColliderListener()
{
	AddComponentID(ColliderComponent::ID);
}

void ColliderListener::OnAddComponent(EntityHandle handle, uint32 id)
{
	auto colliderComp = ECS.GetComponent<ColliderComponent>(handle);

	colliderComp->HitObj->m_UserMap["Entity"] = colliderComp->m_Entity;
}