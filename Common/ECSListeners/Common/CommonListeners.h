#ifndef __COMMON_LISTENER_H__
#define __COMMON_LISTENER_H__

// ゲーム用モデルのコライダーセットアップ用
class ModelColliderListener : public ECSListener
{
public:
	ModelColliderListener();

	void OnAddComponent(EntityHandle handle,uint32 id)override;
	void OnMadeEntityFromJson(EntityHandle handle)override;
};

// ゲーム用モデルのボーンコントローラーリスナー
class ModelBoneControllerListener : public ECSListener
{
public:
	ModelBoneControllerListener();

	void OnAddComponent(EntityHandle handle, uint32 id)override;
	
};

// ColliderComponentのリスナー
class ColliderListener : public ECSListener
{
public:
	ColliderListener();

	void OnAddComponent(EntityHandle handle, uint32 id)override;
};


#endif