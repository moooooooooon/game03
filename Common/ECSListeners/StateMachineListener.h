#ifndef __STATE_MACHINE_LISTENER_H__
#define __STATE_MACHINE_LISTENER_H__
 
class StateMachineListener : public ECSListener
{
public:
	StateMachineListener();

	void OnMadeEntityFromJson(EntityHandle handle)override;

};

#endif